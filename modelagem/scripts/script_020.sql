/* ---------------------------------------------------------------------- */
/* Script generated with: DeZign for Databases v6.1.0                     */
/* Target DBMS:           PostgreSQL 8.3                                  */
/* Project file:          modelagem_travelcorporate.dez                   */
/* Project name:                                                          */
/* Author:                                                                */
/* Script type:           Alter database script                           */
/* Created on:            2017-11-28 17:04                                */
/* ---------------------------------------------------------------------- */


/* ---------------------------------------------------------------------- */
/* Drop foreign key constraints                                           */
/* ---------------------------------------------------------------------- */
ALTER TABLE public.funcionario DROP CONSTRAINT empresa_funcionario;
ALTER TABLE public.funcionario DROP CONSTRAINT funcao_funcionario;
ALTER TABLE public.funcionario DROP CONSTRAINT perfil_funcionario;

/* ---------------------------------------------------------------------- */
/* Add sequences                                                          */
/* ---------------------------------------------------------------------- */
CREATE SEQUENCE seq_setor INCREMENT 1 START 1;

/* ---------------------------------------------------------------------- */
/* Modify table "funcionario"                                             */
/* ---------------------------------------------------------------------- */
ALTER TABLE public.funcionario ADD
    id_empresa_filial public.id;
ALTER TABLE public.funcionario ADD
    id_setor public.id;

/* ---------------------------------------------------------------------- */
/* Add table "setor"                                                      */
/* ---------------------------------------------------------------------- */
CREATE TABLE setor (
    id_setor public.id  NOT NULL,
    nome CHARACTER VARYING(150)  NOT NULL,
    status public.status  NOT NULL,
    id_setor_pai public.id,
    id_empresa public.id,
    CONSTRAINT pk_setor PRIMARY KEY (id_setor)
);

/* ---------------------------------------------------------------------- */
/* Add foreign key constraints                                            */
/* ---------------------------------------------------------------------- */
ALTER TABLE public.funcionario ADD CONSTRAINT empresa_funcionario 
    FOREIGN KEY (id_empresa) REFERENCES public.empresa (id_empresa);
ALTER TABLE public.funcionario ADD CONSTRAINT funcao_funcionario 
    FOREIGN KEY (id_funcao) REFERENCES public.funcao (id_funcao);
ALTER TABLE public.funcionario ADD CONSTRAINT perfil_funcionario 
    FOREIGN KEY (id_perfil) REFERENCES public.perfil (id_perfil);
ALTER TABLE public.funcionario ADD CONSTRAINT empresa_funcionario_filial 
    FOREIGN KEY (id_empresa_filial) REFERENCES public.empresa (id_empresa);
ALTER TABLE public.funcionario ADD CONSTRAINT setor_funcionario 
    FOREIGN KEY (id_setor) REFERENCES setor (id_setor);
ALTER TABLE setor ADD CONSTRAINT setor_setor 
    FOREIGN KEY (id_setor_pai) REFERENCES setor (id_setor);
ALTER TABLE setor ADD CONSTRAINT empresa_setor 
    FOREIGN KEY (id_empresa) REFERENCES public.empresa (id_empresa);
