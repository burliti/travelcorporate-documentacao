/* ---------------------------------------------------------------------- */
/* Script generated with: DeZign for Databases v6.1.0                     */
/* Target DBMS:           PostgreSQL 8.3                                  */
/* Project file:          modelagem_travelcorporate.dez                   */
/* Project name:                                                          */
/* Author:                                                                */
/* Script type:           Alter database script                           */
/* Created on:            2017-10-19 11:36                                */
/* ---------------------------------------------------------------------- */


/* ---------------------------------------------------------------------- */
/* Drop foreign key constraints                                           */
/* ---------------------------------------------------------------------- */


/* ---------------------------------------------------------------------- */
/* Drop and recreate table "viagem"                                       */
/* ---------------------------------------------------------------------- */

/* Table must be recreated because some of the changes can't be done with the regular commands available. */
CREATE TABLE public.viagem_TMP (
    id_viagem public.id  NOT NULL,
    id_empresa public.id  NOT NULL,
    sequencial public.id  NOT NULL,
    objetivo CHARACTER VARYING(500),
    descricao CHARACTER VARYING(150),
    observacao TEXT,
    id_cliente public.id,
    id_projeto public.id,
    data_ida DATE,
    data_volta DATE,
    status NUMERIC(1)  NOT NULL,
    id_funcionario public.id);
INSERT INTO public.viagem_TMP
    (id_viagem,id_empresa,sequencial, objetivo,descricao,observacao,id_cliente,id_projeto,data_ida,data_volta,status,id_funcionario)
SELECT
    id_viagem,id_empresa,1,objetivo,descricao,observacao,id_cliente,id_projeto,data_ida,data_volta,status,id_funcionario
FROM public.viagem;

DROP TABLE public.viagem cascade;
ALTER TABLE public.viagem_TMP RENAME TO viagem;
ALTER TABLE public.viagem ADD CONSTRAINT pk_viagem 
    PRIMARY KEY (id_viagem);

/* ---------------------------------------------------------------------- */
/* Add foreign key constraints                                            */
/* ---------------------------------------------------------------------- */
ALTER TABLE public.viagem ADD CONSTRAINT cliente_viagem 
    FOREIGN KEY (id_cliente) REFERENCES public.cliente (id_cliente);
ALTER TABLE public.viagem ADD CONSTRAINT projeto_viagem 
    FOREIGN KEY (id_projeto) REFERENCES public.projeto (id_projeto);
ALTER TABLE public.viagem ADD CONSTRAINT empresa_viagem 
    FOREIGN KEY (id_empresa) REFERENCES public.empresa (id_empresa);
ALTER TABLE public.viagem ADD CONSTRAINT funcionario_viagem 
    FOREIGN KEY (id_funcionario) REFERENCES public.funcionario (id_funcionario);
