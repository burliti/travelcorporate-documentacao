﻿/* ---------------------------------------------------------------------- */
/* Script generated with: DeZign for Databases v6.1.0                     */
/* Target DBMS:           PostgreSQL 8.3                                  */
/* Project file:          modelagem_travelcorporate.dez                   */
/* Project name:                                                          */
/* Author:                                                                */
/* Script type:           Alter database script                           */
/* Created on:            2018-01-28 02:13                                */
/* ---------------------------------------------------------------------- */


/* ---------------------------------------------------------------------- */
/* Drop foreign key constraints                                           */
/* ---------------------------------------------------------------------- */
ALTER TABLE public.reembolso_viagem DROP CONSTRAINT empresa_reembolso_viagem;
ALTER TABLE public.reembolso_viagem DROP CONSTRAINT lancamento_reembolso_viagem;
ALTER TABLE public.reembolso_viagem DROP CONSTRAINT viagem_reembolso_viagem;
ALTER TABLE public.reembolso_viagem DROP CONSTRAINT funcionario_reembolso_viagem_4;
ALTER TABLE public.reembolso_viagem DROP CONSTRAINT funcionario_reembolso_viagem_5;
ALTER TABLE public.reembolso_viagem DROP CONSTRAINT funcionario_reembolso_viagem_6;
ALTER TABLE public.reembolso_viagem DROP CONSTRAINT funcionario_reembolso_viagem_7;

/* ---------------------------------------------------------------------- */
/* Add sequences                                                          */
/* ---------------------------------------------------------------------- */
CREATE SEQUENCE public.seq_mensagem INCREMENT 1 START 1;
CREATE SEQUENCE public.seq_assunto INCREMENT 1 START 1;
CREATE SEQUENCE public.seq_mensagem_forma_notificacao INCREMENT 1 START 1;
CREATE SEQUENCE public.seq_mensagem_destinatarios INCREMENT 1 START 1;
CREATE SEQUENCE public.seq_forma_notificacao INCREMENT 1 START 1;
CREATE SEQUENCE public.seq_mensagem_confirmacao_leitura INCREMENT 1 START 1;
CREATE SEQUENCE public.seq_forma_notificacao_empresa INCREMENT 1 START 1;

/* ---------------------------------------------------------------------- */
/* Add domains                                                            */
/* ---------------------------------------------------------------------- */
CREATE DOMAIN public.status_mensagem AS NUMERIC CHECK (VALUE IN (1, 2, 9));

/* ---------------------------------------------------------------------- */
/* Modify table "reembolso_viagem"                                        */
/* ---------------------------------------------------------------------- */
ALTER TABLE public.reembolso_viagem ALTER COLUMN status TYPE public.status_reembolso_viagem USING status::status_reembolso_viagem;

/* ---------------------------------------------------------------------- */
/* Add table "mensagem"                                                   */
/* ---------------------------------------------------------------------- */
CREATE TABLE public.mensagem (
    id_mensagem NUMERIC(12)  NOT NULL,
    id_empresa public.id  NOT NULL,
    id_assunto public.id,
    assunto_descricao CHARACTER VARYING(100)  NOT NULL,
    id_funcionario_autor public.id,
    flg_mensagem_sistema public.sim_nao  NOT NULL,
    status public.status_mensagem  NOT NULL,
    flg_permite_responder public.sim_nao  NOT NULL,
    flg_exige_confirmacao public.sim_nao  NOT NULL,
    flg_bloqueia_sem_responder public.sim_nao  NOT NULL,
    texto_botao_confirmacao CHARACTER VARYING(50),
    CONSTRAINT pk_mensagem PRIMARY KEY (id_mensagem)
);

/* ---------------------------------------------------------------------- */
/* Add table "assunto"                                                    */
/* ---------------------------------------------------------------------- */
CREATE TABLE public.assunto (
    id_assunto public.id  NOT NULL,
    id_empresa public.id  NOT NULL,
    nome CHARACTER VARYING(100)  NOT NULL,
    status public.status  NOT NULL,
    flg_permite_responder public.sim_nao  NOT NULL,
    flg_exige_confirmacao public.sim_nao  NOT NULL,
    flg_bloqueia_sem_responder public.sim_nao  NOT NULL,
    CONSTRAINT pk_assunto PRIMARY KEY (id_assunto)
);

/* ---------------------------------------------------------------------- */
/* Add table "forma_notificacao"                                          */
/* ---------------------------------------------------------------------- */
CREATE TABLE public.forma_notificacao (
    id_forma_notificacao public.id  NOT NULL,
    nome CHARACTER VARYING(100)  NOT NULL,
    status public.status  NOT NULL,
    classe CHARACTER VARYING(1000)  NOT NULL,
    nome_fila CHARACTER(50)  NOT NULL,
    CONSTRAINT pk_forma_notificacao PRIMARY KEY (id_forma_notificacao)
);

/* ---------------------------------------------------------------------- */
/* Add table "forma_notificacao_empresa"                                  */
/* ---------------------------------------------------------------------- */
CREATE TABLE public.forma_notificacao_empresa (
    id_forma_notificacao_empresa public.id  NOT NULL,
    id_forma_notificacao public.id  NOT NULL,
    id_empresa public.id  NOT NULL,
    status public.status  NOT NULL,
    CONSTRAINT pk_forma_notificacao_empresa PRIMARY KEY (id_forma_notificacao_empresa)
);

/* ---------------------------------------------------------------------- */
/* Add table "mensagem_destinatarios"                                     */
/* ---------------------------------------------------------------------- */
CREATE TABLE public.mensagem_destinatarios (
    id_mensagem_destinatario NUMERIC(12)  NOT NULL,
    id_empresa public.id  NOT NULL,
    id_mensagem NUMERIC(12)  NOT NULL,
    id_funcionario public.id  NOT NULL,
    CONSTRAINT pk_mensagem_destinatarios PRIMARY KEY (id_mensagem_destinatario)
);

/* ---------------------------------------------------------------------- */
/* Add table "mensagem_forma_notificacao"                                 */
/* ---------------------------------------------------------------------- */
CREATE TABLE public.mensagem_forma_notificacao (
    id_mensagem_forma_notificacao NUMERIC(12)  NOT NULL,
    id_forma_notificacao NUMERIC(8)  NOT NULL,
    id_mensagem NUMERIC(12)  NOT NULL,
    id_empresa NUMERIC(8)  NOT NULL,
    CONSTRAINT pk_mensagem_forma_notificacao PRIMARY KEY (id_mensagem_forma_notificacao)
);

/* ---------------------------------------------------------------------- */
/* Add table "mensagem_confirmacao_leitura"                               */
/* ---------------------------------------------------------------------- */
CREATE TABLE public.mensagem_confirmacao_leitura (
    id_forma_notificacao NUMERIC(8)  NOT NULL,
    id_empresa public.id  NOT NULL,
    id_mensagem_destinatario NUMERIC(12)  NOT NULL,
    id_mensagem_confirmacao_leitura NUMERIC(12)  NOT NULL,
    data_hora TIMESTAMP  NOT NULL,
    CONSTRAINT pk_mensagem_confirmacao_leitura PRIMARY KEY (id_forma_notificacao)
);

/* ---------------------------------------------------------------------- */
/* Add foreign key constraints                                            */
/* ---------------------------------------------------------------------- */
ALTER TABLE public.reembolso_viagem ADD CONSTRAINT empresa_reembolso_viagem 
    FOREIGN KEY (id_empresa) REFERENCES public.empresa (id_empresa);
ALTER TABLE public.reembolso_viagem ADD CONSTRAINT lancamento_reembolso_viagem 
    FOREIGN KEY (id_lancamento_credito) REFERENCES public.lancamento (id_lancamento);
ALTER TABLE public.reembolso_viagem ADD CONSTRAINT viagem_reembolso_viagem 
    FOREIGN KEY (id_viagem) REFERENCES public.viagem (id_viagem);
ALTER TABLE public.reembolso_viagem ADD CONSTRAINT funcionario_reembolso_viagem_4 
    FOREIGN KEY (id_funcionario_lancamento) REFERENCES public.funcionario (id_funcionario);
ALTER TABLE public.reembolso_viagem ADD CONSTRAINT funcionario_reembolso_viagem_5 
    FOREIGN KEY (id_funcionario_reembolso) REFERENCES public.funcionario (id_funcionario);
ALTER TABLE public.reembolso_viagem ADD CONSTRAINT funcionario_reembolso_viagem_6 
    FOREIGN KEY (id_funcionario_registro_reembolso) REFERENCES public.funcionario (id_funcionario);
ALTER TABLE public.reembolso_viagem ADD CONSTRAINT funcionario_reembolso_viagem_7 
    FOREIGN KEY (id_funcionario_cancelamento) REFERENCES public.funcionario (id_funcionario);
ALTER TABLE public.mensagem ADD CONSTRAINT empresa_mensagem 
    FOREIGN KEY (id_empresa) REFERENCES public.empresa (id_empresa);
ALTER TABLE public.mensagem ADD CONSTRAINT funcionario_mensagem 
    FOREIGN KEY (id_funcionario_autor) REFERENCES public.funcionario (id_funcionario);
ALTER TABLE public.mensagem ADD CONSTRAINT assunto_mensagem 
    FOREIGN KEY (id_assunto) REFERENCES public.assunto (id_assunto);
ALTER TABLE public.assunto ADD CONSTRAINT empresa_assunto 
    FOREIGN KEY (id_empresa) REFERENCES public.empresa (id_empresa);
ALTER TABLE public.forma_notificacao_empresa ADD CONSTRAINT forma_notificacao_forma_notificacao_empresa 
    FOREIGN KEY (id_forma_notificacao) REFERENCES public.forma_notificacao (id_forma_notificacao);
ALTER TABLE public.forma_notificacao_empresa ADD CONSTRAINT empresa_forma_notificacao_empresa 
    FOREIGN KEY (id_empresa) REFERENCES public.empresa (id_empresa);
ALTER TABLE public.mensagem_destinatarios ADD CONSTRAINT mensagem_mensagem_destinatarios 
    FOREIGN KEY (id_mensagem) REFERENCES public.mensagem (id_mensagem);
ALTER TABLE public.mensagem_destinatarios ADD CONSTRAINT funcionario_mensagem_destinatarios 
    FOREIGN KEY (id_funcionario) REFERENCES public.funcionario (id_funcionario);
ALTER TABLE public.mensagem_destinatarios ADD CONSTRAINT empresa_mensagem_destinatarios 
    FOREIGN KEY (id_empresa) REFERENCES public.empresa (id_empresa);
ALTER TABLE public.mensagem_forma_notificacao ADD CONSTRAINT mensagem_mensagem_forma_notificacao 
    FOREIGN KEY (id_mensagem) REFERENCES public.mensagem (id_mensagem);
ALTER TABLE public.mensagem_forma_notificacao ADD CONSTRAINT forma_notificacao_mensagem_forma_notificacao 
    FOREIGN KEY (id_forma_notificacao) REFERENCES public.forma_notificacao (id_forma_notificacao);
ALTER TABLE public.mensagem_forma_notificacao ADD CONSTRAINT empresa_mensagem_forma_notificacao 
    FOREIGN KEY (id_empresa) REFERENCES public.empresa (id_empresa);
ALTER TABLE public.mensagem_confirmacao_leitura ADD CONSTRAINT forma_notificacao_mensagem_confirmacao_leitura 
    FOREIGN KEY (id_forma_notificacao) REFERENCES public.forma_notificacao (id_forma_notificacao);
ALTER TABLE public.mensagem_confirmacao_leitura ADD CONSTRAINT mensagem_destinatarios_mensagem_confirmacao_leitura 
    FOREIGN KEY (id_mensagem_destinatario) REFERENCES public.mensagem_destinatarios (id_mensagem_destinatario);
ALTER TABLE public.mensagem_confirmacao_leitura ADD CONSTRAINT empresa_mensagem_confirmacao_leitura 
    FOREIGN KEY (id_empresa) REFERENCES public.empresa (id_empresa);
