--
-- PostgreSQL database dump
--

-- Dumped from database version 9.5.4
-- Dumped by pg_dump version 9.5.4

-- Started on 2017-10-10 21:59:41 -04

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 213 (class 1259 OID 30823)
-- Name: cidade; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE cidade (
    id_cidade id NOT NULL,
    nome character varying(400) NOT NULL,
    id_estado numeric(8,0)
);


ALTER TABLE cidade OWNER TO postgres;

--
-- TOC entry 214 (class 1259 OID 30831)
-- Name: estado; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE estado (
    id_estado id NOT NULL,
    nome character varying(300) NOT NULL,
    sigla character(2) NOT NULL
);


ALTER TABLE estado OWNER TO postgres;

--
-- TOC entry 2496 (class 0 OID 30823)
-- Dependencies: 213
-- Data for Name: cidade; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY cidade (id_cidade, nome, id_estado) FROM stdin;
37706	Simões Filho	5
37707	Sitio da Baraúna	5
37708	Sitio do Mato	5
37709	Sitio do Meio	5
37710	Sitio do Quinto	5
37711	Sitio Grande	5
37712	Sitio Novo	5
37713	Soares	5
44916	Ingleses do Rio Vermelho	24
44917	Invernada	24
44918	Iomere	24
44919	Ipira	24
44920	Ipomeia	24
44921	Ipora do Oeste	24
44922	Ipuacu	24
44923	Ipumirim	24
44924	Iraceminha	24
44925	Irakitan	24
44926	Irani	24
44927	Iraputa	24
44928	Irati	24
44929	Irineopolis	24
44930	Ita	24
44931	Itaio	24
44932	Itaiopolis	24
44933	Itajai	24
44934	Itajuba	24
44935	Itapema	24
44936	Itapiranga	24
44937	Itapoa	24
44938	Itapocu	24
44939	Itoupava	24
44940	Ituporanga	24
44941	Jabora	24
44942	Jacinto Machado	24
44943	Jaguaruna	24
44944	Jaragua do Sul	24
44945	Jardinopolis	24
44946	Joacaba	24
44947	Joinville	24
44948	José Boiteux	24
44949	Jupia	24
44950	Lacerdopolis	24
44951	Lages	24
44952	Lagoa	24
44953	Lagoa da Estiva	24
44954	Laguna	24
44955	Lajeado Grande	24
44956	Laurentino	24
44957	Lauro Muller	24
44958	Leão	24
44959	Lebon Regis	24
44960	Leoberto Leal	24
44961	Lindoia do Sul	24
44962	Linha das Palmeiras	24
44963	Lontras	24
44964	Lourdes	24
44965	Luiz Alves	24
44966	Luzerna	24
44967	Machados	24
44968	Macieira	24
44969	Mafra	24
44970	Major Gercino	24
44971	Major Vieira	24
44972	Maracaja	24
44973	Marari	24
44974	Marata	24
44975	Maravilha	24
44976	Marcilio Dias	24
44977	Marechal Bormann	24
44978	Marema	24
44979	Mariflor	24
44980	Marombas	24
44981	Marombas Bossardi	24
44982	Massaranduba	24
44983	Matos Costa	24
44984	Meleiro	24
44985	Mirador	24
44986	Mirim	24
44987	Mirim Doce	24
44988	Modelo	24
44989	Mondai	24
44990	Monte Alegre	24
44991	Monte Carlo	24
44992	Monte Castelo	24
44993	Morro Chato	24
44994	Morro da Fumaca	24
44995	Morro do Meio	24
44996	Morro Grande	24
44997	Navegantes	24
44998	Nossa Senhora de Caravaggio	24
44999	Nova Cultura	24
45000	Nova Erechim	24
45001	Nova Guarita	24
45002	Nova Itaberaba	24
45003	Nova Petropolis	24
45004	Nova Teutonia	24
45005	Nova Trento	24
45006	Nova Veneza	24
45007	Novo Horizonte	24
45008	Orleans	24
45009	Otacilio Costa	24
45010	Ouro	24
45011	Ouro Verde	24
45012	Paiál	24
45013	Painel	24
45014	Palhoca	24
45015	Palma Sola	24
45016	Palmeira	24
45017	Palmitos	24
45018	Pantano do Sul	24
45019	Papanduva	24
45020	Paraiso	24
45021	Passo de Torres	24
45022	Passo Manso	24
45023	Passos Maiá	24
45024	Paula Pereira	24
45025	Paulo Lopes	24
45026	Pedras Grandes	24
45027	Penha	24
45028	Perico	24
45029	Peritiba	24
45030	Pescaria Brava	24
45031	Petrolândia	24
45032	Picarras	24
45033	Pindotiba	24
45034	Pinhalzinho	24
45035	Pinheiral	24
45036	Pinheiro Preto	24
45037	Pinheiros	24
45038	Pirabeiraba	24
45039	Piratuba	24
45040	Planalto	24
45041	Planalto Alegre	24
45042	Poço Preto	24
45043	Pomerode	24
45044	Ponte Alta	24
45045	Ponte Alta do Norte	24
45046	Ponte Serrada	24
45047	Porto Belo	24
45048	Porto União	24
45049	Pouso Redondo	24
45050	Praiá Grande	24
45051	Prata	24
45052	Presidente Castelo Branco	24
37714	Sobradinho	5
37715	Souto Soares	5
37716	Subauma	5
37717	Sussuarana	5
37718	Tabocas do Brejo Velho	5
37719	Taboleiro do Castro	5
37720	Taboquinhas	5
37721	Taguá	5
37722	Tamburil	5
37723	Tanhaçu	5
37724	Tanque Novo	5
37725	Tanquinho	5
37726	Tanquinho do Poço	5
37727	Taperoá	5
37728	Tapiraipe	5
37729	Tapirama	5
37730	Tapiramutá	5
37731	Tapiranga	5
37732	Tapuia	5
37733	Taquarendi	5
37734	Taquarinha	5
37735	Tartaruga	5
37736	Tauape	5
37737	Teixeira de Freitas	5
37738	Teodoro Sampaio	5
37739	Teofilândia	5
37740	Teolândia	5
37741	Terra Nova	5
37742	Tijuaçu	5
37743	Tiquarucu	5
37744	Tremedal	5
37745	Triunfo do Sincorá	5
37746	Tucano	5
37747	Uauá	5
37748	Ubaíra	5
37749	Ubaitaba	5
37750	Ubata	5
37751	Ubiracaba	5
37752	Ubiraita	5
37753	Uibaí	5
37754	Umburanas	5
37755	Umbuzeiro	5
37756	Una	5
37757	Urandi	5
37758	Uruçuca	5
37759	Utinga	5
37760	Vale Verde	5
37761	Valença	5
37762	Valente	5
37763	Várzea da Roca	5
37764	Várzea do Caldas	5
37765	Várzea do Cerco	5
37766	Várzea do Poço	5
37767	Várzea Nova	5
37768	Várzeas	5
37769	Varzedo	5
37770	Velha Boipeba	5
37771	Ventura	5
37772	Vera Cruz	5
37773	Vereda	5
37774	Vila do Cafe	5
37775	Vitória da Conquista	5
37776	Volta Grande	5
37777	Wagner	5
37778	Wanderley	5
37779	Wenceslau Guimarães	5
37780	Xique-xique	5
37781	Abaiára	6
37782	Abílio Martins	6
37783	Acarape	6
37784	Acaraú	6
37785	Acopiara	6
37786	Adrianópolis	6
37787	Água Verde	6
37788	Aguaí	6
37789	Aiua	6
37790	Aiuaba	6
37791	AlaGoiásnha	6
37792	Alcântaras	6
37793	Algodoes	6
37794	Almofala	6
37795	Altaneira	6
37796	Alto Santo	6
37797	Amanaiára	6
37798	Amanari	6
37799	Amaniutuba	6
37800	Amarelas	6
37801	Amaro	6
37802	America	6
37803	Amontada	6
37804	Anaua	6
37805	Aningas	6
37806	Anjinhos	6
37807	Antonina do Norte	6
37808	Antonio Bezerra	6
37809	Antonio Diogo	6
37810	Antonio Marques	6
37811	Aprazível	6
37812	Apuiarés	6
37813	Aquinopolis	6
37814	Aquiraz	6
37815	Aracas	6
37816	Aracati	6
37817	Aracatiaçu	6
37818	Aracatiara	6
37819	Aracoiaba	6
37820	Arajara	6
37821	Aranau	6
37822	Arapa	6
37823	Arapari	6
37824	Araporanga	6
37825	Araquém	6
37826	Ararendá	6
37827	Araripe	6
37828	Ararius	6
37829	Aratama	6
37830	Araticum	6
37831	Aratuba	6
37832	Areial	6
37833	Ariscos dos Marianos	6
37834	Arneiroz	6
37835	Aroeiras	6
37836	Arrojado	6
37837	Aruaru	6
37838	Assaré	6
37839	Assunção	6
37840	Aurora	6
37841	Baixa Grande	6
37842	Baixio	6
37843	Baixio da Donana	6
37844	Banabuiú	6
37845	Bandeira	6
37846	Barão de Aquiraz	6
37847	Barbalha	6
37848	Barra	6
37849	Barra do Sotero	6
37850	Barra Nova	6
37851	Barreira	6
37852	Barreira dos Vianas	6
37853	Barreiras	6
37854	Barreiros	6
37855	Barrento	6
37856	Barro	6
37857	Barro Alto	6
37858	Barroquinha	6
37859	Baturité	6
37860	Bau	6
37861	Beberibe	6
37862	Bela Cruz	6
37863	Bela Vista	6
37864	Betânia	6
37865	Bitupita	6
37866	Bixopa	6
37867	Boa Água	6
37868	Boa Esperança	6
37869	Boa Viagem	6
37870	Boa Vista	6
37871	Boa Vista do Caxitore	6
37872	Bonfim	6
37873	Bonhu	6
37874	Bonito	6
37875	Borges	6
37876	Brejinho	6
37877	Brejo Grande	6
37878	Brejo Santo	6
37879	Brotas	6
37880	Buritizal	6
37881	Buritizinho	6
37882	Cabreiro	6
37883	Cachoeira	6
37884	Cachoeira Grande	6
37885	Caiçara	6
37886	Caicarinha	6
37887	Caio Prado	6
37888	Caioca	6
37889	Caipu	6
37890	Calabaça	6
37891	Caldeirão	6
37892	California	6
37893	Camara	6
37894	Camboas	6
37895	Camilos	6
37896	Camocim	6
37897	Campanário	6
37898	Campestre	6
37899	Campos Sales	6
37900	Canaan	6
37901	Canafístula	6
37902	Cangati	6
37903	Canindé	6
37904	Canindezinho	6
37905	Capistrano	6
37906	Caponga	6
37907	Caponga da Bernarda	6
37908	Caracara	6
37909	Caridade	6
37910	Cariré	6
37911	Caririaçu	6
37912	Cariús	6
37913	Cariutaba	6
37914	Carmelopolis	6
37915	Carnaubal	6
37916	Carnaúbas	6
37917	Carnaubinha	6
37918	Carquejo	6
37919	Carrapateiras	6
37920	Caruatai	6
37921	Carvalho	6
37922	Carvoeiro	6
37923	Cascavel	6
37924	Castanhão	6
37925	Catarina	6
37926	Catolé	6
37927	Catuana	6
37928	Catunda	6
37929	Caucaiá	6
37930	Caxitore	6
37931	Cedro	6
37932	Cemoaba	6
37933	Chaval	6
37934	Choro	6
37935	Chorozinho	6
37936	Cipo dos Anjos	6
37937	Cococi	6
37938	Codia	6
37939	Coite	6
37940	Colina	6
37941	Conceição	6
37942	Coreau	6
37943	Córrego dos Fernandes	6
37944	Crateús	6
37945	Crato	6
37946	Crioulos	6
37947	Cristais	6
37948	Croata	6
37949	Cruxati	6
37950	Cruz	6
37951	Cruz de Pedra	6
37952	Cruzeirinho	6
37953	Cuncas	6
37954	Curatis	6
37955	Curupira	6
37956	Custodio	6
37957	Daniel de Queiros	6
37958	Delmiro Gouveia	6
37959	Deputado Irapuan Pinheiro	6
37960	Deserto	6
37961	Dom Leme	6
37962	Dom Mauricio	6
37963	Dom Quintino	6
37964	Domingos da Costa	6
37965	Donato	6
37966	Dourados	6
37967	Ebron	6
37968	Ema	6
37969	Ematuba	6
37970	Encantado	6
37971	Engenheiro João Tome	6
37972	Engenheiro José Lopes	6
37973	Engenho Velho	6
37974	Ererê	6
37975	Espacinha	6
37976	Esperança	6
37977	Espinho	6
37978	Eusebio	6
37979	Farias Brito	6
37980	Fatima	6
37981	Feiticeiro	6
37982	Feitosa	6
37983	Felizardo	6
37984	Flamengo	6
37985	Flores	6
37986	Forquilha	6
37987	Fortaleza	6
37988	Fortim	6
37989	Frecheirinha	6
37990	Gado	6
37991	Gado dos Rodrigues	6
37992	Gameleira de São Sebastião	6
37993	Garças	6
37994	Gázea	6
37995	General Sampaio	6
37996	General Tibúrcio	6
37997	Genipapeiro	6
37998	Gererau	6
37999	Giqui	6
38000	Girau	6
38001	Graça	6
38002	Granja	6
38003	Granjeiro	6
38004	Groaíras	6
38005	Guaiuba	6
38006	Guajiru	6
38007	Guanaces	6
38008	Guaraciaba do Norte	6
38009	Guaramiranga	6
38010	Guararu	6
38011	Guassi	6
38012	Guassosse	6
38013	Guia	6
38014	Guriu	6
38015	Hidrolândia	6
38016	Holanda	6
38017	Horizonte	6
38018	Iapi	6
38019	Iara	6
38020	Ibaretama	6
38021	Ibiapaba	6
38022	Ibiapina	6
38023	Ibicatu	6
38024	Ibicua	6
38025	Ibicuitaba	6
38026	Ibicuitinga	6
38027	Iborepi	6
38028	Ibuacu	6
38029	Ibuguacu	6
38030	Icapuí	6
38031	Icaraí	6
38032	Ico	6
38033	Icozinho	6
38034	Ideal	6
38035	Igaroi	6
38036	Iguatu	6
38037	Independência	6
38038	Ingazeiras	6
38039	Inhamuns	6
38040	Inhucu	6
38041	Inhuporanga	6
38042	Ipaporanga	6
38043	Ipaumirim	6
38044	Ipu	6
38045	Ipueiras	6
38046	Ipueiras dos Gomes	6
38047	Iracema	6
38048	Irajá	6
38049	Irapuã	6
38050	Iratinga	6
38051	Irauçuba	6
38052	Isidoro	6
38053	Itacima	6
38054	Itaguá	6
38055	Itaiçaba	6
38056	Itaipaba	6
38057	Itaitinga	6
38058	Itãs	6
38059	Itapagé	6
38060	Itapebussu	6
38061	Itapeim	6
38062	Itapipoca	6
38063	Itapiuna	6
38064	Itapó	6
38065	Itarema	6
38066	Itatira	6
38067	Jaburuna	6
38068	Jacampari	6
38069	Jacarecoara	6
38070	Jacaúna	6
38071	Jaguarão	6
38072	Jaguaretama	6
38073	Jaguaribara	6
38074	Jaguaribe	6
38075	Jaguaruana	6
38076	Jaibaras	6
38077	Jamacaru	6
38078	Jandrangoeira	6
38079	Jardim	6
38080	Jardimirim	6
38081	Jati	6
38082	Jijoca de Jericoacoara	6
38083	João Cordeiro	6
38084	Jordão	6
38085	José de Alencar	6
38086	Juá	6
38087	Juatama	6
38088	Juazeiro de Baixo	6
38089	Juazeiro do Norte	6
38090	Jubaiá	6
38091	Jucas	6
38092	Jurema	6
38093	Justiniano Serpa	6
38094	Lacerda	6
38095	Ladeira Grande	6
38096	Lagoa de São José	6
38097	Lagoa do Juvenal	6
38098	Lagoa do Mato	6
38099	Lagoa dos Crioulos	6
38100	Lagoa Grande	6
38101	LaGoiásnha	6
38102	Lambedouro	6
38103	Lameiro	6
38104	Lavras da Mangabeira	6
38105	Lima Campos	6
38106	Limoeiro do Norte	6
38107	Lisieux	6
38108	Livramento	6
38109	Logradouro	6
38110	Macambira	6
38111	Maçãoca	6
38112	Macarau	6
38113	Maceió	6
38114	Madalena	6
38115	Major Simplicio	6
38116	Majorlândia	6
38117	Malhada Grande	6
38118	Mangabeira	6
38119	Manibu	6
38120	Manituba	6
38121	Mapuã	6
38122	Maracanau	6
38123	Maraguá	6
38124	Maranguape	6
38125	Mararupa	6
38126	Marco	6
38127	Marinheiros	6
38128	Marrecas	6
38129	Marrocos	6
38130	Marruas	6
38131	Martinopole	6
38132	Massape	6
38133	Mata Fresca	6
38134	Matias	6
38135	Matriz	6
38136	Mauriti	6
38137	Mel	6
38138	Meruoca	6
38139	Messejana	6
38140	Miguel Xavier	6
38141	Milagres	6
38142	Milha	6
38143	Milton Belo	6
38144	Mineirolândia	6
38145	Miragem	6
38146	Miraima	6
38147	Mirambe	6
38148	Missão Nova	6
38149	Missão Velha	6
38150	Missi	6
38151	Moitas	6
38152	Mombaca	6
38153	Mondubim	6
38154	Monguba	6
38155	Monsenhor Tabosa	6
38156	Monte Alegre	6
38157	Monte Castelo	6
38158	Monte Grave	6
38159	Monte Sion	6
38160	Montenebo	6
38161	Morada Nova	6
38162	Moraujo	6
38163	Morrinhos	6
38164	Morrinhos Novos	6
38165	Morro Branco	6
38166	Mucambo	6
38167	Mulungu	6
38168	Mumbaba	6
38169	Mundau	6
38170	Muribeca	6
38171	Muriti	6
38172	Mutambeiras	6
38173	Naraniu	6
38174	Nascente	6
38175	Nenenlândia	6
38176	Nossa Senhora do Livramento	6
38177	Nova Betania	6
38178	Nova Fátima	6
38179	Nova Floresta	6
38180	Nova Olinda	6
38181	Nova Russas	6
38182	Nova Vida	6
38183	Novo Assis	6
38184	Novo Oriente	6
38185	Ocara	6
38186	Oiticica	6
38187	Olho-d'agua	6
38188	Olho-d'agua da Bica	6
38189	Oliveiras	6
38190	Orós	6
38191	Pacajus	6
38192	Pacatuba	6
38193	Pacoti	6
38194	Pacuja	6
38195	Padre Cicero	6
38196	Padre Linhares	6
38197	Padre Vieira	6
38198	Pajeu	6
38199	Pajucara	6
38200	Palestina	6
38201	Palestina do Norte	6
38202	Palhano	6
38203	Palmacia	6
38204	Palmatoria	6
38205	Panacui	6
38206	Paracua	6
38207	Paracuru	6
38208	Paraipaba	6
38209	Parajuru	6
38210	Parambu	6
38211	Paramoti	6
38212	Parangaba	6
38213	Parapui	6
38214	Parazinho	6
38215	Paripueira	6
38216	Passagem	6
38217	Passagem Funda	6
38218	Pasta	6
38219	Patacas	6
38220	Patriarca	6
38221	Pavuna	6
38222	Pecem	6
38223	Pedra Branca	6
38224	Pedras	6
38225	Pedrinhas	6
38226	Peixe	6
38227	Peixe Gordo	6
38228	Penaforte	6
38229	Pentecoste	6
38230	Pereiro	6
38231	Pernambuquinho	6
38232	Pessoa Anta	6
38233	Pindoguaba	6
38234	Pindoretama	6
38235	Pio X	6
38236	Piquet Carneiro	6
38237	Pirabibu	6
38238	Pirangi	6
38239	Pires Ferreira	6
38240	Pitombeira	6
38241	Pitombeiras	6
38242	Placido Martins	6
38243	Poço	6
38244	Poço Comprido	6
38245	Poço Grande	6
38246	Podimirim	6
38247	Ponta da Serra	6
38248	Poranga	6
38249	Porfirio Sampaio	6
38250	Porteiras	6
38251	Potengi	6
38252	Poti	6
38253	Potiretama	6
38254	Prata	6
38255	Prudente de Morais	6
38256	Quatiguaba	6
38257	Queimadas	6
38258	Quimami	6
38259	Quincoe	6
38260	Quincunca	6
38261	Quitaius	6
38262	Quiterianopolis	6
38263	Quixada	6
38264	Quixariu	6
38265	Quixelo	6
38266	Quixeramobim	6
38267	Quixere	6
38268	Quixoa	6
38269	Raimundo Martins	6
38270	Redenção	6
38271	Reriutaba	6
38272	Riachao do Banabuiu	6
38273	Riacho Grande	6
38274	Riacho Verde	6
38275	Riacho Vermelho	6
38276	Rinare	6
38277	Roldão	6
38278	Russas	6
38279	Sabiaguaba	6
38280	Saboeiro	6
38281	Sacramento	6
38282	Salão	6
38283	Salitre	6
38284	Sambaiba	6
38285	Santa Ana	6
38286	Santa Fé	6
38287	Santa Felicia	6
38288	Santa Luzia	6
38289	Santa Quiteria	6
38290	Santa Tereza	6
38291	Santana	6
38292	Santana do Acarau	6
38293	Santana do Cariri	6
38294	Santarem	6
38295	Santo Antonio	6
38296	Santo Antonio da Pindoba	6
38297	Santo Antonio dos Fernandes	6
38298	São Bartolomeu	6
38299	São Benedito	6
38300	São Domingos	6
38301	São Felipe	6
38302	São Francisco	6
38303	São Gerardo	6
38304	São Goncalo do Amarante	6
38305	São Goncalo do Umari	6
38306	São João de Deus	6
38307	São João do Jaguaribe	6
38308	São João dos Queiroz	6
38309	São Joaquim	6
38310	São Joaquim do Salgado	6
38311	São José	6
38312	São José das Lontras	6
38313	São José de Solonopole	6
38314	São José do Torto	6
38315	São Luis do Curu	6
38316	São Miguel	6
38317	São Paulo	6
38318	São Pedro	6
38319	São Romão	6
38320	São Sebastião	6
38321	São Vicente	6
38322	Sapo	6
38323	Sapupara	6
38324	Sebastião de Abreu	6
38325	Senador Carlos Jereissati	6
38326	Senador Pompeu	6
38327	Senador Sa	6
38328	Sereno de Cima	6
38329	Serra do Felix	6
38330	Serragem	6
38331	Serrota	6
38332	Serrote	6
38333	Sitia	6
38334	Sitios Novos	6
38335	Siupe	6
38336	Sobral	6
38337	Soledade	6
38338	Solonopole	6
38339	Suassurana	6
38340	Sucatinga	6
38341	Sucesso	6
38342	Sussuanha	6
38343	Tabainha	6
38344	Taboleiro	6
38345	Tabuleiro do Norte	6
38346	Taiba	6
38347	Tamboril	6
38348	Tanques	6
38349	Tapera	6
38350	Taperuaba	6
38351	Tapuiara	6
38352	Targinos	6
38353	Tarrafas	6
38354	Taua	6
38355	Tejucuoca	6
38356	Tiangua	6
38357	Timonha	6
38358	Tipi	6
38359	Tome	6
38360	Trairi	6
38361	Trapia	6
38362	Trici	6
38363	Troia	6
38364	Trussu	6
38365	Tucunduba	6
38366	Tucuns	6
38367	Tuina	6
38368	Tururu	6
38369	Ubajara	6
38370	Ubauna	6
38371	Ubiracu	6
38372	Uiraponga	6
38373	Umari	6
38374	Umarituba	6
38375	Umburanas	6
38376	Umirim	6
38377	Uruburetama	6
38378	Uruoca	6
38379	Uruque	6
38380	Varjota	6
38381	Varzea	6
38382	Varzea Alegre	6
38383	Varzea da Volta	6
38384	Varzea do Gilo	6
38385	Vazantes	6
38386	Ventura	6
38387	Vertentes do Lagedo	6
38388	Vicosa	6
38389	Vicosa do Cearáa	6
38390	Vila Soares	6
38391	Brasilia	7
38392	Brazlândia	7
38393	Candangolândia	7
38394	Ceilândia	7
38395	Cruzeiro	7
38396	Gama	7
38397	Guara	7
38398	Lago Norte	7
38399	Lago Sul	7
38400	Nucleo Bandeirante	7
38401	Paranoa	7
38402	Planaltina	7
38403	Recanto das Emas	7
38404	Riacho Fundo	7
38405	Samambaiá	7
38406	Santa Maria	7
38407	São Sebastião	7
38408	Sobradinho	7
38409	Taguatinga	7
38410	Acioli	8
38411	Afonso Claudio	8
38412	Agha	8
38413	Água Doce do Norte	8
38414	Aguia Branca	8
38415	Airituba	8
38416	Alegre	8
38417	Alfredo Chaves	8
38418	Alto Calcado	8
38419	Alto Caldeirão	8
38420	Alto Mutum Preto	8
38421	Alto Rio Novo	8
38422	Alto Santa Maria	8
38423	Anchieta	8
38424	Angelo Frechiani	8
38425	Anutiba	8
38426	Apiaca	8
38427	Aracatiba	8
38428	Arace	8
38429	Aracruz	8
38430	Aracui	8
38431	Araguaiá	8
38432	Ararai	8
38433	Argolas	8
38434	Atilio Vivacqua	8
38435	Baiá Nova	8
38436	Baixo Guandu	8
38437	Barra de Novo Brasil	8
38438	Barra de São Francisco	8
38439	Barra do Itapemirim	8
38440	Barra Nova	8
38441	Barra Seca	8
38442	Baunilha	8
38443	Bebedouro	8
38444	Boa Esperanca	8
38445	Boapaba	8
38446	Bom Jesus do Norte	8
38447	Bonsucesso	8
38448	Braco do Rio	8
38449	Brejetuba	8
38450	Burarama	8
38451	Cachoeirinha de Itauna	8
38452	Cachoeiro de Itapemirim	8
38453	Cafe	8
38454	Calogi	8
38455	Camara	8
38456	Carapina	8
38457	Cariacica	8
38458	Castelo	8
38459	Celina	8
38460	Colatina	8
38461	Conceição da Barra	8
38462	Conceição do Castelo	8
38463	Conceição do Muqui	8
38464	Conduru	8
38465	Coqueiral	8
38466	Córrego dos Monos	8
38467	Cotaxe	8
38468	Cristal do Norte	8
38469	Crubixa	8
38470	Desengano	8
38471	Divino de São Lourenço	8
38472	Divino Espirito Santo	8
38473	Djalma Coutinho	8
38474	Domingos Martins	8
38475	Dona America	8
38476	Dores do Rio Preto	8
38477	Duas Barras	8
38478	Ecoporanga	8
38479	Estrela do Norte	8
38480	Fartura	8
38481	Fazenda Guandu	8
38482	Fundão	8
38483	Garrafão	8
38484	Gironda	8
38485	Goiásabeiras	8
38486	Governador Lacerda de Aguiar	8
38487	Governador Lindenberg	8
38488	Graca Aranha	8
38489	Gruta	8
38490	Guacui	8
38491	Guarana	8
38492	Guarapari	8
38493	Guararema	8
38494	Ibatiba	8
38495	Ibes	8
38496	Ibicaba	8
38497	Ibiracu	8
38498	Ibitirama	8
38499	Ibitirui	8
38500	Ibituba	8
38501	Iconha	8
38502	Imburana	8
38503	Iriritiba	8
38504	Irundi	8
38505	Irupi	8
38506	Isabel	8
38507	Itabaiána	8
38508	Itacu	8
38509	Itaguacu	8
38510	Itaici	8
38511	Itaimbe	8
38512	Itaipava	8
38513	Itamira	8
38514	Itaoca	8
38515	Itapecoa	8
38516	Itapemirim	8
38517	Itaperuna	8
38518	Itapina	8
38519	Itaquari	8
38520	Itarana	8
38521	Itaunas	8
38522	Itauninhas	8
38523	Iuna	8
38524	Jabaquara	8
38525	Jacaraipe	8
38526	Jacigua	8
38527	Jacupemba	8
38528	Jaguare	8
38529	Jeronimo Monteiro	8
38530	Joacuba	8
38531	João Neiva	8
38532	Joatuba	8
38533	José Carlos	8
38534	Jucu	8
38535	Lajinha	8
38536	Laranja da Terra	8
38537	Limoeiro	8
38538	Linhares	8
38539	Mangarai	8
38540	Mantenopolis	8
38541	Marataizes	8
38542	Marechal Floriano	8
38543	Marilândia	8
38544	Matilde	8
38545	Melgaco	8
38546	Menino Jesus	8
38547	Mimoso do Sul	8
38548	Montanha	8
38549	Monte Carmelo do Rio Novo	8
38550	Monte Pio	8
38551	Monte Sinai	8
38552	Mucurici	8
38553	Mundo Novo	8
38554	Muniz Freire	8
38555	Muqui	8
38556	Nestor Gomes	8
38557	Nova Almeida	8
38558	Nova Canaa	8
38559	Nova Venecia	8
38560	Nova Verona	8
38561	Novo Brasil	8
38562	Novo Horizonte	8
38563	Pacotuba	8
38564	Paineiras	8
38565	Palmeira	8
38566	Palmerino	8
38567	Pancas	8
38568	Paraju	8
38569	Paulista	8
38570	Pedro Canario	8
38571	Pendanga	8
38572	Pequia	8
38573	Perdição	8
38574	Piacu	8
38575	Pinheiros	8
38576	Piracema	8
38577	Piuma	8
38578	Ponte de Itabapoana	8
38579	Ponto Belo	8
38580	Pontoes	8
38581	Poranga	8
38582	Porto Barra do Riacho	8
38583	Praiá Grande	8
38584	Presidente Kennedy	8
38585	Princesa	8
38586	Queimado	8
38587	Quilometro Null do Mutum	8
38588	Regencia	8
38589	Riacho	8
38590	Ribeirão do Cristo	8
38591	Rio Bananal	8
38592	Rio Calcado	8
38593	Rio Muqui	8
38594	Rio Novo do Sul	8
38595	Rio Preto	8
38596	Rive	8
38597	Sagrada Familia	8
38598	Santa Angelica	8
38599	Santa Cruz	8
38600	Santa Julia	8
38601	Santa Leopoldina	8
38602	Santa Luzia de Mantenopolis	8
38603	Santa Luzia do Azul	8
38604	Santa Luzia do Norte	8
38605	Santa Maria de Jetiba	8
38606	Santa Marta	8
38607	Santa Teresa	8
38608	Santa Terezinha	8
38609	Santissima Trindade	8
38610	Santo Agostinho	8
38611	Santo Antonio	8
38612	Santo Antonio do Canaa	8
38613	Santo Antonio do Muqui	8
38614	Santo Antonio do Pousalegre	8
38615	Santo Antonio do Quinze	8
38616	São Domingos do Norte	8
38617	São Francisco do Novo Brasil	8
38618	São Francisco Xavier do Guandu	8
38619	São Gabriel da Palha	8
38620	São Geraldo	8
38621	São Jacinto	8
38622	São João de Petropolis	8
38623	São João de Vicosa	8
38624	São João do Sobrado	8
38625	São Jorge da Barra Seca	8
38626	São Jorge do Oliveira	8
38627	São Jorge do Tiradentes	8
38628	São José das Torres	8
38629	São José de Mantenopolis	8
38630	São José do Calcado	8
38631	São José do Sobradinho	8
38632	São Mateus	8
38633	São Pedro	8
38634	São Pedro de Itabapoana	8
38635	São Pedro de Rates	8
38636	São Rafael	8
38637	São Roque do Cannaa	8
38638	São Tiago	8
38639	São Torquato	8
38640	Sapucaiá	8
38641	Serra	8
38642	Serra Pelada	8
38643	Sobreiro	8
38644	Sooretama	8
38645	Timbui	8
38646	Todos Os Santos	8
38647	Urania	8
38648	Vargem Alta	8
38649	Vargem Grande do Soturno	8
38650	Venda Nova do Imigrante	8
38651	Viana	8
38652	Vieira Machado	8
38653	Vila Nelita	8
38654	Vila Nova de Bananal	8
38655	Vila Pavão	8
38656	Vila Valerio	8
38657	Vila Velha	8
38658	Vila Verde	8
38659	Vinhatico	8
38660	Vinte E Cinco de Julho	8
38661	Vitória	8
38662	Abadia de Goiás	9
38663	Abadiania	9
38664	Acreuna	9
38665	Adelândia	9
38666	Água Fria de Goiás	9
38667	Água Limpa	9
38668	Águas Lindas de Goiás	9
38669	Alexania	9
38670	Aloândia	9
38671	Alto Alvorada	9
38672	Alto Horizonte	9
38673	Alto Paraiso de Goiás	9
38674	Alvorada do Norte	9
38675	Amaralina	9
38676	Americano do Brasil	9
38677	Amorinopolis	9
38678	Anapolis	9
38679	Anhanguera	9
38680	Anicuns	9
38681	Aparecida	9
38682	Aparecida de Goiásania	9
38683	Aparecida de Goiás	9
38684	Aparecida do Rio Claro	9
38685	Aparecida do Rio Doce	9
38686	Apore	9
38687	Aracu	9
38688	Aragarcas	9
38689	AraGoiásania	9
38690	Araguapaz	9
38691	Arenopolis	9
38692	Aruana	9
38693	Aurilândia	9
38694	Auriverde	9
38695	Avelinopolis	9
38696	Bacilândia	9
38697	Baliza	9
38698	Bandeirantes	9
38699	Barbosilândia	9
38700	Barro Alto	9
38701	Bela Vista de Goiás	9
38702	Bom Jardim de Goiás	9
38703	Bom Jesus de Goiás	9
38704	Bonfinopolis	9
38705	Bonopolis	9
38706	Brazabrantes	9
38707	Britania	9
38708	Buenolândia	9
38709	Buriti Alegre	9
38710	Buriti de Goiás	9
38711	Buritinopolis	9
38712	Cabeceiras	9
38713	Cachoeira Alta	9
38714	Cachoeira de Goiás	9
38715	Cachoeira Dourada	9
38716	Cacu	9
38717	Caiáponia	9
38718	Caicara	9
38719	Calcilândia	9
38720	Caldas Novas	9
38721	Caldazinha	9
38722	Calixto	9
38723	Campestre de Goiás	9
38724	Campinacu	9
38725	Campinorte	9
38726	Campo Alegre de Goiás	9
38727	Campo Limpo	9
38728	Campolândia	9
38729	Campos Belos	9
38730	Campos Verdes	9
38731	Cana Brava	9
38732	Canada	9
38733	Capelinha	9
38734	Caraiba	9
38735	Carmo do Rio Verde	9
38736	Castelândia	9
38737	Castrinopolis	9
38738	Catalão	9
38739	Caturai	9
38740	Cavalcante	9
38741	Cavalheiro	9
38742	Cebrasa	9
38743	Ceres	9
38744	Cezarina	9
38745	Chapadao do Ceu	9
38746	Choupana	9
38747	Cibele	9
38748	Cidade Ocidental	9
38749	Cirilândia	9
38750	Cocalzinho de Goiás	9
38751	Colinas do Sul	9
38752	Córrego do Ouro	9
38753	Córrego Rico	9
38754	Corumba de Goiás	9
38755	Corumbaiba	9
38756	Cristalina	9
38757	Cristianopolis	9
38758	Crixas	9
38759	Crominia	9
38760	Cruzeiro do Norte	9
38761	Cumari	9
38762	Damianopolis	9
38763	Damolândia	9
38764	Davidopolis	9
38765	Davinopolis	9
38766	Diolândia	9
38767	Diorama	9
38768	Divinopolis de Goiás	9
38769	Domiciano Ribeiro	9
38770	Doverlândia	9
38771	Edealina	9
38772	Edeia	9
38773	Estrela do Norte	9
38774	Faina	9
38775	Fazenda Nova	9
38776	Firminopolis	9
38777	Flores de Goiás	9
38778	Formosa	9
38779	Formoso	9
38780	Forte	9
38781	Gameleira de Goiás	9
38782	Geriacu	9
38783	Goiásalândia	9
38784	Goiásanapolis	9
38785	Goiásandira	9
38786	Goiásanesia	9
38787	Goiásania	9
38788	Goiásanira	9
38789	Goiás	9
38790	Goiásatuba	9
38791	Gouvelândia	9
38792	Guapo	9
38793	Guaraita	9
38794	Guarani de Goiás	9
38795	Guarinos	9
38796	Heitorai	9
38797	Hidrolândia	9
38798	Hidrolina	9
38799	Iaciara	9
38800	Inaciolândia	9
38801	Indiara	9
38802	Inhumas	9
38803	Interlândia	9
38804	Ipameri	9
38805	Ipora	9
38806	Israelândia	9
38807	Itaberai	9
38808	Itaguacu	9
38809	Itaguari	9
38810	Itaguaru	9
38811	Itaja	9
38812	Itapaci	9
38813	Itapirapua	9
38814	Itapuranga	9
38815	Itaruma	9
38816	Itaucu	9
38817	Itumbiara	9
38818	Ivolândia	9
38819	Jacilândia	9
38820	Jandaiá	9
38821	Jaragua	9
38822	Jatai	9
38823	Jaupaci	9
38824	Jeroaquara	9
38825	Jesupolis	9
38826	Joanapolis	9
38827	Joviania	9
38828	Juscelândia	9
38829	Juscelino Kubitschek	9
38830	Jussara	9
38831	Lagoa do Bauzinho	9
38832	Lagolândia	9
38833	Leopoldo de Bulhoes	9
38834	Lucilândia	9
38835	Luziania	9
38836	Mairipotaba	9
38837	Mambai	9
38838	Mara Rosa	9
38839	Marcianopolis	9
38840	Marzagão	9
38841	Matrincha	9
38842	Maurilândia	9
38843	Meia Ponte	9
38844	Messianopolis	9
38845	Mimoso de Goiás	9
38846	Minacu	9
38847	Mineiros	9
38848	Moipora	9
38849	Monte Alegre de Goiás	9
38850	Montes Claros de Goiás	9
38851	Montividiu	9
38852	Montividiu do Norte	9
38853	Morrinhos	9
38854	Morro Agudo de Goiás	9
38855	Mossamedes	9
38856	Mozarlândia	9
38857	Mundo Novo	9
38858	Mutunopolis	9
38859	Natinopolis	9
38860	Nazario	9
38861	Neropolis	9
38862	Niquelândia	9
38863	Nova America	9
38864	Nova Aurora	9
38865	Nova Crixas	9
38866	Nova Gloria	9
38867	Nova Iguacu de Goiás	9
38868	Nova Roma	9
38869	Nova Veneza	9
38870	Novo Brasil	9
38871	Novo Gama	9
38872	Novo Planalto	9
38873	Olaria do Angico	9
38874	Olhos D'agua	9
38875	Orizona	9
38876	Ouro Verde de Goiás	9
38877	Ouroana	9
38878	Ouvidor	9
38879	Padre Bernardo	9
38880	Palestina de Goiás	9
38881	Palmeiras de Goiás	9
38882	Palmelo	9
38883	Palminopolis	9
38884	Panama	9
38885	Paranaiguara	9
38886	Parauna	9
38887	Pau-terra	9
38888	Pedra Branca	9
38889	Perolândia	9
38890	Petrolina de Goiás	9
38891	Pilar de Goiás	9
38892	Piloândia	9
38893	Piracanjuba	9
38894	Piranhas	9
38895	Pirenopolis	9
38896	Pires Belo	9
38897	Pires do Rio	9
38898	Planaltina	9
38899	Pontalina	9
38900	Porangatu	9
38901	Porteirão	9
38902	Portelândia	9
38903	Posse	9
38904	Posse D'abadia	9
38905	Professor Jamil	9
38906	Quirinopolis	9
38907	Registro do Araguaiá	9
38908	Rialma	9
38909	Rianapolis	9
38910	Rio Quente	9
38911	Rio Verde	9
38912	Riverlândia	9
38913	Rodrigues Nascimento	9
38914	Rosalândia	9
38915	Rubiataba	9
38916	Sanclerlândia	9
38917	Santa Barbara de Goiás	9
38918	Santa Cruz das Lajes	9
38919	Santa Cruz de Goiás	9
38920	Santa Fé de Goiás	9
38921	Santa Helena de Goiás	9
38922	Santa Isabel	9
38923	Santa Rita do Araguaiá	9
38924	Santa Rita do Novo Destino	9
38925	Santa Rosa	9
38926	Santa Rosa de Goiás	9
38927	Santa Tereza de Goiás	9
38928	Santa Terezinha de Goiás	9
38929	Santo Antonio da Barra	9
38930	Santo Antonio de Goiás	9
38931	Santo Antonio do Descoberto	9
38932	Santo Antonio do Rio Verde	9
38933	São Domingos	9
38934	São Francisco de Goiás	9
38935	São Gabriel de Goiás	9
38936	São João	9
38937	São João D'alianca	9
38938	São João da Parauna	9
38939	São Luis de Montes Belos	9
38940	São Luiz do Norte	9
38941	São Luiz do Tocantins	9
38942	São Miguel do Araguaiá	9
38943	São Miguel do Passa Quatro	9
38944	São Patricio	9
38945	São Sebastião do Rio Claro	9
38946	São Simão	9
38947	São Vicente	9
38948	Sarandi	9
38949	Senador Canedo	9
38950	Serra Dourada	9
38951	Serranopolis	9
38952	Silvania	9
38953	Simolândia	9
38954	Sitio D'abadia	9
38955	Sousania	9
38956	Taquaral de Goiás	9
38957	Taveira	9
38958	Teresina de Goiás	9
38959	Terezopolis de Goiás	9
38960	Termas do Itaja	9
38961	Três Ranchos	9
38962	Trindade	9
38963	Trombas	9
38964	Tupiracaba	9
38965	Turvania	9
38966	Turvelândia	9
38967	Uirapuru	9
38968	Uruacu	9
38969	Uruana	9
38970	Uruita	9
38971	Urutai	9
38972	Uva	9
38973	Valdelândia	9
38974	Valparaiso de Goiás	9
38975	Varjão	9
38976	Vianopolis	9
38977	Vicentinopolis	9
38978	Vila Boa	9
38979	Vila Brasilia	9
38980	Vila Propicio	9
38981	Vila Sertaneja	9
38982	Acailândia	10
38983	Afonso Cunha	10
38984	Água Doce do Maranhão	10
38985	Alcantara	10
38986	Aldeias Altas	10
38987	Altamira do Maranhão	10
38988	Alto Alegre do Maranhão	10
38989	Alto Alegre do Pindare	10
38990	Alto Parnaiba	10
38991	Amapa do Maranhão	10
38992	Amarante do Maranhão	10
38993	Anajatuba	10
38994	Anapurus	10
38995	Anil	10
38996	Apicum-açu	10
38997	Araguana	10
38998	Araioses	10
38999	Arame	10
39000	Arari	10
39001	Aurizona	10
39002	Axixa	10
39003	Bacabal	10
39004	Bacabeira	10
39005	Bacatuba	10
39006	Bacuri	10
39007	Bacurituba	10
39008	Balsas	10
39009	Barão de Grajau	10
39010	Barão de Tromai	10
39011	Barra do Corda	10
39012	Barreirinhas	10
39013	Barro Duro	10
39014	Bela Vista do Maranhão	10
39015	Belagua	10
39016	Benedito Leite	10
39017	Bequimão	10
39018	Bernardo do Mearim	10
39019	Boa Vista do Gurupi	10
39020	Boa Vista do Pindare	10
39021	Bom Jardim	10
39022	Bom Jesus das Selvas	10
39023	Bom Lugar	10
39024	Bonfim do Arari	10
39025	Brejo	10
39026	Brejo de Areia	10
39027	Brejo de São Felix	10
39028	Buriti	10
39029	Buriti Bravo	10
39030	Buriti Cortado	10
39031	Buriticupu	10
39032	Buritirama	10
39033	Cachoeira Grande	10
39034	Cajapio	10
39035	Cajari	10
39036	Campestre do Maranhão	10
39037	Candido Mendes	10
39038	Cantanhede	10
39039	Capinzal do Norte	10
39040	Caraiba do Norte	10
39041	Carolina	10
39042	Carutapera	10
39043	Caxias	10
39044	Cedral	10
39045	Central do Maranhão	10
39046	Centro do Guilherme	10
39047	Centro Novo do Maranhão	10
39048	Chapadinha	10
39049	Cidelândia	10
39050	Codo	10
39051	Codozinho	10
39052	Coelho Neto	10
39053	Colinas	10
39054	Conceição do Lago-açu	10
39055	Coroata	10
39056	Curupa	10
39057	Cururupu	10
39058	Curva Grande	10
39059	Custodio Lima	10
39060	Davinopolis	10
39061	Dom Pedro	10
39062	Duque Bacelar	10
39063	Esperantinopolis	10
39064	Estandarte	10
39065	Estreito	10
39066	Feira Nova do Maranhão	10
39067	Fernando Falção	10
39068	Formosa da Serra Negra	10
39069	Fortaleza dos Nogueiras	10
39070	Fortuna	10
39071	Frecheiras	10
39072	Godofredo Viana	10
39073	Gonçalves Dias	10
39074	Governador Archer	10
39075	Governador Edison Lobão	10
39076	Governador Eugenio Barros	10
39077	Governador Luiz Rocha	10
39078	Governador Newton Bello	10
39079	Governador Nunes Freire	10
39080	Graca Aranha	10
39081	Grajau	10
39082	Guimaraes	10
39083	Humberto de Campos	10
39084	Ibipira	10
39085	Icatu	10
39086	Igarape do Meio	10
39087	Igarape Grande	10
39088	Imperatriz	10
39089	Itaipava do Grajau	10
39090	Itamatare	10
39091	Itapecuru Mirim	10
39092	Itapera	10
39093	Itinga do Maranhão	10
39094	Jatoba	10
39095	Jenipapo dos Vieiras	10
39096	João Lisboa	10
39097	Josélândia	10
39098	Junco do Maranhão	10
39099	Lago da Pedra	10
39100	Lago do Junco	10
39101	Lago dos Rodrigues	10
39102	Lago Verde	10
39103	Lagoa do Mato	10
39104	Lagoa Grande do Maranhão	10
39105	Lajeado Novo	10
39106	Lapela	10
39107	Leandro	10
39108	Lima Campos	10
39109	Loreto	10
39110	Luis Domingues	10
39111	Magalhaes de Almeida	10
39112	Maioba	10
39113	Maracacume	10
39114	Maraja do Sena	10
39115	Maranhaozinho	10
39116	Marianopolis	10
39117	Mata	10
39118	Mata Roma	10
39119	Matinha	10
39120	Matoes	10
39121	Matoes do Norte	10
39122	Milagres do Maranhão	10
39123	Mirador	10
39124	Miranda do Norte	10
39125	Mirinzal	10
39126	Monção	10
39127	Montes Altos	10
39128	Morros	10
39129	Nina Rodrigues	10
39130	Nova Colinas	10
39131	Nova Iorque	10
39132	Nova Olinda do Maranhão	10
39133	Olho D'agua Das Cunhas	10
39134	Olinda Nova do Maranhão	10
39135	Paco do Lumiar	10
39136	Palmeirândia	10
39137	Papagaio	10
39138	Paraibano	10
39139	Parnarama	10
39140	Passagem Franca	10
39141	Pastos Bons	10
39142	Paulino Neves	10
39143	Paulo Ramos	10
39144	Pedreiras	10
39145	Pedro do Rosario	10
39146	Penalva	10
39147	Peri Mirim	10
39148	Peritoro	10
39149	Pimentel	10
39150	Pindare Mirim	10
39151	Pinheiro	10
39152	Pio Xii	10
39153	Pirapemas	10
39154	Poção de Pedras	10
39155	Porto das Gabarras	10
39156	Porto Franco	10
39157	Porto Rico do Maranhão	10
39158	Presidente Dutra	10
39159	Presidente Juscelino	10
39160	Presidente Medici	10
39161	Presidente Sarney	10
39162	Presidente Vargas	10
39163	Primeira Cruz	10
39164	Raposa	10
39165	Resplandes	10
39166	Riachão	10
39167	Ribamar Fiquene	10
39168	Ribeirão Azul	10
39169	Rocado	10
39170	Roque	10
39171	Rosario	10
39172	Sambaiba	10
39173	Santa Filomena do Maranhão	10
39174	Santa Helena	10
39175	Santa Ines	10
39176	Santa Luzia	10
39177	Santa Luzia do Parua	10
39178	Santa Quiteria do Maranhão	10
39179	Santa Rita	10
39180	Santana do Maranhão	10
39181	Santo Amaro	10
39182	Santo Antonio dos Lopes	10
39183	São Benedito do Rio Preto	10
39184	São Bento	10
39185	São Bernardo	10
39186	São Domingos do Azeitão	10
39187	São Domingos do Maranhão	10
39188	São Felix de Balsas	10
39189	São Francisco do Brejão	10
39190	São Francisco do Maranhão	10
39191	São João Batista	10
39192	São João de Cortes	10
39193	São João do Caru	10
39194	São João do Paraiso	10
39195	São João do Soter	10
39196	São João dos Patos	10
39197	São Joaquim dos Melos	10
39198	São José de Ribamar	10
39199	São José dos Basilios	10
39200	São Luis	10
39201	São Luis Gonzaga do Maranhão	10
39202	São Mateus do Maranhão	10
39203	São Pedro da Água Branca	10
39204	São Pedro dos Crentes	10
39205	São Raimundo das Mangabeiras	10
39206	São Raimundo de Codo	10
39207	São Raimundo do Doca Bezerra	10
39208	São Roberto	10
39209	São Vicente Ferrer	10
39210	Satubinha	10
39211	Senador Alexandre Costa	10
39212	Senador La Rocque	10
39213	Serrano do Maranhão	10
39214	Sitio Novo	10
39215	Sucupira do Norte	10
39216	Sucupira do Riachão	10
39217	Tasso Fragoso	10
39218	Timbiras	10
39219	Timon	10
39220	Trizidela do Vale	10
39221	Tufilândia	10
39222	Tuntum	10
39223	Turiacu	10
39224	Turilândia	10
39225	Tutoia	10
39226	Urbano Santos	10
39227	Vargem Grande	10
39228	Viana	10
39229	Vila Nova dos Martirios	10
39230	Vitória do Mearim	10
39231	Vitorino Freire	10
39232	Ze Doca	10
39233	Abadia dos Dourados	13
39234	Abaete	13
39235	Abaete dos Mendes	13
39236	Abaiba	13
39237	Abre Campo	13
39238	Abreus	13
39239	Acaiáca	13
39240	Acucena	13
39241	Acurui	13
39242	Adao Colares	13
39243	Água Boa	13
39244	Água Branca de Minas	13
39245	Água Comprida	13
39246	Água Viva	13
39247	Águanil	13
39248	Águas de Araxa	13
39249	Águas de Contendas	13
39250	Águas Ferreas	13
39251	Águas Formosas	13
39252	Águas Vermelhas	13
39253	Aimores	13
39254	Aiuruoca	13
39255	Alagoa	13
39256	Albertina	13
39257	Alberto Isaacson	13
39258	Albertos	13
39259	Aldeia	13
39260	Alegre	13
39261	Alegria	13
39262	Alem Paraiba	13
39263	Alexandrita	13
39264	Alfenas	13
39265	Alfredo Vasconcelos	13
39266	Almeida	13
39267	Almenara	13
39268	Alpercata	13
39269	Alpinopolis	13
39270	Alterosa	13
39271	Alto Caparão	13
39272	Alto Capim	13
39273	Alto de Santa Helena	13
39274	Alto Jequitiba	13
39275	Alto Maranhão	13
39276	Alto Rio Doce	13
39277	Altolândia	13
39278	Alvação	13
39279	Alvarenga	13
39280	Alvinopolis	13
39281	Alvorada	13
39282	Alvorada de Minas	13
39283	Amanda	13
39284	Amanhece	13
39285	Amarantina	13
39286	Amparo da Serra	13
39287	Andiroba	13
39288	Andradas	13
39289	Andrelândia	13
39290	Andrequice	13
39291	Angaturama	13
39292	Angelândia	13
39293	Angicos de Minas	13
39294	Anguereta	13
39295	Angustura	13
39296	Antonio Carlos	13
39297	Antonio Dias	13
39298	Antonio dos Santos	13
39299	Antonio Ferreira	13
39300	Antonio Pereira	13
39301	Antonio Prado de Minas	13
39302	Antunes	13
39303	Aparecida de Minas	13
39304	Aracai	13
39305	Aracati de Minas	13
39306	Aracitaba	13
39307	Aracuai	13
39308	Araguari	13
39309	Aramirim	13
39310	Aranha	13
39311	Arantina	13
39312	Araponga	13
39313	Arapora	13
39314	Arapua	13
39315	Araujos	13
39316	Arauna	13
39317	Araxa	13
39318	Arcangelo	13
39319	Arceburgo	13
39320	Arcos	13
39321	Areado	13
39322	Argenita	13
39323	Argirita	13
39324	Aricanduva	13
39325	Arinos	13
39326	Aristides Batista	13
39327	Ascenção	13
39328	Assarai	13
39329	Astolfo Dutra	13
39330	Ataleia	13
39331	Augusto de Lima	13
39332	Avai do Jacinto	13
39333	Azurita	13
39334	Babilonia	13
39335	Bação	13
39336	Baependi	13
39337	Baguari	13
39338	Baioes	13
39339	Baixa	13
39340	Balbinopolis	13
39341	Baldim	13
39342	Bambui	13
39343	Bandeira	13
39344	Bandeira do Sul	13
39345	Bandeirantes	13
39346	Barão de Cocais	13
39347	Barão de Monte Alto	13
39348	Barbacena	13
39349	Barra Alegre	13
39350	Barra da Figueira	13
39351	Barra do Ariranha	13
39352	Barra do Cuiete	13
39353	Barra Feliz	13
39354	Barra Longa	13
39355	Barranco Alto	13
39356	Barreiro	13
39357	Barreiro Branco	13
39358	Barreiro da Raiz	13
39359	Barreiro do Rio Verde	13
39360	Barretos de Alvinopolis	13
39361	Barroção	13
39362	Barroso	13
39363	Bau	13
39364	Bela Vista de Minas	13
39365	Belisario	13
39366	Belmiro Braga	13
39367	Belo Horizonte	13
39368	Belo Oriente	13
39369	Belo Vale	13
39370	Bentopolis de Minas	13
39371	Berilo	13
39372	Berizal	13
39373	Bertopolis	13
39374	Betim	13
39375	Bias Fortes	13
39376	Bicas	13
39377	Bicuiba	13
39378	Biquinhas	13
39379	Bituri	13
39380	Boa Esperanca	13
39381	Boa Familia	13
39382	Boa União de Itabirinha	13
39383	Boa Vista de Minas	13
39384	Bocaina de Minas	13
39385	Bocaiuva	13
39386	Bom Despacho	13
39387	Bom Jardim de Minas	13
39388	Bom Jesus da Cachoeira	13
39389	Bom Jesus da Penha	13
39390	Bom Jesus de Cardosos	13
39391	Bom Jesus do Amparo	13
39392	Bom Jesus do Divino	13
39393	Bom Jesus do Galho	13
39394	Bom Jesus do Madeira	13
39395	Bom Pastor	13
39396	Bom Repouso	13
39397	Bom Retiro	13
39398	Bom Sucesso	13
39399	Bom Sucesso de Patos	13
39400	Bonanca	13
39401	Bonfim	13
39402	Bonfinopolis de Minas	13
39403	Bonito de Minas	13
39404	Borba Gato	13
39405	Borda da Mata	13
39406	Botelhos	13
39407	Botumirim	13
39408	Bras Pires	13
39409	Brasilândia de Minas	13
39410	Brasilia de Minas	13
39411	Brasopolis	13
39412	Braunas	13
39413	Brejauba	13
39414	Brejaubinha	13
39415	Brejo Bonito	13
39416	Brejo do Amparo	13
39417	Brumadinho	13
39418	Brumal	13
39419	Buarque de Macedo	13
39420	Bueno	13
39421	Bueno Brandão	13
39422	Buenopolis	13
39423	Bugre	13
39424	Buritis	13
39425	Buritizeiro	13
39426	Caatinga	13
39427	Cabeceira Grande	13
39428	Cabo Verde	13
39429	Caburu	13
39430	Cacaratiba	13
39431	Cacarema	13
39432	Cachoeira Alegre	13
39433	Cachoeira da Prata	13
39434	Cachoeira de Minas	13
39435	Cachoeira de Pajeu	13
39436	Cachoeira de Santa Cruz	13
39437	Cachoeira do Brumado	13
39438	Cachoeira do Campo	13
39439	Cachoeira do Manteiga	13
39440	Cachoeira do Vale	13
39441	Cachoeira dos Antunes	13
39442	Cachoeira Dourada	13
39443	Cachoeirinha	13
39444	Caetano Lopes	13
39445	Caetanopolis	13
39446	Caete	13
39447	Caiána	13
39448	Caiápo	13
39449	Cajuri	13
39450	Caldas	13
39451	Calixto	13
39452	Camacho	13
39453	Camanducaiá	13
39454	Camargos	13
39455	Cambui	13
39456	Cambuquira	13
39457	Campanario	13
39458	Campanha	13
39459	Campestre	13
39460	Campestrinho	13
39461	Campina Verde	13
39462	Campo Alegre de Minas	13
39463	Campo Azul	13
39464	Campo Belo	13
39465	Campo do Meio	13
39466	Campo Florido	13
39467	Campo Redondo	13
39468	Campolide	13
39469	Campos Altos	13
39470	Campos Gerais	13
39471	Cana Verde	13
39472	Canaa	13
39473	Canabrava	13
39474	Canapolis	13
39475	Canastrão	13
39476	Candeias	13
39477	Canoeiros	13
39478	Cantagalo	13
39479	Caparão	13
39480	Capela Nova	13
39481	Capelinha	13
39482	Capetinga	13
39483	Capim Branco	13
39484	Capinopolis	13
39485	Capitania	13
39486	Capitao Andrade	13
39487	Capitao Eneas	13
39488	Capitolio	13
39489	Caputira	13
39490	Carai	13
39491	Caranaiba	13
39492	Carandai	13
39493	Carangola	13
39494	Caratinga	13
39495	Carbonita	13
39496	Cardeal Mota	13
39497	Careacu	13
39498	Carioca	13
39499	Carlos Alves	13
39500	Carlos Chagas	13
39501	Carmesia	13
39502	Carmo da Cachoeira	13
39503	Carmo da Mata	13
39504	Carmo de Minas	13
39505	Carmo do Cajuru	13
39506	Carmo do Paranaiba	13
39507	Carmo do Rio Claro	13
39508	Carmopolis de Minas	13
39509	Carneirinho	13
39510	Carrancas	13
39511	Carvalho de Brito	13
39512	Carvalhopolis	13
39513	Carvalhos	13
39514	Casa Grande	13
39515	Cascalho Rico	13
39516	Cassia	13
39517	Cataguarino	13
39518	Cataguases	13
39519	Catajas	13
39520	Catas Altas	13
39521	Catas Altas da Noruega	13
39522	Catiara	13
39523	Catuji	13
39524	Catune	13
39525	Catuni	13
39526	Catuti	13
39527	Caxambu	13
39528	Cedro do Abaete	13
39529	Centenario	13
39530	Central de Minas	13
39531	Central de Santa Helena	13
39532	Centralina	13
39533	Cervo	13
39534	Chacara	13
39535	Chale	13
39536	Chapada de Minas	13
39537	Chapada do Norte	13
39538	Chapada Gaucha	13
39539	Chaveslândia	13
39540	Chiador	13
39541	Chonim	13
39542	Chumbo	13
39543	Cipotanea	13
39544	Cisneiros	13
39545	Citrolândia	13
39546	Claraval	13
39547	Claro de Minas	13
39548	Claro dos Poçoes	13
39549	Claudio	13
39550	Claudio Manuel	13
39551	Cocais	13
39552	Coco	13
39553	Coimbra	13
39554	Coluna	13
39555	Comendador Gomes	13
39556	Comercinho	13
39557	Conceição da Aparecida	13
39558	Conceição da Barra de Minas	13
39559	Conceição da Boa Vista	13
39560	Conceição da Brejauba	13
39561	Conceição da Ibitipoca	13
39562	Conceição das Alagoas	13
39563	Conceição das Pedras	13
39564	Conceição de Ipanema	13
39565	Conceição de Itagua	13
39566	Conceição de Minas	13
39567	Conceição de Piracicaba	13
39568	Conceição de Tronqueiras	13
39569	Conceição do Capim	13
39570	Conceição do Formoso	13
39571	Conceição do Mato Dentro	13
39572	Conceição do Para	13
39573	Conceição do Rio Acima	13
39574	Conceição do Rio Verde	13
39575	Conceição dos Ouros	13
39576	Concordia de Mucuri	13
39577	Condado do Norte	13
39578	Conego João Pio	13
39579	Conego Marinho	13
39580	Confins	13
39581	Congonhal	13
39582	Congonhas	13
39583	Congonhas do Norte	13
39584	Conquista	13
39585	Conselheiro Lafaiete	13
39586	Conselheiro Mata	13
39587	Conselheiro Pena	13
39588	Consolação	13
39589	Contagem	13
39590	Contrato	13
39591	Contria	13
39592	Coqueiral	13
39593	Coração de Jesus	13
39594	Cordisburgo	13
39595	Cordislândia	13
39596	Corinto	13
39597	Coroaci	13
39598	Coromandel	13
39599	Coronel Fabriciano	13
39600	Coronel Murta	13
39601	Coronel Pacheco	13
39602	Coronel Xavier Chaves	13
39603	Córrego Danta	13
39604	Córrego do Barro	13
39605	Córrego do Bom Jesus	13
39606	Córrego do Ouro	13
39607	Córrego Fundo	13
39608	Córrego Novo	13
39609	Corregos	13
39610	Correia de Almeida	13
39611	Correntinho	13
39612	Costa Sena	13
39613	Costas	13
39614	Costas da Mantiqueira	13
39615	Couto de Magalhaes de Minas	13
39616	Crisolia	13
39617	Crisolita	13
39618	Crispim Jaques	13
39619	Cristais	13
39620	Cristalia	13
39621	Cristiano Otoni	13
39622	Cristina	13
39623	Crucilândia	13
39624	Cruzeiro da Fortaleza	13
39625	Cruzeiro dos Peixotos	13
39626	Cruzilia	13
39627	Cubas	13
39628	Cuite Velho	13
39629	Cuparaque	13
39630	Curimatai	13
39631	Curral de Dentro	13
39632	Curvelo	13
39633	Datas	13
39634	Delfim Moreira	13
39635	Delfinopolis	13
39636	Delta	13
39637	Deputado Augusto Clementino	13
39638	Derribadinha	13
39639	Descoberto	13
39640	Desembargador Otoni	13
39641	Desemboque	13
39642	Desterro de Entre Rios	13
39643	Desterro do Melo	13
39644	Diamante de Uba	13
39645	Diamantina	13
39646	Dias	13
39647	Dias Tavares/siderurgica	13
39648	Diogo de Vasconcelos	13
39649	Dionisio	13
39650	Divinesia	13
39651	Divino	13
39652	Divino das Laranjeiras	13
39653	Divino de Virgolândia	13
39654	Divino Espirito Santo	13
39655	Divinolândia de Minas	13
39656	Divinopolis	13
39657	Divisa Alegre	13
39658	Divisa Nova	13
39659	Divisopolis	13
39660	Dois de Abril	13
39661	Dom Bosco	13
39662	Dom Cavati	13
39663	Dom Joaquim	13
39664	Dom Lara	13
39665	Dom Modesto	13
39666	Dom Silverio	13
39667	Dom Vicoso	13
39668	Dona Euzebia	13
39669	Dores da Vitória	13
39670	Dores de Campos	13
39671	Dores de Guanhaes	13
39672	Dores do Indaiá	13
39673	Dores do Paraibuna	13
39674	Dores do Turvo	13
39675	Doresopolis	13
39676	Douradinho	13
39677	Douradoquara	13
39678	Doutor Campolina	13
39679	Doutor Lund	13
39680	Durande	13
39681	Edgard Melo	13
39682	Eloi Mendes	13
39683	Emboabas	13
39684	Engenheiro Caldas	13
39685	Engenheiro Correia	13
39686	Engenheiro Navarro	13
39687	Engenheiro Schnoor	13
39688	Engenho do Ribeiro	13
39689	Engenho Novo	13
39690	Entre Folhas	13
39691	Entre Rios de Minas	13
39692	Epaminondas Otoni	13
39693	Ermidinha	13
39694	Ervalia	13
39695	Esmeraldas	13
39696	Esmeraldas de Ferros	13
39697	Espera Feliz	13
39698	Espinosa	13
39699	Espirito Santo do Dourado	13
39700	Esteios	13
39701	Estevao de Araujo	13
39702	Estiva	13
39703	Estrela da Barra	13
39704	Estrela Dalva	13
39705	Estrela de Jordania	13
39706	Estrela do Indaiá	13
39707	Estrela do Sul	13
39708	Eugenopolis	13
39709	Euxenita	13
39710	Ewbank da Camara	13
39711	Expedicionario Alicio	13
39712	Extração	13
39713	Extrema	13
39714	Fama	13
39715	Faria Lemos	13
39716	Farias	13
39717	Fechados	13
39718	Felicina	13
39719	Felicio dos Santos	13
39720	Felisburgo	13
39721	Felixlândia	13
39722	Fernandes Tourinho	13
39723	Fernao Dias	13
39724	Ferreiras	13
39725	Ferreiropolis	13
39726	Ferros	13
39727	Ferruginha	13
39728	Fervedouro	13
39729	Fidalgo	13
39730	Fidelândia	13
39731	Flor de Minas	13
39732	Floralia	13
39733	Floresta	13
39734	Florestal	13
39735	Florestina	13
39736	Fonseca	13
39737	Formiga	13
39738	Formoso	13
39739	Fortaleza de Minas	13
39740	Fortuna de Minas	13
39741	Francisco Badaro	13
39742	Francisco Dumont	13
39743	Francisco Sa	13
39744	Franciscopolis	13
39745	Frei Eustaquio	13
39746	Frei Gaspar	13
39747	Frei Inocencio	13
39748	Frei Lagonegro	13
39749	Frei Orlando	13
39750	Frei Serafim	13
39751	Freire Cardoso	13
39752	Fronteira	13
39753	Fronteira dos Vales	13
39754	Fruta de Leite	13
39755	Frutal	13
39756	Funchal	13
39757	Funilândia	13
39758	Furnas	13
39759	Furquim	13
39760	Galego	13
39761	Galena	13
39762	Galileia	13
39763	Gama	13
39764	Gameleiras	13
39765	Garapuava	13
39766	Gavião	13
39767	Glaucilândia	13
39768	Glaura	13
39769	Glucinio	13
39770	Goiásabeira	13
39771	Goiásana	13
39772	Goiásanases	13
39773	Gonçalves	13
39774	Gonzaga	13
39775	Gororos	13
39776	Gorutuba	13
39777	Gouvea	13
39778	Governador Valadares	13
39779	Graminea	13
39780	Granada	13
39781	Grão Mogol	13
39782	Grota	13
39783	Grupiara	13
39784	Guacui	13
39785	Guaipava	13
39786	Guanhaes	13
39787	Guape	13
39788	Guaraciaba	13
39789	Guaraciama	13
39790	Guaranesia	13
39791	Guarani	13
39792	Guaranilândia	13
39793	Guarara	13
39794	Guarataiá	13
39795	Guarda dos Ferreiros	13
39796	Guarda-mor	13
39797	Guardinha	13
39798	Guaxima	13
39799	Guaxupe	13
39800	Guidoval	13
39801	Guimarania	13
39802	Guinda	13
39803	Guiricema	13
39804	Gurinhata	13
39805	Heliodora	13
39806	Hematita	13
39807	Hermilo Alves	13
39808	Honoropolis	13
39809	Iapu	13
39810	Ibertioga	13
39811	Ibia	13
39812	Ibiai	13
39813	Ibiracatu	13
39814	Ibiraci	13
39815	Ibirite	13
39816	Ibitira	13
39817	Ibitiura de Minas	13
39818	Ibituruna	13
39819	Icarai de Minas	13
39820	Igarape	13
39821	Igaratinga	13
39822	Iguatama	13
39823	Ijaci	13
39824	Ilheus do Prata	13
39825	Ilicinea	13
39826	Imbe de Minas	13
39827	Inconfidentes	13
39828	Indaiábira	13
39829	Independencia	13
39830	Indianopolis	13
39831	Ingai	13
39832	Inhai	13
39833	Inhapim	13
39834	Inhauma	13
39835	Inimutaba	13
39836	Ipaba	13
39837	Ipanema	13
39838	Ipatinga	13
39839	Ipiacu	13
39840	Ipoema	13
39841	Ipuiuna	13
39842	Irai de Minas	13
39843	Itabira	13
39844	Itabirinha de Mantena	13
39845	Itabirito	13
39846	Itaboca	13
39847	Itacambira	13
39848	Itacarambi	13
39849	Itaci	13
39850	Itacolomi	13
39851	Itaguara	13
39852	Itaim	13
39853	Itaipe	13
39854	Itajuba	13
39855	Itajutiba	13
39856	Itamarandiba	13
39857	Itamarati	13
39858	Itamarati de Minas	13
39859	Itambacuri	13
39860	Itambe do Mato Dentro	13
39861	Itamirim	13
39862	Itamogi	13
39863	Itamonte	13
39864	Itamuri	13
39865	Itanhandu	13
39866	Itanhomi	13
39867	Itaobim	13
39868	Itapagipe	13
39869	Itapanhoacanga	13
39870	Itapecerica	13
39871	Itapeva	13
39872	Itapiru	13
39873	Itapirucu	13
39874	Itatiaiucu	13
39875	Itau de Minas	13
39876	Itauna	13
39877	Itauninha	13
39878	Itaverava	13
39879	Iterere	13
39880	Itinga	13
39881	Itira	13
39882	Itueta	13
39883	Itui	13
39884	Ituiutaba	13
39885	Itumirim	13
39886	Iturama	13
39887	Itutinga	13
39888	Jaboticatubas	13
39889	Jacarandira	13
39890	Jacare	13
39891	Jacinto	13
39892	Jacui	13
39893	Jacutinga	13
39894	Jaguaracu	13
39895	Jaguarão	13
39896	Jaguaritira	13
39897	Jaiba	13
39898	Jampruca	13
39899	Janauba	13
39900	Januaria	13
39901	Japaraiba	13
39902	Japonvar	13
39903	Jardinesia	13
39904	Jeceaba	13
39905	Jenipapo de Minas	13
39906	Jequeri	13
39907	Jequitai	13
39908	Jequitiba	13
39909	Jequitinhonha	13
39910	Jesuania	13
39911	Joaima	13
39912	Joanesia	13
39913	João Monlevade	13
39914	João Pinheiro	13
39915	Joaquim Felicio	13
39916	Jordania	13
39917	José Gonçalves de Minas	13
39918	José Raydan	13
39919	Josélândia	13
39920	Josénopolis	13
39921	Juatuba	13
39922	Jubai	13
39923	Juiracu	13
39924	Juiz de Fora	13
39925	Junco de Minas	13
39926	Juramento	13
39927	Jureia	13
39928	Juruaiá	13
39929	Jurumirim	13
39930	Justinopolis	13
39931	Juvenilia	13
39932	Lacerdinha	13
39933	Ladainha	13
39934	Lagamar	13
39935	Lagoa Bonita	13
39936	Lagoa da Prata	13
39937	Lagoa dos Patos	13
39938	Lagoa Dourada	13
39939	Lagoa Formosa	13
39940	Lagoa Grande	13
39941	Lagoa Santa	13
39942	Lajinha	13
39943	Lambari	13
39944	Lamim	13
39945	Lamounier	13
39946	Lapinha	13
39947	Laranjal	13
39948	Laranjeiras de Caldas	13
39949	Lassance	13
39950	Lavras	13
39951	Leandro Ferreira	13
39952	Leme do Prado	13
39953	Leopoldina	13
39954	Levinopolis	13
39955	Liberdade	13
39956	Lima Duarte	13
39957	Limeira D'oeste	13
39958	Limeira de Mantena	13
39959	Lobo Leite	13
39960	Lontra	13
39961	Lourenço Velho	13
39962	Lufa	13
39963	Luisburgo	13
39964	Luislândia	13
39965	Luiz Pires de Minas	13
39966	Luizlândia do Oeste	13
39967	Luminarias	13
39968	Luminosa	13
39969	Luz	13
39970	Macaiá	13
39971	Machacalis	13
39972	Machado	13
39973	Macuco	13
39974	Macuco de Minas	13
39975	Madre de Deus de Minas	13
39976	Mae dos Homens	13
39977	Major Ezequiel	13
39978	Major Porto	13
39979	Malacacheta	13
39980	Mamonas	13
39981	Manga	13
39982	Manhuacu	13
39983	Manhumirim	13
39984	Mantena	13
39985	Mantiqueira	13
39986	Mantiqueira do Palmital	13
39987	Mar de Espanha	13
39988	Marambainha	13
39989	Maravilhas	13
39990	Maria da Fé	13
39991	Mariana	13
39992	Marilac	13
39993	Marilândia	13
39994	Mario Campos	13
39995	Maripa de Minas	13
39996	Marlieria	13
39997	Marmelopolis	13
39998	Martinesia	13
39999	Martinho Campos	13
40000	Martins Guimaraes	13
40001	Martins Soares	13
40002	Mata Verde	13
40003	Materlândia	13
40004	Mateus Leme	13
40005	Mathias Lobato	13
40006	Matias Barbosa	13
40007	Matias Cardoso	13
40008	Matipo	13
40009	Mato Verde	13
40010	Matozinhos	13
40011	Matutina	13
40012	Medeiros	13
40013	Medina	13
40014	Melo Viana	13
40015	Mendanha	13
40016	Mendes Pimentel	13
40017	Mendonca	13
40018	Merces	13
40019	Merces de Água Limpa	13
40020	Mesquita	13
40021	Mestre Caetano	13
40022	Miguel Burnier	13
40023	Milagre	13
40024	Milho Verde	13
40025	Minas Novas	13
40026	Minduri	13
40027	Mirabela	13
40028	Miradouro	13
40029	Miragaiá	13
40030	Mirai	13
40031	Miralta	13
40032	Mirantão	13
40033	Miraporanga	13
40034	Miravania	13
40035	Missionario	13
40036	Mocambeiro	13
40037	Mocambinho	13
40038	Moeda	13
40039	Moema	13
40040	Monjolinho de Minas	13
40041	Monjolos	13
40042	Monsenhor Horta	13
40043	Monsenhor Isidro	13
40044	Monsenhor João Alexandre	13
40045	Monsenhor Paulo	13
40046	Montalvania	13
40047	Monte Alegre de Minas	13
40048	Monte Azul	13
40049	Monte Belo	13
40050	Monte Carmelo	13
40051	Monte Celeste	13
40052	Monte Formoso	13
40053	Monte Rei	13
40054	Monte Santo de Minas	13
40055	Monte Sião	13
40056	Monte Verde	13
40057	Montes Claros	13
40058	Montezuma	13
40059	Morada Nova de Minas	13
40060	Morro	13
40061	Morro da Garca	13
40062	Morro do Ferro	13
40063	Morro do Pilar	13
40064	Morro Vermelho	13
40065	Mucuri	13
40066	Mundo Novo de Minas	13
40067	Munhoz	13
40068	Muquem	13
40069	Muriae	13
40070	Mutum	13
40071	Muzambinho	13
40072	Nacip Raydan	13
40073	Nanuque	13
40074	Naque	13
40075	Naque-nanuque	13
40076	Natalândia	13
40077	Natercia	13
40078	Nazaré de Minas	13
40079	Nazareno	13
40080	Nelson de Sena	13
40081	Neolândia	13
40082	Nepomuceno	13
40083	Nhandutiba	13
40084	Nicolândia	13
40085	Ninheira	13
40086	Nova Belem	13
40087	Nova Era	13
40088	Nova Esperanca	13
40089	Nova Lima	13
40090	Nova Minda	13
40091	Nova Modica	13
40092	Nova Ponte	13
40093	Nova Porteirinha	13
40094	Nova Resende	13
40095	Nova Serrana	13
40096	Nova União	13
40097	Novilhona	13
40098	Novo Cruzeiro	13
40099	Novo Horizonte	13
40100	Novo Oriente de Minas	13
40101	Novorizonte	13
40102	Ocidente	13
40103	Olaria	13
40104	Olegario Maciel	13
40105	Olhos D'agua Do Oeste	13
40106	Olhos-d'agua	13
40107	Olimpio Campos	13
40108	Olimpio Noronha	13
40109	Oliveira	13
40110	Oliveira Fortes	13
40111	Onca de Pitangui	13
40112	Oratorios	13
40113	Orizania	13
40114	Ouro Branco	13
40115	Ouro Fino	13
40116	Ouro Preto	13
40117	Ouro Verde de Minas	13
40118	Paciencia	13
40119	Padre Brito	13
40120	Padre Carvalho	13
40121	Padre Felisberto	13
40122	Padre Fialho	13
40123	Padre João Afonso	13
40124	Padre Julio Maria	13
40125	Padre Paraiso	13
40126	Padre Pinto	13
40127	Padre Viegas	13
40128	Pai Pedro	13
40129	Paineiras	13
40130	Pains	13
40131	Paiolinho	13
40132	Paiva	13
40133	Palma	13
40134	Palmeiral	13
40135	Palmeirinha	13
40136	Palmital dos Carvalhos	13
40137	Palmopolis	13
40138	Pantano	13
40139	Papagaios	13
40140	Para de Minas	13
40141	Paracatu	13
40142	Paraguacu	13
40143	Paraguai	13
40144	Paraiso Garcia	13
40145	Paraisopolis	13
40146	Parãopeba	13
40147	Paredao de Minas	13
40148	Parque Durval de Barros	13
40149	Parque Industrial	13
40150	Passa Dez	13
40151	Passa Quatro	13
40152	Passa Tempo	13
40153	Passa Vinte	13
40154	Passabem	13
40155	Passagem de Mariana	13
40156	Passos	13
40157	Patis	13
40158	Patos de Minas	13
40159	Patrimonio	13
40160	Patrocinio	13
40161	Patrocinio do Muriae	13
40162	Paula Candido	13
40163	Paula Lima	13
40164	Paulistas	13
40165	Pavão	13
40166	Pe do Morro	13
40167	Pecanha	13
40168	Pedra Azul	13
40169	Pedra Bonita	13
40170	Pedra Corrida	13
40171	Pedra do Anta	13
40172	Pedra do Indaiá	13
40173	Pedra do Sino	13
40174	Pedra Dourada	13
40175	Pedra Grande	13
40176	Pedra Menina	13
40177	Pedralva	13
40178	Pedras de Maria da Cruz	13
40179	Pedrinopolis	13
40180	Pedro Leopoldo	13
40181	Pedro Lessa	13
40182	Pedro Teixeira	13
40183	Pedro Versiani	13
40184	Penedia	13
40185	Penha de Franca	13
40186	Penha do Capim	13
40187	Penha do Cassiano	13
40188	Penha do Norte	13
40189	Penha Longa	13
40190	Pequeri	13
40191	Pequi	13
40192	Perdigão	13
40193	Perdilândia	13
40194	Perdizes	13
40195	Perdoes	13
40196	Pereirinhas	13
40197	Periquito	13
40198	Perpetuo Socorro	13
40199	Pescador	13
40200	Petunia	13
40201	Piacatuba	13
40202	Pião	13
40203	Piauí	13
40204	Pic Sagarana	13
40205	Piedade de Caratinga	13
40206	Piedade de Ponte Nova	13
40207	Piedade do Parãopeba	13
40208	Piedade do Rio Grande	13
40209	Piedade dos Gerais	13
40210	Pilar	13
40211	Pimenta	13
40212	Pindaibas	13
40213	Pingo-d'agua	13
40214	Pinheirinhos	13
40215	Pinheiros Altos	13
40216	Pinhotiba	13
40217	Pintopolis	13
40218	Pintos Negreiros	13
40219	Piracaiba	13
40220	Piracema	13
40221	Pirajuba	13
40222	Piranga	13
40223	Pirangucu	13
40224	Piranguinho	13
40225	Piranguita	13
40226	Pirapanema	13
40227	Pirapetinga	13
40228	Pirapora	13
40229	Pirauba	13
40230	Pires E Albuquerque	13
40231	Piscamba	13
40232	Pitangui	13
40233	Pitarana	13
40234	Piumhi	13
40235	Planalto de Minas	13
40236	Planura	13
40237	Plautino Soares	13
40238	Poaiá	13
40239	Poço Fundo	13
40240	Poçoes de Paineiras	13
40241	Poços de Caldas	13
40242	Pocrane	13
40243	Pompeu	13
40244	Poncianos	13
40245	Pontalete	13
40246	Ponte Alta	13
40247	Ponte Alta de Minas	13
40248	Ponte dos Ciganos	13
40249	Ponte Firme	13
40250	Ponte Nova	13
40251	Ponte Segura	13
40252	Pontevila	13
40253	Ponto Chique	13
40254	Ponto Chique do Martelo	13
40255	Ponto do Marambaiá	13
40256	Ponto dos Volantes	13
40257	Porteirinha	13
40258	Porto Agrario	13
40259	Porto das Flores	13
40260	Porto dos Mendes	13
40261	Porto Firme	13
40262	Pote	13
40263	Pouso Alegre	13
40264	Pouso Alto	13
40265	Prados	13
40266	Prata	13
40267	Pratapolis	13
40268	Pratinha	13
40269	Presidente Bernardes	13
40270	Presidente Juscelino	13
40271	Presidente Kubitschek	13
40272	Presidente Olegario	13
40273	Presidente Pena	13
40274	Professor Sperber	13
40275	Providencia	13
40276	Prudente de Morais	13
40277	Quartel de São João	13
40278	Quartel do Sacramento	13
40279	Quartel Geral	13
40280	Quatituba	13
40281	Queixada	13
40282	Queluzita	13
40283	Quem-quem	13
40284	Quilombo	13
40285	Quintinos	13
40286	Raposos	13
40287	Raul Soares	13
40288	Ravena	13
40289	Realeza	13
40290	Recreio	13
40291	Reduto	13
40292	Resende Costa	13
40293	Resplendor	13
40294	Ressaquinha	13
40295	Riachinho	13
40296	Riacho da Cruz	13
40297	Riacho dos Machados	13
40298	Ribeirão das Neves	13
40299	Ribeirão de São Domingos	13
40300	Ribeirão Vermelho	13
40301	Ribeiro Junqueira	13
40302	Ribeiros	13
40303	Rio Acima	13
40304	Rio Casca	13
40305	Rio das Mortes	13
40306	Rio do Prado	13
40307	Rio Doce	13
40308	Rio Espera	13
40309	Rio Manso	13
40310	Rio Melo	13
40311	Rio Novo	13
40312	Rio Paranaiba	13
40313	Rio Pardo de Minas	13
40314	Rio Piracicaba	13
40315	Rio Pomba	13
40316	Rio Pretinho	13
40317	Rio Preto	13
40318	Rio Vermelho	13
40319	Ritapolis	13
40320	Roca Grande	13
40321	Rocas Novas	13
40322	Rochedo de Minas	13
40323	Rodeador	13
40324	Rodeiro	13
40325	Rodrigo Silva	13
40326	Romaria	13
40327	Rosario da Limeira	13
40328	Rosario de Minas	13
40329	Rosario do Pontal	13
40330	Roseiral	13
40331	Rubelita	13
40332	Rubim	13
40333	Sabara	13
40334	Sabinopolis	13
40335	Sacramento	13
40336	Salinas	13
40337	Salitre de Minas	13
40338	Salto da Divisa	13
40339	Sanatorio Santa Fé	13
40340	Santa Barbara	13
40341	Santa Barbara do Leste	13
40342	Santa Barbara do Monte Verde	13
40343	Santa Barbara do Tugurio	13
40344	Santa Cruz da Aparecida	13
40345	Santa Cruz de Botumirim	13
40346	Santa Cruz de Minas	13
40347	Santa Cruz de Salinas	13
40348	Santa Cruz do Escalvado	13
40349	Santa Cruz do Prata	13
40350	Santa da Pedra	13
40351	Santa Efigenia	13
40352	Santa Efigenia de Minas	13
40353	Santa Fé de Minas	13
40354	Santa Filomena	13
40355	Santa Helena de Minas	13
40356	Santa Isabel	13
40357	Santa Juliana	13
40358	Santa Luzia	13
40359	Santa Luzia da Serra	13
40360	Santa Margarida	13
40361	Santa Maria de Itabira	13
40362	Santa Maria do Salto	13
40363	Santa Maria do Suacui	13
40364	Santa Rita da Estrela	13
40365	Santa Rita de Caldas	13
40366	Santa Rita de Jacutinga	13
40367	Santa Rita de Minas	13
40368	Santa Rita de Ouro Preto	13
40369	Santa Rita do Cedro	13
40370	Santa Rita do Ibitipoca	13
40371	Santa Rita do Itueto	13
40372	Santa Rita do Rio do Peixe	13
40373	Santa Rita do Sapucai	13
40374	Santa Rita Durão	13
40375	Santa Rosa da Serra	13
40376	Santa Rosa de Lima	13
40377	Santa Rosa dos Dourados	13
40378	Santa Teresa do Bonito	13
40379	Santa Terezinha de Minas	13
40380	Santa Vitória	13
40381	Santana da Vargem	13
40382	Santana de Caldas	13
40383	Santana de Cataguases	13
40384	Santana de Patos	13
40385	Santana de Pirapama	13
40386	Santana do Alfie	13
40387	Santana do Aracuai	13
40388	Santana do Campestre	13
40389	Santana do Capivari	13
40390	Santana do Deserto	13
40391	Santana do Garambeu	13
40392	Santana do Jacare	13
40393	Santana do Manhuacu	13
40394	Santana do Paraiso	13
40395	Santana do Parãopeba	13
40396	Santana do Riacho	13
40397	Santana do Tabuleiro	13
40398	Santana dos Montes	13
40399	Santo Antonio da Boa Vista	13
40400	Santo Antonio da Fortaleza	13
40401	Santo Antonio da Vargem Alegre	13
40402	Santo Antonio do Amparo	13
40403	Santo Antonio do Aventureiro	13
40404	Santo Antonio do Boqueirão	13
40405	Santo Antonio do Cruzeiro	13
40406	Santo Antonio do Gloria	13
40407	Santo Antonio do Grama	13
40408	Santo Antonio do Itambe	13
40409	Santo Antonio do Jacinto	13
40410	Santo Antonio do Leite	13
40411	Santo Antonio do Manhuacu	13
40412	Santo Antonio do Monte	13
40413	Santo Antonio do Mucuri	13
40414	Santo Antonio do Norte	13
40415	Santo Antonio do Pirapetinga	13
40416	Santo Antonio do Retiro	13
40417	Santo Antonio do Rio Abaixo	13
40418	Santo Antonio dos Araujos	13
40419	Santo Antonio dos Campos	13
40420	Santo Hilario	13
40421	Santo Hipolito	13
40422	Santos Dumont	13
40423	São Bartolomeu	13
40424	São Benedito	13
40425	São Bento Abade	13
40426	São Bento de Caldas	13
40427	São Bras do Suacui	13
40428	São Braz	13
40429	São Candido	13
40430	São Domingos	13
40431	São Domingos da Bocaina	13
40432	São Domingos das Dores	13
40433	São Domingos do Prata	13
40434	São Felix de Minas	13
40435	São Francisco	13
40436	São Francisco de Paula	13
40437	São Francisco de Sales	13
40438	São Francisco do Gloria	13
40439	São Francisco do Humaita	13
40440	São Geraldo	13
40441	São Geraldo da Piedade	13
40442	São Geraldo de Tumiritinga	13
40443	São Geraldo do Baguari	13
40444	São Geraldo do Baixio	13
40445	São Goncalo de Botelhos	13
40446	São Goncalo do Abaete	13
40447	São Goncalo do Monte	13
40448	São Goncalo do Para	13
40449	São Goncalo do Rio Abaixo	13
40450	São Goncalo do Rio das Pedras	13
40451	São Goncalo do Rio Preto	13
40452	São Goncalo do Sapucai	13
40453	São Gotardo	13
40454	São Jeronimo dos Poçoes	13
40455	São João Batista do Gloria	13
40456	São João da Chapada	13
40457	São João da Lagoa	13
40458	São João da Mata	13
40459	São João da Ponte	13
40460	São João da Sapucaiá	13
40461	São João da Serra	13
40462	São João da Serra Negra	13
40463	São João da Vereda	13
40464	São João das Missoes	13
40465	São João Del Rei	13
40466	São João do Bonito	13
40467	São João do Jacutinga	13
40468	São João do Manhuacu	13
40469	São João do Manteninha	13
40470	São João do Oriente	13
40471	São João do Pacui	13
40472	São João do Paraiso	13
40473	São João Evangelista	13
40474	São João Nepomuceno	13
40475	São Joaquim	13
40476	São Joaquim de Bicas	13
40477	São José da Barra	13
40478	São José da Bela Vista	13
40479	São José da Lapa	13
40480	São José da Pedra Menina	13
40481	São José da Safira	13
40482	São José da Varginha	13
40483	São José das Tronqueiras	13
40484	São José do Acacio	13
40485	São José do Alegre	13
40486	São José do Barreiro	13
40487	São José do Batatal	13
40488	São José do Buriti	13
40489	São José do Divino	13
40490	São José do Goiásabal	13
40491	São José do Itueto	13
40492	São José do Jacuri	13
40493	São José do Mantimento	13
40494	São José do Mato Dentro	13
40495	São José do Pantano	13
40496	São José do Parãopeba	13
40497	São José dos Lopes	13
40498	São José dos Salgados	13
40499	São Lourenço	13
40500	São Manoel do Guaiácu	13
40501	São Mateus de Minas	13
40502	São Miguel do Anta	13
40503	São Pedro da Garca	13
40504	São Pedro da União	13
40505	São Pedro das Tabocas	13
40506	São Pedro de Caldas	13
40507	São Pedro do Avai	13
40508	São Pedro do Gloria	13
40509	São Pedro do Jequitinhonha	13
40510	São Pedro do Suacui	13
40511	São Pedro dos Ferros	13
40512	São Roberto	13
40513	São Romão	13
40514	São Roque de Minas	13
40515	São Sebastião da Barra	13
40516	São Sebastião da Bela Vista	13
40517	São Sebastião da Vala	13
40518	São Sebastião da Vargem Alegre	13
40519	São Sebastião da Vitória	13
40520	São Sebastião de Braunas	13
40521	São Sebastião do Anta	13
40522	São Sebastião do Baixio	13
40523	São Sebastião do Barreado	13
40524	São Sebastião do Barreiro	13
40525	São Sebastião do Bonsucesso	13
40526	São Sebastião do Bugre	13
40527	São Sebastião do Gil	13
40528	São Sebastião do Maranhão	13
40529	São Sebastião do Oculo	13
40530	São Sebastião do Oeste	13
40531	São Sebastião do Paraiso	13
40532	São Sebastião do Pontal	13
40533	São Sebastião do Rio Preto	13
40534	São Sebastião do Rio Verde	13
40535	São Sebastião do Sacramento	13
40536	São Sebastião do Soberbo	13
40537	São Sebastião dos Poçoes	13
40538	São Sebastião dos Robertos	13
40539	São Sebastião dos Torres	13
40540	São Tiago	13
40541	São Tomas de Aquino	13
40542	São Tome das Letras	13
40543	São Vicente	13
40544	São Vicente da Estrela	13
40545	São Vicente de Minas	13
40546	São Vicente do Grama	13
40547	São Vicente do Rio Doce	13
40548	São Vitor	13
40549	Sapucai	13
40550	Sapucai-mirim	13
40551	Sapucaiá	13
40552	Sapucaiá de Guanhaes	13
40553	Sapucaiá do Norte	13
40554	Sarandira	13
40555	Sardoa	13
40556	Sarzedo	13
40557	Saudade	13
40558	Sem Peixe	13
40559	Senador Amaral	13
40560	Senador Cortes	13
40561	Senador Firmino	13
40562	Senador José Bento	13
40563	Senador Melo Viana	13
40564	Senador Modestino Gonçalves	13
40565	Senador Mourão	13
40566	Senhora da Gloria	13
40567	Senhora da Penha	13
40568	Senhora das Dores	13
40569	Senhora de Oliveira	13
40570	Senhora do Carmo	13
40571	Senhora do Porto	13
40572	Senhora dos Remedios	13
40573	Sereno	13
40574	Sericita	13
40575	Seritinga	13
40576	Serra Azul	13
40577	Serra Azul de Minas	13
40578	Serra Bonita	13
40579	Serra da Canastra	13
40580	Serra da Saudade	13
40581	Serra das Araras	13
40582	Serra do Camapua	13
40583	Serra do Salitre	13
40584	Serra dos Aimores	13
40585	Serra dos Lemes	13
40586	Serra Nova	13
40587	Serrania	13
40588	Serranopolis de Minas	13
40589	Serranos	13
40590	Serro	13
40591	Sertaozinho	13
40592	Sete Cachoeiras	13
40593	Sete Lagoas	13
40594	Setubinha	13
40595	Silva Campos	13
40596	Silva Xavier	13
40597	Silvano	13
40598	Silveira Carvalho	13
40599	Silveirania	13
40600	Silvestre	13
40601	Silvianopolis	13
40602	Simao Campos	13
40603	Simao Pereira	13
40604	Simonesia	13
40605	Sobral Pinto	13
40606	Sobralia	13
40607	Soledade de Minas	13
40608	Sopa	13
40609	Tabajara	13
40610	Tabauna	13
40611	Tabuão	13
40612	Tabuleiro	13
40613	Taiobeiras	13
40614	Taparuba	13
40615	Tapira	13
40616	Tapirai	13
40617	Tapuirama	13
40618	Taquaracu de Minas	13
40619	Taruacu	13
40620	Tarumirim	13
40621	Tebas	13
40622	Teixeiras	13
40623	Tejuco	13
40624	Teofilo Otoni	13
40625	Terra Branca	13
40626	Timoteo	13
40627	Tiradentes	13
40628	Tiros	13
40629	Tobati	13
40630	Tocandira	13
40631	Tocantins	13
40632	Tocos do Moji	13
40633	Toledo	13
40634	Tomas Gonzaga	13
40635	Tombos	13
40636	Topazio	13
40637	Torneiros	13
40638	Torreoes	13
40639	Três Coracoes	13
40640	Três Ilhas	13
40641	Três Marias	13
40642	Três Pontas	13
40643	Trimonte	13
40644	Tuiutinga	13
40645	Tumiritinga	13
40646	Tupaciguara	13
40647	Tuparece	13
40648	Turmalina	13
40649	Turvolândia	13
40650	Uba	13
40651	Ubai	13
40652	Ubaporanga	13
40653	Ubari	13
40654	Uberaba	13
40655	Uberlândia	13
40656	Umburatiba	13
40657	Umbuzeiro	13
40658	Unai	13
40659	União de Minas	13
40660	Uruana de Minas	13
40661	Urucania	13
40662	Urucuia	13
40663	Usina Monte Alegre	13
40664	Vai-volta	13
40665	Valadares	13
40666	Valão	13
40667	Vale Verde de Minas	13
40668	Valo Fundo	13
40669	Vargem Alegre	13
40670	Vargem Bonita	13
40671	Vargem Grande do Rio Pardo	13
40672	Vargem Linda	13
40673	Varginha	13
40674	Varjao de Minas	13
40675	Varzea da Palma	13
40676	Varzelândia	13
40677	Vau-açu	13
40678	Vazante	13
40679	Vera Cruz de Minas	13
40680	Verdelândia	13
40681	Vereda do Paraiso	13
40682	Veredas	13
40683	Veredinha	13
40684	Verissimo	13
40685	Vermelho	13
40686	Vermelho Novo	13
40687	Vermelho Velho	13
40688	Vespasiano	13
40689	Vicosa	13
40690	Vieiras	13
40691	Vila Bom Jesus	13
40692	Vila Costina	13
40693	Vila Nova de Minas	13
40694	Vila Nova dos Poçoes	13
40695	Vila Pereira	13
40696	Vilas Boas	13
40697	Virgem da Lapa	13
40698	Virginia	13
40699	Virginopolis	13
40700	Virgolândia	13
40701	Visconde do Rio Branco	13
40702	Vista Alegre	13
40703	Vitorinos	13
40704	Volta Grande	13
40705	Wenceslau Braz	13
40706	Zelândia	13
40707	Zito Soares	13
40708	Água Boa	12
40709	Água Clara	12
40710	Albuquerque	12
40711	Alcinopolis	12
40712	Alto Sucuriu	12
40713	Amambai	12
40714	Amandina	12
40715	Amolar	12
40716	Anastacio	12
40717	Anaurilândia	12
40718	Angelica	12
40719	Anhandui	12
40720	Antonio João	12
40721	Aparecida do Taboado	12
40722	Aquidauana	12
40723	Aral Moreira	12
40724	Arapua	12
40725	Areado	12
40726	Arvore Grande	12
40727	Baiánopolis	12
40728	Balsamo	12
40729	Bandeirantes	12
40730	Bataguassu	12
40731	Bataipora	12
40732	Baus	12
40733	Bela Vista	12
40734	Bocaja	12
40735	Bodoquena	12
40736	Bom Fim	12
40737	Bonito	12
40738	Boqueirão	12
40739	Brasilândia	12
40740	Caarapo	12
40741	Cabeceira do Apa	12
40742	Cachoeira	12
40743	Camapua	12
40744	Camisão	12
40745	Campestre	12
40746	Campo Grande	12
40747	Capão Seco	12
40748	Caracol	12
40749	Carumbe	12
40750	Cassilândia	12
40751	Chapadao do Sul	12
40752	Cipolândia	12
40753	Coimbra	12
40754	Congonha	12
40755	Corguinho	12
40756	Coronel Sapucaiá	12
40757	Corumba	12
40758	Costa Rica	12
40759	Coxim	12
40760	Cristalina	12
40761	Cruzaltina	12
40762	Culturama	12
40763	Cupins	12
40764	Debrasa	12
40765	Deodapolis	12
40766	Dois Irmaos do Buriti	12
40767	Douradina	12
40768	Dourados	12
40769	Eldorado	12
40770	Fatima do Sul	12
40771	Figueirão	12
40772	Garcias	12
40773	Gloria de Dourados	12
40774	Guacu	12
40775	Guaculândia	12
40776	Guadalupe do Alto Parana	12
40777	Guia Lopes da Laguna	12
40778	Iguatemi	12
40779	Ilha Comprida	12
40780	Ilha Grande	12
40781	Indaiá do Sul	12
40782	Indaiá Grande	12
40783	Indapolis	12
40784	Inocencia	12
40785	Ipezal	12
40786	Itahum	12
40787	Itapora	12
40788	Itaquirai	12
40789	Ivinhema	12
40790	Jabuti	12
40791	Jacarei	12
40792	Japora	12
40793	Jaraguari	12
40794	Jardim	12
40795	Jatei	12
40796	Jauru	12
40797	Juscelândia	12
40798	Juti	12
40799	Ladario	12
40800	Lagoa Bonita	12
40801	Laguna Carapa	12
40802	Maracaju	12
40803	Miranda	12
40804	Montese	12
40805	Morangas	12
40806	Morraria do Sul	12
40807	Morumbi	12
40808	Mundo Novo	12
40809	Navirai	12
40810	Nhecolândia	12
40811	Nioaque	12
40812	Nossa Senhora de Fatima	12
40813	Nova Alvorada do Sul	12
40814	Nova America	12
40815	Nova Andradina	12
40816	Nova Esperanca	12
40817	Nova Jales	12
40818	Novo Horizonte do Sul	12
40819	Oriente	12
40820	Paiáguas	12
40821	Palmeiras	12
40822	Panambi	12
40823	Paraiso	12
40824	Paranaiba	12
40825	Paranhos	12
40826	Pedro Gomes	12
40827	Picadinha	12
40828	Pirapora	12
40829	Piraputanga	12
40830	Ponta Pora	12
40831	Ponte Vermelha	12
40832	Pontinha do Cocho	12
40833	Porto Esperanca	12
40834	Porto Murtinho	12
40835	Porto Vilma	12
40836	Porto Xv de Novembro	12
40837	Presidente Castelo	12
40838	Prudencio Thomaz	12
40839	Quebra Coco	12
40840	Quebracho	12
40841	Ribas do Rio Pardo	12
40842	Rio Brilhante	12
40843	Rio Negro	12
40844	Rio Verde de Mato Grosso	12
40845	Rochedinho	12
40846	Rochedo	12
40847	Sanga Puita	12
40848	Santa Rita do Pardo	12
40849	Santa Terezinha	12
40850	São Gabriel do Oeste	12
40851	São João do Apore	12
40852	São José	12
40853	São José do Sucuriu	12
40854	São Pedro	12
40855	São Romão	12
40856	Selviria	12
40857	Sete Quedas	12
40858	Sidrolândia	12
40859	Sonora	12
40860	Tacuru	12
40861	Tamandare	12
40862	Taquari	12
40863	Taquarussu	12
40864	Taunay	12
40865	Terenos	12
40866	Três Lagoas	12
40867	Velhacaria	12
40868	Vicentina	12
40869	Vila Formosa	12
40870	Vila Marques	12
40871	Vila Rica	12
40872	Vila União	12
40873	Vila Vargas	12
40874	Vista Alegre	12
40875	Acorizal	11
40876	Água Boa	11
40877	Água Fria	11
40878	Águacu	11
40879	Águapei	11
40880	Águas Claras	11
40881	Ainhumas	11
40882	Alcantilado	11
40883	Alta Floresta	11
40884	Alto Araguaiá	11
40885	Alto Boa Vista	11
40886	Alto Coite	11
40887	Alto Garcas	11
40888	Alto Juruena	11
40889	Alto Paraguai	11
40890	Alto Paraiso	11
40891	Alto Taquari	11
40892	Analândia do Norte	11
40893	Apiacas	11
40894	Araguaiána	11
40895	Araguainha	11
40896	Araputanga	11
40897	Arenapolis	11
40898	Aripuana	11
40899	Arruda	11
40900	Assari	11
40901	Barão de Melgaco	11
40902	Barra do Bugres	11
40903	Barra do Garcas	11
40904	Batovi	11
40905	Baus	11
40906	Bauxi	11
40907	Bel Rios	11
40908	Bezerro Branco	11
40909	Boa Esperanca	11
40910	Boa União	11
40911	Boa Vista	11
40912	Bom Sucesso	11
40913	Brasnorte	11
40914	Buriti	11
40915	Caceres	11
40916	Caite	11
40917	Campinapolis	11
40918	Campo Novo do Parecis	11
40919	Campo Verde	11
40920	Campos de Julio	11
40921	Campos Novos	11
40922	Canabrava do Norte	11
40923	Canarana	11
40924	Cangas	11
40925	Capão Grande	11
40926	Capão Verde	11
40927	Caramujo	11
40928	Caravagio	11
40929	Carlinda	11
40930	Cassununga	11
40931	Castanheira	11
40932	Catuai	11
40933	Chapada dos Guimaraes	11
40934	Cidade Morena	11
40935	Claudia	11
40936	Cocalinho	11
40937	Colider	11
40938	Colorado do Norte	11
40939	Comodoro	11
40940	Confresa	11
40941	Coronel Ponce	11
40942	Cotrel	11
40943	Cotriguacu	11
40944	Coxipo açu	11
40945	Coxipo da Ponte	11
40946	Coxipo do Ouro	11
40947	Cristinopolis	11
40948	Cristo Rei	11
40949	Cuiaba	11
40950	Curvelândia	11
40951	Del Rios	11
40952	Denise	11
40953	Diamantino	11
40954	Dom Aquino	11
40955	Engenho	11
40956	Engenho Velho	11
40957	Entre Rios	11
40958	Estrela do Leste	11
40959	Faval	11
40960	Fazenda de Cima	11
40961	Feliz Natal	11
40962	Figueiropolis D Oeste	11
40963	Filadelfia	11
40964	Flor da Serra	11
40965	Fontanilhas	11
40966	Gaucha do Norte	11
40967	General Carneiro	11
40968	Gloria D'oeste	11
40969	Guaranta do Norte	11
40970	Guarita	11
40971	Guiratinga	11
40972	Horizonte do Oeste	11
40973	Indianapolis	11
40974	Indiavai	11
40975	Irenopolis	11
40976	Itamarati Norte	11
40977	Itauba	11
40978	Itiquira	11
40979	Jaciara	11
40980	Jangada	11
40981	Jarudore	11
40982	Jatoba	11
40983	Jauru	11
40984	Josélândia	11
40985	Juara	11
40986	Juina	11
40987	Juruena	11
40988	Juscimeira	11
40989	Lambari D'oeste	11
40990	Lavouras	11
40991	Lucas do Rio Verde	11
40992	Lucialva	11
40993	Luciara	11
40994	Machado	11
40995	Marcelândia	11
40996	Marzagão	11
40997	Mata Dentro	11
40998	Matupa	11
40999	Mimoso	11
41000	Mirassol D'oeste	11
41001	Nobres	11
41002	Nonoai do Norte	11
41003	Nortelândia	11
41004	Nossa Senhora da Guia	11
41005	Nossa Senhora do Livramento	11
41006	Nova Alvorada	11
41007	Nova Bandeirantes	11
41008	Nova Brasilândia	11
41009	Nova Brasilia	11
41010	Nova Canaa do Norte	11
41011	Nova Catanduva	11
41012	Nova Galileia	11
41013	Nova Guarita	11
41014	Nova Lacerda	11
41015	Nova Marilândia	11
41016	Nova Maringa	11
41017	Nova Monte Verde	11
41018	Nova Mutum	11
41019	Nova Olimpia	11
41020	Nova Ubirata	11
41021	Nova Xavantina	11
41022	Novo Diamantino	11
41023	Novo Eldorado	11
41024	Novo Horizonte do Norte	11
41025	Novo Mundo	11
41026	Novo Parana	11
41027	Novo São Joaquim	11
41028	Padronal	11
41029	Pai Andre	11
41030	Paraiso do Leste	11
41031	Paranaita	11
41032	Paranatinga	11
41033	Passagem da Conceição	11
41034	Pedra Preta	11
41035	Peixoto de Azevedo	11
41036	Pirizal	11
41037	Placa de Santo Antonio	11
41038	Planalto da Serra	11
41039	Poçone	11
41040	Pombas	11
41041	Pontal do Araguaiá	11
41042	Ponte Branca	11
41043	Ponte de Pedra	11
41044	Pontes E Lacerda	11
41045	Pontinopolis	11
41046	Porto Alegre do Norte	11
41047	Porto dos Gauchos	11
41048	Porto Esperidião	11
41049	Porto Estrela	11
41050	Poxoreo	11
41051	Praiá Rica	11
41052	Primavera	11
41053	Primavera do Leste	11
41054	Progresso	11
41055	Querencia	11
41056	Rancharia	11
41057	Reserva do Cabacal	11
41058	Ribeirão Cascalheira	11
41059	Ribeirão dos Cocais	11
41060	Ribeirãozinho	11
41061	Rio Branco	11
41062	Rio Manso	11
41063	Riolândia	11
41064	Rondonopolis	11
41065	Rosario Oeste	11
41066	Salto do Ceu	11
41067	Sangradouro	11
41068	Santa Carmem	11
41069	Santa Elvira	11
41070	Santa Fé	11
41071	Santa Rita	11
41072	Santa Terezinha	11
41073	Santaninha	11
41074	Santo Afonso	11
41075	Santo Antonio do Leverger	11
41076	Santo Antonio do Rio Bonito	11
41077	São Cristovão	11
41078	São Domingos	11
41079	São Felix do Araguaiá	11
41080	São Joaquim	11
41081	São Jorge	11
41082	São José	11
41083	São José do Apui	11
41084	São José do Planalto	11
41085	São José do Povo	11
41086	São José do Rio Claro	11
41087	São José do Xingu	11
41088	São José dos Quatro Marcos	11
41089	São Lourenço de Fatima	11
41090	São Pedro da Cipa	11
41091	São Vicente	11
41092	Sapezal	11
41093	Selma	11
41094	Serra Nova	11
41095	Sinop	11
41096	Sonho Azul	11
41097	Sorriso	11
41098	Sumidouro	11
41099	Tabapora	11
41100	Tangara da Serra	11
41101	Tapirapua	11
41102	Tapurah	11
41103	Terra Nova do Norte	11
41104	Terra Roxa	11
41105	Tesouro	11
41106	Toricueyje	11
41107	Torixoreu	11
41108	Três Pontes	11
41109	União do Sul	11
41110	Vale dos Sonhos	11
41111	Vale Rico	11
41112	Varginha	11
41113	Varzea Grande	11
41114	Vera	11
41115	Vila Atlantica	11
41116	Vila Bela da Santissima Trindade	11
41117	Vila Bueno	11
41118	Vila Mutum	11
41119	Vila Operaria	11
41120	Vila Paulista	11
41121	Vila Progresso	11
41122	Vila Rica	11
41123	Vila Santo Antonio	11
41124	Abaetetuba	14
41125	Abel Figueiredo	14
41126	Acara	14
41127	Afua	14
41128	Agropolis Bela Vista	14
41129	Água Azul do Norte	14
41130	Água Fria	14
41131	Alenquer	14
41132	Algodoal	14
41133	Almeirim	14
41134	Almoco	14
41135	Alta Para	14
41136	Altamira	14
41137	Alter do Chão	14
41138	Alvorada	14
41139	Americano	14
41140	Anajas	14
41141	Ananindeua	14
41142	Anapu	14
41143	Antonio Lemos	14
41144	Apeu	14
41145	Apinages	14
41146	Arapixuna	14
41147	Araquaim	14
41148	Arco-iris	14
41149	Areias	14
41150	Arumanduba	14
41151	Aruri	14
41152	Aturiai	14
41153	Augusto Correa	14
41154	Aurora do Para	14
41155	Aveiro	14
41156	Bagre	14
41157	Baião	14
41158	Bannach	14
41159	Barcarena	14
41160	Barreira Branca	14
41161	Barreira dos Campos	14
41162	Barreiras	14
41163	Baturite	14
41164	Beja	14
41165	Bela Vista do Caracol	14
41166	Belem	14
41167	Belterra	14
41168	Benevides	14
41169	Benfica	14
41170	Boa Esperanca	14
41171	Boa Fé	14
41172	Boa Sorte	14
41173	Boa Vista do Iririteua	14
41174	Boim	14
41175	Bom Jardim	14
41176	Bom Jesus do Tocantins	14
41177	Bonito	14
41178	Braganca	14
41179	Brasil Novo	14
41180	Brasilia Legal	14
41181	Brejo do Meio	14
41182	Brejo Grande do Araguaiá	14
41183	Breu Branco	14
41184	Breves	14
41185	Bujaru	14
41186	Cachoeira de Piria	14
41187	Cachoeira do Arari	14
41188	Cafezal	14
41189	Cairari	14
41190	Caju	14
41191	Camara do Marajo	14
41192	Cambuquira	14
41193	Cameta	14
41194	Camiranga	14
41195	Canaa dos Carajas	14
41196	Capanema	14
41197	Capitao Poço	14
41198	Caracara do Arari	14
41199	Carajas	14
41200	Carapajo	14
41201	Caraparu	14
41202	Caratateua	14
41203	Caripi	14
41204	Carrazedo	14
41205	Castanhal	14
41206	Castelo dos Sonhos	14
41207	Chaves	14
41208	Colares	14
41209	Conceição	14
41210	Conceição do Araguaiá	14
41211	Concordia do Para	14
41212	Condeixa	14
41213	Coqueiro	14
41214	Cripurizão	14
41215	Cripurizinho	14
41216	Cuiu-cuiu	14
41217	Cumaru do Norte	14
41218	Curionopolis	14
41219	Curralinho	14
41220	Curua	14
41221	Curuai	14
41222	Curuca	14
41223	Curucambaba	14
41224	Curumu	14
41225	Dom Eliseu	14
41226	Eldorado dos Carajas	14
41227	Emborai	14
41228	Espirito Santo do Taua	14
41229	Faro	14
41230	Fernandes Belo	14
41231	Flexal	14
41232	Floresta	14
41233	Floresta do Araguaiá	14
41234	Garrafao do Norte	14
41235	Goiásanesia do Para	14
41236	Gradaus	14
41237	Guajara-açu	14
41238	Guajara-miri	14
41239	Gurupa	14
41240	Gurupizinho	14
41241	Hidreletrica Tucurui	14
41242	Iatai	14
41243	Icoraci	14
41244	Igarape da Lama	14
41245	Igarape-açu	14
41246	Igarape-miri	14
41247	Inanu	14
41248	Inhangapi	14
41249	Ipixuna do Para	14
41250	Irituia	14
41251	Itaituba	14
41252	Itapixuna	14
41253	Itatupa	14
41254	Itupiranga	14
41255	Jacareacanga	14
41256	Jacunda	14
41257	Jaguarari	14
41258	Jamanxinzinho	14
41259	Jambuacu	14
41260	Jândiai	14
41261	Japerica	14
41262	Joana Coeli	14
41263	Joana Peres	14
41264	Joanes	14
41265	Juaba	14
41266	Jubim	14
41267	Juruti	14
41268	Km Null	14
41269	Lauro Sodre	14
41270	Ligação do Para	14
41271	Limoeiro do Ajuru	14
41272	Mae do Rio	14
41273	Magalhaes Barata	14
41274	Maiáuata	14
41275	Manjeiro	14
41276	Maraba	14
41277	Maracana	14
41278	Marajoara	14
41279	Marapanim	14
41280	Marituba	14
41281	Maruda	14
41282	Mata Geral	14
41283	Matapiquara	14
41284	Medicilândia	14
41285	Melgaco	14
41286	Menino Deus do Anapu	14
41287	Meruu	14
41288	Mirasselvas	14
41289	Miritituba	14
41290	Mocajuba	14
41291	Moiraba	14
41292	Moju	14
41293	Monsaras	14
41294	Monte Alegre	14
41295	Monte Alegre do Mau	14
41296	Monte Dourado	14
41297	Morada Nova	14
41298	Mosqueiro	14
41299	Muana	14
41300	Mujui dos Campos	14
41301	Muraja	14
41302	Murucupi	14
41303	Murumuru	14
41304	Muta	14
41305	Mutucal	14
41306	Nazaré de Mocajuba	14
41307	Nazaré dos Patos	14
41308	Nova Esperanca do Piria	14
41309	Nova Ipixuna	14
41310	Nova Mocajuba	14
41311	Nova Timboteua	14
41312	Novo Planalto	14
41313	Novo Progresso	14
41314	Novo Repartimento	14
41315	Nucleo Urbano Quilometro Null	14
41316	Obidos	14
41317	Oeiras do Para	14
41318	Oriximina	14
41319	Osvaldilândia	14
41320	Otelo	14
41321	Ourem	14
41322	Ourilândia do Norte	14
41323	Outeiro	14
41324	Pacaja	14
41325	Pacoval	14
41326	Palestina do Para	14
41327	Paragominas	14
41328	Paratins	14
41329	Parauapebas	14
41330	Pau D'arco	14
41331	Pedreira	14
41332	Peixe-boi	14
41333	Penhalonga	14
41334	Perseveranca	14
41335	Pesqueiro	14
41336	Piabas	14
41337	Picarra	14
41338	Pinhal	14
41339	Piraquara	14
41340	Piria	14
41341	Placas	14
41342	Ponta de Pedras	14
41343	Ponta de Ramos	14
41344	Portel	14
41345	Porto de Moz	14
41346	Porto Salvo	14
41347	Porto Trombetas	14
41348	Prainha	14
41349	Primavera	14
41350	Quatipuru	14
41351	Quatro Bocas	14
41352	Redenção	14
41353	Remansão	14
41354	Repartimento	14
41355	Rio Maria	14
41356	Rio Vermelho	14
41357	Riozinho	14
41358	Rondon do Para	14
41359	Ruropolis	14
41360	Salinopolis	14
41361	Salvaterra	14
41362	Santa Barbara do Para	14
41363	Santa Cruz do Arari	14
41364	Santa Isabel do Para	14
41365	Santa Luzia do Para	14
41366	Santa Maria	14
41367	Santa Maria das Barreiras	14
41368	Santa Maria do Para	14
41369	Santa Rosa da Vigia	14
41370	Santa Terezinha	14
41371	Santana do Araguaiá	14
41372	Santarem	14
41373	Santarem Novo	14
41374	Santo Antonio	14
41375	Santo Antonio do Taua	14
41376	São Caetano de Odivelas	14
41377	São Domingos do Araguaiá	14
41378	São Domingos do Capim	14
41379	São Felix do Xingu	14
41380	São Francisco	14
41381	São Francisco da Jararaca	14
41382	São Francisco do Para	14
41383	São Geraldo do Araguaiá	14
41384	São João da Ponta	14
41385	São João de Pirabas	14
41386	São João do Acangata	14
41387	São João do Araguaiá	14
41388	São João do Piria	14
41389	São João dos Ramos	14
41390	São Joaquim do Tapara	14
41391	São Jorge	14
41392	São José do Gurupi	14
41393	São José do Piria	14
41394	São Luiz do Tapajos	14
41395	São Miguel do Guama	14
41396	São Miguel dos Macacos	14
41397	São Pedro de Viseu	14
41398	São Pedro do Capim	14
41399	São Raimundo de Borralhos	14
41400	São Raimundo do Araguaiá	14
41401	São Raimundo dos Furtados	14
41402	São Roberto	14
41403	São Sebastião da Boa Vista	14
41404	São Sebastião de Vicosa	14
41405	Sapucaiá	14
41406	Senador José Porfirio	14
41407	Serra Pelada	14
41408	Soure	14
41409	Tailândia	14
41410	Tatuteua	14
41411	Tauari	14
41412	Tauarizinho	14
41413	Tentugal	14
41414	Terra Alta	14
41415	Terra Santa	14
41416	Tijoca	14
41417	Timboteua	14
41418	Tome-açu	14
41419	Tracuateua	14
41420	Trairão	14
41421	Tucuma	14
41422	Tucurui	14
41423	Ulianopolis	14
41424	Uruara	14
41425	Urucuri	14
41426	Urucuriteua	14
41427	Val-de-caes	14
41428	Veiros	14
41429	Vigia	14
41430	Vila do Carmo do Tocantins	14
41431	Vila dos Cabanos	14
41432	Vila Franca	14
41433	Vila Goreth	14
41434	Vila Isol	14
41435	Vila Nova	14
41436	Vila Planalto	14
41437	Vila Santa Fé	14
41438	Vila Socorro	14
41439	Vilarinho do Monte	14
41440	Viseu	14
41441	Vista Alegre	14
41442	Vista Alegre do Para	14
41443	Vitória do Xingu	14
41444	Xinguara	14
41445	Xinguarinha	14
41446	Água Branca	15
41447	Aguiar	15
41448	Alagoa Grande	15
41449	Alagoa Nova	15
41450	AlaGoiásnha	15
41451	Alcantil	15
41452	Algodao de Jandaira	15
41453	Alhandra	15
41454	Amparo	15
41455	Aparecida	15
41456	Aracagi	15
41457	Arara	15
41458	Araruna	15
41459	Areia	15
41460	Areia de Baraunas	15
41461	Areial	15
41462	Areias	15
41463	Aroeiras	15
41464	Assis Chateaubriand	15
41465	Assunção	15
41466	Baiá da Traição	15
41467	Balancos	15
41468	Bananeiras	15
41469	Barauna	15
41470	Barra de Santa Rosa	15
41471	Barra de Santana	15
41472	Barra de São Miguel	15
41473	Barra do Camaratuba	15
41474	Bayeux	15
41475	Belem	15
41476	Belem do Brejo do Cruz	15
41477	Bernardino Batista	15
41478	Boa Ventura	15
41479	Boa Vista	15
41480	Bom Jesus	15
41481	Bom Sucesso	15
41482	Bonito de Santa Fé	15
41483	Boqueirão	15
41484	Borborema	15
41485	Brejo do Cruz	15
41486	Brejo dos Santos	15
41487	Caapora	15
41488	Cabaceiras	15
41489	Cabedelo	15
41490	Cachoeira	15
41491	Cachoeira dos Indios	15
41492	Cachoeirinha	15
41493	Cacimba de Areia	15
41494	Cacimba de Dentro	15
41495	Cacimbas	15
41496	Caicara	15
41497	Cajazeiras	15
41498	Cajazeirinhas	15
41499	Caldas Brandão	15
41500	Camalau	15
41501	Campina Grande	15
41502	Campo Alegre	15
41503	Campo Grande	15
41504	Camurupim	15
41505	Capim	15
41506	Caraubas	15
41507	Cardoso	15
41508	Carrapateira	15
41509	Casinha do Homem	15
41510	Casserengue	15
41511	Catingueira	15
41512	Catole	15
41513	Catole do Rocha	15
41514	Caturite	15
41515	Cepilho	15
41516	Conceição	15
41517	Condado	15
41518	Conde	15
41519	Congo	15
41520	Coremas	15
41521	Coronel Maiá	15
41522	Coxixola	15
41523	Cruz do Espirito Santo	15
41524	Cubati	15
41525	Cuite	15
41526	Cuite de Mamanguape	15
41527	Cuitegi	15
41528	Cupissura	15
41529	Curral de Cima	15
41530	Curral Velho	15
41531	Damião	15
41532	Desterro	15
41533	Diamante	15
41534	Dona Ines	15
41535	Duas Estradas	15
41536	Emas	15
41537	Engenheiro Avidos	15
41538	Esperanca	15
41539	Fagundes	15
41540	Fatima	15
41541	Fazenda Nova	15
41542	Forte Velho	15
41543	Frei Martinho	15
41544	Gado Bravo	15
41545	Galante	15
41546	Guarabira	15
41547	Guarita	15
41548	Gurinhem	15
41549	Gurjão	15
41550	Ibiara	15
41551	Igaracy	15
41552	Imaculada	15
41553	Inga	15
41554	Itabaiána	15
41555	Itajubatiba	15
41556	Itaporanga	15
41557	Itapororoca	15
41558	Itatuba	15
41559	Jacarau	15
41560	Jerico	15
41561	João Pessoa	15
41562	Juarez Tavora	15
41563	Juazeirinho	15
41564	Junco do Serido	15
41565	Juripiranga	15
41566	Juru	15
41567	Lagoa	15
41568	Lagoa de Dentro	15
41569	Lagoa Seca	15
41570	Lastro	15
41571	Lerolândia	15
41572	Livramento	15
41573	Logradouro	15
41574	Lucena	15
41575	Mae D'agua	15
41576	Maiá	15
41577	Malta	15
41578	Mamanguape	15
41579	Manaira	15
41580	Marcação	15
41581	Mari	15
41582	Marizopolis	15
41583	Massaranduba	15
41584	Mata Limpa	15
41585	Mata Virgem	15
41586	Mataraca	15
41587	Matinhas	15
41588	Mato Grosso	15
41589	Matureia	15
41590	Melo	15
41591	Mogeiro	15
41592	Montadas	15
41593	Monte Horebe	15
41594	Monteiro	15
41595	Montevideo	15
41596	Mulungu	15
41597	Muquem	15
41598	Natuba	15
41599	Nazaré	15
41600	Nazarezinho	15
41601	Nossa Senhora do Livramento	15
41602	Nova Floresta	15
41603	Nova Olinda	15
41604	Nova Palmeira	15
41605	Nucleo N Null	15
41606	Odilândia	15
41607	Olho D'agua	15
41608	Olivedos	15
41609	Ouro Velho	15
41610	Parari	15
41611	Passagem	15
41612	Patos	15
41613	Paulista	15
41614	Pedra Branca	15
41615	Pedra Lavrada	15
41616	Pedras de Fogo	15
41617	Pedro Regis	15
41618	Pelo Sinal	15
41619	Pereiros	15
41620	Pianco	15
41621	Picui	15
41622	Pilar	15
41623	Piloes	15
41624	Piloezinhos	15
41625	Pindurão	15
41626	Pio X	15
41627	Piraua	15
41628	Pirpirituba	15
41629	Pitanga de Estrada	15
41630	Pitimbu	15
41631	Pocinhos	15
41632	Poço Dantas	15
41633	Poço de José de Moura	15
41634	Pombal	15
41635	Porteirinha de Pedra	15
41636	Prata	15
41637	Princesa Isabel	15
41638	Puxinana	15
41639	Queimadas	15
41640	Quixaba	15
41641	Remigio	15
41642	Riachão	15
41643	Riachao do Poço	15
41644	Riacho de Santo Antonio	15
41645	Riacho dos Cavalos	15
41646	Ribeira	15
41647	Rio Tinto	15
41648	Rua Nova	15
41649	Salema	15
41650	Salgadinho	15
41651	Salgado de São Felix	15
41652	Santa Cecilia de Umbuzeiro	15
41653	Santa Cruz	15
41654	Santa Gertrudes	15
41655	Santa Helena	15
41656	Santa Ines	15
41657	Santa Luzia	15
41658	Santa Luzia do Cariri	15
41659	Santa Maria	15
41660	Santa Rita	15
41661	Santa Teresinha	15
41662	Santa Terezinha	15
41663	Santana de Mangueira	15
41664	Santana dos Garrotes	15
41665	Santarem	15
41666	Santo Andre	15
41667	São Bento	15
41668	São Bento de Pombal	15
41669	São Domingos de Pombal	15
41670	São Domingos do Cariri	15
41671	São Francisco	15
41672	São Goncalo	15
41673	São João Bosco	15
41674	São João do Cariri	15
41675	São João do Rio do Peixe	15
41676	São João do Tigre	15
41677	São José da Lagoa Tapada	15
41678	São José da Mata	15
41679	São José de Caiána	15
41680	São José de Espinharas	15
41681	São José de Marimbas	15
41682	São José de Piranhas	15
41683	São José de Princesa	15
41684	São José do Bonfim	15
41685	São José do Brejo do Cruz	15
41686	São José do Sabugi	15
41687	São José dos Cordeiros	15
41688	São José dos Ramos	15
41689	São Mamede	15
41690	São Miguel de Taipu	15
41691	São Pedro	15
41692	São Sebastião de Lagoa de Roca	15
41693	São Sebastião do Umbuzeiro	15
41694	Sape	15
41695	Serido	15
41696	Serido/sao Vicente do Serido	15
41697	Serra Branca	15
41698	Serra da Raiz	15
41699	Serra Grande	15
41700	Serra Redonda	15
41701	Serraria	15
41702	Sertaozinho	15
41703	Sobrado	15
41704	Solanea	15
41705	Soledade	15
41706	Sossego	15
41707	Sousa	15
41708	Sucuru	15
41709	Sume	15
41710	Tacima	15
41711	Tambau	15
41712	Tambauzinho	15
41713	Taperoa	15
41714	Tavares	15
41715	Teixeira	15
41716	Tenorio	15
41717	Triunfo	15
41718	Uirauna	15
41719	Umari	15
41720	Umbuzeiro	15
41721	Varzea	15
41722	Varzea Comprida	15
41723	Varzea Nova	15
41724	Vazante	15
41725	Vieiropolis	15
41726	Vista Serrana	15
41727	Zabele	15
41728	Abreu E Lima	17
41729	Afogados da Ingazeira	17
41730	Afranio	17
41731	Agrestina	17
41732	Água Fria	17
41733	Água Preta	17
41734	Águas Belas	17
41735	Airi	17
41736	AlaGoiásnha	17
41737	Albuquerque Ne	17
41738	Algodoes	17
41739	Alianca	17
41740	Altinho	17
41741	Amaraji	17
41742	Ameixas	17
41743	Angelim	17
41744	Apoti	17
41745	Aracoiaba	17
41746	Araripina	17
41747	Arcoverde	17
41748	Aripibu	17
41749	Arizona	17
41750	Barra de Farias	17
41751	Barra de Guabiraba	17
41752	Barra de São Pedro	17
41753	Barra do Brejo	17
41754	Barra do Chata	17
41755	Barra do Jardim	17
41756	Barra do Riachão	17
41757	Barra do Sirinhaem	17
41758	Barreiros	17
41759	Batateira	17
41760	Belem de Maria	17
41761	Belem de São Francisco	17
41762	Belo Jardim	17
41763	Bengalas	17
41764	Bentivi	17
41765	Bernardo Vieira	17
41766	Betania	17
41767	Bezerros	17
41768	Bizarra	17
41769	Boas Novas	17
41770	Bodoco	17
41771	Bom Conselho	17
41772	Bom Jardim	17
41773	Bom Nome	17
41774	Bonfim	17
41775	Bonito	17
41776	Brejão	17
41777	Brejinho	17
41778	Brejo da Madre de Deus	17
41779	Buenos Aires	17
41780	Buique	17
41781	Cabanas	17
41782	Cabo de Santo Agostinho	17
41783	Cabrobo	17
41784	Cachoeira do Roberto	17
41785	Cachoeirinha	17
41786	Caetes	17
41787	Caicarinha da Penha	17
41788	Calcado	17
41789	Caldeiroes	17
41790	Calumbi	17
41791	Camaragibe	17
41792	Camela	17
41793	Camocim de São Felix	17
41794	Camutanga	17
41795	Canaa	17
41796	Canhotinho	17
41797	Capoeiras	17
41798	Caraiba	17
41799	Caraibeiras	17
41800	Carapotos	17
41801	Carice	17
41802	Carima	17
41803	Caririmirim	17
41804	Carnaiba	17
41805	Carnaubeira da Penha	17
41806	Carneiro	17
41807	Carpina	17
41808	Carqueja	17
41809	Caruaru	17
41810	Casinhas	17
41811	Catende	17
41812	Catimbau	17
41813	Catole	17
41814	Cavaleiro	17
41815	Cedro	17
41816	Cha de Alegria	17
41817	Cha do Rocha	17
41818	Cha Grande	17
41819	Cimbres	17
41820	Clarana	17
41821	Cocau	17
41822	Conceição das Crioulas	17
41823	Condado	17
41824	Correntes	17
41825	Cortes	17
41826	Couro D'antas	17
41827	Cristalia	17
41828	Cruanji	17
41829	Cruzes	17
41830	Cuiambuca	17
41831	Cumaru	17
41832	Cupira	17
41833	Curral Queimado	17
41834	Custodia	17
41835	Dois Leoes	17
41836	Dormentes	17
41837	Entroncamento	17
41838	Escada	17
41839	Espirito Santo	17
41840	Exu	17
41841	Fazenda Nova	17
41842	Feira Nova	17
41843	Feitoria	17
41844	Fernando de Noronha	17
41845	Ferreiros	17
41846	Flores	17
41847	Floresta	17
41848	Frei Miguelinho	17
41849	Frexeiras	17
41850	Gameleira	17
41851	Garanhuns	17
41852	Gloria do Goiás	17
41853	Goiasana	17
41854	Gonçalves Ferreira	17
41855	Granito	17
41856	Gravata	17
41857	Gravata do Ibiapina	17
41858	Grotão	17
41859	Guanumbi	17
41860	Henrique Dias	17
41861	Iateca	17
41862	Iati	17
41863	Ibimirim	17
41864	Ibirajuba	17
41865	Ibiranga	17
41866	Ibiratinga	17
41867	Ibitiranga	17
41868	Ibo	17
41869	Icaicara	17
41870	Igapo	17
41871	Igarapeassu	17
41872	Igarapeba	17
41873	Igarassu	17
41874	Iguaraci	17
41875	Inaja	17
41876	Ingazeira	17
41877	Ipojuca	17
41878	Ipubi	17
41879	Ipuera	17
41880	Iraguacu	17
41881	Irajai	17
41882	Iratama	17
41883	Itacuruba	17
41884	Itaiba	17
41885	Itamaraca	17
41886	Itambe	17
41887	Itapetim	17
41888	Itapissuma	17
41889	Itaquitinga	17
41890	Ituguacu	17
41891	Iuitepora	17
41892	Jabitaca	17
41893	Jaboatão	17
41894	Jaboatao dos Guararapes	17
41895	Japecanga	17
41896	Jaqueira	17
41897	Jatauba	17
41898	Jatiuca	17
41899	Jatoba	17
41900	Jenipapo	17
41901	João Alfredo	17
41902	Joaquim Nabuco	17
41903	José da Costa	17
41904	José Mariano	17
41905	Jucaral	17
41906	Jucati	17
41907	Jupi	17
41908	Jurema	17
41909	Jutai	17
41910	Lagoa	17
41911	Lagoa de São José	17
41912	Lagoa do Barro	17
41913	Lagoa do Carro	17
41914	Lagoa do Itaenga	17
41915	Lagoa do Ouro	17
41916	Lagoa do Souza	17
41917	Lagoa dos Gatos	17
41918	Lagoa Grande	17
41919	Laje de São José	17
41920	Laje Grande	17
41921	Lajedo	17
41922	Lajedo do Cedro	17
41923	Limoeiro	17
41924	Livramento do Tiuma	17
41925	Luanda	17
41926	Macaparana	17
41927	Machados	17
41928	Macuje	17
41929	Manari	17
41930	Mandacaiá	17
41931	Mandacaru	17
41932	Manicoba	17
41933	Maraiál	17
41934	Maravilha	17
41935	Mimoso	17
41936	Miracica	17
41937	Mirandiba	17
41938	Morais	17
41939	Moreilândia	17
41940	Moreno	17
41941	Moxoto	17
41942	Mulungu	17
41943	Murupe	17
41944	Mutuca	17
41945	Nascente	17
41946	Navarro	17
41947	Nazaré da Mata	17
41948	Negras	17
41949	Nossa Senhora da Luz	17
41950	Nossa Senhora do Carmo	17
41951	Nossa Senhora do O	17
41952	Nova Cruz	17
41953	Olho D'agua De Dentro	17
41954	Olinda	17
41955	Oratorio	17
41956	Ori	17
41957	Orobo	17
41958	Oroco	17
41959	Ouricuri	17
41960	Pajeu	17
41961	Palmares	17
41962	Palmeirina	17
41963	Panelas	17
41964	Pao de Acucar	17
41965	Pao de Acucar do Poção	17
41966	Papagaio	17
41967	Paquevira	17
41968	Para	17
41969	Paranatama	17
41970	Paratibe	17
41971	Parnamirim	17
41972	Passagem do To	17
41973	Passira	17
41974	Pau Ferro	17
41975	Paudalho	17
41976	Paulista	17
41977	Pedra	17
41978	Perpetuo Socorro	17
41979	Pesqueira	17
41980	Petrolândia	17
41981	Petrolina	17
41982	Pirituba	17
41983	Poção	17
41984	Poção de Afranio	17
41985	Poço Comprido	17
41986	Poço Fundo	17
41987	Pombos	17
41988	Pontas de Pedra	17
41989	Ponte dos Carvalhos	17
41990	Praiá da Conceição	17
41991	Primavera	17
41992	Quipapa	17
41993	Quitimbu	17
41994	Quixaba	17
41995	Rainha Isabel	17
41996	Rajada	17
41997	Rancharia	17
41998	Recife	17
41999	Riacho das Almas	17
42000	Riacho do Meio	17
42001	Riacho Fechado	17
42002	Riacho Pequeno	17
42003	Ribeirão	17
42004	Rio da Barra	17
42005	Rio Formoso	17
42006	Saire	17
42007	Salgadinho	17
42008	Salgueiro	17
42009	Saloa	17
42010	Salobro	17
42011	Sanharo	17
42012	Santa Cruz	17
42013	Santa Cruz da Baixa Verde	17
42014	Santa Cruz do Capibaribe	17
42015	Santa Filomena	17
42016	Santa Maria da Boa Vista	17
42017	Santa Maria do Cambuca	17
42018	Santa Rita	17
42019	Santa Terezinha	17
42020	Santana de São Joaquim	17
42021	Santo Agostinho	17
42022	Santo Antonio das Queimadas	17
42023	Santo Antonio dos Palmares	17
42024	São Benedito do Sul	17
42025	São Bento do Una	17
42026	São Caetano do Navio	17
42027	São Caitano	17
42028	São Domingos	17
42029	São João	17
42030	São Joaquim do Monte	17
42031	São José	17
42032	São José da Coroa Grande	17
42033	São José do Belmonte	17
42034	São José do Egito	17
42035	São Lazaro	17
42036	São Lourenço da Mata	17
42037	São Pedro	17
42038	São Vicente	17
42039	São Vicente Ferrer	17
42040	Sapucarana	17
42041	Saue	17
42042	Serra Branca	17
42043	Serra do Vento	17
42044	Serra Talhada	17
42045	Serrita	17
42046	Serrolândia	17
42047	Sertania	17
42048	Sertaozinho de Baixo	17
42049	Siriji	17
42050	Sirinhaem	17
42051	Sitio dos Nunes	17
42052	Solidão	17
42053	Surubim	17
42054	Tabira	17
42055	Tabocas	17
42056	Tacaimbo	17
42057	Tacaratu	17
42058	Tamandare	17
42059	Tamboata	17
42060	Tapiraim	17
42061	Taquaritinga do Norte	17
42062	Tara	17
42063	Tauapiranga	17
42064	Tejucupapo	17
42065	Terezinha	17
42066	Terra Nova	17
42067	Timbauba	17
42068	Timorante	17
42069	Toritama	17
42070	Tracunhaem	17
42071	Trapia	17
42072	Três Ladeiras	17
42073	Trindade	17
42074	Triunfo	17
42075	Tupanaci	17
42076	Tupanatinga	17
42077	Tupaoca	17
42078	Tuparetama	17
42079	Umas	17
42080	Umburetama	17
42081	Upatininga	17
42082	Urimama	17
42083	Urucu-mirim	17
42084	Urucuba	17
42085	Vasques	17
42086	Veneza	17
42087	Venturosa	17
42088	Verdejante	17
42089	Vertente do Lerio	17
42090	Vertentes	17
42091	Vicencia	17
42092	Vila Nova	17
42093	Viração	17
42094	Vitória de Santo Antão	17
42095	Volta do Moxoto	17
42096	Xexeu	17
42097	Xucuru	17
42098	Ze Gomes	17
42099	Acaua	18
42100	Agricolândia	18
42101	Água Branca	18
42102	AlaGoiásnha do Piaui	18
42103	Alegrete do Piaui	18
42104	Alto Longa	18
42105	Altos	18
42106	Alvorada do Gurgueia	18
42107	Amarante	18
42108	Angical do Piaui	18
42109	Anisio de Abreu	18
42110	Antonio Almeida	18
42111	Aroazes	18
42112	Arraiál	18
42113	Assunção do Piaui	18
42114	Avelino Lopes	18
42115	Baixa Grande do Ribeiro	18
42116	Barra D'alcantara	18
42117	Barras	18
42118	Barreiras do Piaui	18
42119	Barro Duro	18
42120	Batalha	18
42121	Bela Vista do Piaui	18
42122	Belem do Piaui	18
42123	Beneditinos	18
42124	Bertolinia	18
42125	Betania do Piaui	18
42126	Boa Hora	18
42127	Bocaina	18
42128	Bom Jesus	18
42129	Bom Principio do Piaui	18
42130	Bonfim do Piaui	18
42131	Boqueirão do Piaui	18
42132	Brasileira	18
42133	Brejo do Piaui	18
42134	Buriti dos Lopes	18
42135	Buriti dos Montes	18
42136	Cabeceiras do Piaui	18
42137	Cajazeiras do Piaui	18
42138	Cajueiro da Praiá	18
42139	Caldeirão Grande do Piaui	18
42140	Campinas do Piaui	18
42141	Campo Alegre do Fidalgo	18
42142	Campo Grande do Piaui	18
42143	Campo Largo do Piaui	18
42144	Campo Maior	18
42145	Canavieira	18
42146	Canto do Buriti	18
42147	Capitao de Campos	18
42148	Capitao Gervasio Oliveira	18
42149	Caracol	18
42150	Caraubas do Piaui	18
42151	Caridade do Piaui	18
42152	Castelo do Piaui	18
42153	Caxingo	18
42154	Cocal	18
42155	Cocal de Telha	18
42156	Cocal dos Alves	18
42157	Coivaras	18
42158	Colonia do Gurgueia	18
42159	Colonia do Piaui	18
42160	Conceição do Caninde	18
42161	Coronel José Dias	18
42162	Corrente	18
42163	Cristalândia do Piaui	18
42164	Cristino Castro	18
42165	Curimata	18
42166	Currais	18
42167	Curral Novo do Piaui	18
42168	Curralinhos	18
42169	Demerval Lobão	18
42170	Dirceu Arcoverde	18
42171	Dom Expedito Lopes	18
42172	Dom Inocencio	18
42173	Domingos Mourão	18
42174	Elesbao Veloso	18
42175	Eliseu Martins	18
42176	Esperantina	18
42177	Fartura do Piaui	18
42178	Flores do Piaui	18
42179	Floresta do Piaui	18
42180	Floriano	18
42181	Francinopolis	18
42182	Francisco Ayres	18
42183	Francisco Macedo	18
42184	Francisco Santos	18
42185	Fronteiras	18
42186	Geminiano	18
42187	Gilbues	18
42188	Guadalupe	18
42189	Guaribas	18
42190	Hugo Napoleão	18
42191	Ilha Grande	18
42192	Inhuma	18
42193	Ipiranga do Piaui	18
42194	Isaiás Coelho	18
42195	Itainopolis	18
42196	Itaueira	18
42197	Jacobina do Piaui	18
42198	Jaicos	18
42199	Jardim do Mulato	18
42200	Jatoba do Piaui	18
42201	Jerumenha	18
42202	João Costa	18
42203	Joaquim Pires	18
42204	Joca Marques	18
42205	José de Freitas	18
42206	Juazeiro do Piaui	18
42207	Julio Borges	18
42208	Jurema	18
42209	Lagoa Alegre	18
42210	Lagoa de São Francisco	18
42211	Lagoa do Barro do Piaui	18
42212	Lagoa do Piaui	18
42213	Lagoa do Sitio	18
42214	LaGoiásnha do Piaui	18
42215	Landri Sales	18
42216	Luis Correia	18
42217	Luzilândia	18
42218	Madeiro	18
42219	Manoel Emidio	18
42220	Marcolândia	18
42221	Marcos Parente	18
42222	Massape do Piaui	18
42223	Matias Olimpio	18
42224	Miguel Alves	18
42225	Miguel Leão	18
42226	Milton Brandão	18
42227	Monsenhor Gil	18
42228	Monsenhor Hipolito	18
42229	Monte Alegre do Piaui	18
42230	Morro Cabeca No Tempo	18
42231	Morro do Chapeu do Piaui	18
42232	Murici dos Portelas	18
42233	Nazaré do Piaui	18
42234	Nossa Senhora de Nazaré	18
42235	Nossa Senhora dos Remedios	18
42236	Nova Santa Rita	18
42237	Novo Nilo	18
42238	Novo Oriente do Piaui	18
42239	Novo Santo Antonio	18
42240	Oeiras	18
42241	Olho D'agua Do Piaui	18
42242	Padre Marcos	18
42243	Paes Landim	18
42244	Pajeu do Piaui	18
42245	Palmeira do Piaui	18
42246	Palmeirais	18
42247	Paqueta	18
42248	Parnagua	18
42249	Parnaiba	18
42250	Passagem Franca do Piaui	18
42251	Patos do Piaui	18
42252	Paulistana	18
42253	Pavussu	18
42254	Pedro Ii	18
42255	Pedro Laurentino	18
42256	Picos	18
42257	Pimenteiras	18
42258	Pio Ix	18
42259	Piracuruca	18
42260	Piripiri	18
42261	Porto	18
42262	Porto Alegre do Piaui	18
42263	Prata do Piaui	18
42264	Queimada Nova	18
42265	Redenção do Gurgueia	18
42266	Regeneração	18
42267	Riacho Frio	18
42268	Ribeira do Piaui	18
42269	Ribeiro Gonçalves	18
42270	Rio Grande do Piaui	18
42271	Santa Cruz do Piaui	18
42272	Santa Cruz dos Milagres	18
42273	Santa Filomena	18
42274	Santa Luz	18
42275	Santa Rosa do Piaui	18
42276	Santana do Piaui	18
42277	Santo Antonio de Lisboa	18
42278	Santo Antonio dos Milagres	18
42279	Santo Inacio do Piaui	18
42280	São Braz do Piaui	18
42281	São Felix do Piaui	18
42282	São Francisco de Assis do Piaui	18
42283	São Francisco do Piaui	18
42284	São Goncalo do Gurgueia	18
42285	São Goncalo do Piaui	18
42286	São João da Canabrava	18
42287	São João da Fronteira	18
42288	São João da Serra	18
42289	São João da Varjota	18
42290	São João do Arraiál	18
42291	São João do Piaui	18
42292	São José do Divino	18
42293	São José do Peixe	18
42294	São José do Piaui	18
42295	São Julião	18
42296	São Lourenço do Piaui	18
42297	São Luis do Piaui	18
42298	São Miguel da Baixa Grande	18
42299	São Miguel do Fidalgo	18
42300	São Miguel do Tapuio	18
42301	São Pedro do Piaui	18
42302	São Raimundo Nonato	18
42303	Sebastião Barros	18
42304	Sebastião Leal	18
42305	Sigefredo Pacheco	18
42306	Simoes	18
42307	Simplicio Mendes	18
42308	Socorro do Piaui	18
42309	Sussuapara	18
42310	Tamboril do Piaui	18
42311	Tanque do Piaui	18
42312	Teresina	18
42313	União	18
42314	Urucui	18
42315	Valenca do Piaui	18
42316	Varzea Branca	18
42317	Varzea Grande	18
42318	Vera Mendes	18
42319	Vila Nova do Piaui	18
42320	Wall Ferraz	18
42321	Abapa	16
42322	Abatia	16
42323	Acampamento das Minas	16
42324	Acungui	16
42325	Adhemar de Barros	16
42326	Adrianopolis	16
42327	Agostinho	16
42328	Água Azul	16
42329	Água Boa	16
42330	Água Branca	16
42331	Água do Boi	16
42332	Água Mineral	16
42333	Água Vermelha	16
42334	Agudos do Sul	16
42335	Alecrim	16
42336	Alexandra	16
42337	Almirante Tamandare	16
42338	Altamira do Parana	16
42339	Altaneira	16
42340	Alto Alegre	16
42341	Alto Alegre do Iguacu	16
42342	Alto Amparo	16
42343	Alto do Amparo	16
42344	Alto Para	16
42345	Alto Parana	16
42346	Alto Piquiri	16
42347	Alto Pora	16
42348	Alto Sabia	16
42349	Alto Santa Fé	16
42350	Alto São João	16
42351	Altonia	16
42352	Alvorada do Iguacu	16
42353	Alvorada do Sul	16
42354	Amapora	16
42355	Amorinha	16
42356	Ampere	16
42357	Anahy	16
42358	Andira	16
42359	Andorinhas	16
42360	Angai	16
42361	Angulo	16
42362	Antas	16
42363	Antonina	16
42364	Antonio Brandao de Oliveira	16
42365	Antonio Olinto	16
42366	Anunciação	16
42367	Aparecida do Oeste	16
42368	Apiaba	16
42369	Apucarana	16
42370	Aquidaban	16
42371	Aranha	16
42372	Arapongas	16
42373	Arapoti	16
42374	Arapua	16
42375	Arapuan	16
42376	Ararapira	16
42377	Araruna	16
42378	Araucaria	16
42379	Areia Branca dos Assis	16
42380	Areias	16
42381	Aricanduva	16
42382	Ariranha do Ivai	16
42383	Aroeira	16
42384	Arquimedes	16
42385	Assai	16
42386	Assis Chateaubriand	16
42387	Astorga	16
42388	Atalaiá	16
42389	Aurora do Iguacu	16
42390	Bairro Cachoeira	16
42391	Bairro do Felisberto	16
42392	Bairro Limoeiro	16
42393	Balsa Nova	16
42394	Bandeirantes	16
42395	Bandeirantes D'oeste	16
42396	Banhado	16
42397	Barão de Lucena	16
42398	Barbosa Ferraz	16
42399	Barra Bonita	16
42400	Barra do Jacare	16
42401	Barra Grande	16
42402	Barra Mansa	16
42403	Barra Santa Salete	16
42404	Barração	16
42405	Barras	16
42406	Barreiro	16
42407	Barreiro das Frutas	16
42408	Barreiros	16
42409	Barrinha	16
42410	Barro Preto	16
42411	Bateias	16
42412	Baulândia	16
42413	Bela Vista	16
42414	Bela Vista do Caroba	16
42415	Bela Vista do Ivai	16
42416	Bela Vista do Paraiso	16
42417	Bela Vista do Piquiri	16
42418	Bela Vista do Tapiracui	16
42419	Bentopolis	16
42420	Bernardelli	16
42421	Betaras	16
42422	Bituruna	16
42423	Boa Esperanca	16
42424	Boa Esperanca do Iguacu	16
42425	Boa Ventura de São Roque	16
42426	Boa Vista	16
42427	Boa Vista da Aparecida	16
42428	Bocaina	16
42429	Bocaiuva do Sul	16
42430	Bom Jardim do Sul	16
42431	Bom Jesus do Sul	16
42432	Bom Progresso	16
42433	Bom Retiro	16
42434	Bom Sucesso	16
42435	Bom Sucesso do Sul	16
42436	Borda do Campo	16
42437	Borda do Campo de São Sebastião	16
42438	Borman	16
42439	Borrazopolis	16
42440	Botuquara	16
42441	Bourbonia	16
42442	Braganey	16
42443	Bragantina	16
42444	Brasilândia do Sul	16
42445	Bugre	16
42446	Bulção	16
42447	Cabrito	16
42448	Cacatu	16
42449	Cachoeira	16
42450	Cachoeira de Cima	16
42451	Cachoeira de São José	16
42452	Cachoeira do Espirito Santo	16
42453	Cachoeirinha	16
42454	Cadeadinho	16
42455	Caetano Mendes	16
42456	Cafeara	16
42457	Cafeeiros	16
42458	Cafelândia	16
42459	Cafezal do Sul	16
42460	Caita	16
42461	Caixa Prego	16
42462	California	16
42463	Calogeras	16
42464	Cambara	16
42465	Cambe	16
42466	Cambiju	16
42467	Cambira	16
42468	Campestrinho	16
42469	Campina	16
42470	Campina da Lagoa	16
42471	Campina do Miranguava	16
42472	Campina do Simão	16
42473	Campina dos Furtados	16
42474	Campina Grande do Sul	16
42475	Campinas	16
42476	Campo Bonito	16
42477	Campo do Meio	16
42478	Campo do Tenente	16
42479	Campo Largo	16
42480	Campo Largo da Roseira	16
42481	Campo Magro	16
42482	Campo Mourão	16
42483	Candido de Abreu	16
42484	Candoi	16
42485	Canela	16
42486	Cantagalo	16
42487	Canzianopolis	16
42488	Capanema	16
42489	Capão Alto	16
42490	Capão Bonito	16
42491	Capão da Lagoa	16
42492	Capão Grande	16
42493	Capão Rico	16
42494	Capitao Leonidas Marques	16
42495	Capivara	16
42496	Capoeirinha	16
42497	Cara Pintado	16
42498	Cara-cara	16
42499	Caraja	16
42500	Carambei	16
42501	Caramuru	16
42502	Caratuva	16
42503	Carazinho	16
42504	Carbonera	16
42505	Carlopolis	16
42506	Casa Nova	16
42507	Cascatinha	16
42508	Cascavel	16
42509	Castro	16
42510	Catanduvas	16
42511	Catanduvas do Sul	16
42512	Catarinenses	16
42513	Caxambu	16
42514	Cedro	16
42515	Centenario	16
42516	Centenario do Sul	16
42517	Central Lupion	16
42518	Centralito	16
42519	Centro Novo	16
42520	Cerne	16
42521	Cerrado Grande	16
42522	Cerro Azul	16
42523	Cerro Velho	16
42524	Ceu Azul	16
42525	Chopinzinho	16
42526	Cianorte	16
42527	Cidade Gaucha	16
42528	Cintra Pimentel	16
42529	Clevelândia	16
42530	Coitinho	16
42531	Colombo	16
42532	Colonia Acioli	16
42533	Colonia Castelhanos	16
42534	Colonia Castrolanda	16
42535	Colonia Centenario	16
42536	Colonia Cristina	16
42537	Colonia Dom Carlos	16
42538	Colonia Esperanca	16
42539	Colonia General Carneiro	16
42540	Colonia Iapo	16
42541	Colonia Melissa	16
42542	Colonia Murici	16
42543	Colonia Padre Paulo	16
42544	Colonia Pereira	16
42545	Colonia Santos Andrade	16
42546	Colonia São José	16
42547	Colonia Sapucai	16
42548	Colonia Saude	16
42549	Colonia Tapera	16
42550	Colorado	16
42551	Comur	16
42552	Conceição	16
42553	Conchas Velhas	16
42554	Conciolândia	16
42555	Congonhas	16
42556	Congonhinhas	16
42557	Conselheiro Mairinck	16
42558	Conselheiro Zacarias	16
42559	Contenda	16
42560	Copacabana do Norte	16
42561	Corbelia	16
42562	Cornelio Procopio	16
42563	Coronel Domingos Soares	16
42564	Coronel Firmino Martins	16
42565	Coronel Vivida	16
42566	Correia de Freitas	16
42567	Corumbatai do Sul	16
42568	Corvo	16
42569	Costeira	16
42570	Covo	16
42571	Coxilha Rica	16
42572	Cristo Rei	16
42573	Cruz Machado	16
42574	Cruzeiro do Iguacu	16
42575	Cruzeiro do Norte	16
42576	Cruzeiro do Oeste	16
42577	Cruzeiro do Sul	16
42578	Cruzmaltina	16
42579	Cunhaporanga	16
42580	Curitiba	16
42581	Curiuva	16
42582	Curucaca	16
42583	Deputado José Afonso	16
42584	Despique	16
42585	Despraiádo	16
42586	Dez de Maio	16
42587	Diamante	16
42588	Diamante D'oeste	16
42589	Diamante do Norte	16
42590	Diamante do Sul	16
42591	Doce Grande	16
42592	Dois Irmaos	16
42593	Dois Marcos	16
42594	Dois Vizinhos	16
42595	Dom Rodrigo	16
42596	Dorizon	16
42597	Douradina	16
42598	Doutor Antonio Paranhos	16
42599	Doutor Camargo	16
42600	Doutor Ernesto	16
42601	Doutor Oliveira Castro	16
42602	Doutor Ulysses	16
42603	Eduardo Xavier da Silva	16
42604	Emboguacu	16
42605	Emboque	16
42606	Encantado D'oeste	16
42607	Encruzilhada	16
42608	Eneas Marques	16
42609	Engenheiro Beltrão	16
42610	Entre Rios	16
42611	Entre Rios do Oeste	16
42612	Esperanca do Norte	16
42613	Esperanca Nova	16
42614	Espigao Alto do Iguacu	16
42615	Espirito Santo	16
42616	Estação General Lucio	16
42617	Estação Roca Nova	16
42618	Europa	16
42619	Euzebio de Oliveira	16
42620	Faisqueira	16
42621	Farol	16
42622	Faxina	16
42623	Faxinal	16
42624	Faxinal do Ceu	16
42625	Faxinal dos Elias	16
42626	Faxinal Santa Cruz	16
42627	Fazenda do Brigadeiro	16
42628	Fazenda dos Barbosas	16
42629	Fazenda Jangada	16
42630	Fazenda Rio Grande	16
42631	Fazenda Salmo Null	16
42632	Fazendinha	16
42633	Felpudo	16
42634	Fenix	16
42635	Fernandes Pinheiro	16
42636	Fernao Dias	16
42637	Ferraria	16
42638	Ferreiras	16
42639	Figueira	16
42640	Figueira do Oeste	16
42641	Fiusas	16
42642	Flor da Serra	16
42643	Flor da Serra do Sul	16
42644	Florai	16
42645	Floresta	16
42646	Florestopolis	16
42647	Floriano	16
42648	Florida	16
42649	Floropolis	16
42650	Fluviopolis	16
42651	Formigone	16
42652	Formosa do Oeste	16
42653	Foz do Iguacu	16
42654	Foz do Jordão	16
42655	Francisco Alves	16
42656	Francisco Beltrão	16
42657	Francisco Frederico Teixeira Guimaraes	16
42658	Frei Timoteo	16
42659	Fueros	16
42660	Fundão	16
42661	Gamadinho	16
42662	Gamela	16
42663	Gaucha	16
42664	Gavião	16
42665	General Carneiro	16
42666	General Osorio	16
42667	Geremia Lunardelli	16
42668	Godoy Moreira	16
42669	Goiásoere	16
42670	Goiásoxim	16
42671	Goiáss	16
42672	Gonçalves Junior	16
42673	Graciosa	16
42674	Grandes Rios	16
42675	Guaipora	16
42676	Guaira	16
42677	Guairaca	16
42678	Guajuvira	16
42679	Guamiranga	16
42680	Guamirim	16
42681	Guapirama	16
42682	Guapore	16
42683	Guaporema	16
42684	Guara	16
42685	Guaraci	16
42686	Guaragi	16
42687	Guaraituba	16
42688	Guarani	16
42689	Guaraniacu	16
42690	Guarapuava	16
42691	Guarapuavinha	16
42692	Guaraquecaba	16
42693	Guararema	16
42694	Guaratuba	16
42695	Guarauna	16
42696	Guaravera	16
42697	Guardamoria	16
42698	Harmonia	16
42699	Herculândia	16
42700	Herval Grande	16
42701	Herveira	16
42702	Hidreletrica Itaipu	16
42703	Honorio Serpa	16
42704	Iarama	16
42705	Ibaiti	16
42706	Ibema	16
42707	Ibiaci	16
42708	Ibipora	16
42709	Icara	16
42710	Icaraima	16
42711	Icatu	16
42712	Igrejinha	16
42713	Iguaracu	16
42714	Iguatemi	16
42715	Iguatu	16
42716	Iguipora	16
42717	Ilha do Mel	16
42718	Ilha dos Valadares	16
42719	Imbau	16
42720	Imbauzinho	16
42721	Imbituva	16
42722	Inacio Martins	16
42723	Inaja	16
42724	Independencia	16
42725	Indianopolis	16
42726	Inspetor Carvalho	16
42727	Invernada	16
42728	Invernadinha	16
42729	Iolopolis	16
42730	Ipiranga	16
42731	Ipora	16
42732	Iracema do Oeste	16
42733	Irapuan	16
42734	Irati	16
42735	Irere	16
42736	Iretama	16
42737	Itaguaje	16
42738	Itaiácoca	16
42739	Itaipulândia	16
42740	Itambaraca	16
42741	Itambe	16
42742	Itapanhacanga	16
42743	Itapara	16
42744	Itapejara D'oeste	16
42745	Itaperucu	16
42746	Itaqui	16
42747	Itauna do Sul	16
42748	Itinga	16
42749	Ivai	16
42750	Ivailândia	16
42751	Ivaipora	16
42752	Ivaitinga	16
42753	Ivate	16
42754	Ivatuba	16
42755	Jaborandi	16
42756	Jaboti	16
42757	Jaboticabal	16
42758	Jaburu	16
42759	Jacare	16
42760	Jacarezinho	16
42761	Jaciaba	16
42762	Jacutinga	16
42763	Jaguapita	16
42764	Jaguariaiva	16
42765	Jandaiá do Sul	16
42766	Jangada	16
42767	Jangada do Sul	16
42768	Janiopolis	16
42769	Japira	16
42770	Japura	16
42771	Jaracatia	16
42772	Jardim	16
42773	Jardim Alegre	16
42774	Jardim Olinda	16
42775	Jardim Paredão	16
42776	Jardim Paulista	16
42777	Jardinopolis	16
42778	Jataizinho	16
42779	Javacae	16
42780	Jesuitas	16
42781	Joa	16
42782	Joaquim Tavora	16
42783	Jordaozinho	16
42784	José Lacerda	16
42785	Juciara	16
42786	Jundiai do Sul	16
42787	Juranda	16
42788	Jussara	16
42789	Juvinopolis	16
42790	Kalore	16
42791	Km Null	16
42792	Lagoa	16
42793	Lagoa Bonita	16
42794	Lagoa dos Ribas	16
42795	Lagoa Dourada	16
42796	Lagoa Seca	16
42797	Lagoa Verde	16
42798	LaGoiásnha	16
42799	Lajeado	16
42800	Lajeado Bonito	16
42801	Lajeado Grande	16
42802	Lambari	16
42803	Lapa	16
42804	Laranja Azeda	16
42805	Laranjal	16
42806	Laranjeiras do Sul	16
42807	Lavra	16
42808	Lavrinha	16
42809	Leopolis	16
42810	Lerroville	16
42811	Lidianopolis	16
42812	Lindoeste	16
42813	Linha Giacomini	16
42814	Loanda	16
42815	Lobato	16
42816	Londrina	16
42817	Lopei	16
42818	Lovat	16
42819	Luar	16
42820	Luiziana	16
42821	Lunardelli	16
42822	Lupionopolis	16
42823	Macaco	16
42824	Macucos	16
42825	Maira	16
42826	Maita	16
42827	Mallet	16
42828	Malu	16
42829	Mambore	16
42830	Mandacaiá	16
42831	Mandaguacu	16
42832	Mandaguari	16
42833	Mandiocaba	16
42834	Mandirituba	16
42835	Manfrinopolis	16
42836	Mangueirinha	16
42837	Manoel Ribas	16
42838	Maraba	16
42839	Maracana	16
42840	Marajo	16
42841	Maravilha	16
42842	Marcelino	16
42843	Marcionopolis	16
42844	Marechal Candido Rondon	16
42845	Margarida	16
42846	Maria Helena	16
42847	Maria Luiza	16
42848	Marialva	16
42849	Mariental	16
42850	Marilândia do Sul	16
42851	Marilena	16
42852	Marilu	16
42853	Mariluz	16
42854	Marimbondo	16
42855	Maringa	16
42856	Mariopolis	16
42857	Maripa	16
42858	Maristela	16
42859	Mariza	16
42860	Marmelândia	16
42861	Marmeleiro	16
42862	Marques de Abrantes	16
42863	Marquinho	16
42864	Marrecas	16
42865	Martins	16
42866	Marumbi	16
42867	Matelândia	16
42868	Matinhos	16
42869	Mato Queimado	16
42870	Mato Rico	16
42871	Maua da Serra	16
42872	Medianeira	16
42873	Meia-lua	16
42874	Memoria	16
42875	Mendeslândia	16
42876	Mercedes	16
42877	Mirador	16
42878	Miranda	16
42879	Mirante do Piquiri	16
42880	Miraselva	16
42881	Missal	16
42882	Monjolinho	16
42883	Monte Real	16
42884	Moreira Sales	16
42885	Morretes	16
42886	Morro Alto	16
42887	Morro Ingles	16
42888	Munhoz de Melo	16
42889	Natingui	16
42890	Nilza	16
42891	Nordestina	16
42892	Nossa Senhora Aparecida	16
42893	Nossa Senhora da Aparecida	16
42894	Nossa Senhora da Candelaria	16
42895	Nossa Senhora das Gracas	16
42896	Nossa Senhora de Lourdes	16
42897	Nossa Senhora do Carmo	16
42898	Nova Alianca do Ivai	16
42899	Nova Altamira	16
42900	Nova America da Colina	16
42901	Nova Amoreira	16
42902	Nova Aurora	16
42903	Nova Bilac	16
42904	Nova Brasilia	16
42905	Nova Brasilia do Itarare	16
42906	Nova Cantu	16
42907	Nova Concordia	16
42908	Nova Esperanca	16
42909	Nova Esperanca do Sudoeste	16
42910	Nova Fatima	16
42911	Nova Laranjeiras	16
42912	Nova Londrina	16
42913	Nova Lourdes	16
42914	Nova Olimpia	16
42915	Nova Prata do Iguacu	16
42916	Nova Riqueza	16
42917	Nova Santa Barbara	16
42918	Nova Santa Rosa	16
42919	Nova Tebas	16
42920	Nova Tirol	16
42921	Nova Videira	16
42922	Novo Horizonte	16
42923	Novo Itacolomi	16
42924	Novo Jardim	16
42925	Novo Sarandi	16
42926	Novo Sobradinho	16
42927	Novo Três Passos	16
42928	Olaria	16
42929	Olho Agudo	16
42930	Olho D'agua	16
42931	Oroite	16
42932	Ortigueira	16
42933	Ourilândia	16
42934	Ourizona	16
42935	Ouro Verde do Oeste	16
42936	Ouro Verde do Piquiri	16
42937	Padre Ponciano	16
42938	Paicandu	16
42939	Paiol de Baixo	16
42940	Paiol Queimado	16
42941	Paiquere	16
42942	Palmas	16
42943	Palmeira	16
42944	Palmeirinha	16
42945	Palmira	16
42946	Palmital	16
42947	Palmital de São Silvestre	16
42948	Palmitopolis	16
42949	Palotina	16
42950	Panema	16
42951	Pangare	16
42952	Papagaios Novos	16
42953	Paraiso do Norte	16
42954	Parana D'oeste	16
42955	Paranacity	16
42956	Paranagi	16
42957	Paranagua	16
42958	Paranapoema	16
42959	Paranavai	16
42960	Passa Una	16
42961	Passo da Ilha	16
42962	Passo dos Pupos	16
42963	Passo Fundo	16
42964	Passo Liso	16
42965	Pato Bragado	16
42966	Pato Branco	16
42967	Patos Velhos	16
42968	Pau D'alho Do Sul	16
42969	Paula Freitas	16
42970	Paulistania	16
42971	Paulo Frontin	16
42972	Peabiru	16
42973	Pedra Branca do Araraquara	16
42974	Pedras	16
42975	Pedro Lustosa	16
42976	Pelado	16
42977	Perobal	16
42978	Perola	16
42979	Perola do Oeste	16
42980	Perola Independente	16
42981	Piassuguera	16
42982	Pien	16
42983	Pinare	16
42984	Pinhais	16
42985	Pinhal do São Bento	16
42986	Pinhal Preto	16
42987	Pinhalão	16
42988	Pinhalzinho	16
42989	Pinhão	16
42990	Pinheiro	16
42991	Piquirivai	16
42992	Piracema	16
42993	Pirai do Sul	16
42994	Pirapo	16
42995	Piraquara	16
42996	Piriquitos	16
42997	Pitanga	16
42998	Pitangueiras	16
42999	Pitangui	16
43000	Planaltina do Parana	16
43001	Planalto	16
43002	Pocinho	16
43003	Poema	16
43004	Ponta do Pasto	16
43005	Ponta Grossa	16
43006	Pontal do Parana	16
43007	Porecatu	16
43008	Portão	16
43009	Porteira Preta	16
43010	Porto Amazonas	16
43011	Porto Barreiro	16
43012	Porto Belo	16
43013	Porto Brasilio	16
43014	Porto Camargo	16
43015	Porto de Cima	16
43016	Porto Meira	16
43017	Porto Mendes	16
43018	Porto Rico	16
43019	Porto San Juan	16
43020	Porto Santana	16
43021	Porto São Carlos	16
43022	Porto São José	16
43023	Porto Vitória	16
43024	Prado Ferreira	16
43025	Pranchita	16
43026	Prata	16
43027	Prata Um	16
43028	Presidente Castelo Branco	16
43029	Presidente Kennedy	16
43030	Primeiro de Maio	16
43031	Progresso	16
43032	Prudentopolis	16
43033	Pulinopolis	16
43034	Quatigua	16
43035	Quatro Barras	16
43036	Quatro Pontes	16
43037	Quebra Freio	16
43038	Quedas do Iguacu	16
43039	Queimados	16
43040	Querencia do Norte	16
43041	Quinta do Sol	16
43042	Quinzopolis	16
43043	Quitandinha	16
43044	Ramilândia	16
43045	Rancho Alegre	16
43046	Rancho Alegre D'oeste	16
43047	Realeza	16
43048	Reboucas	16
43049	Região dos Valos	16
43050	Reianopolis	16
43051	Renascenca	16
43052	Reserva	16
43053	Reserva do Iguacu	16
43054	Retiro	16
43055	Retiro Grande	16
43056	Ribeirão Bonito	16
43057	Ribeirão Claro	16
43058	Ribeirão do Pinhal	16
43059	Ribeirão do Pinheiro	16
43060	Rio Abaixo	16
43061	Rio Azul	16
43062	Rio Bom	16
43063	Rio Bonito	16
43064	Rio Bonito do Iguacu	16
43065	Rio Branco do Ivai	16
43066	Rio Branco do Sul	16
43067	Rio Claro do Sul	16
43068	Rio da Prata	16
43069	Rio das Antas	16
43070	Rio das Mortes	16
43071	Rio das Pedras	16
43072	Rio das Pombas	16
43073	Rio do Mato	16
43074	Rio do Salto	16
43075	Rio Negro	16
43076	Rio Novo	16
43077	Rio Pinheiro	16
43078	Rio Quatorze	16
43079	Rio Saltinho	16
43080	Rio Saudade	16
43081	Rio Verde	16
43082	Roberto Silveira	16
43083	Roca Velha	16
43084	Rolândia	16
43085	Romeopolis	16
43086	Roncador	16
43087	Rondinha	16
43088	Rondon	16
43089	Rosario do Ivai	16
43090	Sabaudia	16
43091	Sagrada Familia	16
43092	Salgado Filho	16
43093	Salles de Oliveira	16
43094	Saltinho do Oeste	16
43095	Salto do Itarare	16
43096	Salto do Lontra	16
43097	Salto Portão	16
43098	Samambaiá	16
43099	Santa Amelia	16
43100	Santa Cecilia do Pavão	16
43101	Santa Clara	16
43102	Santa Cruz de Monte Castelo	16
43103	Santa Eliza	16
43104	Santa Esmeralda	16
43105	Santa Fé	16
43106	Santa Fé do Pirapo	16
43107	Santa Helena	16
43108	Santa Ines	16
43109	Santa Isabel do Ivai	16
43110	Santa Izabel do Oeste	16
43111	Santa Lucia	16
43112	Santa Lurdes	16
43113	Santa Luzia da Alvorada	16
43114	Santa Margarida	16
43115	Santa Maria	16
43116	Santa Maria do Oeste	16
43117	Santa Maria do Rio do Peixe	16
43118	Santa Mariana	16
43119	Santa Monica	16
43120	Santa Quiteria	16
43121	Santa Rita	16
43122	Santa Rita do Oeste	16
43123	Santa Rosa	16
43124	Santa Tereza do Oeste	16
43125	Santa Terezinha de Itaipu	16
43126	Santa Zelia	16
43127	Santana	16
43128	Santana do Itarare	16
43129	Santo Antonio	16
43130	Santo Antonio da Platina	16
43131	Santo Antonio do Caiua	16
43132	Santo Antonio do Iratim	16
43133	Santo Antonio do Palmital	16
43134	Santo Antonio do Paraiso	16
43135	Santo Antonio do Sudoeste	16
43136	Santo Inacio	16
43137	Santo Rei	16
43138	São Bento	16
43139	São Braz	16
43140	São Camilo	16
43141	São Carlos do Ivai	16
43142	São Cirilo	16
43143	São Clemente	16
43144	São Cristovão	16
43145	São Domingos	16
43146	São Francisco	16
43147	São Francisco de Imbau	16
43148	São Francisco de Sales	16
43149	São Gabriel	16
43150	São Gotardo	16
43151	São Jeronimo da Serra	16
43152	São João	16
43153	São João D'oeste	16
43154	São João da Boa Vista	16
43155	São João do Caiua	16
43156	São João do Ivai	16
43157	São João do Pinhal	16
43158	São João do Triunfo	16
43159	São Joaquim	16
43160	São Joaquim do Pontal	16
43161	São Jorge D'oeste	16
43162	São Jorge do Ivai	16
43163	São Jorge do Patrocinio	16
43164	São José	16
43165	São José da Boa Vista	16
43166	São José das Palmeiras	16
43167	São José do Iguacu	16
43168	São José do Itavo	16
43169	São José do Ivai	16
43170	São José dos Pinhais	16
43171	São Judas Tadeu	16
43172	São Leonardo	16
43173	São Lourenço	16
43174	São Luiz	16
43175	São Luiz do Puruna	16
43176	São Manoel do Parana	16
43177	São Marcos	16
43178	São Martinho	16
43179	São Mateus do Sul	16
43180	São Miguel	16
43181	São Miguel do Cambui	16
43182	São Miguel do Iguacu	16
43183	São Paulo	16
43184	São Pedro	16
43185	São Pedro do Florido	16
43186	São Pedro do Iguacu	16
43187	São Pedro do Ivai	16
43188	São Pedro do Parana	16
43189	São Pedro Lopei	16
43190	São Pio X	16
43191	São Roque	16
43192	São Roque do Pinhal	16
43193	São Salvador	16
43194	São Sebastião	16
43195	São Sebastião da Amoreira	16
43196	São Silvestre	16
43197	São Tome	16
43198	São Valentim	16
43199	São Vicente	16
43200	Sape	16
43201	Sapopema	16
43202	Sarandi	16
43203	Saudade do Iguacu	16
43204	Sede Alvorada	16
43205	Sede Chaparral	16
43206	Sede Nova Sant'ana	16
43207	Sede Progresso	16
43208	Selva	16
43209	Senges	16
43210	Senhor Bom Jesus dos Gramados	16
43211	Serra dos Dourados	16
43212	Serra Negra	16
43213	Serranopolis do Iguacu	16
43214	Serraria Klas	16
43215	Serrinha	16
43216	Sertaneja	16
43217	Sertanopolis	16
43218	Sertaozinho	16
43219	Sete Saltos de Cima	16
43220	Silvolândia	16
43221	Siqueira Belo	16
43222	Siqueira Campos	16
43223	Socavão	16
43224	Socorro	16
43225	Sulina	16
43226	Sumare	16
43227	Sussui	16
43228	Sutis	16
43229	Taipa	16
43230	Tamarana	16
43231	Tambarutaca	16
43232	Tamboara	16
43233	Tanque Grande	16
43234	Tapejara	16
43235	Tapira	16
43236	Tapui	16
43237	Taquara	16
43238	Taquari dos Polacos	16
43239	Taquari dos Russos	16
43240	Taquaruna	16
43241	Teixeira Soares	16
43242	Telemaco Borba	16
43243	Teolândia	16
43244	Tereza Breda	16
43245	Tereza Cristina	16
43246	Terra Boa	16
43247	Terra Nova	16
43248	Terra Rica	16
43249	Terra Roxa	16
43250	Tibagi	16
43251	Tijucas do Sul	16
43252	Tijuco Preto	16
43253	Timbu Velho	16
43254	Tindiquera	16
43255	Tiradentes	16
43256	Toledo	16
43257	Tomaz Coelho	16
43258	Tomazina	16
43259	Tranqueira	16
43260	Três Barras do Parana	16
43261	Três Bicos	16
43262	Três Bocas	16
43263	Três Capoes	16
43264	Três Corregos	16
43265	Três Lagoas	16
43266	Três Pinheiros	16
43267	Três Placas	16
43268	Triangulo	16
43269	Trindade	16
43270	Triolândia	16
43271	Tronco	16
43272	Tunas do Parana	16
43273	Tuneiras do Oeste	16
43274	Tupassi	16
43275	Tupinamba	16
43276	Turvo	16
43277	Ubaldino Taques	16
43278	Ubauna	16
43279	Ubirata	16
43280	Umuarama	16
43281	União	16
43282	União da Vitória	16
43283	União do Oeste	16
43284	Uniflor	16
43285	Urai	16
43286	Usina	16
43287	Uvaiá	16
43288	Valentins	16
43289	Valerio	16
43290	Vargeado	16
43291	Vassoural	16
43292	Ventania	16
43293	Vera Cruz do Oeste	16
43294	Vera Guarani	16
43295	Vere	16
43296	Vida Nova	16
43297	Vidigal	16
43298	Vila Alta	16
43299	Vila Diniz	16
43300	Vila dos Roldos	16
43301	Vila Florida	16
43302	Vila Gandhi	16
43303	Vila Guay	16
43304	Vila Nova	16
43305	Vila Nova de Florenca	16
43306	Vila Paraiso	16
43307	Vila Reis	16
43308	Vila Rica do Ivai	16
43309	Vila Roberto Brzezinski	16
43310	Vila Silva Jardim	16
43311	Vila Velha	16
43312	Virmond	16
43313	Vista Alegre	16
43314	Vista Bonita	16
43315	Vitória	16
43316	Vitorino	16
43317	Warta	16
43318	Wenceslau Braz	16
43319	Xambre	16
43320	Xaxim	16
43321	Yolanda	16
43322	Abarracamento	19
43323	Abraão	19
43324	Afonso Arinos	19
43325	Agulhas Negras	19
43326	Amparo	19
43327	Andrade Pinto	19
43328	Angra dos Reis	19
43329	Anta	19
43330	Aperibe	19
43331	Araruama	19
43332	Areal	19
43333	Armação de Buzios	19
43334	Arraiál do Cabo	19
43335	Arrozal	19
43336	Avelar	19
43337	Bacaxa	19
43338	Baltazar	19
43339	Banquete	19
43340	Barão de Juparana	19
43341	Barcelos	19
43342	Barra Alegre	19
43343	Barra de Macae	19
43344	Barra de São João	19
43345	Barra do Pirai	19
43346	Barra Mansa	19
43347	Barra Seca	19
43348	Belford Roxo	19
43349	Bemposta	19
43350	Boa Esperanca	19
43351	Boa Sorte	19
43352	Boa Ventura	19
43353	Bom Jardim	19
43354	Bom Jesus do Itabapoana	19
43355	Bom Jesus do Querendo	19
43356	Cabo Frio	19
43357	Cabucu	19
43358	Cachoeiras de Macacu	19
43359	Cachoeiros	19
43360	Calheiros	19
43361	Cambiasca	19
43362	Cambuci	19
43363	Campo do Coelho	19
43364	Campos dos Goytacazes	19
43365	Campos Elyseos	19
43366	Cantagalo	19
43367	Carabucu	19
43368	Carapebus	19
43369	Cardoso Moreira	19
43370	Carmo	19
43371	Cascatinha	19
43372	Casimiro de Abreu	19
43373	Cava	19
43374	Coelho da Rocha	19
43375	Colonia	19
43376	Comendador Levy Gasparian	19
43377	Comendador Venancio	19
43378	Conceição de Jacarei	19
43379	Conceição de Macabu	19
43380	Conrado	19
43381	Conselheiro Paulino	19
43382	Conservatoria	19
43383	Cordeiro	19
43384	Coroa Grande	19
43385	Correas	19
43386	Córrego da Prata	19
43387	Córrego do Ouro	19
43388	Correntezas	19
43389	Cunhambebe	19
43390	Dorândia	19
43391	Dores de Macabu	19
43392	Doutor Elias	19
43393	Doutor Loreti	19
43394	Duas Barras	19
43395	Duque de Caxias	19
43396	Engenheiro Passos	19
43397	Engenheiro Paulo de Frontin	19
43398	Engenheiro Pedreira	19
43399	Estrada Nova	19
43400	Euclidelândia	19
43401	Falção	19
43402	Floriano	19
43403	Fumaca	19
43404	Funil	19
43405	Gavioes	19
43406	Getulândia	19
43407	Glicério	19
43408	Goitacazes	19
43409	Governador Portela	19
43410	Guapimirim	19
43411	Guia de Pacobaiba	19
43412	Ibitiguacu	19
43413	Ibitioca	19
43414	Ibituporanga	19
43415	Iguaba Grande	19
43416	Imbarie	19
43417	Inconfidencia	19
43418	Inhomirim	19
43419	Inoa	19
43420	Ipiabas	19
43421	Ipiiba	19
43422	Ipuca	19
43423	Itabapoana	19
43424	Itaborai	19
43425	Itacurussa	19
43426	Itaguai	19
43427	Itaipava	19
43428	Itaipu	19
43429	Itajara	19
43430	Italva	19
43431	Itambi	19
43432	Itaocara	19
43433	Itaperuna	19
43434	Itatiaiá	19
43435	Jacuecanga	19
43436	Jaguarembe	19
43437	Jamapara	19
43438	Japeri	19
43439	Japuiba	19
43440	Laje do Muriae	19
43441	Laranjais	19
43442	Lidice	19
43443	Lumiar	19
43444	Macabuzinho	19
43445	Macae	19
43446	Macuco	19
43447	Mage	19
43448	Mambucaba	19
43449	Mangaratiba	19
43450	Maniva	19
43451	Manoel Ribeiro	19
43452	Manuel Duarte	19
43453	Marangatu	19
43454	Marica	19
43455	Mendes	19
43456	Mesquita	19
43457	Miguel Pereira	19
43458	Miracema	19
43459	Monera	19
43460	Monjolo	19
43461	Monte Alegre	19
43462	Monte Verde	19
43463	Monumento	19
43464	Morangaba	19
43465	Morro do Coco	19
43466	Morro Grande	19
43467	Mussurepe	19
43468	Natividade	19
43469	Neves	19
43470	Nhunguacu	19
43471	Nilopolis	19
43472	Niteroi	19
43473	Nossa Senhora da Aparecida	19
43474	Nossa Senhora da Penha	19
43475	Nossa Senhora do Amparo	19
43476	Nova Friburgo	19
43477	Nova Iguacu	19
43478	Olinda	19
43479	Ourania	19
43480	Papucaiá	19
43481	Paquequer Pequeno	19
43482	Paracambi	19
43483	Paraiba do Sul	19
43484	Paraiso do Tobias	19
43485	Parãoquena	19
43486	Parapeuna	19
43487	Parati	19
43488	Parati Mirim	19
43489	Passa Três	19
43490	Paty do Alferes	19
43491	Pedra Selada	19
43492	Pedro do Rio	19
43493	Penedo	19
43494	Pentagna	19
43495	Petropolis	19
43496	Piabeta	19
43497	Pião	19
43498	Pinheiral	19
43499	Pipeiras	19
43500	Pirai	19
43501	Pirangai	19
43502	Pirapetinga de Bom Jesus	19
43503	Porciuncula	19
43504	Portela	19
43505	Porto das Caixas	19
43506	Porto Real	19
43507	Porto Velho do Cunha	19
43508	Posse	19
43509	Praiá de Aracatiba	19
43510	Pureza	19
43511	Purilândia	19
43512	Quarteis	19
43513	Quatis	19
43514	Queimados	19
43515	Quissama	19
43516	Raposo	19
43517	Renascenca	19
43518	Resende	19
43519	Retiro do Muriae	19
43520	Rialto	19
43521	Ribeirão de São Joaquim	19
43522	Rio Bonito	19
43523	Rio Claro	19
43524	Rio das Flores	19
43525	Rio das Ostras	19
43526	Rio de Janeiro	19
43527	Riograndina	19
43528	Rosal	19
43529	Sacra Familia do Tingua	19
43530	Salutaris	19
43531	Sambaetiba	19
43532	Sampaio Correia	19
43533	Sana	19
43534	Santa Clara	19
43535	Santa Cruz	19
43536	Santa Cruz da Serra	19
43537	Santa Isabel	19
43538	Santa Isabel do Rio Preto	19
43539	Santa Maria	19
43540	Santa Maria Madalena	19
43541	Santa Rita da Floresta	19
43542	Santanesia	19
43543	Santo Aleixo	19
43544	Santo Amaro de Campos	19
43545	Santo Antonio de Padua	19
43546	Santo Antonio do Imbe	19
43547	Santo Eduardo	19
43548	São Fidelis	19
43549	São Francisco de Itabapoana	19
43550	São Goncalo	19
43551	São João da Barra	19
43552	São João de Meriti	19
43553	São João do Paraiso	19
43554	São João Marcos	19
43555	São Joaquim	19
43556	São José de Uba	19
43557	São José do Ribeirão	19
43558	São José do Turvo	19
43559	São José do Vale do Rio Preto	19
43560	São Mateus	19
43561	São Pedro da Aldeia	19
43562	São Sebastião de Campos	19
43563	São Sebastião do Alto	19
43564	São Sebastião do Paraiba	19
43565	São Sebastião dos Ferreiros	19
43566	São Vicente de Paula	19
43567	Sapucaiá	19
43568	Saquarema	19
43569	Saracuruna	19
43570	Sebastião de Lacerda	19
43571	Seropedica	19
43572	Serrinha	19
43573	Sete Pontes	19
43574	Silva Jardim	19
43575	Sodrelândia	19
43576	Sossego	19
43577	Subaio	19
43578	Sumidouro	19
43579	Surui	19
43580	Taboas	19
43581	Tamoios	19
43582	Tangua	19
43583	Tapera	19
43584	Tarituba	19
43585	Teresopolis	19
43586	Tocos	19
43587	Trajano de Morais	19
43588	Travessão	19
43589	Três Irmaos	19
43590	Três Rios	19
43591	Triunfo	19
43592	Valao do Barro	19
43593	Valenca	19
43594	Vargem Alegre	19
43595	Varre-sai	19
43596	Vassouras	19
43597	Venda das Flores	19
43598	Venda das Pedras	19
43599	Vila da Grama	19
43600	Vila do Frade	19
43601	Vila Muriqui	19
43602	Vila Nova de Campos	19
43603	Visconde de Imbe	19
43604	Volta Redonda	19
43605	Werneck	19
43606	Xerem	19
43607	Acari	20
43608	Açu	20
43609	Afonso Bezerra	20
43610	Água Nova	20
43611	Alexandria	20
43612	Almino Afonso	20
43613	Alto do Rodrigues	20
43614	Angicos	20
43615	Antonio Martins	20
43616	Apodi	20
43617	Areia Branca	20
43618	Ares	20
43619	Baiá Formosa	20
43620	Barão de Serra Branca	20
43621	Barauna	20
43622	Barcelona	20
43623	Belo Horizonte	20
43624	Bento Fernandes	20
43625	Boa Saude	20
43626	Bodo	20
43627	Bom Jesus	20
43628	Brejinho	20
43629	Caicara do Norte	20
43630	Caicara do Rio do Vento	20
43631	Caico	20
43632	Campo Grande	20
43633	Campo Redondo	20
43634	Canguaretama	20
43635	Caraubas	20
43636	Carnauba dos Dantas	20
43637	Carnaubais	20
43638	Cearáa-mirim	20
43639	Cerro Cora	20
43640	Coronel Ezequiel	20
43641	Coronel João Pessoa	20
43642	Córrego de São Mateus	20
43643	Cruzeta	20
43644	Currais Novos	20
43645	Doutor Severiano	20
43646	Encanto	20
43647	Equador	20
43648	Espirito Santo	20
43649	Espirito Santo do Oeste	20
43650	Extremoz	20
43651	Felipe Guerra	20
43652	Fernando Pedroza	20
43653	Firmamento	20
43654	Florania	20
43655	Francisco Dantas	20
43656	Frutuoso Gomes	20
43657	Galinhos	20
43658	Gameleira	20
43659	Goiásaninha	20
43660	Governador Dix-sept Rosado	20
43661	Grossos	20
43662	Guamare	20
43663	Ielmo Marinho	20
43664	Igreja Nova	20
43665	Ipanguacu	20
43666	Ipiranga	20
43667	Ipueira	20
43668	Itaja	20
43669	Itau	20
43670	Jacana	20
43671	Jandaira	20
43672	Janduis	20
43673	Japi	20
43674	Jardim de Angicos	20
43675	Jardim de Piranhas	20
43676	Jardim do Serido	20
43677	João Camara	20
43678	João Dias	20
43679	José da Penha	20
43680	Jucurutu	20
43681	Jundia de Cima	20
43682	Lagoa D'anta	20
43683	Lagoa de Pedras	20
43684	Lagoa de Velhos	20
43685	Lagoa Nova	20
43686	Lagoa Salgada	20
43687	Lajes	20
43688	Lajes Pintadas	20
43689	Lucrecia	20
43690	Luis Gomes	20
43691	Macaiba	20
43692	Macau	20
43693	Major Felipe	20
43694	Major Sales	20
43695	Marcelino Vieira	20
43696	Martins	20
43697	Mata de São Braz	20
43698	Maxaranguape	20
43699	Messias Targino	20
43700	Montanhas	20
43701	Monte Alegre	20
43702	Monte das Gameleiras	20
43703	Mossoro	20
43704	Natal	20
43705	Nisia Floresta	20
43706	Nova Cruz	20
43707	Olho-d'agua Do Borges	20
43708	Ouro Branco	20
43709	Parana	20
43710	Parazinho	20
43711	Parelhas	20
43712	Parnamirim	20
43713	Passa E Fica	20
43714	Passagem	20
43715	Patu	20
43716	Pau dos Ferros	20
43717	Pedra Grande	20
43718	Pedra Preta	20
43719	Pedro Avelino	20
43720	Pedro Velho	20
43721	Pendencias	20
43722	Piloes	20
43723	Poço Branco	20
43724	Portalegre	20
43725	Porto do Mangue	20
43726	Pureza	20
43727	Rafael Fernandes	20
43728	Rafael Godeiro	20
43729	Riacho da Cruz	20
43730	Riacho de Santana	20
43731	Riachuelo	20
43732	Rio do Fogo	20
43733	Rodolfo Fernandes	20
43734	Rosario	20
43735	Ruy Barbosa	20
43736	Salva Vida	20
43737	Santa Cruz	20
43738	Santa Fé	20
43739	Santa Maria	20
43740	Santa Teresa	20
43741	Santana do Matos	20
43742	Santana do Serido	20
43743	Santo Antonio	20
43744	Santo Antonio do Potengi	20
43745	São Bento do Norte	20
43746	São Bento do Trairi	20
43747	São Bernardo	20
43748	São Fernando	20
43749	São Francisco do Oeste	20
43750	São Geraldo	20
43751	São Goncalo do Amarante	20
43752	São João do Sabugi	20
43753	São José da Passagem	20
43754	São José de Mipibu	20
43755	São José do Campestre	20
43756	São José do Serido	20
43757	São Miguel	20
43758	São Miguel de Touros	20
43759	São Paulo do Potengi	20
43760	São Pedro	20
43761	São Rafael	20
43762	São Tome	20
43763	São Vicente	20
43764	Senador Eloi de Souza	20
43765	Senador Georgino Avelino	20
43766	Serra Caiáda	20
43767	Serra da Tapuia	20
43768	Serra de São Bento	20
43769	Serra do Mel	20
43770	Serra Negra do Norte	20
43771	Serrinha	20
43772	Serrinha dos Pintos	20
43773	Severiano Melo	20
43774	Sitio Novo	20
43775	Taboleiro Grande	20
43776	Taipu	20
43777	Tangara	20
43778	Tenente Ananias	20
43779	Tenente Laurentino Cruz	20
43780	Tibau	20
43781	Tibau do Sul	20
43782	Timbauba dos Batistas	20
43783	Touros	20
43784	Trairi	20
43785	Triunfo Potiguar	20
43786	Umarizal	20
43787	Upanema	20
43788	Varzea	20
43789	Venha-ver	20
43790	Vera Cruz	20
43791	Vicosa	20
43792	Vila Flor	20
43793	Abuna	22
43794	Alta Alegre dos Parecis	22
43795	Alta Floresta do Oeste	22
43796	Alto Paraiso	22
43797	Alvorada do Oeste	22
43798	Ariquemes	22
43799	Buritis	22
43800	Cabixi	22
43801	Cacaulândia	22
43802	Cacoal	22
43803	Calama	22
43804	Campo Novo de Rondonia	22
43805	Candeias do Jamari	22
43806	Castanheiras	22
43807	Cerejeiras	22
43808	Chupinguaiá	22
43809	Colorado do Oeste	22
43810	Corumbiara	22
43811	Costa Marques	22
43812	Cujubim	22
43813	Espigao D'oeste	22
43814	Governador Jorge Teixeira	22
43815	Guajara-mirim	22
43816	Jaci Parana	22
43817	Jamari	22
43818	Jaru	22
43819	Ji-parana	22
43820	Machadinho D'oeste	22
43821	Marco Rondon	22
43822	Ministro Andreazza	22
43823	Mirante da Serra	22
43824	Monte Negro	22
43825	Nova Brasilândia D'oeste	22
43826	Nova Mamore	22
43827	Nova União	22
43828	Nova Vida	22
43829	Novo Horizonte do Oeste	22
43830	Ouro Preto do Oeste	22
43831	Parecis	22
43832	Pedras Negras	22
43833	Pimenta Bueno	22
43834	Pimenteiras do Oeste	22
43835	Porto Velho	22
43836	Presidente Medici	22
43837	Primavera de Rondonia	22
43838	Principe da Beira	22
43839	Rio Crespo	22
43840	Riozinho	22
43841	Rolim de Moura	22
43842	Santa Luzia do Oeste	22
43843	São Felipe D'oeste	22
43844	São Francisco do Guapore	22
43845	São Miguel do Guapore	22
43846	Seringueiras	22
43847	Tabajara	22
43848	Teixeiropolis	22
43849	Theobroma	22
43850	Urupa	22
43851	Vale do Anari	22
43852	Vale do Paraiso	22
43853	Vila Extrema	22
43854	Vilhena	22
43855	Vista Alegre do Abuna	22
43856	Alto Alegre	23
43857	Amajari	23
43858	Boa Vista	23
43859	Bonfim	23
43860	Canta	23
43861	Caracarai	23
43862	Caroebe	23
43863	Iracema	23
43864	Mucajai	23
43865	Normândia	23
43866	Pacaraima	23
43867	Rorainopolis	23
43868	São João da Baliza	23
43869	São Luiz	23
43870	Uiramuta	23
43871	Acegua	21
43872	Afonso Rodrigues	21
43873	Água Santa	21
43874	Águas Claras	21
43875	Agudo	21
43876	Ajuricaba	21
43877	Albardão	21
43878	Alecrim	21
43879	Alegrete	21
43880	Alegria	21
43881	Alfredo Brenner	21
43882	Almirante Tamandare	21
43883	Alpestre	21
43884	Alto Alegre	21
43885	Alto da União	21
43886	Alto Feliz	21
43887	Alto Paredão	21
43888	Alto Recreio	21
43889	Alto Uruguai	21
43890	Alvorada	21
43891	Amaral Ferrador	21
43892	Ametista do Sul	21
43893	Andre da Rocha	21
43894	Anta Gorda	21
43895	Antonio Kerpel	21
43896	Antonio Prado	21
43897	Arambare	21
43898	Ararica	21
43899	Aratiba	21
43900	Arco Verde	21
43901	Arco-iris	21
43902	Arroio Canoas	21
43903	Arroio do Meio	21
43904	Arroio do Padre	21
43905	Arroio do Sal	21
43906	Arroio do So	21
43907	Arroio do Tigre	21
43908	Arroio dos Ratos	21
43909	Arroio Grande	21
43910	Arvore So	21
43911	Arvorezinha	21
43912	Atafona	21
43913	Atiacu	21
43914	Augusto Pestana	21
43915	Aurea	21
43916	Avelino Paranhos	21
43917	Azevedo Sodre	21
43918	Bacupari	21
43919	Bage	21
43920	Baliza	21
43921	Balneario Pinhal	21
43922	Banhado do Colegio	21
43923	Barão	21
43924	Barão de Cotegipe	21
43925	Barão do Triunfo	21
43926	Barra do Guarita	21
43927	Barra do Ouro	21
43928	Barra do Quarai	21
43929	Barra do Ribeiro	21
43930	Barra do Rio Azul	21
43931	Barra Funda	21
43932	Barração	21
43933	Barreirinho	21
43934	Barreiro	21
43935	Barro Preto	21
43936	Barro Vermelho	21
43937	Barros Cassal	21
43938	Basilio	21
43939	Bela Vista	21
43940	Beluno	21
43941	Benjamin Constant do Sul	21
43942	Bento Gonçalves	21
43943	Bexiga	21
43944	Boa Esperanca	21
43945	Boa Vista	21
43946	Boa Vista das Missoes	21
43947	Boa Vista do Burica	21
43948	Boa Vista do Cadeado	21
43949	Boa Vista do Incra	21
43950	Boa Vista do Sul	21
43951	Boca do Monte	21
43952	Boi Preto	21
43953	Bojuru	21
43954	Bom Jardim	21
43955	Bom Jesus	21
43956	Bom Principio	21
43957	Bom Progresso	21
43958	Bom Retiro	21
43959	Bom Retiro do Guaiba	21
43960	Bom Retiro do Sul	21
43961	Bonito	21
43962	Boqueirão	21
43963	Boqueirão do Leão	21
43964	Borore	21
43965	Bossoroca	21
43966	Botucarai	21
43967	Braga	21
43968	Brochier	21
43969	Buriti	21
43970	Butia	21
43971	Butias	21
43972	Cacapava do Sul	21
43973	Cacequi	21
43974	Cachoeira do Sul	21
43975	Cachoeirinha	21
43976	Cacique Doble	21
43977	Cadorna	21
43978	Caibate	21
43979	Caicara	21
43980	Camaqua	21
43981	Camargo	21
43982	Cambara do Sul	21
43983	Camobi	21
43984	Campestre Baixo	21
43985	Campestre da Serra	21
43986	Campina das Missoes	21
43987	Campina Redonda	21
43988	Campinas	21
43989	Campinas do Sul	21
43990	Campo Bom	21
43991	Campo Branco	21
43992	Campo do Meio	21
43993	Campo Novo	21
43994	Campo Santo	21
43995	Campo Seco	21
43996	Campo Vicente	21
43997	Campos Borges	21
43998	Candelaria	21
43999	Candido Freire	21
44000	Candido Godoi	21
44001	Candiota	21
44002	Canela	21
44003	Cangucu	21
44004	Canhembora	21
44005	Canoas	21
44006	Canudos	21
44007	Capane	21
44008	Capão Bonito	21
44009	Capão Comprido	21
44010	Capão da Canoa	21
44011	Capão da Porteira	21
44012	Capão do Cedro	21
44013	Capão do Cipo	21
44014	Capão do Leão	21
44015	Capela de Santana	21
44016	Capela Velha	21
44017	Capinzal	21
44018	Capitão	21
44019	Capivari do Sul	21
44020	Capivarita	21
44021	Capo-ere	21
44022	Capoeira Grande	21
44023	Caraa	21
44024	Caraja Seival	21
44025	Carazinho	21
44026	Carlos Barbosa	21
44027	Carlos Gomes	21
44028	Carovi	21
44029	Casca	21
44030	Cascata	21
44031	Caseiros	21
44032	Castelinho	21
44033	Catuipe	21
44034	Cavajureta	21
44035	Cavera	21
44036	Caxias do Sul	21
44037	Cazuza Ferreira	21
44038	Cedro Marcado	21
44039	Centenario	21
44040	Centro Linha Brasil	21
44041	Cerrito	21
44042	Cerrito Alegre	21
44043	Cerrito do Ouro Ou Vila do Cerrito	21
44044	Cerro Alto	21
44045	Cerro Branco	21
44046	Cerro Claro	21
44047	Cerro do Martins	21
44048	Cerro do Roque	21
44049	Cerro Grande	21
44050	Cerro Grande do Sul	21
44051	Cerro Largo	21
44052	Chapada	21
44053	Charqueadas	21
44054	Charrua	21
44055	Chiapetta	21
44056	Chicoloma	21
44057	Chimarrão	21
44058	Chorão	21
44059	Chui	21
44060	Chuvisca	21
44061	Cidreira	21
44062	Cinquentenario	21
44063	Ciriaco	21
44064	Clara	21
44065	Clemente Argolo	21
44066	Coimbra	21
44067	Colinas	21
44068	Colonia das Almas	21
44069	Colonia Medeiros	21
44070	Colonia Municipal	21
44071	Colonia Nova	21
44072	Colonia São João	21
44073	Colonia Z-null	21
44074	Coloninha	21
44075	Colorado	21
44076	Comandai	21
44077	Condor	21
44078	Consolata	21
44079	Constantina	21
44080	Coqueiro Baixo	21
44081	Coqueiros do Sul	21
44082	Cordilheira	21
44083	Coroados	21
44084	Coronel Barros	21
44085	Coronel Bicaco	21
44086	Coronel Finzito	21
44087	Coronel Pilar	21
44088	Coronel Teixeira	21
44089	Cortado	21
44090	Costa da Cadeia	21
44091	Costão	21
44092	Cotipora	21
44093	Coxilha	21
44094	Coxilha Grande	21
44095	Cr-null	21
44096	Crissiumal	21
44097	Cristal	21
44098	Cristal do Sul	21
44099	Criuva	21
44100	Cruz Alta	21
44101	Cruz Altense	21
44102	Cruzeiro	21
44103	Cruzeiro do Sul	21
44104	Curral Alto	21
44105	Curumim	21
44106	Daltro Filho	21
44107	Dario Lassance	21
44108	David Canabarro	21
44109	Delfina	21
44110	Deodoro	21
44111	Deposito	21
44112	Derrubadas	21
44113	Dezesseis de Novembro	21
44114	Dilermando de Aguiar	21
44115	Divino	21
44116	Dois Irmaos	21
44117	Dois Irmaos das Missoes	21
44118	Dois Lajeados	21
44119	Dom Diogo	21
44120	Dom Feliciano	21
44121	Dom Pedrito	21
44122	Dom Pedro de Alcantara	21
44123	Dona Francisca	21
44124	Dona Otilia	21
44125	Dourado	21
44126	Doutor Bozano	21
44127	Doutor Edgardo Pereira Velho	21
44128	Doutor Mauricio Cardoso	21
44129	Doutor Ricardo	21
44130	Eldorado do Sul	21
44131	Eletra	21
44132	Encantado	21
44133	Encruzilhada	21
44134	Encruzilhada do Sul	21
44135	Engenho Velho	21
44136	Entre Rios do Sul	21
44137	Entre-ijuis	21
44138	Entrepelado	21
44139	Erebango	21
44140	Erechim	21
44141	Ernestina	21
44142	Ernesto Alves	21
44143	Erval Grande	21
44144	Erval Seco	21
44145	Erveiras	21
44146	Esmeralda	21
44147	Esperanca do Sul	21
44148	Espigão	21
44149	Espigao Alto	21
44150	Espinilho Grande	21
44151	Espirito Santo	21
44152	Espumoso	21
44153	Esquina Araujo	21
44154	Esquina Bom Sucesso	21
44155	Esquina Gaucha	21
44156	Esquina Ipiranga	21
44157	Esquina Piratini	21
44158	Estação	21
44159	Estancia Grande	21
44160	Estancia Velha	21
44161	Esteio	21
44162	Esteira	21
44163	Estreito	21
44164	Estrela	21
44165	Estrela Velha	21
44166	Eugenio de Castro	21
44167	Evangelista	21
44168	Fagundes Varela	21
44169	Fão	21
44170	Faria Lemos	21
44171	Farinhas	21
44172	Farrapos	21
44173	Farroupilha	21
44174	Faxinal	21
44175	Faxinal do Soturno	21
44176	Faxinalzinho	21
44177	Fazenda Fialho	21
44178	Fazenda Souza	21
44179	Fazenda Vilanova	21
44180	Feliz	21
44181	Ferreira	21
44182	Flores da Cunha	21
44183	Floresta	21
44184	Floriano Peixoto	21
44185	Florida	21
44186	Fontoura Xavier	21
44187	Formigueiro	21
44188	Formosa	21
44189	Forninho	21
44190	Forquetinha	21
44191	Fortaleza dos Valos	21
44192	Frederico Westphalen	21
44193	Frei Sebastião	21
44194	Freire	21
44195	Garibaldi	21
44196	Garibaldina	21
44197	Garruchos	21
44198	Gaurama	21
44199	General Camara	21
44200	Gentil	21
44201	Getulio Vargas	21
44202	Girua	21
44203	Gloria	21
44204	Glorinha	21
44205	Goiáso-en	21
44206	Gramado	21
44207	Gramado dos Loureiros	21
44208	Gramado São Pedro	21
44209	Gramado Xavier	21
44210	Gravatai	21
44211	Guabiju	21
44212	Guaiba	21
44213	Guajuviras	21
44214	Guapore	21
44215	Guarani das Missoes	21
44216	Guassupi	21
44217	Harmonia	21
44218	Herval	21
44219	Herveiras	21
44220	Hidraulica	21
44221	Horizontina	21
44222	Hulha Negra	21
44223	Humaita	21
44224	Ibarama	21
44225	Ibare	21
44226	Ibiaca	21
44227	Ibiraiáras	21
44228	Ibirapuita	21
44229	Ibiruba	21
44230	Igrejinha	21
44231	Ijucapirama	21
44232	Ijui	21
44233	Ilha dos Marinheiros	21
44234	Ilopolis	21
44235	Imbe	21
44236	Imigrante	21
44237	Independencia	21
44238	Inhacora	21
44239	Ipe	21
44240	Ipiranga	21
44241	Ipiranga do Sul	21
44242	Ipuacu	21
44243	Irai	21
44244	Irui	21
44245	Itaara	21
44246	Itacolomi	21
44247	Itacurubi	21
44248	Itai	21
44249	Itaimbezinho	21
44250	Itão	21
44251	Itapua	21
44252	Itapuca	21
44253	Itaqui	21
44254	Itati	21
44255	Itatiba do Sul	21
44256	Itauba	21
44257	Ituim	21
44258	Ivai	21
44259	Ivora	21
44260	Ivoti	21
44261	Jaboticaba	21
44262	Jacuizinho	21
44263	Jacutinga	21
44264	Jaguarão	21
44265	Jaguarete	21
44266	Jaguari	21
44267	Jansen	21
44268	Jaquirana	21
44269	Jari	21
44270	Jazidas Ou Capela São Vicente	21
44271	João Arregui	21
44272	João Rodrigues	21
44273	Joca Tavares	21
44274	Joia	21
44275	José Otavio	21
44276	Jua	21
44277	Julio de Castilhos	21
44278	Lagoa Bonita	21
44279	Lagoa dos Três Cantos	21
44280	Lagoa Vermelha	21
44281	Lagoão	21
44282	Lajeado	21
44283	Lajeado Bonito	21
44284	Lajeado Cerne	21
44285	Lajeado do Bugre	21
44286	Lajeado Grande	21
44287	Lara	21
44288	Laranjeira	21
44289	Lava-pes	21
44290	Lavras do Sul	21
44291	Leonel Rocha	21
44292	Liberato Salzano	21
44293	Lindolfo Collor	21
44294	Linha Comprida	21
44295	Linha Nova	21
44296	Linha Vitória	21
44297	Loreto	21
44298	Macambara	21
44299	Machadinho	21
44300	Magisterio	21
44301	Mampituba	21
44302	Manchinha	21
44303	Mangueiras	21
44304	Manoel Viana	21
44305	Maquine	21
44306	Marata	21
44307	Marau	21
44308	Marcelino Ramos	21
44309	Marcorama	21
44310	Mariana Pimentel	21
44311	Mariano Moro	21
44312	Mariante	21
44313	Mariapolis	21
44314	Marques de Souza	21
44315	Mata	21
44316	Matarazzo	21
44317	Mato Castelhano	21
44318	Mato Grande	21
44319	Mato Leitão	21
44320	Mato Queimado	21
44321	Maua	21
44322	Maximiliano de Almeida	21
44323	Medianeira	21
44324	Minas do Leão	21
44325	Miraguai	21
44326	Miraguaiá	21
44327	Mirim	21
44328	Montauri	21
44329	Monte Alegre	21
44330	Monte Alegre dos Campos	21
44331	Monte Alverne	21
44332	Monte Belo do Sul	21
44333	Monte Bonito	21
44334	Montenegro	21
44335	Mormaco	21
44336	Morrinhos	21
44337	Morrinhos do Sul	21
44338	Morro Alto	21
44339	Morro Azul	21
44340	Morro Redondo	21
44341	Morro Reuter	21
44342	Morungava	21
44343	Mostardas	21
44344	Mucum	21
44345	Muitos Capoes	21
44346	Muliterno	21
44347	Nao-me-toque	21
44348	Nazaré	21
44349	Nicolau Vergueiro	21
44350	Nonoai	21
44351	Nossa Senhora Aparecida	21
44352	Nossa Senhora da Conceição	21
44353	Nova Alvorada	21
44354	Nova Araca	21
44355	Nova Bassano	21
44356	Nova Boa Vista	21
44357	Nova Brescia	21
44358	Nova Candelaria	21
44359	Nova Esperanca do Sul	21
44360	Nova Hartz	21
44361	Nova Milano	21
44362	Nova Padua	21
44363	Nova Palma	21
44364	Nova Petropolis	21
44365	Nova Prata	21
44366	Nova Ramada	21
44367	Nova Roma do Sul	21
44368	Nova Santa Rita	21
44369	Nova Sardenha	21
44370	Novo Barreiro	21
44371	Novo Cabrais	21
44372	Novo Hamburgo	21
44373	Novo Horizonte	21
44374	Novo Machado	21
44375	Novo Planalto	21
44376	Novo Tiradentes	21
44377	Oliva	21
44378	Oralina	21
44379	Osorio	21
44380	Osvaldo Cruz	21
44381	Osvaldo Kroeff	21
44382	Otavio Rocha	21
44383	Pacheca	21
44384	Padilha	21
44385	Padre Gonzales	21
44386	Paim Filho	21
44387	Palmares do Sul	21
44388	Palmas	21
44389	Palmeira das Missoes	21
44390	Palmitinho	21
44391	Pampeiro	21
44392	Panambi	21
44393	Pantano Grande	21
44394	Parai	21
44395	Paraiso do Sul	21
44396	Pareci Novo	21
44397	Parobe	21
44398	Passa Sete	21
44399	Passinhos	21
44400	Passo Burmann	21
44401	Passo da Areia	21
44402	Passo da Caveira	21
44403	Passo das Pedras	21
44404	Passo do Adão	21
44405	Passo do Sabão	21
44406	Passo do Sobrado	21
44407	Passo Fundo	21
44408	Passo Novo	21
44409	Passo Raso	21
44410	Paulo Bento	21
44411	Pavão	21
44412	Paverama	21
44413	Pedras Altas	21
44414	Pedreiras	21
44415	Pedro Garcia	21
44416	Pedro Osorio	21
44417	Pedro Paiva	21
44418	Pejucara	21
44419	Pelotas	21
44420	Picada Cafe	21
44421	Pinhal	21
44422	Pinhal Alto	21
44423	Pinhal da Serra	21
44424	Pinhal Grande	21
44425	Pinhalzinho	21
44426	Pinheirinho do Vale	21
44427	Pinheiro Machado	21
44428	Pinheiro Marcado	21
44429	Pinto Bandeira	21
44430	Pirai	21
44431	Pirapo	21
44432	Piratini	21
44433	Pitanga	21
44434	Planalto	21
44435	Plano Alto	21
44436	Poço das Antas	21
44437	Poligono do Erval	21
44438	Polo Petroquimico de Triunfo	21
44439	Pontão	21
44440	Ponte Preta	21
44441	Portão	21
44442	Porto Alegre	21
44443	Porto Batista	21
44444	Porto Lucena	21
44445	Porto Maua	21
44446	Porto Vera Cruz	21
44447	Porto Xavier	21
44448	Pouso Novo	21
44449	Povo Novo	21
44450	Povoado Tozzo	21
44451	Pranchada	21
44452	Pratos	21
44453	Presidente Lucena	21
44454	Progresso	21
44455	Protasio Alves	21
44456	Pulador	21
44457	Putinga	21
44458	Quarai	21
44459	Quaraim	21
44460	Quatro Irmaos	21
44461	Quevedos	21
44462	Quilombo	21
44463	Quinta	21
44464	Quintão	21
44465	Quinze de Novembro	21
44466	Quiteria	21
44467	Rancho Velho	21
44468	Redentora	21
44469	Refugiado	21
44470	Relvado	21
44471	Restinga Seca	21
44472	Rincão de São Pedro	21
44473	Rincão Del Rei	21
44474	Rincão do Cristovao Pereira	21
44475	Rincão do Meio	21
44476	Rincão do Segredo	21
44477	Rincão Doce	21
44478	Rincão dos Kroeff	21
44479	Rincão dos Mendes	21
44480	Rincão Vermelho	21
44481	Rio Azul	21
44482	Rio Branco	21
44483	Rio da Ilha	21
44484	Rio dos Indios	21
44485	Rio Grande	21
44486	Rio Pardinho	21
44487	Rio Pardo	21
44488	Rio Telha	21
44489	Rio Tigre	21
44490	Rio Toldo	21
44491	Riozinho	21
44492	Roca Sales	21
44493	Rodeio Bonito	21
44494	Rolador	21
44495	Rolante	21
44496	Rolantinho da Figueira	21
44497	Ronda Alta	21
44498	Rondinha	21
44499	Roque Gonzales	21
44500	Rosario	21
44501	Rosario do Sul	21
44502	Sagrada Familia	21
44503	Saica	21
44504	Saldanha Marinho	21
44505	Saltinho	21
44506	Salto	21
44507	Salto do Jacui	21
44508	Salvador das Missoes	21
44509	Salvador do Sul	21
44510	Sananduva	21
44511	Sant Auta	21
44512	Santa Barbara	21
44513	Santa Barbara do Sul	21
44514	Santa Catarina	21
44515	Santa Cecilia	21
44516	Santa Clara do Ingai	21
44517	Santa Clara do Sul	21
44518	Santa Cristina	21
44519	Santa Cruz	21
44520	Santa Cruz da Concordia	21
44521	Santa Cruz do Sul	21
44522	Santa Flora	21
44523	Santa Ines	21
44524	Santa Izabel do Sul	21
44525	Santa Lucia	21
44526	Santa Lucia do Piai	21
44527	Santa Luiza	21
44528	Santa Luzia	21
44529	Santa Maria	21
44530	Santa Maria do Herval	21
44531	Santa Rita do Sul	21
44532	Santa Rosa	21
44533	Santa Silvana	21
44534	Santa Teresinha	21
44535	Santa Tereza	21
44536	Santa Vitória do Palmar	21
44537	Santana	21
44538	Santana da Boa Vista	21
44539	Santana do Livramento	21
44540	Santiago	21
44541	Santo Amaro do Sul	21
44542	Santo Angelo	21
44543	Santo Antonio	21
44544	Santo Antonio da Patrulha	21
44545	Santo Antonio das Missoes	21
44546	Santo Antonio de Castro	21
44547	Santo Antonio do Bom Retiro	21
44548	Santo Antonio do Palma	21
44549	Santo Antonio do Planalto	21
44550	Santo Augusto	21
44551	Santo Cristo	21
44552	Santo Expedito do Sul	21
44553	Santo Inacio	21
44554	São Bento	21
44555	São Bom Jesus	21
44556	São Borja	21
44557	São Carlos	21
44558	São Domingos do Sul	21
44559	São Francisco	21
44560	São Francisco de Assis	21
44561	São Francisco de Paula	21
44562	São Gabriel	21
44563	São Jeronimo	21
44564	São João	21
44565	São João Batista	21
44566	São João Bosco	21
44567	São João da Urtiga	21
44568	São João do Polesine	21
44569	São Jorge	21
44570	São José	21
44571	São José da Gloria	21
44572	São José das Missoes	21
44573	São José de Castro	21
44574	São José do Centro	21
44575	São José do Herval	21
44576	São José do Hortencio	21
44577	São José do Inhacora	21
44578	São José do Norte	21
44579	São José do Ouro	21
44580	São José dos Ausentes	21
44581	São Leopoldo	21
44582	São Lourenço das Missoes	21
44583	São Lourenço do Sul	21
44584	São Luis Rei	21
44585	São Luiz	21
44586	São Luiz Gonzaga	21
44587	São Manuel	21
44588	São Marcos	21
44589	São Martinho	21
44590	São Martinho da Serra	21
44591	São Miguel	21
44592	São Miguel das Missoes	21
44593	São Nicolau	21
44594	São Paulo	21
44595	São Paulo das Missoes	21
44596	São Pedro	21
44597	São Pedro da Serra	21
44598	São Pedro do Butia	21
44599	São Pedro do Iraxim	21
44600	São Pedro do Sul	21
44601	São Roque	21
44602	São Sebastião	21
44603	São Sebastião do Cai	21
44604	São Sepe	21
44605	São Simão	21
44606	São Valentim	21
44607	São Valentim do Sul	21
44608	São Valerio do Sul	21
44609	São Vendelino	21
44610	São Vicente do Sul	21
44611	Sapiranga	21
44612	Sapucaiá do Sul	21
44613	Sarandi	21
44614	Scharlau	21
44615	Seberi	21
44616	Seca	21
44617	Sede Aurora	21
44618	Sede Nova	21
44619	Segredo	21
44620	Seival	21
44621	Selbach	21
44622	Senador Salgado Filho	21
44623	Sentinela do Sul	21
44624	Serafim Schmidt	21
44625	Serafina Correa	21
44626	Serio	21
44627	Serra dos Gregorios	21
44628	Serrinha	21
44629	Sertão	21
44630	Sertao Santana	21
44631	Sertaozinho	21
44632	Sete de Setembro	21
44633	Sete Lagoas	21
44634	Severiano de Almeida	21
44635	Silva Jardim	21
44636	Silveira	21
44637	Silveira Martins	21
44638	Sinimbu	21
44639	Sirio	21
44640	Sitio Gabriel	21
44641	Sobradinho	21
44642	Soledade	21
44643	Souza Ramos	21
44644	Suspiro	21
44645	Tabai	21
44646	Tabajara	21
44647	Taim	21
44648	Tainhas	21
44649	Tamandua	21
44650	Tanque	21
44651	Tapejara	21
44652	Tapera	21
44653	Tapes	21
44654	Taquara	21
44655	Taquari	21
44656	Taquarichim	21
44657	Taquarucu do Sul	21
44658	Tavares	21
44659	Tenente Portela	21
44660	Terra de Areia	21
44661	Tesouras	21
44662	Teutonia	21
44663	Tiaraju	21
44664	Timbauva	21
44665	Tiradentes do Sul	21
44666	Toropi	21
44667	Toroqua	21
44668	Torquato Severo	21
44669	Torres	21
44670	Torrinhas	21
44671	Touro Passo	21
44672	Tramandai	21
44673	Travesseiro	21
44674	Trentin	21
44675	Três Arroios	21
44676	Três Barras	21
44677	Três Cachoeiras	21
44678	Três Coroas	21
44679	Três de Maio	21
44680	Três Forquilhas	21
44681	Três Palmeiras	21
44682	Três Passos	21
44683	Três Vendas	21
44684	Trindade do Sul	21
44685	Triunfo	21
44686	Tronqueiras	21
44687	Tucunduva	21
44688	Tuiuti	21
44689	Tunas	21
44690	Tunel Verde	21
44691	Tupanci do Sul	21
44692	Tupancireta	21
44693	Tupancy Ou Vila Block	21
44694	Tupandi	21
44695	Tupantuba	21
44696	Tuparendi	21
44697	Tupi Silveira	21
44698	Tupinamba	21
44699	Turucu	21
44700	Turvinho	21
44701	Ubiretama	21
44702	Umbu	21
44703	União da Serra	21
44704	Unistalda	21
44705	Uruguaiána	21
44706	Vacacai	21
44707	Vacaria	21
44708	Valdastico	21
44709	Vale do Rio Cai	21
44710	Vale do Sol	21
44711	Vale Real	21
44712	Vale Veneto	21
44713	Vale Verde	21
44714	Vanini	21
44715	Venancio Aires	21
44716	Vera Cruz	21
44717	Veranopolis	21
44718	Vertentes	21
44719	Vespasiano Correa	21
44720	Viadutos	21
44721	Viamão	21
44722	Vicente Dutra	21
44723	Victor Graeff	21
44724	Vila Bender	21
44725	Vila Cruz	21
44726	Vila Flores	21
44727	Vila Langaro	21
44728	Vila Laranjeira	21
44729	Vila Maria	21
44730	Vila Nova do Sul	21
44731	Vila Rica	21
44732	Vila Seca	21
44733	Vila Turvo	21
44734	Vista Alegre	21
44735	Vista Alegre do Prata	21
44736	Vista Gaucha	21
44737	Vitória	21
44738	Vitória das Missoes	21
44739	Volta Alegre	21
44740	Volta Fechada	21
44741	Volta Grande	21
44742	Xadrez	21
44743	Xangri-la	21
44744	Xingu	21
44745	Abdon Batista	24
44746	Abelardo Luz	24
44747	Agrolândia	24
44748	Agronomica	24
44749	Água Doce	24
44750	Águas Brancas	24
44751	Águas Claras	24
44752	Águas de Chapeco	24
44753	Águas Frias	24
44754	Águas Mornas	24
44755	Aguti	24
44756	Aiure	24
44757	Alfredo Wagner	24
44758	Alto Alegre	24
44759	Alto Bela Vista	24
44760	Alto da Serra	24
44761	Anchieta	24
44762	Angelina	24
44763	Anita Garibaldi	24
44764	Anitapolis	24
44765	Anta Gorda	24
44766	Antonio Carlos	24
44767	Apiuna	24
44768	Arabuta	24
44769	Araquari	24
44770	Ararangua	24
44771	Armazem	24
44772	Arnopolis	24
44773	Arroio Trinta	24
44774	Arvoredo	24
44775	Ascurra	24
44776	Atalanta	24
44777	Aterrado Torto	24
44778	Aurora	24
44779	Azambuja	24
44780	Baiá Alta	24
44781	Balneario Arroio do Silva	24
44782	Balneario Barra do Sul	24
44783	Balneario Camboriu	24
44784	Balneario Gaivota	24
44785	Balneario Morro dos Conventos	24
44786	Bandeirante	24
44787	Barra Bonita	24
44788	Barra Clara	24
44789	Barra da Lagoa	24
44790	Barra da Prata	24
44791	Barra Fria	24
44792	Barra Grande	24
44793	Barra Velha	24
44794	Barreiros	24
44795	Barro Branco	24
44796	Bateias de Baixo	24
44797	Bela Vista	24
44798	Bela Vista do Sul	24
44799	Bela Vista do Toldo	24
44800	Belmonte	24
44801	Benedito Novo	24
44802	Biguacu	24
44803	Blumenau	24
44804	Bocaina do Sul	24
44805	Boiteuxburgo	24
44806	Bom Jardim da Serra	24
44807	Bom Jesus	24
44808	Bom Jesus do Oeste	24
44809	Bom Retiro	24
44810	Bom Sucesso	24
44811	Bombinhas	24
44812	Botuvera	24
44813	Braco do Norte	24
44814	Braco do Trombudo	24
44815	Brunopolis	24
44816	Brusque	24
44817	Cacador	24
44818	Cachoeira de Fatima	24
44819	Cachoeira do Bom Jesus	24
44820	Caibi	24
44821	Calmon	24
44822	Camboriu	24
44823	Cambuinzal	24
44824	Campeche	24
44825	Campinas	24
44826	Campo Alegre	24
44827	Campo Belo do Sul	24
44828	Campo Ere	24
44829	Campos Novos	24
44830	Canasvieiras	24
44831	Canelinha	24
44832	Canoas	24
44833	Canoinhas	24
44834	Capão Alto	24
44835	Capinzal	24
44836	Capivari de Baixo	24
44837	Caraiba	24
44838	Catanduvas	24
44839	Catuira	24
44840	Caxambu do Sul	24
44841	Cedro Alto	24
44842	Celso Ramos	24
44843	Cerro Negro	24
44844	Chapadao do Lageado	24
44845	Chapeco	24
44846	Claraiba	24
44847	Cocal do Sul	24
44848	Colonia Santa Tereza	24
44849	Colonia Santana	24
44850	Concordia	24
44851	Cordilheira Alta	24
44852	Coronel Freitas	24
44853	Coronel Martins	24
44854	Correia Pinto	24
44855	Corupa	24
44856	Criciuma	24
44857	Cunha Pora	24
44858	Cunhatai	24
44859	Curitibanos	24
44860	Dal Pai	24
44861	Dalbergia	24
44862	Descanso	24
44863	Dionisio Cerqueira	24
44864	Dona Emma	24
44865	Doutor Pedrinho	24
44866	Engenho Velho	24
44867	Enseada de Brito	24
44868	Entre Rios	24
44869	Ermo	24
44870	Erval Velho	24
44871	Espinilho	24
44872	Estação Cocal	24
44873	Faxinal dos Guedes	24
44874	Fazenda Zandavalli	24
44875	Felipe Schmidt	24
44876	Figueira	24
44877	Flor do Sertão	24
44878	Florianopolis	24
44879	Formosa do Sul	24
44880	Forquilhinha	24
44881	Fragosos	24
44882	Fraiburgo	24
44883	Frederico Wastner	24
44884	Frei Rogerio	24
44885	Galvão	24
44886	Garcia	24
44887	Garopaba	24
44888	Garuva	24
44889	Gaspar	24
44890	Goiáso-en	24
44891	Governador Celso Ramos	24
44892	Grão Para	24
44893	Grapia	24
44894	Gravatal	24
44895	Guabiruba	24
44896	Guaporanga	24
44897	Guaraciaba	24
44898	Guaramirim	24
44899	Guaruja do Sul	24
44900	Guata	24
44901	Guatambu	24
44902	Hercilio Luz	24
44903	Herciliopolis	24
44904	Herval D'oeste	24
44905	Ibiam	24
44906	Ibicare	24
44907	Ibicui	24
44908	Ibirama	24
44909	Icara	24
44910	Ilhota	24
44911	Imarui	24
44912	Imbituba	24
44913	Imbuia	24
44914	Indaiál	24
44915	Indios	24
36668	Acrelândia	1
36669	Assis Brasil	1
36670	Brasileia	1
36671	Bujari	1
36672	Capixaba	1
36673	Cruzeiro do Sul	1
36674	Epitaciolândia	1
36675	Feijó	1
36676	Jordão	1
36677	Mâncio Lima	1
36678	Manoel Urbano	1
36679	Marechal Thaumaturgo	1
36680	Placido de Castro	1
36681	Porto Acre	1
36682	Porto Walter	1
36683	Rio Branco	1
36684	Rodrigues Alves	1
36685	Santa Rosa	1
36686	Sena Madureira	1
36687	Senador Guiomard	1
36688	Tarauacá	1
36689	Xapuri	1
36690	Água Branca	2
36691	Alazão	2
36692	Alecrim	2
36693	Anadia	2
36694	Anel	2
36695	Anum Novo	2
36696	Anum Velho	2
36697	Arapiraca	2
36698	Atalaiá	2
36699	Baixa da Onça	2
36700	Baixa do Capim	2
36701	Balsamo	2
36702	Bananeiras	2
36703	Barra de Santo Antonio	2
36704	Barra de São Miguel	2
36705	Barra do Bonifácio	2
36706	Barra Grande	2
36707	Batalha	2
36708	Batingas	2
36709	Belem	2
36710	Belo Monte	2
36711	Boa Sorte	2
36712	Boa Vista	2
36713	Boca da Mata	2
36714	Bom Jardim	2
36715	Bonifácio	2
36716	Branquinha	2
36717	Cacimbinhas	2
36718	Cajarana	2
36719	Cajueiro	2
36720	Caldeirões de Cima	2
36721	Camadanta	2
36722	Campestre	2
36723	Campo Alegre	2
36724	Campo Grande	2
36725	Canaã	2
36726	Canafístula	2
36727	Canapi	2
36728	Canastra	2
36729	Cangandu	2
36730	Capela	2
36731	Carneiros	2
36732	Carrasco	2
36733	Chã Preta	2
36734	Coite do Noia	2
36735	Colonia Leopoldina	2
36736	Coqueiro Seco	2
36737	Coruripe	2
36738	Coruripe da Cal	2
36739	Craíbas	2
36740	Delmiro Gouveia	2
36741	Dois Riachos	2
36742	Entremontes	2
36743	Estrela de Alagoas	2
36744	Feira Grande	2
36745	Feliz Deserto	2
36746	Fernão Velho	2
36747	Flexeiras	2
36748	Floriano Peixoto	2
36749	Gaspar	2
36750	Girau do Ponciano	2
36751	Ibateguara	2
36752	Igaci	2
36753	Igreja Nova	2
36754	Inhapi	2
36755	Acare dos Homens	2
36756	Jacuípe	2
36757	Japaratinga	2
36758	Jaramataiá	2
36759	Jenipapo	2
36760	Joaquim Gomes	2
36761	Jundiá	2
36762	Junqueiro	2
36763	Lagoa da Areia	2
36764	Lagoa da Canoa	2
36765	Lagoa da Pedra	2
36766	Lagoa Dantas	2
36767	Lagoa do Caldeirão	2
36768	Lagoa do Canto	2
36769	Lagoa do Exu	2
36770	Lagoa do Rancho	2
36771	Lajes do Caldeirão	2
36772	Laranjal	2
36773	Limoeiro de Anadia	2
36774	Maceió	2
36775	Major Isidoro	2
36776	Mar Vermelho	2
36777	Maragogi	2
36778	Maravilha	2
36779	Marechal Deodoro	2
36780	Maribondo	2
36781	Massaranduba	2
36782	Mata Grande	2
36783	Matriz de Camaragibe	2
36784	Messias	2
36785	Minador do Negrão	2
36786	Monteirópolis	2
36787	Moreira	2
36788	Munguba	2
36789	Murici	2
36790	Novo Lino	2
36791	Olho D Água Grande	2
36792	Olho D'Água Das Flores	2
36793	Olho D'Água De Cima	2
36794	Olho D'Água Do Casado	2
36795	Olho D'Água Dos Dandanhas	2
36796	Olivença	2
36797	Ouro Branco	2
36798	Palestina	2
36799	Palmeira de Fora	2
36800	Palmeira dos Indios	2
36801	Pão de Açúcar	2
36802	Pariconha	2
36803	Paripueira	2
36804	Passo de Camaragibe	2
36805	Pau D'arco	2
36806	Pau Ferro	2
36807	Paulo Jacinto	2
36808	Penedo	2
36809	Piaçabuçu	2
36810	Pilar	2
36811	Pindoba	2
36812	Piranhas	2
36813	Poção	2
36814	Poço da Pedra	2
36815	Poço das Trincheiras	2
36816	Porto Calvo	2
36817	Porto de Pedras	2
36818	Porto Real do Colégio	2
36819	Poxim	2
36820	Quebrângulo	2
36821	Riacho do Sertão	2
36822	Riacho Fundo de Cima	2
36823	Rio Largo	2
36824	Rocha Cavalcante	2
36825	Roteiro	2
36826	Santa Efigênia	2
36827	Santa Luzia do Norte	2
36828	Santana do Ipanema	2
36829	Santana do Mundaú	2
36830	Santo Antonio	2
36831	São Brás	2
36832	São José da Laje	2
36833	São José da Tapera	2
36834	São Luis do Quitunde	2
36835	São Miguel dos Campos	2
36836	São Miguel dos Milagres	2
36837	São Sebastião	2
36838	Sapucaiá	2
36839	Satuba	2
36840	Senador Rui Palmeira	2
36841	Serra da Mandioca	2
36842	Serra do São José	2
36843	Taboleiro do Pinto	2
36844	Taboquinha	2
36845	Tanque D'arca	2
36846	Taquarana	2
36847	Tatuamunha	2
36848	Teotônio Vilela	2
36849	Traipu	2
36850	União dos Palmares	2
36851	Usina Camaçari	2
36852	Vicosa	2
36853	Vila Aparecida	2
36854	Vila São Francisco	2
36855	Alvarães	4
36856	Amatari	4
36857	Amaturá	4
36858	Anamã	4
36859	Anori	4
36860	Apuí	4
36861	Ariau	4
36862	Atalaiá do Norte	4
36863	Augusto Montenegro	4
36864	Autazes	4
36865	Axinim	4
36866	Badajos	4
36867	Balbina	4
36868	Barcelos	4
36869	Barreirinha	4
36870	Benjamin Constant	4
36871	Beruri	4
36872	Boa Vista do Ramos	4
36873	Boca do Acre	4
36874	Borba	4
36875	Caapiranga	4
36876	Cametá	4
36877	Canuma	4
36878	Canutama	4
36879	Carauari	4
36880	Careiro	4
36881	Careiro da Várzea	4
36882	Carvoeiro	4
36883	Coari	4
36884	Codajas	4
36885	Cucui	4
36886	Eirunepé	4
36887	Envira	4
36888	Floriano Peixoto	4
36889	Fonte Boa	4
36890	Freguesia do Andira	4
36891	Guajará	4
36892	Humaitá	4
36893	Iauarete	4
36894	Icana	4
36895	Ipixuna	4
36896	Iranduba	4
36897	Itacoatiara	4
36898	Itamarati	4
36899	Itapiranga	4
36900	Japurá	4
36901	Juruá	4
36902	Jutai	4
36903	Lábrea	4
36904	Lago Preto	4
36905	Manacapuru	4
36906	Manaquiri	4
36907	Manaus	4
36908	Manicoré	4
36909	Maraã	4
36910	Massauari	4
36911	Maués	4
36912	Mocambo	4
36913	Moura	4
36914	Murutinga	4
36915	Nhamundá	4
36916	Nova Olinda do Norte	4
36917	Novo Airão	4
36918	Novo Aripuanã	4
36919	Osorio da Fonseca	4
36920	Parintins	4
36921	Pauini	4
36922	Pedras	4
36923	Presidente Figueiredo	4
36924	Repartimento	4
36925	Rio Preto da Eva	4
36926	Santa Isabel do Rio Negro	4
36927	Santa Rita	4
36928	Santo Antonio do Ica	4
36929	São Felipe	4
36930	São Gabriel da Cachoeira	4
36931	São Paulo de Olivença	4
36932	São Sebastião do Uatumã	4
36933	Silves	4
36934	Tabatinga	4
36935	Tapauá	4
36936	Tefe	4
36937	Tonantins	4
36938	Uarini	4
36939	Urucará	4
36940	Urucurituba	4
36941	Vila Pitinga	4
36942	Abacate da Pedreira	3
36943	Água Branca do Amapari	3
36944	Amapá	3
36945	Amapari	3
36946	Ambé	3
36947	Aporema	3
36948	Ariri	3
36949	Bailique	3
36950	Boca do Jari	3
36951	Calcoene	3
36952	Cantanzal	3
36953	Carmo	3
36954	Clevelândia do Norte	3
36955	Corre Água	3
36956	Cunani	3
36957	Curiau	3
36958	Cutias	3
36959	Fazendinha	3
36960	Ferreira Gomes	3
36961	Fortaleza	3
36962	Gaivota	3
36963	Gurupora	3
36964	Igarapé do Lago	3
36965	Ilha de Santana	3
36966	Inajá	3
36967	Itaubal	3
36968	Laranjal do Jari	3
36969	Livramento do Pacuí	3
36970	Lourenço	3
36971	Macapá	3
36972	Mazagão	3
36973	Mazagão Velho	3
36974	Oiapoque	3
36975	Paredão	3
36976	Porto Grande	3
36977	Pracuúba	3
36978	Santa Luzia do Pacuí	3
36979	Santa Maria	3
36980	Santana	3
36981	São Joaquim do Pacuí	3
36982	São Sebastião do Livramento	3
36983	São Tome	3
36984	Serra do Navio	3
36985	Sucuriju	3
36986	Tartarugalzinho	3
36987	Vila Velha	3
36988	Vitória do Jari	3
36989	Abadia	5
36990	Abaíra	5
36991	Abaré	5
36992	Abelhas	5
36993	Abobora	5
36994	Abrantes	5
36995	Acajutiba	5
36996	açu da Torre	5
36997	Acudina	5
36998	Acupe	5
36999	Adustina	5
37000	Afligidos	5
37001	Afrânio Peixoto	5
37002	Água Doce	5
37003	Água Fria	5
37004	Águas do Paulista	5
37005	Aiquara	5
37006	AlaGoiásnhas	5
37007	Alcobaça	5
37008	Alegre	5
37009	Algodão	5
37010	Algodoes	5
37011	Almadina	5
37012	Alto Bonito	5
37013	Amado Bahia	5
37014	Amaniú	5
37015	Amargosa	5
37016	Amelia Rodrigues	5
37017	America Dourada	5
37018	Américo Alves	5
37019	Anagé	5
37020	Andaraí	5
37021	Andorinha	5
37022	Angical	5
37023	Angico	5
37024	Anguera	5
37025	Antas	5
37026	Antonio Cardoso	5
37027	Antonio Gonçalves	5
37028	Aporá	5
37029	Apuarema	5
37030	Aracas	5
37031	Aracatu	5
37032	Araci	5
37033	Aramari	5
37034	Arapiranga	5
37035	Arataca	5
37036	Aratuípe	5
37037	Areias	5
37038	Arembepe	5
37039	ArGoiásm	5
37040	Argolo	5
37041	Aribice	5
37042	Aritagua	5
37043	Aurelino Leal	5
37044	Baiánópolis	5
37045	Baixa do Palmeira	5
37046	Baixa Grande	5
37047	Baixão	5
37048	Baixinha	5
37049	Baluarte	5
37050	Banco Central	5
37051	Banco da Vitória	5
37052	Bandeira do Almada	5
37053	Bandeira do Colonia	5
37054	Bândiacu	5
37055	Banzaê	5
37056	Baraúnas	5
37057	Barcelos do Sul	5
37058	Barra	5
37059	Barra da Estiva	5
37060	Barra do Choca	5
37061	Barra do Jacuípe	5
37062	Barra do Mendes	5
37063	Barra do Pojuca	5
37064	Barra do Rocha	5
37065	Barra do Tarrachil	5
37066	Barracas	5
37067	Barreiras	5
37068	Barro Alto	5
37069	Barro Preto	5
37070	Barro Vermelho	5
37071	Barrocas	5
37072	Bastião	5
37073	Bate Pe	5
37074	Batinga	5
37075	Bela Flor	5
37076	Belem da Cachoeira	5
37077	Belmonte	5
37078	Belo Campo	5
37079	Bem-bom	5
37080	Bendegó	5
37081	Bento Simões	5
37082	Biritinga	5
37083	Boa Espera	5
37084	Boa Nova	5
37085	Boa União	5
37086	Boa Vista do Lagamar	5
37087	Boa Vista do Tupim	5
37088	Boacu	5
37089	Boca do Córrego	5
37090	Bom Jesus da Lapa	5
37091	Bom Jesus da Serra	5
37092	Bom Sossego	5
37093	Bonfim da Feira	5
37094	Boninal	5
37095	Bonito	5
37096	Boquira	5
37097	Botuporã	5
37098	Botuquara	5
37099	Brejinho das Ametistas	5
37100	Brejo da Serra	5
37101	Brejo Luiza de Brito	5
37102	Brejo Novo	5
37103	Brejões	5
37104	Brejolândia	5
37105	Brotas de Macaúbas	5
37106	Brumado	5
37107	Bucuituba	5
37108	Buerarema	5
37109	Buracica	5
37110	Buranhém	5
37111	Buril	5
37112	Buris de Abrantes	5
37113	Buritirama	5
37114	Caatiba	5
37115	Cabaceiras do Paraguaçu	5
37116	Cabrália	5
37117	Cacha Pregos	5
37118	Cachoeira	5
37119	Cachoeira do Mato	5
37120	Caculé	5
37121	Caem	5
37122	Caetanos	5
37123	Caeté-açu	5
37124	Caetité	5
37125	Cafarnaum	5
37126	Caiçara	5
37127	Caimbé	5
37128	Cairu	5
37129	Caiubi	5
37130	Cajuí	5
37131	Caldas do Jorro	5
37132	Caldeirão	5
37133	Caldeirão Grande	5
37134	Caldeiras	5
37135	Camacan	5
37136	Camaçari	5
37137	Camamu	5
37138	Camassandi	5
37139	Camirim	5
37140	Campinhos	5
37141	Campo Alegre de Lourdes	5
37142	Campo Formoso	5
37143	Camurugi	5
37144	Canabravinha	5
37145	Canápolis	5
37146	Canarana	5
37147	Canatiba	5
37148	Canavieiras	5
37149	Canche	5
37150	Candeal	5
37151	Candeias	5
37152	Candiba	5
37153	Cândido Sales	5
37154	Canoão	5
37155	Cansanção	5
37156	Canto do Sol	5
37157	Canudos	5
37158	Capão	5
37159	Capela do Alto Alegre	5
37160	Capim Grosso	5
37161	Caraguataí	5
37162	Caraíbas	5
37163	Caraibuna	5
37164	Caraipe	5
37165	Caraiva	5
37166	Caravelas	5
37167	Cardeal da Silva	5
37168	Carinhanha	5
37169	Caripare	5
37170	Carnaíba do Sertão	5
37171	Carrapichel	5
37172	Casa Nova	5
37173	Castelo Novo	5
37174	Castro Alves	5
37175	Catinga do Moura	5
37176	Catingal	5
37177	Catolândia	5
37178	Catoles	5
37179	Catolezinho	5
37180	Catu	5
37181	Catu de Abrantes	5
37182	Caturama	5
37183	Cavunge	5
37184	Central	5
37185	Ceraima	5
37186	Chorrochó	5
37187	Cicero Dantas	5
37188	Cinco Rios	5
37189	Cipo	5
37190	Coaraci	5
37191	Cocos	5
37192	Colonia	5
37193	Comercio	5
37194	Conceição da Feira	5
37195	Conceição do Almeida	5
37196	Conceição do Coite	5
37197	Conceição do Jacuípe	5
37198	Conde	5
37199	Condeúba	5
37200	Contendas do Sincorá	5
37201	Copixaba	5
37202	Coqueiros	5
37203	Coquinhos	5
37204	Coração de Maria	5
37205	Cordeiros	5
37206	Coribe	5
37207	Coronel João Sa	5
37208	Correntina	5
37209	Corta Mão	5
37210	Cotegipe	5
37211	Coutos	5
37212	Cravolândia	5
37213	Crisópolis	5
37214	Cristalândia	5
37215	Cristópolis	5
37216	Crussai	5
37217	Cruz das Almas	5
37218	Cumuruxatiba	5
37219	Cunhangi	5
37220	Curaca	5
37221	Curral Falso	5
37222	Dario Meira	5
37223	Delfino	5
37224	Descoberto	5
37225	Dias Coelho	5
37226	Dias D Avila	5
37227	Diogenes Sampaio	5
37228	Dom Basilio	5
37229	Dom Macedo Costa	5
37230	Dona Maria	5
37231	Duas Barras do Morro	5
37232	Elisio Medrado	5
37233	Encruzilhada	5
37234	Engenheiro Franca	5
37235	Engenheiro Pontes	5
37236	Entre Rios	5
37237	Érico Cardoso	5
37238	Esplanada	5
37239	Euclides da Cunha	5
37240	Eunápolis	5
37241	Fatima	5
37242	Feira da Mata	5
37243	Feira de Santana	5
37244	Ferradas	5
37245	Filadelfia	5
37246	Filanesia	5
37247	Firmino Alves	5
37248	Floresta Azul	5
37249	Formosa do Rio Preto	5
37250	Franca	5
37251	Gabiarra	5
37252	Galeão	5
37253	Gamboa	5
37254	Gameleira da Lapa	5
37255	Gameleira do Assuruá	5
37256	Gandu	5
37257	Gavião	5
37258	Gentio do Ouro	5
37259	Geolândia	5
37260	Gloria	5
37261	Gongogi	5
37262	Governador João Durval Carneiro	5
37263	Governador Mangabeira	5
37264	Guajirus	5
37265	Guai	5
37266	Guajeru	5
37267	Guanambi	5
37268	Guapira	5
37269	Guarajuba	5
37270	Guaratinga	5
37271	Guerém	5
37272	Guine	5
37273	Guirapa	5
37274	Gurupá Mirim	5
37275	Heliopolis	5
37276	Helvécia	5
37277	Hidrolândia	5
37278	Humildes	5
37279	Iacu	5
37280	Ibatui	5
37281	Ibiacu	5
37282	Ibiajara	5
37283	Ibiapora	5
37284	Ibiassucê	5
37285	Ibicaraí	5
37286	Ibicoara	5
37287	Ibicuí	5
37288	Ibipeba	5
37289	Ibipetum	5
37290	Ibipitanga	5
37291	Ibiquera	5
37292	Ibiraba	5
37293	Ibiraja	5
37294	Ibiranhem	5
37295	Ibirapitanga	5
37296	Ibirapuã	5
37297	Ibirataiá	5
37298	Ibitiara	5
37299	Ibitiguira	5
37300	Ibitira	5
37301	Ibititá	5
37302	Ibitunane	5
37303	Ibitupa	5
37304	Ibo	5
37305	Ibotirama	5
37306	Ichu	5
37307	Ico	5
37308	Igaporã	5
37309	Igara	5
37310	Igarité	5
37311	Igatu	5
37312	Igrapiúna	5
37313	Iguá	5
37314	Iguai	5
37315	Iguaibi	5
37316	Iguatemi	5
37317	Iguira	5
37318	Iguitu	5
37319	Ilha de Mare	5
37320	Ilhéus	5
37321	Indai	5
37322	Inema	5
37323	Inhambupe	5
37324	Inhata	5
37325	Inhaúmas	5
37326	Inhobim	5
37327	Inubia	5
37328	Ipecaetá	5
37329	Ipiaú	5
37330	Ipirá	5
37331	Ipiuna	5
37332	Ipucaba	5
37333	Ipupiara	5
37334	Irajuba	5
37335	Iramaiá	5
37336	Iraporanga	5
37337	Iraquara	5
37338	Irara	5
37339	Irecê	5
37340	Irundiara	5
37341	Ita-azul	5
37342	Itabela	5
37343	Itaberaba	5
37344	Itabuna	5
37345	Itacaré	5
37346	Itacava	5
37347	Itachama	5
37348	Itacimirim	5
37349	Itaeté	5
37350	Itagi	5
37351	Itagiba	5
37352	Itagimirim	5
37353	Itaguaçu da Bahia	5
37354	Itaiá	5
37355	Itaibo	5
37356	Itaipu	5
37357	Itaitu	5
37358	Itajaí	5
37359	Itaju do Colonia	5
37360	Itajubaquara	5
37361	Itajuípe	5
37362	Itajuru	5
37363	Itamaraju	5
37364	Itamari	5
37365	Itambé	5
37366	Itamira	5
37367	Itamotinga	5
37368	Itanage	5
37369	Itanagra	5
37370	Itanhém	5
37371	Itanhi	5
37372	Itaparica	5
37373	Itapé	5
37374	Itapebi	5
37375	Itapeipu	5
37376	Itapetinga	5
37377	Itapicuru	5
37378	Itapirema	5
37379	Itapitanga	5
37380	Itaporã	5
37381	Itapura	5
37382	Itaquara	5
37383	Itaquarai	5
37384	Itarantim	5
37385	Itati	5
37386	Itatim	5
37387	Itatingui	5
37388	Itiruçu	5
37389	Itiúba	5
37390	Itororó	5
37391	Ituaçu	5
37392	Ituberá	5
37393	Itupeva	5
37394	Iuiú	5
37395	Jaborandi	5
37396	Jacaraci	5
37397	Jacobina	5
37398	Jacu	5
37399	Jacuípe	5
37400	Jacuruna	5
37401	Jaguaquara	5
37402	Jaguara	5
37403	Jaguarari	5
37404	Jaguaripe	5
37405	Jaíba	5
37406	Jandaira	5
37407	Japomirim	5
37408	Japu	5
37409	Jaua	5
37410	Jequié	5
37411	Jequirica	5
37412	Jeremoabo	5
37413	Jiribatuba	5
37414	Jitaúna	5
37415	João Amaro	5
37416	João Correia	5
37417	João Dourado	5
37418	José Gonçalves	5
37419	Juacema	5
37420	Juazeiro	5
37421	Jucuruçu	5
37422	Juerana	5
37423	Junco	5
37424	Jupagua	5
37425	Juraci	5
37426	Juremal	5
37427	Jussara	5
37428	Jussari	5
37429	Jussiape	5
37430	Km Sete	5
37431	Lafaiete Coutinho	5
37432	Lagoa Clara	5
37433	Lagoa de Melquiades	5
37434	Lagoa do Boi	5
37435	Lagoa Grande	5
37436	Lagoa José Luis	5
37437	Lagoa Preta	5
37438	Lagoa Real	5
37439	Laje	5
37440	Laje do Banco	5
37441	Lajedão	5
37442	Lajedinho	5
37443	Lajedo Alto	5
37444	Lajedo do Tabocal	5
37445	Lamarão	5
37446	Lamarão do Passe	5
37447	Lapão	5
37448	Largo	5
37449	Lauro de Freitas	5
37450	Lençóis	5
37451	Licínio de Almeida	5
37452	Limoeiro do Bom Viver	5
37453	Livramento do Brumado	5
37454	Lucaiá	5
37455	Luis Viana	5
37456	Lustosa	5
37457	Macajuba	5
37458	Macarani	5
37459	Macaúbas	5
37460	Macururé	5
37461	Madre de Deus	5
37462	Maetinga	5
37463	Maiquinique	5
37464	Mairi	5
37465	Malhada	5
37466	Malhada de Pedras	5
37467	Mandiroba	5
37468	Mangue Seco	5
37469	Maniacu	5
37470	Manoel Vitorino	5
37471	Mansidão	5
37472	Mantiba	5
37473	Mar Grande	5
37474	Maracas	5
37475	Maragogipe	5
37476	Maragogipinho	5
37477	Marau	5
37478	Marcionílio Souza	5
37479	Marcolino Moura	5
37480	Maria Quitéria	5
37481	Maricoabo	5
37482	Mariquita	5
37483	Mascote	5
37484	Massacara	5
37485	Massaroca	5
37486	Mata da Aliança	5
37487	Mata de São João	5
37488	Mataripe	5
37489	Matina	5
37490	Matinha	5
37491	Medeiros Neto	5
37492	Miguel Calmon	5
37493	Milagres	5
37494	Mimoso do Oeste	5
37495	Minas do Espirito Santo	5
37496	Minas do Mimoso	5
37497	Mirandela	5
37498	Miranga	5
37499	Mirangaba	5
37500	Mirante	5
37501	Mocambo	5
37502	Mogiquicaba	5
37503	Monte Cruzeiro	5
37504	Monte Gordo	5
37505	Monte Recôncavo	5
37506	Monte Santo	5
37507	Morpará	5
37508	Morrinhos	5
37509	Morro das Flores	5
37510	Morro de São Paulo	5
37511	Morro do Chapéu	5
37512	Mortugaba	5
37513	Mucugê	5
37514	Mucuri	5
37515	Mulungu do Morro	5
37516	Mundo Novo	5
37517	Muniz Ferreira	5
37518	Muquém do São Francisco	5
37519	Muritiba	5
37520	Mutas	5
37521	Mutuípe	5
37522	Nagé	5
37523	Narandiba	5
37524	Nazaré	5
37525	Nilo Peçanha	5
37526	Nordestina	5
37527	Nova Alegria	5
37528	Nova Brasilia	5
37529	Nova Canaã	5
37530	Nova Fatima	5
37531	Nova Ibiá	5
37532	Nova Itaipé	5
37533	Nova Itarana	5
37534	Nova Lídice	5
37535	Nova Redenção	5
37536	Nova Soure	5
37537	Nova Viçosa	5
37538	Novo Acre	5
37539	Novo Horizonte	5
37540	Novo Triunfo	5
37541	Núcleo Residencial Pilar	5
37542	Nuguacu	5
37543	Olhos D'Água Do Seco	5
37544	Olhos D'Água Do Serafim	5
37545	Olindina	5
37546	Oliveira dos Brejinhos	5
37547	Olivença	5
37548	Onhã	5
37549	Oriente Novo	5
37550	Ouricana	5
37551	Ouriçangas	5
37552	Ouricuri do Ouro	5
37553	Ourolândia	5
37554	Outeiro Redondo	5
37555	Paiol	5
37556	Pageu do Vento	5
37557	Palame	5
37558	Palmas de Monte Alto	5
37559	Palmeiras	5
37560	Parafuso	5
37561	Paramirim	5
37562	Parateca	5
37563	Paratinga	5
37564	Paripiranga	5
37565	Pataiba	5
37566	Patamute	5
37567	Pau A Pique	5
37568	Pau Brasil	5
37569	Paulo Afonso	5
37570	Pe de Serra	5
37571	Pedrão	5
37572	Pedras Altas do Mirim	5
37573	Pedro Alexandre	5
37574	Peixe	5
37575	Petim	5
37576	Piabanha	5
37577	Piatã	5
37578	Piçarrão	5
37579	Pilão Arcado	5
37580	Pimenteira	5
37581	Pindaí	5
37582	Pindobaçu	5
37583	Pinhões	5
37584	Pintadas	5
37585	Piragi	5
37586	Pirai do Norte	5
37587	Pirajá	5
37588	Pirajuia	5
37589	Piri	5
37590	Piripá	5
37591	Piritiba	5
37592	Pituba	5
37593	Planaltino	5
37594	Planalto	5
37595	Poço Central	5
37596	Poço de Fora	5
37597	Poções	5
37598	Poços	5
37599	Pojuca	5
37600	Polo Petroquímico de Camaçari	5
37601	Ponta da Areia	5
37602	Ponto Novo	5
37603	Porto Novo	5
37604	Porto Seguro	5
37605	Posto da Mata	5
37606	Potiraguá	5
37607	Poxim do Sul	5
37608	Prado	5
37609	Presidente Dutra	5
37610	Presidente Jânio Quadros	5
37611	Presidente Tancredo Neves	5
37612	Prevenido	5
37613	Quaracu	5
37614	Queimadas	5
37615	Quijingue	5
37616	Quixaba	5
37617	Quixabeira	5
37618	Rafael Jambeiro	5
37619	Recife	5
37620	Remanso	5
37621	Remédios	5
37622	Retirolândia	5
37623	Riachão das Neves	5
37624	Riachão do Jacuípe	5
37625	Riachão do Utinga	5
37626	Riacho da Guia	5
37627	Riacho de Santana	5
37628	Riacho Seco	5
37629	Ribeira do Amparo	5
37630	Ribeira do Pombal	5
37631	Ribeirão do Largo	5
37632	Ribeirão do Salto	5
37633	Rio da Dona	5
37634	Rio de Contas	5
37635	Rio do Antonio	5
37636	Rio do Braco	5
37637	Rio do Meio	5
37638	Rio do Pires	5
37639	Rio Fundo	5
37640	Rio Real	5
37641	Rodelas	5
37642	Ruy Barbosa	5
37643	Saldanha	5
37644	Salgadalia	5
37645	Salinas da Margarida	5
37646	Salobrinho	5
37647	Salobro	5
37648	Salvador	5
37649	Sambaíba	5
37650	Santa Barbara	5
37651	Santa Brigida	5
37652	Santa Cruz Cabrália	5
37653	Santa Cruz da Vitória	5
37654	Santa Inês	5
37655	Santa Luzia	5
37656	Santa Maria da Vitória	5
37657	Santa Rita de Cassia	5
37658	Santa Terezinha	5
37659	Santaluz	5
37660	Santana	5
37661	Santana do Sobrado	5
37662	Santanópolis	5
37663	Santiago do Iguape	5
37664	Santo Amaro	5
37665	Santo Antonio de Barcelona	5
37666	Santo Antonio de Jesus	5
37667	Santo Estevão	5
37668	Santo Inácio	5
37669	São Desidério	5
37670	São Domingos	5
37671	São Felipe	5
37672	São Felix	5
37673	São Felix do Coribe	5
37674	São Francisco do Conde	5
37675	São Gabriel	5
37676	São Gonçalo dos Campos	5
37677	São João da Fortaleza	5
37678	São João da Vitória	5
37679	São José da Vitória	5
37680	São José do Colonia	5
37681	São José do Jacuípe	5
37682	São José do Prado	5
37683	São José do Rio Grande	5
37684	São Miguel das Matas	5
37685	São Paulinho	5
37686	São Roque do Paraguaçu	5
37687	São Sebastião do Passe	5
37688	São Timóteo	5
37689	Sapeaçu	5
37690	Satiro Dias	5
37691	Saubara	5
37692	Saudável	5
37693	Saúde	5
37694	Seabra	5
37695	Sebastião Laranjeiras	5
37696	Senhor do Bonfim	5
37697	Sento Se	5
37698	Sergi	5
37699	Serra da Canabrava	5
37700	Serra do Ramalho	5
37701	Serra Dourada	5
37702	Serra Grande	5
37703	Serra Preta	5
37704	Serrinha	5
37705	Serrolândia	5
45053	Presidente Getulio	24
45054	Presidente Juscelino	24
45055	Presidente Kennedy	24
45056	Presidente Nereu	24
45057	Princesa	24
45058	Quarta Linha	24
45059	Quilombo	24
45060	Quilometro Doze	24
45061	Rancho Queimado	24
45062	Ratones	24
45063	Residencia Fuck	24
45064	Ribeirão da Ilha	24
45065	Ribeirão Pequeno	24
45066	Rio Antinha	24
45067	Rio Bonito	24
45068	Rio D'una	24
45069	Rio da Anta	24
45070	Rio da Luz	24
45071	Rio das Antas	24
45072	Rio das Furnas	24
45073	Rio do Campo	24
45074	Rio do Oeste	24
45075	Rio do Sul	24
45076	Rio dos Bugres	24
45077	Rio dos Cedros	24
45078	Rio Fortuna	24
45079	Rio Maina	24
45080	Rio Negrinho	24
45081	Rio Preto do Sul	24
45082	Rio Rufino	24
45083	Riqueza	24
45084	Rodeio	24
45085	Romelândia	24
45086	Sai	24
45087	Salete	24
45088	Saltinho	24
45089	Salto Veloso	24
45090	Sanga da Toca	24
45091	Sangão	24
45092	Santa Cecilia	24
45093	Santa Cruz do Timbo	24
45094	Santa Helena	24
45095	Santa Izabel	24
45096	Santa Lucia	24
45097	Santa Maria	24
45098	Santa Rosa de Lima	24
45099	Santa Rosa do Sul	24
45100	Santa Terezinha	24
45101	Santa Terezinha do Progresso	24
45102	Santa Terezinha do Salto	24
45103	Santiago do Sul	24
45104	Santo Amaro da Imperatriz	24
45105	Santo Antonio de Lisboa	24
45106	São Bento Baixo	24
45107	São Bento do Sul	24
45108	São Bernardino	24
45109	São Bonifacio	24
45110	São Carlos	24
45111	São Cristovão	24
45112	São Cristovao do Sul	24
45113	São Defende	24
45114	São Domingos	24
45115	São Francisco do Sul	24
45116	São Gabriel	24
45117	São João Batista	24
45118	São João do Itaperiu	24
45119	São João do Oeste	24
45120	São João do Rio Vermelho	24
45121	São João do Sul	24
45122	São Joaquim	24
45123	São José	24
45124	São José do Cedro	24
45125	São José do Cerrito	24
45126	São José do Laranjal	24
45127	São Leonardo	24
45128	São Lourenço do Oeste	24
45129	São Ludgero	24
45130	São Martinho	24
45131	São Miguel D'oeste	24
45132	São Miguel da Boa Vista	24
45133	São Miguel da Serra	24
45134	São Pedro de Alcantara	24
45135	São Pedro Tobias	24
45136	São Roque	24
45137	São Sebastião do Arvoredo	24
45138	São Sebastião do Sul	24
45139	Sapiranga	24
45140	Saudades	24
45141	Schroeder	24
45142	Seara	24
45143	Sede Oldemburg	24
45144	Serra Alta	24
45145	Sertao do Maruim	24
45146	Sideropolis	24
45147	Sombrio	24
45148	Sorocaba do Sul	24
45149	Sul Brasil	24
45150	Taio	24
45151	Tangara	24
45152	Taquara Verde	24
45153	Taquaras	24
45154	Tigipio	24
45155	Tigrinhos	24
45156	Tijucas	24
45157	Timbe do Sul	24
45158	Timbo	24
45159	Timbo Grande	24
45160	Três Barras	24
45161	Treviso	24
45162	Treze de Maio	24
45163	Treze Tilias	24
45164	Trombudo Central	24
45165	Tubarão	24
45166	Tunapolis	24
45167	Tupitinga	24
45168	Turvo	24
45169	União do Oeste	24
45170	Urubici	24
45171	Uruguai	24
45172	Urupema	24
45173	Urussanga	24
45174	Vargeão	24
45175	Vargem	24
45176	Vargem Bonita	24
45177	Vargem do Cedro	24
45178	Vidal Ramos	24
45179	Videira	24
45180	Vila Conceição	24
45181	Vila de Volta Grande	24
45182	Vila Milani	24
45183	Vila Nova	24
45184	Vitor Meireles	24
45185	Witmarsum	24
45186	Xanxere	24
45187	Xavantina	24
45188	Xaxim	24
45189	Zortea	24
45190	Altos Verdes	26
45191	Amparo de São Francisco	26
45192	Aquidaba	26
45193	Aracaju	26
45194	Araua	26
45195	Areia Branca	26
45196	Barra dos Coqueiros	26
45197	Barracas	26
45198	Boquim	26
45199	Brejo Grande	26
45200	Campo do Brito	26
45201	Canhoba	26
45202	Caninde de São Francisco	26
45203	Capela	26
45204	Carira	26
45205	Carmopolis	26
45206	Cedro de São João	26
45207	Cristinapolis	26
45208	Cumbe	26
45209	Divina Pastora	26
45210	Estancia	26
45211	Feira Nova	26
45212	Frei Paulo	26
45213	Gararu	26
45214	General Maynard	26
45215	Graccho Cardoso	26
45216	Ilha das Flores	26
45217	Indiaroba	26
45218	Itabaiána	26
45219	Itabaiáninha	26
45220	Itabi	26
45221	Itaporanga D'ajuda	26
45222	Japaratuba	26
45223	Japoata	26
45224	Lagarto	26
45225	Lagoa Funda	26
45226	Laranjeiras	26
45227	Macambira	26
45228	Malhada dos Bois	26
45229	Malhador	26
45230	Maruim	26
45231	Miranda	26
45232	Moita Bonita	26
45233	Monte Alegre de Sergipe	26
45234	Mosqueiro	26
45235	Muribeca	26
45236	Neopolis	26
45237	Nossa Senhora Aparecida	26
45238	Nossa Senhora da Gloria	26
45239	Nossa Senhora das Dores	26
45240	Nossa Senhora de Lourdes	26
45241	Nossa Senhora do Socorro	26
45242	Pacatuba	26
45243	Palmares	26
45244	Pedra Mole	26
45245	Pedras	26
45246	Pedrinhas	26
45247	Pinhão	26
45248	Pirambu	26
45249	Poço Redondo	26
45250	Poço Verde	26
45251	Porto da Folha	26
45252	Propria	26
45253	Riachao do Dantas	26
45254	Riachuelo	26
45255	Ribeiropolis	26
45256	Rosario do Catete	26
45257	Salgado	26
45258	Samambaiá	26
45259	Santa Luzia do Itanhy	26
45260	Santa Rosa de Lima	26
45261	Santana do São Francisco	26
45262	Santo Amaro das Brotas	26
45263	São Cristovão	26
45264	São Domingos	26
45265	São Francisco	26
45266	São José	26
45267	São Mateus da Palestina	26
45268	São Miguel do Aleixo	26
45269	Simao Dias	26
45270	Siriri	26
45271	Telha	26
45272	Tobias Barreto	26
45273	Tomar do Geru	26
45274	Umbauba	26
45275	Adamantina	25
45276	Adolfo	25
45277	Agisse	25
45278	Água Vermelha	25
45279	Águai	25
45280	Águas da Prata	25
45281	Águas de Lindoia	25
45282	Águas de Santa Barbara	25
45283	Águas de São Pedro	25
45284	Agudos	25
45285	Agulha	25
45286	Ajapi	25
45287	Alambari	25
45288	Alberto Moreira	25
45289	Aldeia	25
45290	Aldeia de Carapicuiba	25
45291	Alfredo Guedes	25
45292	Alfredo Marcondes	25
45293	Altair	25
45294	Altinopolis	25
45295	Alto Alegre	25
45296	Alto Pora	25
45297	Aluminio	25
45298	Alvares Florence	25
45299	Alvares Machado	25
45300	Alvaro de Carvalho	25
45301	Alvinlândia	25
45302	Amadeu Amaral	25
45303	Amandaba	25
45304	Ameliopolis	25
45305	Americana	25
45306	Americo Brasiliense	25
45307	Americo de Campos	25
45308	Amparo	25
45309	Ana Dias	25
45310	Analândia	25
45311	Anapolis	25
45312	Andes	25
45313	Andradina	25
45314	Angatuba	25
45315	Anhembi	25
45316	Anhumas	25
45317	Aparecida	25
45318	Aparecida D'oeste	25
45319	Aparecida de Monte Alto	25
45320	Aparecida de São Manuel	25
45321	Aparecida do Bonito	25
45322	Apiai	25
45323	Apiai-mirim	25
45324	Arabela	25
45325	Aracacu	25
45326	Aracaiba	25
45327	Aracariguama	25
45328	Aracatuba	25
45329	Aracoiaba da Serra	25
45330	Aramina	25
45331	Arandu	25
45332	Arapei	25
45333	Araraquara	25
45334	Araras	25
45335	Araxas	25
45336	Arcadas	25
45337	Arco-iris	25
45338	Arealva	25
45339	Areias	25
45340	Areiopolis	25
45341	Ariranha	25
45342	Ariri	25
45343	Artemis	25
45344	Artur Nogueira	25
45345	Aruja	25
45346	Aspasia	25
45347	Assis	25
45348	Assistencia	25
45349	Atibaiá	25
45350	Atlantida	25
45351	Auriflama	25
45352	Avai	25
45353	Avanhandava	25
45354	Avare	25
45355	Avencas	25
45356	Bacaetava	25
45357	Bacuriti	25
45358	Bady Bassitt	25
45359	Baguacu	25
45360	Bairro Alto	25
45361	Balbinos	25
45362	Balsamo	25
45363	Bananal	25
45364	Bandeirantes D'oeste	25
45365	Barão Ataliba Nogueira	25
45366	Barão de Antonina	25
45367	Barão de Geraldo	25
45368	Barbosa	25
45369	Bariri	25
45370	Barra Bonita	25
45371	Barra do Chapeu	25
45372	Barra do Turvo	25
45373	Barra Dourada	25
45374	Barrania	25
45375	Barretos	25
45376	Barrinha	25
45377	Barueri	25
45378	Bastos	25
45379	Batatais	25
45380	Batatuba	25
45381	Batista Botelho	25
45382	Bauru	25
45383	Bebedouro	25
45384	Bela Floresta	25
45385	Bela Vista São-carlense	25
45386	Bento de Abreu	25
45387	Bernardino de Campos	25
45388	Bertioga	25
45389	Bilac	25
45390	Birigui	25
45391	Biritiba-mirim	25
45392	Biritiba-ussu	25
45393	Boa Esperanca do Sul	25
45394	Boa Vista dos Andradas	25
45395	Boa Vista Paulista	25
45396	Bocaina	25
45397	Bofete	25
45398	Boituva	25
45399	Bom Fim do Bom Jesus	25
45400	Bom Jesus dos Perdoes	25
45401	Bom Retiro da Esperanca	25
45402	Bom Sucesso de Itarare	25
45403	Bonfim Paulista	25
45404	Bora	25
45405	Boraceia	25
45406	Borborema	25
45407	Borebi	25
45408	Botafogo	25
45409	Botelho	25
45410	Botucatu	25
45411	Botujuru	25
45412	Braco	25
45413	Braganca Paulista	25
45414	Bras Cubas	25
45415	Brasitania	25
45416	Brauna	25
45417	Brejo Alegre	25
45418	Brodowski	25
45419	Brotas	25
45420	Bueno de Andrada	25
45421	Buri	25
45422	Buritama	25
45423	Buritizal	25
45424	Cabralia Paulista	25
45425	Cabreuva	25
45426	Cacapava	25
45427	Cachoeira de Emas	25
45428	Cachoeira Paulista	25
45429	Caconde	25
45430	Cafelândia	25
45431	Cafesopolis	25
45432	Caiábu	25
45433	Caibura	25
45434	Caieiras	25
45435	Caiua	25
45436	Cajamar	25
45437	Cajati	25
45438	Cajobi	25
45439	Cajuru	25
45440	Cambaquara	25
45441	Cambaratiba	25
45442	Campestrinho	25
45443	Campina de Fora	25
45444	Campina do Monte Alegre	25
45445	Campinal	25
45446	Campinas	25
45447	Campo Limpo Paulista	25
45448	Campos de Cunha	25
45449	Campos do Jordão	25
45450	Campos Novos Paulista	25
45451	Cananeia	25
45452	Canas	25
45453	Cândia	25
45454	Candido Mota	25
45455	Candido Rodrigues	25
45456	Canguera	25
45457	Canitar	25
45458	Capão Bonito	25
45459	Capela do Alto	25
45460	Capivari	25
45461	Capivari da Mata	25
45462	Caporanga	25
45463	Capuava	25
45464	Caraguatatuba	25
45465	Carapicuiba	25
45466	Cardeal	25
45467	Cardoso	25
45468	Caruara	25
45469	Casa Branca	25
45470	Cassia dos Coqueiros	25
45471	Castilho	25
45472	Catanduva	25
45473	Catigua	25
45474	Catucaba	25
45475	Caucaiá do Alto	25
45476	Cedral	25
45477	Cerqueira Cesar	25
45478	Cerquilho	25
45479	Cesario Lange	25
45480	Cezar de Souza	25
45481	Charqueada	25
45482	Chavantes	25
45483	Cipo-guacu	25
45484	Clarinia	25
45485	Clementina	25
45486	Cocaes	25
45487	Colina	25
45488	Colombia	25
45489	Conceição de Monte Alegre	25
45490	Conchal	25
45491	Conchas	25
45492	Cordeiropolis	25
45493	Coroados	25
45494	Coronel Goulart	25
45495	Coronel Macedo	25
45496	Corredeira	25
45497	Córrego Rico	25
45498	Corumbatai	25
45499	Cosmopolis	25
45500	Cosmorama	25
45501	Costa Machado	25
45502	Cotia	25
45503	Cravinhos	25
45504	Cristais Paulista	25
45505	Cruz das Posses	25
45506	Cruzalia	25
45507	Cruzeiro	25
45508	Cubatão	25
45509	Cuiaba Paulista	25
45510	Cunha	25
45511	Curupa	25
45512	Dalas	25
45513	Descalvado	25
45514	Diadema	25
45515	Dirce Reis	25
45516	Dirceu	25
45517	Divinolândia	25
45518	Dobrada	25
45519	Dois Corregos	25
45520	Dolcinopolis	25
45521	Domelia	25
45522	Dourado	25
45523	Dracena	25
45524	Duartina	25
45525	Dumont	25
45526	Duplo Ceu	25
45527	Echapora	25
45528	Eldorado	25
45529	Eleuterio	25
45530	Elias Fausto	25
45531	Elisiario	25
45532	Embauba	25
45533	Embu	25
45534	Embu-guacu	25
45535	Emilianopolis	25
45536	Eneida	25
45537	Engenheiro Balduino	25
45538	Engenheiro Coelho	25
45539	Engenheiro Maiá	25
45540	Engenheiro Schmidt	25
45541	Esmeralda	25
45542	Esperanca D'oeste	25
45543	Espigão	25
45544	Espirito Santo do Pinhal	25
45545	Espirito Santo do Turvo	25
45546	Estiva Gerbi	25
45547	Estrela D'oeste	25
45548	Estrela do Norte	25
45549	Euclides da Cunha Paulista	25
45550	Eugenio de Melo	25
45551	Fartura	25
45552	Fatima	25
45553	Fatima Paulista	25
45554	Fazenda Velha	25
45555	Fernando Prestes	25
45556	Fernandopolis	25
45557	Fernão	25
45558	Ferraz de Vasconcelos	25
45559	Flora Rica	25
45560	Floreal	25
45561	Floresta do Sul	25
45562	Florida Paulista	25
45563	Florinea	25
45564	Franca	25
45565	Francisco Morato	25
45566	Franco da Rocha	25
45567	Frutal do Campo	25
45568	Gabriel Monteiro	25
45569	Galia	25
45570	Garca	25
45571	Gardenia	25
45572	Gastao Vidigal	25
45573	Gavião Peixoto	25
45574	General Salgado	25
45575	Getulina	25
45576	Glicério	25
45577	Gramadinho	25
45578	Guachos	25
45579	Guaiánas	25
45580	Guaicara	25
45581	Guaimbe	25
45582	Guaira	25
45583	Guamium	25
45584	Guapiacu	25
45585	Guapiara	25
45586	Guapiranga	25
45587	Guara	25
45588	Guaracai	25
45589	Guaraci	25
45590	Guaraciaba D'oeste	25
45591	Guarani D'oeste	25
45592	Guaranta	25
45593	Guarapiranga	25
45594	Guarapua	25
45595	Guararapes	25
45596	Guararema	25
45597	Guaratingueta	25
45598	Guarei	25
45599	Guariba	25
45600	Guariroba	25
45601	Guarizinho	25
45602	Guaruja	25
45603	Guarulhos	25
45604	Guatapara	25
45605	Guzolândia	25
45606	Herculândia	25
45607	Holambra	25
45608	Holambra Ii	25
45609	Hortolândia	25
45610	Iacanga	25
45611	Iacri	25
45612	Iaras	25
45613	Ibate	25
45614	Ibiporanga	25
45615	Ibira	25
45616	Ibirarema	25
45617	Ibitinga	25
45618	Ibitiruna	25
45619	Ibitiuva	25
45620	Ibitu	25
45621	Ibiuna	25
45622	Icem	25
45623	Ida Iolanda	25
45624	Iepe	25
45625	Igacaba	25
45626	Igaracu do Tiete	25
45627	Igarai	25
45628	Igarapava	25
45629	Igarata	25
45630	Iguape	25
45631	Ilha Comprida	25
45632	Ilha Diana	25
45633	Ilha Solteira	25
45634	Ilhabela	25
45635	Indaiá do Águapei	25
45636	Indaiátuba	25
45637	Indiana	25
45638	Indiapora	25
45639	Ingas	25
45640	Inubia Paulista	25
45641	Ipaussu	25
45642	Ipero	25
45643	Ipeuna	25
45644	Ipigua	25
45645	Iporanga	25
45646	Ipua	25
45647	Iracemapolis	25
45648	Irape	25
45649	Irapua	25
45650	Irapuru	25
45651	Itabera	25
45652	Itaboa	25
45653	Itai	25
45654	Itaiuba	25
45655	Itajobi	25
45656	Itaju	25
45657	Itanhaem	25
45658	Itaoca	25
45659	Itapecerica da Serra	25
45660	Itapetininga	25
45661	Itapeuna	25
45662	Itapeva	25
45663	Itapevi	25
45664	Itapira	25
45665	Itapirapua Paulista	25
45666	Itapolis	25
45667	Itaporanga	25
45668	Itapui	25
45669	Itapura	25
45670	Itaquaquecetuba	25
45671	Itaqueri da Serra	25
45672	Itarare	25
45673	Itariri	25
45674	Itatiba	25
45675	Itatinga	25
45676	Itirapina	25
45677	Itirapua	25
45678	Itobi	25
45679	Itororo do Paranapanema	25
45680	Itu	25
45681	Itupeva	25
45682	Ituverava	25
45683	Iubatinga	25
45684	Jaborandi	25
45685	Jaboticabal	25
45686	Jacare	25
45687	Jacarei	25
45688	Jaci	25
45689	Jacipora	25
45690	Jacuba	25
45691	Jacupiranga	25
45692	Jafa	25
45693	Jaguariuna	25
45694	Jales	25
45695	Jamaica	25
45696	Jambeiro	25
45697	Jandira	25
45698	Jardim Belval	25
45699	Jardim Presidente Dutra	25
45700	Jardim Santa Luzia	25
45701	Jardim Silveira	25
45702	Jardinopolis	25
45703	Jarinu	25
45704	Jatoba	25
45705	Jau	25
45706	Jeriquara	25
45707	Joanopolis	25
45708	João Ramalho	25
45709	Joaquim Egidio	25
45710	Jordanesia	25
45711	José Bonifacio	25
45712	Juliania	25
45713	Julio Mesquita	25
45714	Jumirim	25
45715	Jundiai	25
45716	Jundiapeba	25
45717	Junqueira	25
45718	Junqueiropolis	25
45719	Juquia	25
45720	Juquiratiba	25
45721	Juquitiba	25
45722	Juritis	25
45723	Juruce	25
45724	Jurupeba	25
45725	Jurupema	25
45726	Lacio	25
45727	Lagoa Azul	25
45728	Lagoa Branca	25
45729	LaGoiásnha	25
45730	Laranjal Paulista	25
45731	Laras	25
45732	Lauro Penteado	25
45733	Lavinia	25
45734	Lavrinhas	25
45735	Leme	25
45736	Lencois Paulista	25
45737	Limeira	25
45738	Lindoia	25
45739	Lins	25
45740	Lobo	25
45741	Lorena	25
45742	Lourdes	25
45743	Louveira	25
45744	Lucelia	25
45745	Lucianopolis	25
45746	Luis Antonio	25
45747	Luiziania	25
45748	Lupercio	25
45749	Lusitania	25
45750	Lutecia	25
45751	Macatuba	25
45752	Macaubal	25
45753	Macedonia	25
45754	Macucos	25
45755	Magda	25
45756	Mailasqui	25
45757	Mairinque	25
45758	Mairipora	25
45759	Major Prado	25
45760	Manduri	25
45761	Mangaratu	25
45762	Maraba Paulista	25
45763	Maracai	25
45764	Marapoama	25
45765	Marcondesia	25
45766	Maresias	25
45767	Mariapolis	25
45768	Marilia	25
45769	Marinopolis	25
45770	Maristela	25
45771	Martim Francisco	25
45772	Martinho Prado Junior	25
45773	Martinopolis	25
45774	Matão	25
45775	Maua	25
45776	Mendonca	25
45777	Meridiano	25
45778	Mesopolis	25
45779	Miguelopolis	25
45780	Mineiros do Tiete	25
45781	Mira Estrela	25
45782	Miracatu	25
45783	Miraluz	25
45784	Mirandopolis	25
45785	Mirante do Paranapanema	25
45786	Mirassol	25
45787	Mirassolândia	25
45788	Mococa	25
45789	Mogi das Cruzes	25
45790	Mogi-guacu	25
45791	Mogi-mirim	25
45792	Mombuca	25
45793	Moncoes	25
45794	Mongagua	25
45795	Montalvão	25
45796	Monte Alegre do Sul	25
45797	Monte Alto	25
45798	Monte Aprazivel	25
45799	Monte Azul Paulista	25
45800	Monte Cabrão	25
45801	Monte Castelo	25
45802	Monte Mor	25
45803	Monte Verde Paulista	25
45804	Monteiro Lobato	25
45805	Moreira Cesar	25
45806	Morro Agudo	25
45807	Morro do Alto	25
45808	Morungaba	25
45809	Mostardas	25
45810	Motuca	25
45811	Mourão	25
45812	Murutinga do Sul	25
45813	Nantes	25
45814	Narandiba	25
45815	Natividade da Serra	25
45816	Nazaré Paulista	25
45817	Neves Paulista	25
45818	Nhandeara	25
45819	Nipoa	25
45820	Nogueira	25
45821	Nossa Senhora do Remedio	25
45822	Nova Alexandria	25
45823	Nova Alianca	25
45824	Nova America	25
45825	Nova Aparecida	25
45826	Nova Campina	25
45827	Nova Canaa Paulista	25
45828	Nova Castilho	25
45829	Nova Europa	25
45830	Nova Granada	25
45831	Nova Guataporanga	25
45832	Nova Independencia	25
45833	Nova Itapirema	25
45834	Nova Luzitania	25
45835	Nova Odessa	25
45836	Nova Patria	25
45837	Nova Veneza	25
45838	Novais	25
45839	Novo Cravinhos	25
45840	Novo Horizonte	25
45841	Nuporanga	25
45842	Oasis	25
45843	Ocaucu	25
45844	Oleo	25
45845	Olimpia	25
45846	Oliveira Barros	25
45847	Onda Branca	25
45848	Onda Verde	25
45849	Oriente	25
45850	Orindiuva	25
45851	Orlândia	25
45852	Osasco	25
45853	Oscar Bressane	25
45854	Osvaldo Cruz	25
45855	Ourinhos	25
45856	Ouro Fino Paulista	25
45857	Ouro Verde	25
45858	Ouroeste	25
45859	Pacaembu	25
45860	Padre Nobrega	25
45861	Palestina	25
45862	Palmares Paulista	25
45863	Palmeira D'oeste	25
45864	Palmeiras de São Paulo	25
45865	Palmital	25
45866	Panorama	25
45867	Paraguacu Paulista	25
45868	Paraibuna	25
45869	Paraiso	25
45870	Paraisolândia	25
45871	Paranabi	25
45872	Paranapanema	25
45873	Paranapiacaba	25
45874	Paranapua	25
45875	Parapua	25
45876	Pardinho	25
45877	Pariquera-açu	25
45878	Parisi	25
45879	Parnaso	25
45880	Parque Meia Lua	25
45881	Paruru	25
45882	Patrocinio Paulista	25
45883	Pauliceia	25
45884	Paulinia	25
45885	Paulistania	25
45886	Paulo de Faria	25
45887	Paulopolis	25
45888	Pederneiras	25
45889	Pedra Bela	25
45890	Pedra Branca de Itarare	25
45891	Pedranopolis	25
45892	Pedregulho	25
45893	Pedreira	25
45894	Pedrinhas Paulista	25
45895	Pedro Barros	25
45896	Pedro de Toledo	25
45897	Penapolis	25
45898	Pereira Barreto	25
45899	Pereiras	25
45900	Peruibe	25
45901	Piacatu	25
45902	Picinguaba	25
45903	Piedade	25
45904	Pilar do Sul	25
45905	Pindamonhangaba	25
45906	Pindorama	25
45907	Pinhalzinho	25
45908	Pinheiros	25
45909	Pioneiros	25
45910	Piquerobi	25
45911	Piquete	25
45912	Piracaiá	25
45913	Piracicaba	25
45914	Piraju	25
45915	Pirajui	25
45916	Piramboia	25
45917	Pirangi	25
45918	Pirapitingui	25
45919	Pirapora do Bom Jesus	25
45920	Pirapozinho	25
45921	Pirassununga	25
45922	Piratininga	25
45923	Pitangueiras	25
45924	Planalto	25
45925	Planalto do Sul	25
45926	Platina	25
45927	Poa	25
45928	Poloni	25
45929	Polvilho	25
45930	Pompeia	25
45931	Pongai	25
45932	Pontal	25
45933	Pontalinda	25
45934	Pontes Gestal	25
45935	Populina	25
45936	Porangaba	25
45937	Porto Feliz	25
45938	Porto Ferreira	25
45939	Porto Novo	25
45940	Potim	25
45941	Potirendaba	25
45942	Potunduva	25
45943	Pracinha	25
45944	Pradinia	25
45945	Pradopolis	25
45946	Praiá Grande	25
45947	Pratania	25
45948	Presidente Alves	25
45949	Presidente Bernardes	25
45950	Presidente Epitacio	25
45951	Presidente Prudente	25
45952	Presidente Venceslau	25
45953	Primavera	25
45954	Promissão	25
45955	Prudencio E Moraes	25
45956	Quadra	25
45957	Quata	25
45958	Queiroz	25
45959	Queluz	25
45960	Quintana	25
45961	Quiririm	25
45962	Rafard	25
45963	Rancharia	25
45964	Rechan	25
45965	Redenção da Serra	25
45966	Regente Feijo	25
45967	Reginopolis	25
45968	Registro	25
45969	Restinga	25
45970	Riacho Grande	25
45971	Ribeira	25
45972	Ribeirão Bonito	25
45973	Ribeirão Branco	25
45974	Ribeirão Corrente	25
45975	Ribeirão do Sul	25
45976	Ribeirão dos Indios	25
45977	Ribeirão Grande	25
45978	Ribeirão Pires	25
45979	Ribeirão Preto	25
45980	Ribeiro do Vale	25
45981	Ribeiro dos Santos	25
45982	Rifaina	25
45983	Rincão	25
45984	Rinopolis	25
45985	Rio Claro	25
45986	Rio das Pedras	25
45987	Rio Grande da Serra	25
45988	Riolândia	25
45989	Riversul	25
45990	Roberto	25
45991	Rosalia	25
45992	Rosana	25
45993	Roseira	25
45994	Rubiacea	25
45995	Rubião Junior	25
45996	Rubineia	25
45997	Ruilândia	25
45998	Sabauna	25
45999	Sabino	25
46000	Sagres	25
46001	Sales	25
46002	Sales Oliveira	25
46003	Salesopolis	25
46004	Salmourão	25
46005	Saltinho	25
46006	Salto	25
46007	Salto de Pirapora	25
46008	Salto do Avanhandava	25
46009	Salto Grande	25
46010	Sandovalina	25
46011	Santa Adelia	25
46012	Santa Albertina	25
46013	Santa America	25
46014	Santa Barbara D'oeste	25
46015	Santa Branca	25
46016	Santa Clara D'oeste	25
46017	Santa Cruz da Conceição	25
46018	Santa Cruz da Esperanca	25
46019	Santa Cruz da Estrela	25
46020	Santa Cruz das Palmeiras	25
46021	Santa Cruz do Rio Pardo	25
46022	Santa Cruz dos Lopes	25
46023	Santa Ernestina	25
46024	Santa Eudoxia	25
46025	Santa Fé do Sul	25
46026	Santa Gertrudes	25
46027	Santa Isabel	25
46028	Santa Isabel do Marinheiro	25
46029	Santa Lucia	25
46030	Santa Margarida Paulista	25
46031	Santa Maria da Serra	25
46032	Santa Maria do Gurupa	25
46033	Santa Mercedes	25
46034	Santa Rita D'oeste	25
46035	Santa Rita do Passa Quatro	25
46036	Santa Rita do Ribeira	25
46037	Santa Rosa de Viterbo	25
46038	Santa Salete	25
46039	Santa Teresinha de Piracicaba	25
46040	Santana da Ponte Pensa	25
46041	Santana de Parnaiba	25
46042	Santelmo	25
46043	Santo Anastacio	25
46044	Santo Andre	25
46045	Santo Antonio da Alegria	25
46046	Santo Antonio da Estiva	25
46047	Santo Antonio de Posse	25
46048	Santo Antonio do Aracangua	25
46049	Santo Antonio do Jardim	25
46050	Santo Antonio do Paranapanema	25
46051	Santo Antonio do Pinhal	25
46052	Santo Antonio Paulista	25
46053	Santo Expedito	25
46054	Santopolis do Águapei	25
46055	Santos	25
46056	São Benedito da Cachoeirinha	25
46057	São Benedito das Areias	25
46058	São Bento do Sapucai	25
46059	São Bernardo do Campo	25
46060	São Berto	25
46061	São Caetano do Sul	25
46062	São Carlos	25
46063	São Francisco	25
46064	São Francisco da Praiá	25
46065	São Francisco Xavier	25
46066	São João da Boa Vista	25
46067	São João das Duas Pontes	25
46068	São João de Iracema	25
46069	São João de Itaguacu	25
46070	São João do Marinheiro	25
46071	São João Do Pau D'alho	25
46072	São João Novo	25
46073	São Joaquim da Barra	25
46074	São José da Bela Vista	25
46075	São José das Laranjeiras	25
46076	São José do Barreiro	25
46077	São José do Rio Pardo	25
46078	São José do Rio Preto	25
46079	São José dos Campos	25
46080	São Lourenço da Serra	25
46081	São Lourenço do Turvo	25
46082	São Luis do Paraitinga	25
46083	São Luiz do Guaricanga	25
46084	São Manuel	25
46085	São Martinho D'oeste	25
46086	São Miguel Arcanjo	25
46087	São Paulo	25
46088	São Pedro	25
46089	São Pedro do Turvo	25
46090	São Roque	25
46091	São Roque da Fartura	25
46092	São Sebastião	25
46093	São Sebastião da Grama	25
46094	São Sebastião da Serra	25
46095	São Silvestre de Jacarei	25
46096	São Simão	25
46097	São Vicente	25
46098	Sapezal	25
46099	Sarapui	25
46100	Sarutaiá	25
46101	Sebastianopolis do Sul	25
46102	Serra Azul	25
46103	Serra Negra	25
46104	Serrana	25
46105	Sertaozinho	25
46106	Sete Barras	25
46107	Severinia	25
46108	Silvania	25
46109	Silveiras	25
46110	Simoes	25
46111	Simonsen	25
46112	Socorro	25
46113	Sodrelia	25
46114	Solemar	25
46115	Sorocaba	25
46116	Sousas	25
46117	Sud Mennucci	25
46118	Suinana	25
46119	Sumare	25
46120	Sussui	25
46121	Suzanapolis	25
46122	Suzano	25
46123	Tabajara	25
46124	Tabapua	25
46125	Tabatinga	25
46126	Taboao da Serra	25
46127	Taciba	25
46128	Taguai	25
46129	Taiácu	25
46130	Taiácupeba	25
46131	Taiuva	25
46132	Talhado	25
46133	Tambau	25
46134	Tanabi	25
46135	Tapinas	25
46136	Tapirai	25
46137	Tapiratiba	25
46138	Taquaral	25
46139	Taquaritinga	25
46140	Taquarituba	25
46141	Taquarivai	25
46142	Tarabai	25
46143	Taruma	25
46144	Tatui	25
46145	Taubate	25
46146	Tecainda	25
46147	Tejupa	25
46148	Teodoro Sampaio	25
46149	Termas de Ibira	25
46150	Terra Nova D'oeste	25
46151	Terra Roxa	25
46152	Tibirica	25
46153	Tibirica do Paranapanema	25
46154	Tiete	25
46155	Timburi	25
46156	Toledo	25
46157	Torre de Pedra	25
46158	Torrinha	25
46159	Trabiju	25
46160	Tremembe	25
46161	Três Aliancas	25
46162	Três Fronteiras	25
46163	Três Pontes	25
46164	Três Vendas	25
46165	Tuiuti	25
46166	Tujuguaba	25
46167	Tupa	25
46168	Tupi	25
46169	Tupi Paulista	25
46170	Turiba do Sul	25
46171	Turiuba	25
46172	Turmalina	25
46173	Turvinia	25
46174	Ubarana	25
46175	Ubatuba	25
46176	Ubirajara	25
46177	Uchoa	25
46178	União Paulista	25
46179	Universo	25
46180	Urania	25
46181	Uru	25
46182	Urupes	25
46183	Ururai	25
46184	Utinga	25
46185	Vale Formoso	25
46186	Valentim Gentil	25
46187	Valinhos	25
46188	Valparaiso	25
46189	Vangloria	25
46190	Vargem	25
46191	Vargem Grande do Sul	25
46192	Vargem Grande Paulista	25
46193	Varpa	25
46194	Varzea Paulista	25
46195	Venda Branca	25
46196	Vera Cruz	25
46197	Vicente de Carvalho	25
46198	Vicentinopolis	25
46199	Vila Dirce	25
46200	Vila Nery	25
46201	Vila Xavier	25
46202	Vinhedo	25
46203	Viradouro	25
46204	Vista Alegre do Alto	25
46205	Vitória Brasil	25
46206	Vitoriana	25
46207	Votorantim	25
46208	Votuporanga	25
46209	Zacarias	25
46210	Abreulândia	27
46211	Aguiarnopolis	27
46212	Alianca do Tocantins	27
46213	Almas	27
46214	Alvorada	27
46215	Anajanopolis	27
46216	Ananas	27
46217	Angico	27
46218	Aparecida do Rio Negro	27
46219	Apinaje	27
46220	Aragacui	27
46221	Aragominas	27
46222	Araguacema	27
46223	Araguacu	27
46224	Araguaina	27
46225	Araguana	27
46226	Araguatins	27
46227	Arapoema	27
46228	Arraiás	27
46229	Augustinopolis	27
46230	Aurora do Tocantins	27
46231	Axixa do Tocantins	27
46232	Babaculândia	27
46233	Bandeirantes do Tocantins	27
46234	Barra do Grota	27
46235	Barra do Ouro	27
46236	Barrolândia	27
46237	Bernardo Sayão	27
46238	Bom Jesus do Tocantins	27
46239	Brasilândia	27
46240	Brasilândia do Tocantins	27
46241	Brejinho de Nazaré	27
46242	Buriti do Tocantins	27
46243	Cachoeirinha	27
46244	Campos Lindos	27
46245	Cana Brava	27
46246	Cariri do Tocantins	27
46247	Carmolândia	27
46248	Carrasco Bonito	27
46249	Cartucho	27
46250	Caseara	27
46251	Centenario	27
46252	Chapada da Areia	27
46253	Chapada da Natividade	27
46254	Cocalândia	27
46255	Cocalinho	27
46256	Colinas do Tocantins	27
46257	Colmeia	27
46258	Combinado	27
46259	Conceição do Tocantins	27
46260	Correinha	27
46261	Couto de Magalhaes	27
46262	Crãolândia	27
46263	Cristalândia	27
46264	Crixas	27
46265	Crixas do Tocantins	27
46266	Darcinopolis	27
46267	Dianopolis	27
46268	Divinopolis do Tocantins	27
46269	Dois Irmaos do Tocantins	27
46270	Duere	27
46271	Escondido	27
46272	Esperantina	27
46273	Fatima	27
46274	Figueiropolis	27
46275	Filadelfia	27
46276	Formoso do Araguaiá	27
46277	Fortaleza do Taboção	27
46278	Goiásanorte	27
46279	Goiásatins	27
46280	Guarai	27
46281	Gurupi	27
46282	Ilha Barreira Branca	27
46283	Ipueiras	27
46284	Itacaja	27
46285	Itaguatins	27
46286	Itapiratins	27
46287	Itapora do Tocantins	27
46288	Jau do Tocantins	27
46289	Juarina	27
46290	Jussara	27
46291	Lagoa da Confusão	27
46292	Lagoa do Tocantins	27
46293	Lajeado	27
46294	Lavandeira	27
46295	Lizarda	27
46296	Luzinopolis	27
46297	Marianopolis do Tocantins	27
46298	Mateiros	27
46299	Maurilândia do Tocantins	27
46300	Miracema do Tocantins	27
46301	Mirandopolis	27
46302	Miranorte	27
46303	Monte do Carmo	27
46304	Monte Lindo	27
46305	Monte Santo do Tocantins	27
46306	Mosquito	27
46307	Muricilândia	27
46308	Natal	27
46309	Natividade	27
46310	Nazaré	27
46311	Nova Olinda	27
46312	Nova Rosalândia	27
46313	Novo Acordo	27
46314	Novo Alegre	27
46315	Novo Horizonte	27
46316	Novo Jardim	27
46317	Oliveira de Fatima	27
46318	Palmas	27
46319	Palmeirante	27
46320	Palmeiropolis	27
46321	Paraiso do Tocantins	27
46322	Parana	27
46323	Pau D'arco	27
46324	Pe da Serra	27
46325	Pedro Afonso	27
46326	Pedro Ludovico	27
46327	Peixe	27
46328	Pequizeiro	27
46329	Piloes	27
46330	Pindorama do Tocantins	27
46331	Piraque	27
46332	Pium	27
46333	Ponte Alta do Bom Jesus	27
46334	Ponte Alta do Tocantins	27
46335	Pontes	27
46336	Porãozinho	27
46337	Porto Alegre do Tocantins	27
46338	Porto Lemos	27
46339	Porto Nacional	27
46340	Praiá Norte	27
46341	Presidente Kennedy	27
46342	Principe	27
46343	Pugmil	27
46344	Recursolândia	27
46345	Riachinho	27
46346	Rio da Conceição	27
46347	Rio dos Bois	27
46348	Rio Sono	27
46349	Sampaio	27
46350	Sandolândia	27
46351	Santa Fé do Araguaiá	27
46352	Santa Maria do Tocantins	27
46353	Santa Rita do Tocantins	27
46354	Santa Rosa do Tocantins	27
46355	Santa Tereza do Tocantins	27
46356	Santa Terezinha do Tocantins	27
46357	São Bento do Tocantins	27
46358	São Felix do Tocantins	27
46359	São Miguel do Tocantins	27
46360	São Salvador do Tocantins	27
46361	São Sebastião do Tocantins	27
46362	São Valerio da Natividade	27
46363	Silvanopolis	27
46364	Sitio Novo do Tocantins	27
46365	Sucupira	27
46366	Taguatinga	27
46367	Taipas do Tocantins	27
46368	Talisma	27
46369	Tamboril	27
46370	Taquaralto	27
46371	Taquarussu do Tocantins	27
46372	Tocantinia	27
46373	Tocantinopolis	27
46374	Tupirama	27
46375	Tupirata	27
46376	Tupiratins	27
46377	Venus	27
46378	Wanderlândia	27
46379	Xambioa	27
\.


--
-- TOC entry 2497 (class 0 OID 30831)
-- Dependencies: 214
-- Data for Name: estado; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY estado (id_estado, nome, sigla) FROM stdin;
1	Acre	AC
2	Alagoas	AL
3	Amapá	AP
4	Amazonas	AM
5	Bahia	BA
6	Ceará	CE
7	Distrito Federal	DF
8	Espírito Santo	ES
9	Goiás	GO
10	Maranhão	MA
11	Mato Grosso	MT
12	Mato Grosso do Sul	MS
13	Minas Gerais	MG
14	Pará	PA
15	Paraíba	PB
16	Paraná	PR
17	Pernambuco	PE
18	Piauí	PI
19	Rio de Janeiro	RJ
20	Rio Grande do Norte	RN
21	Rio Grande do Sul	RS
22	Rondônia	RO
23	Roraima	RR
24	Santa Catarina	SC
25	São Paulo	SP
26	Sergipe	SE
27	Tocantins	TO
\.


--
-- TOC entry 2378 (class 2606 OID 30830)
-- Name: pk_cidade; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY cidade
    ADD CONSTRAINT pk_cidade PRIMARY KEY (id_cidade);


--
-- TOC entry 2380 (class 2606 OID 30838)
-- Name: pk_estado; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY estado
    ADD CONSTRAINT pk_estado PRIMARY KEY (id_estado);


--
-- TOC entry 2381 (class 2606 OID 31004)
-- Name: estado_cidade; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY cidade
    ADD CONSTRAINT estado_cidade FOREIGN KEY (id_estado) REFERENCES estado(id_estado);


-- Completed on 2017-10-10 21:59:41 -04

--
-- PostgreSQL database dump complete
--

