/* ---------------------------------------------------------------------- */
/* Script generated with: DeZign for Databases v6.1.0                     */
/* Target DBMS:           PostgreSQL 8.3                                  */
/* Project file:          modelagem_travelcorporate.dez                   */
/* Project name:                                                          */
/* Author:                                                                */
/* Script type:           Alter database script                           */
/* Created on:            2018-02-24 18:33                                */
/* ---------------------------------------------------------------------- */


/* ---------------------------------------------------------------------- */
/* Drop foreign key constraints                                           */
/* ---------------------------------------------------------------------- */
ALTER TABLE public.cidade DROP CONSTRAINT estado_cidade;
ALTER TABLE public.multa DROP CONSTRAINT empresa_multa;
ALTER TABLE public.multa DROP CONSTRAINT funcionario_multa_2;
ALTER TABLE public.multa DROP CONSTRAINT funcionario_multa_3;

/* ---------------------------------------------------------------------- */
/* Modify table "cidade"                                                  */
/* ---------------------------------------------------------------------- */
ALTER TABLE public.cidade ALTER COLUMN id_estado TYPE public.id;

/* ---------------------------------------------------------------------- */
/* Modify table "multa"                                                   */
/* ---------------------------------------------------------------------- */
ALTER TABLE public.multa ADD
    id_cidade public.id;

/* ---------------------------------------------------------------------- */
/* Add foreign key constraints                                            */
/* ---------------------------------------------------------------------- */
ALTER TABLE public.cidade ADD CONSTRAINT estado_cidade 
    FOREIGN KEY (id_estado) REFERENCES public.estado (id_estado);
ALTER TABLE public.multa ADD CONSTRAINT empresa_multa 
    FOREIGN KEY (id_empresa) REFERENCES public.empresa (id_empresa);
ALTER TABLE public.multa ADD CONSTRAINT funcionario_multa_2 
    FOREIGN KEY (id_funcionario_multa) REFERENCES public.funcionario (id_funcionario);
ALTER TABLE public.multa ADD CONSTRAINT funcionario_multa_3 
    FOREIGN KEY (id_funcionario_cadastro) REFERENCES public.funcionario (id_funcionario);
ALTER TABLE public.multa ADD CONSTRAINT cidade_multa 
    FOREIGN KEY (id_cidade) REFERENCES public.cidade (id_cidade);
