/* ---------------------------------------------------------------------- */
/* Script generated with: DeZign for Databases v6.1.0                     */
/* Target DBMS:           PostgreSQL 8.3                                  */
/* Project file:          modelagem_travelcorporate.dez                   */
/* Project name:                                                          */
/* Author:                                                                */
/* Script type:           Alter database script                           */
/* Created on:            2017-10-11 16:19                                */
/* ---------------------------------------------------------------------- */

/* ---------------------------------------------------------------------- */
/* Add sequences                                                          */
/* ---------------------------------------------------------------------- */
CREATE SEQUENCE public.seq_politica INCREMENT 1 START 1;

/* ---------------------------------------------------------------------- */
/* Modify table "recurso"                                                 */
/* ---------------------------------------------------------------------- */
ALTER TABLE public.recurso ADD
    icone CHARACTER VARYING(100);

/* ---------------------------------------------------------------------- */
/* Add table "politica"                                                   */
/* ---------------------------------------------------------------------- */
CREATE TABLE public.politica (
    id_politica public.id  NOT NULL,
    nome CHARACTER VARYING(100)  NOT NULL,
    status public.status,
    texto TEXT,
    id_empresa NUMERIC(8),
    CONSTRAINT pk_politica PRIMARY KEY (id_politica)
);

/* ---------------------------------------------------------------------- */
/* Add foreign key constraints                                            */
/* ---------------------------------------------------------------------- */
ALTER TABLE public.politica ADD CONSTRAINT empresa_politica 
    FOREIGN KEY (id_empresa) REFERENCES public.empresa (id_empresa);
