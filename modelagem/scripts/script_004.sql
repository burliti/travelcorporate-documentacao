﻿/* ---------------------------------------------------------------------- */
/* Script generated with: DeZign for Databases v6.1.0                     */
/* Target DBMS:           PostgreSQL 8.3                                  */
/* Project file:          modelagem_travelcorporate.dez                   */
/* Project name:                                                          */
/* Author:                                                                */
/* Script type:           Alter database script                           */
/* Created on:            2017-10-05 14:52                                */
/* ---------------------------------------------------------------------- */


/* ---------------------------------------------------------------------- */
/* Drop foreign key constraints                                           */
/* ---------------------------------------------------------------------- */
ALTER TABLE public.funcionario DROP CONSTRAINT empresa_funcionario;
ALTER TABLE public.funcionario DROP CONSTRAINT funcao_funcionario;
ALTER TABLE public.projeto DROP CONSTRAINT empresa_projeto;
ALTER TABLE public.cliente DROP CONSTRAINT empresa_cliente;
ALTER TABLE public.lancamento DROP CONSTRAINT viagem_lancamento;
ALTER TABLE public.lancamento DROP CONSTRAINT funcionario_lancamento;
ALTER TABLE public.lancamento DROP CONSTRAINT empresa_lancamento;
ALTER TABLE public.lancamento DROP CONSTRAINT recibo_lancamento;
ALTER TABLE public.lancamento DROP CONSTRAINT categoria_lancamento;
ALTER TABLE public.recibo DROP CONSTRAINT funcionario_recibo;
ALTER TABLE public.recibo DROP CONSTRAINT empresa_recibo;

/* ---------------------------------------------------------------------- */
/* Add sequences                                                          */
/* ---------------------------------------------------------------------- */
CREATE SEQUENCE public.seq_fornecedor INCREMENT 1 START 1;
CREATE SEQUENCE public.seq_fechamento INCREMENT 1 START 1;
CREATE SEQUENCE public.seq_perfil INCREMENT 1 START 1;
CREATE SEQUENCE public.seq_recurso INCREMENT 1 START 1;
CREATE SEQUENCE public.seq_acao INCREMENT 1 START 1;
CREATE SEQUENCE public.seq_acao_recurso INCREMENT 1 START 1;
CREATE SEQUENCE public.seq_estado INCREMENT 1 START 1;
CREATE SEQUENCE public.seq_cidade INCREMENT 1 START 1;

/* ---------------------------------------------------------------------- */
/* Add domains                                                            */
/* ---------------------------------------------------------------------- */
CREATE DOMAIN public.status_fechamento AS NUMERIC(2) CHECK (VALUE IN (1, 2, 3, 99));
CREATE DOMAIN public.monetario AS NUMERIC(10,2);

/* ---------------------------------------------------------------------- */
/* Modify table "funcionario"                                             */
/* ---------------------------------------------------------------------- */
ALTER TABLE public.funcionario ADD
    codigo_referencia CHARACTER VARYING(50);
ALTER TABLE public.funcionario ADD
    id_perfil NUMERIC(8);

/* ---------------------------------------------------------------------- */
/* Modify table "projeto"                                                 */
/* ---------------------------------------------------------------------- */
ALTER TABLE public.projeto ALTER COLUMN valor TYPE public.monetario;

/* ---------------------------------------------------------------------- */
/* Modify table "cliente"                                                 */
/* ---------------------------------------------------------------------- */
ALTER TABLE public.cliente ADD
    codigo_referencia CHARACTER VARYING(50);
ALTER TABLE public.cliente ADD
    cidade CHARACTER VARYING(100);
ALTER TABLE public.cliente ADD
    uf CHARACTER VARYING(2);
ALTER TABLE public.cliente ADD
    observacoes TEXT;
ALTER TABLE public.cliente ADD
    id_cidade NUMERIC(8);

/* ---------------------------------------------------------------------- */
/* Drop and recreate table "lancamento"                                   */
/* ---------------------------------------------------------------------- */

DELETE FROM public.lancamento;

/* Table must be recreated because some of the changes can't be done with the regular commands available. */
ALTER TABLE public.lancamento DROP CONSTRAINT pk_lancamento;
CREATE TABLE public.lancamento_TMP (
    id_lancamento public.id  NOT NULL,
    sequencial NUMERIC(10)  NOT NULL,
    id_empresa NUMERIC(8)  NOT NULL,
    id_funcionario NUMERIC(8)  NOT NULL,
    id_viagem NUMERIC(8),
    id_recibo NUMERIC(8),
    tipo_lancamento public.tipo_lancamento  NOT NULL,
    valor_lancamento public.monetario,
    data_hora_lancamento TIMESTAMP  NOT NULL,
    data_referencia DATE  NOT NULL,
    observacoes CHARACTER VARYING(500),
    id_categoria NUMERIC(8),
    id_cliente NUMERIC(8),
    id_lancamento_referenciado public.id,
    id_fornecedor NUMERIC(8));
INSERT INTO public.lancamento_TMP
    (id_lancamento,id_empresa,id_funcionario,id_viagem,id_recibo,tipo_lancamento,valor_lancamento,data_hora_lancamento,data_referencia,observacoes,id_categoria)
SELECT
    id_lancamento,id_empresa,id_funcionario,id_viagem,id_recibo,tipo_lancamento,valor_lancamento,data_hora_lancamento,data_referencia,observacoes,id_categoria
FROM public.lancamento;

DROP TABLE public.lancamento;
ALTER TABLE public.lancamento_TMP RENAME TO lancamento;
ALTER TABLE public.lancamento ADD CONSTRAINT pk_lancamento
    PRIMARY KEY (id_lancamento);

/* ---------------------------------------------------------------------- */
/* Modify table "recibo"                                                  */
/* ---------------------------------------------------------------------- */
DELETE FROM public.recibo;

ALTER TABLE public.recibo ADD
    valor_recibo public.monetario  NOT NULL;
ALTER TABLE public.recibo ADD
    chave_nfe CHARACTER VARYING(100);
ALTER TABLE public.recibo ADD
    id_fornecedor NUMERIC(8);
COMMENT ON COLUMN public.categoria.flag_interna IS 'Indica se a categoria é interna e não pode ser alterada.';

/* ---------------------------------------------------------------------- */
/* Add table "fechamento"                                                 */
/* ---------------------------------------------------------------------- */
CREATE TABLE public.fechamento (
    id_fechamento public.id  NOT NULL,
    id_empresa NUMERIC(8),
    id_funcionario_fechamento NUMERIC(8)  NOT NULL,
    sequencial NUMERIC(10)  NOT NULL,
    data_hora_cadastro DATE  NOT NULL,
    data_hora_enviado DATE,
    email_destino CHARACTER VARYING(300),
    id_funcionario_recebimento NUMERIC(8),
    id_funcionario_aprovacao NUMERIC(8),
    status public.status_fechamento  NOT NULL,
    saldo_anterior public.monetario  NOT NULL,
    total_debitos public.monetario  NOT NULL,
    total_creditos public.monetario  NOT NULL,
    saldo_final public.monetario  NOT NULL,
    CONSTRAINT pk_fechamento PRIMARY KEY (id_fechamento)
);

/* ---------------------------------------------------------------------- */
/* Add table "lancamento_fechamento"                                      */
/* ---------------------------------------------------------------------- */
CREATE TABLE public.lancamento_fechamento (
    id_lancamento NUMERIC(8)  NOT NULL,
    id_fechamento NUMERIC(8)  NOT NULL,
    CONSTRAINT pk_lancamento_fechamento PRIMARY KEY (id_lancamento, id_fechamento)
);

/* ---------------------------------------------------------------------- */
/* Add table "cidade"                                                    */
/* ---------------------------------------------------------------------- */
CREATE TABLE public.cidade (
    id_cidade public.id  NOT NULL,
    nome CHARACTER VARYING(400)  NOT NULL,
    id_estado NUMERIC(8),
    CONSTRAINT pk_cidade PRIMARY KEY (id_cidade)
);

/* ---------------------------------------------------------------------- */
/* Add table "estado"                                                     */
/* ---------------------------------------------------------------------- */
CREATE TABLE public.estado (
    id_estado public.id  NOT NULL,
    nome CHARACTER VARYING(300)  NOT NULL,
    sigla CHARACTER(2)  NOT NULL,
    CONSTRAINT pk_estado PRIMARY KEY (id_estado)
);

/* ---------------------------------------------------------------------- */
/* Add table "fornecedor"                                                 */
/* ---------------------------------------------------------------------- */
CREATE TABLE public.fornecedor (
    id_fornecedor public.id  NOT NULL,
    id_empresa NUMERIC(8)  NOT NULL,
    nome CHARACTER VARYING(200),
    cpf_cnpj CHARACTER VARYING(20),
    status public.status  NOT NULL,
    CONSTRAINT pk_fornecedor PRIMARY KEY (id_fornecedor)
);

/* ---------------------------------------------------------------------- */
/* Add table "perfil"                                                     */
/* ---------------------------------------------------------------------- */
CREATE TABLE public.perfil (
    id_perfil public.id  NOT NULL,
    nome CHARACTER VARYING(100)  NOT NULL,
    status public.status  NOT NULL,
    id_empresa NUMERIC(8),
    CONSTRAINT pk_perfil PRIMARY KEY (id_perfil)
);

/* ---------------------------------------------------------------------- */
/* Add table "recurso"                                                    */
/* ---------------------------------------------------------------------- */
CREATE TABLE public.recurso (
    id_recurso public.id  NOT NULL,
    nome CHARACTER VARYING(200)  NOT NULL,
    descricao CHARACTER VARYING(400),
    status public.status  NOT NULL,
    CONSTRAINT pk_recurso PRIMARY KEY (id_recurso)
);

/* ---------------------------------------------------------------------- */
/* Add table "perfil_recurso"                                             */
/* ---------------------------------------------------------------------- */
CREATE TABLE public.perfil_recurso (
    id_perfil NUMERIC(8)  NOT NULL,
    id_recurso NUMERIC(8)  NOT NULL,
    CONSTRAINT pk_perfil_recurso PRIMARY KEY (id_perfil, id_recurso)
);

/* ---------------------------------------------------------------------- */
/* Add table "acao"                                                       */
/* ---------------------------------------------------------------------- */
CREATE TABLE public.acao (
    id_acao public.id  NOT NULL,
    nome CHARACTER VARYING(50)  NOT NULL,
    descricao CHARACTER VARYING(100)  NOT NULL,
    status public.status  NOT NULL,
    CONSTRAINT pk_acao PRIMARY KEY (id_acao)
);

/* ---------------------------------------------------------------------- */
/* Add table "acao_recurso"                                               */
/* ---------------------------------------------------------------------- */
CREATE TABLE public.acao_recurso (
    id_acao_recurso public.id  NOT NULL,
    id_acao public.id  NOT NULL,
    id_recurso public.id  NOT NULL,
    CONSTRAINT pk_acao_recurso PRIMARY KEY (id_acao_recurso)
);

/* ---------------------------------------------------------------------- */
/* Add table "perfil_acao_recurso"                                        */
/* ---------------------------------------------------------------------- */
CREATE TABLE public.perfil_acao_recurso (
    id_perfil NUMERIC(8)  NOT NULL,
    id_acao_recurso NUMERIC(8)  NOT NULL,
    CONSTRAINT pk_perfil_acao_recurso PRIMARY KEY (id_perfil, id_acao_recurso)
);

/* ---------------------------------------------------------------------- */
/* Add foreign key constraints                                            */
/* ---------------------------------------------------------------------- */
ALTER TABLE public.funcionario ADD CONSTRAINT empresa_funcionario
    FOREIGN KEY (id_empresa) REFERENCES public.empresa (id_empresa);
ALTER TABLE public.funcionario ADD CONSTRAINT funcao_funcionario
    FOREIGN KEY (id_funcao) REFERENCES public.funcao (id_funcao);
ALTER TABLE public.funcionario ADD CONSTRAINT perfil_funcionario
    FOREIGN KEY (id_perfil) REFERENCES public.perfil (id_perfil);
ALTER TABLE public.projeto ADD CONSTRAINT empresa_projeto
    FOREIGN KEY (id_empresa) REFERENCES public.empresa (id_empresa);
ALTER TABLE public.cliente ADD CONSTRAINT empresa_cliente
    FOREIGN KEY (id_empresa) REFERENCES public.empresa (id_empresa);
ALTER TABLE public.cliente ADD CONSTRAINT cidade_cliente
    FOREIGN KEY (id_cidade) REFERENCES public.cidade (id_cidade);
ALTER TABLE public.lancamento ADD CONSTRAINT viagem_lancamento
    FOREIGN KEY (id_viagem) REFERENCES public.viagem (id_viagem);
ALTER TABLE public.lancamento ADD CONSTRAINT funcionario_lancamento
    FOREIGN KEY (id_funcionario) REFERENCES public.funcionario (id_funcionario);
ALTER TABLE public.lancamento ADD CONSTRAINT empresa_lancamento
    FOREIGN KEY (id_empresa) REFERENCES public.empresa (id_empresa);
ALTER TABLE public.lancamento ADD CONSTRAINT recibo_lancamento
    FOREIGN KEY (id_recibo) REFERENCES public.recibo (id_recibo);
ALTER TABLE public.lancamento ADD CONSTRAINT categoria_lancamento
    FOREIGN KEY (id_categoria) REFERENCES public.categoria (id_categoria);
ALTER TABLE public.lancamento ADD CONSTRAINT cliente_lancamento
    FOREIGN KEY (id_cliente) REFERENCES public.cliente (id_cliente);
ALTER TABLE public.lancamento ADD CONSTRAINT lancamento_lancamento
    FOREIGN KEY (id_lancamento_referenciado) REFERENCES public.lancamento (id_lancamento);
ALTER TABLE public.lancamento ADD CONSTRAINT fornecedor_lancamento
    FOREIGN KEY (id_fornecedor) REFERENCES public.fornecedor (id_fornecedor);
ALTER TABLE public.recibo ADD CONSTRAINT funcionario_recibo
    FOREIGN KEY (id_funcionario) REFERENCES public.funcionario (id_funcionario);
ALTER TABLE public.recibo ADD CONSTRAINT empresa_recibo
    FOREIGN KEY (id_empresa) REFERENCES public.empresa (id_empresa);
ALTER TABLE public.recibo ADD CONSTRAINT fornecedor_recibo
    FOREIGN KEY (id_fornecedor) REFERENCES public.fornecedor (id_fornecedor);
ALTER TABLE public.fechamento ADD CONSTRAINT funcionario_fechamento_1
    FOREIGN KEY (id_funcionario_fechamento) REFERENCES public.funcionario (id_funcionario);
ALTER TABLE public.fechamento ADD CONSTRAINT funcionario_fechamento_2
    FOREIGN KEY (id_funcionario_recebimento) REFERENCES public.funcionario (id_funcionario);
ALTER TABLE public.fechamento ADD CONSTRAINT funcionario_fechamento_3
    FOREIGN KEY (id_funcionario_aprovacao) REFERENCES public.funcionario (id_funcionario);
ALTER TABLE public.fechamento ADD CONSTRAINT empresa_fechamento
    FOREIGN KEY (id_empresa) REFERENCES public.empresa (id_empresa);
ALTER TABLE public.lancamento_fechamento ADD CONSTRAINT lancamento_lancamento_fechamento
    FOREIGN KEY (id_lancamento) REFERENCES public.lancamento (id_lancamento);
ALTER TABLE public.lancamento_fechamento ADD CONSTRAINT fechamento_lancamento_fechamento
    FOREIGN KEY (id_fechamento) REFERENCES public.fechamento (id_fechamento);
ALTER TABLE public.cidade ADD CONSTRAINT estado_cidade
    FOREIGN KEY (id_estado) REFERENCES public.estado (id_estado);
ALTER TABLE public.fornecedor ADD CONSTRAINT empresa_fornecedor
    FOREIGN KEY (id_empresa) REFERENCES public.empresa (id_empresa);
ALTER TABLE public.perfil ADD CONSTRAINT empresa_perfil
    FOREIGN KEY (id_empresa) REFERENCES public.empresa (id_empresa);
ALTER TABLE public.perfil_recurso ADD CONSTRAINT perfil_perfil_recurso
    FOREIGN KEY (id_perfil) REFERENCES public.perfil (id_perfil);
ALTER TABLE public.perfil_recurso ADD CONSTRAINT recurso_perfil_recurso
    FOREIGN KEY (id_recurso) REFERENCES public.recurso (id_recurso);
ALTER TABLE public.acao_recurso ADD CONSTRAINT acao_acao_recurso
    FOREIGN KEY (id_acao) REFERENCES public.acao (id_acao);
ALTER TABLE public.acao_recurso ADD CONSTRAINT recurso_acao_recurso
    FOREIGN KEY (id_recurso) REFERENCES public.recurso (id_recurso);
ALTER TABLE public.perfil_acao_recurso ADD CONSTRAINT perfil_perfil_acao_recurso
    FOREIGN KEY (id_perfil) REFERENCES public.perfil (id_perfil);
ALTER TABLE public.perfil_acao_recurso ADD CONSTRAINT acao_recurso_perfil_acao_recurso
    FOREIGN KEY (id_acao_recurso) REFERENCES public.acao_recurso (id_acao_recurso);
