/* ---------------------------------------------------------------------- */
/* Script generated with: DeZign for Databases v6.1.0                     */
/* Target DBMS:           PostgreSQL 8.3                                  */
/* Project file:          modelagem_travelcorporate.dez                   */
/* Project name:                                                          */
/* Author:                                                                */
/* Script type:           Alter database script                           */
/* Created on:            2017-10-14 03:44                                */
/* ---------------------------------------------------------------------- */

/* ---------------------------------------------------------------------- */
/* Modify table "empresa"                                                 */
/* ---------------------------------------------------------------------- */
ALTER TABLE public.empresa ADD
    logo TEXT;

/* ---------------------------------------------------------------------- */
/* Modify table "fechamento"                                              */
/* ---------------------------------------------------------------------- */
ALTER TABLE public.fechamento ALTER COLUMN data_hora_cadastro TYPE TIMESTAMP;
ALTER TABLE public.fechamento ALTER COLUMN data_hora_enviado TYPE TIMESTAMP;


-- Limpa as funcoes
UPDATE FUNCIONARIO SET ID_FUNCAO =NULL;
DELETE FROM FUNCAO;
