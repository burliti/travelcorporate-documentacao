﻿/* ---------------------------------------------------------------------- */
/* Script generated with: DeZign for Databases v6.1.0                     */
/* Target DBMS:           PostgreSQL 8.3                                  */
/* Project file:          modelagem_travelcorporate.dez                   */
/* Project name:                                                          */
/* Author:                                                                */
/* Script type:           Alter database script                           */
/* Created on:            2018-01-05 22:18                                */
/* ---------------------------------------------------------------------- */


/* ---------------------------------------------------------------------- */
/* Drop foreign key constraints                                           */
/* ---------------------------------------------------------------------- */
ALTER TABLE public.empresa DROP CONSTRAINT empresa_empresa;
ALTER TABLE public.funcionario DROP CONSTRAINT empresa_funcionario;
ALTER TABLE public.funcionario DROP CONSTRAINT funcao_funcionario;
ALTER TABLE public.funcionario DROP CONSTRAINT perfil_funcionario;
ALTER TABLE public.funcionario DROP CONSTRAINT empresa_funcionario_filial;
ALTER TABLE public.funcionario DROP CONSTRAINT setor_funcionario;
ALTER TABLE public.projeto DROP CONSTRAINT empresa_projeto;
ALTER TABLE public.cliente DROP CONSTRAINT empresa_cliente;
ALTER TABLE public.cliente DROP CONSTRAINT cidade_cliente;
ALTER TABLE public.lancamento DROP CONSTRAINT viagem_lancamento;
ALTER TABLE public.lancamento DROP CONSTRAINT funcionario_lancamento_2;
ALTER TABLE public.lancamento DROP CONSTRAINT empresa_lancamento;
ALTER TABLE public.lancamento DROP CONSTRAINT recibo_lancamento;
ALTER TABLE public.lancamento DROP CONSTRAINT categoria_lancamento;
ALTER TABLE public.lancamento DROP CONSTRAINT cliente_lancamento;
ALTER TABLE public.lancamento DROP CONSTRAINT lancamento_lancamento;
ALTER TABLE public.lancamento DROP CONSTRAINT fornecedor_lancamento;
ALTER TABLE public.lancamento DROP CONSTRAINT funcionario_lancamento_9;
ALTER TABLE public.setor DROP CONSTRAINT setor_setor;
ALTER TABLE public.setor DROP CONSTRAINT empresa_setor;

/* ---------------------------------------------------------------------- */
/* Add sequences                                                          */
/* ---------------------------------------------------------------------- */
CREATE SEQUENCE public.seq_centro_custo INCREMENT 1 START 1;

/* ---------------------------------------------------------------------- */
/* Modify table "empresa"                                                 */
/* ---------------------------------------------------------------------- */
ALTER TABLE public.empresa ADD
    id_centro_custo public.id;

/* ---------------------------------------------------------------------- */
/* Modify table "funcionario"                                             */
/* ---------------------------------------------------------------------- */
ALTER TABLE public.funcionario ADD
    id_centro_custo public.id;

/* ---------------------------------------------------------------------- */
/* Modify table "projeto"                                                 */
/* ---------------------------------------------------------------------- */
ALTER TABLE public.projeto ADD
    id_centro_custo public.id;

/* ---------------------------------------------------------------------- */
/* Modify table "cliente"                                                 */
/* ---------------------------------------------------------------------- */
ALTER TABLE public.cliente ADD
    id_centro_custo public.id;

/* ---------------------------------------------------------------------- */
/* Modify table "lancamento"                                              */
/* ---------------------------------------------------------------------- */
ALTER TABLE public.lancamento DROP data_referencia;
COMMENT ON COLUMN public.categoria.flag_interna IS 'Indica se a categoria é interna e não pode ser alterada.';

/* ---------------------------------------------------------------------- */
/* Modify table "setor"                                                   */
/* ---------------------------------------------------------------------- */
ALTER TABLE public.setor ADD
    id_empresa_filial public.id;
ALTER TABLE public.setor ADD
    id_centro_custo NUMERIC(8);

/* ---------------------------------------------------------------------- */
/* Add table "centro_custo"                                               */
/* ---------------------------------------------------------------------- */
CREATE TABLE public.centro_custo (
    id_centro_custo public.id  NOT NULL,
    codigo CHARACTER VARYING(100)  NOT NULL,
    nome CHARACTER VARYING(200)  NOT NULL,
    status public.status  NOT NULL,
    id_empresa public.id,
    id_centro_custo_pai public.id,
    CONSTRAINT pk_centro_custo PRIMARY KEY (id_centro_custo)
);

/* ---------------------------------------------------------------------- */
/* Add foreign key constraints                                            */
/* ---------------------------------------------------------------------- */
ALTER TABLE public.empresa ADD CONSTRAINT empresa_empresa 
    FOREIGN KEY (id_empresa_principal) REFERENCES public.empresa (id_empresa);
ALTER TABLE public.empresa ADD CONSTRAINT centro_custo_empresa 
    FOREIGN KEY (id_centro_custo) REFERENCES public.centro_custo (id_centro_custo);
ALTER TABLE public.funcionario ADD CONSTRAINT empresa_funcionario 
    FOREIGN KEY (id_empresa) REFERENCES public.empresa (id_empresa);
ALTER TABLE public.funcionario ADD CONSTRAINT funcao_funcionario 
    FOREIGN KEY (id_funcao) REFERENCES public.funcao (id_funcao);
ALTER TABLE public.funcionario ADD CONSTRAINT perfil_funcionario 
    FOREIGN KEY (id_perfil) REFERENCES public.perfil (id_perfil);
ALTER TABLE public.funcionario ADD CONSTRAINT empresa_funcionario_filial 
    FOREIGN KEY (id_empresa_filial) REFERENCES public.empresa (id_empresa);
ALTER TABLE public.funcionario ADD CONSTRAINT setor_funcionario 
    FOREIGN KEY (id_setor) REFERENCES public.setor (id_setor);
ALTER TABLE public.funcionario ADD CONSTRAINT centro_custo_funcionario 
    FOREIGN KEY (id_centro_custo) REFERENCES public.centro_custo (id_centro_custo);
ALTER TABLE public.projeto ADD CONSTRAINT empresa_projeto 
    FOREIGN KEY (id_empresa) REFERENCES public.empresa (id_empresa);
ALTER TABLE public.projeto ADD CONSTRAINT centro_custo_projeto 
    FOREIGN KEY (id_centro_custo) REFERENCES public.centro_custo (id_centro_custo);
ALTER TABLE public.cliente ADD CONSTRAINT empresa_cliente 
    FOREIGN KEY (id_empresa) REFERENCES public.empresa (id_empresa);
ALTER TABLE public.cliente ADD CONSTRAINT cidade_cliente 
    FOREIGN KEY (id_cidade) REFERENCES public.cidade (id_cidade);
ALTER TABLE public.cliente ADD CONSTRAINT centro_custo_cliente 
    FOREIGN KEY (id_centro_custo) REFERENCES public.centro_custo (id_centro_custo);
ALTER TABLE public.lancamento ADD CONSTRAINT viagem_lancamento 
    FOREIGN KEY (id_viagem) REFERENCES public.viagem (id_viagem);
ALTER TABLE public.lancamento ADD CONSTRAINT funcionario_lancamento_2 
    FOREIGN KEY (id_funcionario) REFERENCES public.funcionario (id_funcionario);
ALTER TABLE public.lancamento ADD CONSTRAINT empresa_lancamento 
    FOREIGN KEY (id_empresa) REFERENCES public.empresa (id_empresa);
ALTER TABLE public.lancamento ADD CONSTRAINT recibo_lancamento 
    FOREIGN KEY (id_recibo) REFERENCES public.recibo (id_recibo);
ALTER TABLE public.lancamento ADD CONSTRAINT categoria_lancamento 
    FOREIGN KEY (id_categoria) REFERENCES public.categoria (id_categoria);
ALTER TABLE public.lancamento ADD CONSTRAINT cliente_lancamento 
    FOREIGN KEY (id_cliente) REFERENCES public.cliente (id_cliente);
ALTER TABLE public.lancamento ADD CONSTRAINT lancamento_lancamento 
    FOREIGN KEY (id_lancamento_referenciado) REFERENCES public.lancamento (id_lancamento);
ALTER TABLE public.lancamento ADD CONSTRAINT fornecedor_lancamento 
    FOREIGN KEY (id_fornecedor) REFERENCES public.fornecedor (id_fornecedor);
ALTER TABLE public.lancamento ADD CONSTRAINT funcionario_lancamento_9 
    FOREIGN KEY (id_funcionario_lancamento) REFERENCES public.funcionario (id_funcionario);
ALTER TABLE public.setor ADD CONSTRAINT setor_setor 
    FOREIGN KEY (id_setor_pai) REFERENCES public.setor (id_setor);
ALTER TABLE public.setor ADD CONSTRAINT empresa_setor 
    FOREIGN KEY (id_empresa) REFERENCES public.empresa (id_empresa);
ALTER TABLE public.setor ADD CONSTRAINT empresa_setor_filial 
    FOREIGN KEY (id_empresa_filial) REFERENCES public.empresa (id_empresa);
ALTER TABLE public.setor ADD CONSTRAINT centro_custo_setor 
    FOREIGN KEY (id_centro_custo) REFERENCES public.centro_custo (id_centro_custo);
ALTER TABLE public.centro_custo ADD CONSTRAINT empresa_centro_custo 
    FOREIGN KEY (id_empresa) REFERENCES public.empresa (id_empresa);
ALTER TABLE public.centro_custo ADD CONSTRAINT centro_custo_centro_custo 
    FOREIGN KEY (id_centro_custo_pai) REFERENCES public.centro_custo (id_centro_custo);
