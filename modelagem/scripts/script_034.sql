/* ---------------------------------------------------------------------- */
/* Script generated with: DeZign for Databases v6.1.0                     */
/* Target DBMS:           PostgreSQL 8.3                                  */
/* Project file:          modelagem_travelcorporate.dez                   */
/* Project name:                                                          */
/* Author:                                                                */
/* Script type:           Alter database script                           */
/* Created on:            2018-02-26 22:03                                */
/* ---------------------------------------------------------------------- */


/* ---------------------------------------------------------------------- */
/* Drop foreign key constraints                                           */
/* ---------------------------------------------------------------------- */
ALTER TABLE public.configuracao_funcionarios_aprovacao DROP CONSTRAINT funcionario_configuracao_funcionarios_aprovacao;
ALTER TABLE public.configuracao_funcionarios_aprovacao DROP CONSTRAINT configuracao_aprovacao_viagem_2;

/* ---------------------------------------------------------------------- */
/* Modify table "configuracao_funcionarios_aprovacao"                     */
/* ---------------------------------------------------------------------- */
ALTER TABLE public.configuracao_funcionarios_aprovacao ADD
    id_empresa public.id  NOT NULL;

/* ---------------------------------------------------------------------- */
/* Add foreign key constraints                                            */
/* ---------------------------------------------------------------------- */
ALTER TABLE public.configuracao_funcionarios_aprovacao ADD CONSTRAINT funcionario_configuracao_funcionarios_aprovacao 
    FOREIGN KEY (id_funcionario) REFERENCES public.funcionario (id_funcionario);
ALTER TABLE public.configuracao_funcionarios_aprovacao ADD CONSTRAINT configuracao_aprovacao_viagem_2 
    FOREIGN KEY (id_configuracao_aprovacao_viagem) REFERENCES public.configuracao_aprovacao_viagem (id_configuracao_aprovacao_viagem);
ALTER TABLE public.configuracao_funcionarios_aprovacao ADD CONSTRAINT empresa_configuracao_funcionarios_aprovacao 
    FOREIGN KEY (id_empresa) REFERENCES public.empresa (id_empresa);
