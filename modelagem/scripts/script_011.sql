/* ---------------------------------------------------------------------- */
/* Script generated with: DeZign for Databases v6.1.0                     */
/* Target DBMS:           PostgreSQL 8.3                                  */
/* Project file:          modelagem_travelcorporate.dez                   */
/* Project name:                                                          */
/* Author:                                                                */
/* Script type:           Alter database script                           */
/* Created on:            2017-10-15 18:44                                */
/* ---------------------------------------------------------------------- */



/* ---------------------------------------------------------------------- */
/* Modify domain "status_fechamento"                                      */
/* ---------------------------------------------------------------------- */
ALTER DOMAIN public.status_fechamento DROP CONSTRAINT status_fechamento_check;
ALTER DOMAIN public.status_fechamento ADD CONSTRAINT status_fechamento_check CHECK (VALUE IN (1, 2, 3, 4, 99));

/* ---------------------------------------------------------------------- */
/* Modify table "fechamento"                                              */
/* ---------------------------------------------------------------------- */
ALTER TABLE public.fechamento ADD
    motivo_rejeicao TEXT;

