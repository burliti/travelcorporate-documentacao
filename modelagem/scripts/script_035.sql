/* ---------------------------------------------------------------------- */
/* Script generated with: DeZign for Databases v6.1.0                     */
/* Target DBMS:           PostgreSQL 8.3                                  */
/* Project file:          modelagem_travelcorporate.dez                   */
/* Project name:                                                          */
/* Author:                                                                */
/* Script type:           Alter database script                           */
/* Created on:            2018-04-19 17:02                                */
/* ---------------------------------------------------------------------- */


/* ---------------------------------------------------------------------- */
/* Drop foreign key constraints                                           */
/* ---------------------------------------------------------------------- */
ALTER TABLE public.projeto DROP CONSTRAINT centro_custo_projeto;
ALTER TABLE public.projeto DROP CONSTRAINT empresa_projeto;
ALTER TABLE public.viagem DROP CONSTRAINT cliente_viagem;
ALTER TABLE public.viagem DROP CONSTRAINT empresa_viagem;
ALTER TABLE public.viagem DROP CONSTRAINT funcionario_viagem;
ALTER TABLE public.viagem DROP CONSTRAINT projeto_viagem;
ALTER TABLE public.lancamento DROP CONSTRAINT categoria_lancamento;
ALTER TABLE public.lancamento DROP CONSTRAINT cliente_lancamento;
ALTER TABLE public.lancamento DROP CONSTRAINT empresa_lancamento;
ALTER TABLE public.lancamento DROP CONSTRAINT fornecedor_lancamento;
ALTER TABLE public.lancamento DROP CONSTRAINT funcionario_lancamento_10;
ALTER TABLE public.lancamento DROP CONSTRAINT funcionario_lancamento_2;
ALTER TABLE public.lancamento DROP CONSTRAINT funcionario_lancamento_9;
ALTER TABLE public.lancamento DROP CONSTRAINT lancamento_lancamento;
ALTER TABLE public.lancamento DROP CONSTRAINT recibo_lancamento;
ALTER TABLE public.lancamento DROP CONSTRAINT viagem_lancamento;
ALTER TABLE public.categoria DROP CONSTRAINT empresa_categoria;
ALTER TABLE public.setor DROP CONSTRAINT centro_custo_setor;
ALTER TABLE public.setor DROP CONSTRAINT empresa_setor;
ALTER TABLE public.setor DROP CONSTRAINT empresa_setor_filial;
ALTER TABLE public.setor DROP CONSTRAINT setor_setor;

/* ---------------------------------------------------------------------- */
/* Add sequences                                                          */
/* ---------------------------------------------------------------------- */
CREATE SEQUENCE public.seq_solicitacao INCREMENT 1 START 1;
CREATE SEQUENCE public.seq_solicitacao_aereo INCREMENT 1 START 1;
CREATE SEQUENCE public.seq_solicitacao_terrestre INCREMENT 1 START 1;
CREATE SEQUENCE public.seq_solicitacao_hospedagem INCREMENT 1 START 1;
CREATE SEQUENCE public.seq_solicitacao_aluguel_veiculo INCREMENT 1 START 1;
CREATE SEQUENCE public.seq_solicitacao_adiantamento INCREMENT 1 START 1;
CREATE SEQUENCE public.seq_anexo INCREMENT 1 START 1;
CREATE SEQUENCE public.seq_solicitacao_aereo_reserva INCREMENT 1 START 1;
CREATE SEQUENCE public.seq_solicitacao_hospedagem_reserva INCREMENT 1 START 1;
CREATE SEQUENCE public.seq_solicitacao_adiantamento_comprovante INCREMENT 1 START 1;
CREATE SEQUENCE public.seq_solicitacao_terrestre_reserva INCREMENT 1 START 1;
CREATE SEQUENCE public.seq_solicitacao_aluguel_veiculo_reserva INCREMENT 1 START 1;

/* ---------------------------------------------------------------------- */
/* Add domains                                                            */
/* ---------------------------------------------------------------------- */
CREATE DOMAIN public.status_solicitacao_viagem AS NUMERIC(2) CHECK (VALUE IN (1, 2, 3, 4, 5, 6, 7, 8));
CREATE DOMAIN public.forma_adiantamento AS NUMERIC(1) CHECK (VALUE IN (1, 2, 3));
CREATE DOMAIN public.tipo_passagem AS NUMERIC(1) CHECK (VALUE IN (1, 2));

/* ---------------------------------------------------------------------- */
/* Modify table "projeto"                                                 */
/* ---------------------------------------------------------------------- */
ALTER TABLE public.projeto ADD
    codigo_referencia CHARACTER VARYING(50);

/* ---------------------------------------------------------------------- */
/* Modify table "viagem"                                                  */
/* ---------------------------------------------------------------------- */
ALTER TABLE public.viagem ADD
    id_solicitacao public.id;

/* ---------------------------------------------------------------------- */
/* Modify table "lancamento"                                              */
/* ---------------------------------------------------------------------- */
ALTER TABLE public.lancamento ADD
    flag_contabiliza_saldo_viagem public.sim_nao DEFAULT 'S'  NOT NULL;
ALTER TABLE public.lancamento ALTER COLUMN flag_glosado DROP DEFAULT;
ALTER TABLE public.lancamento ALTER COLUMN flag_glosado SET DEFAULT 'N';

/* ---------------------------------------------------------------------- */
/* Modify table "categoria"                                               */
/* ---------------------------------------------------------------------- */
ALTER TABLE public.categoria ADD
    flag_contabiliza_saldo_viagem public.sim_nao DEFAULT 'S'  NOT NULL;
ALTER TABLE public.categoria ALTER COLUMN id_empresa TYPE public.id;
COMMENT ON COLUMN public.categoria.flag_interna IS 'Indica se a categoria é interna e não pode ser alterada.';

/* ---------------------------------------------------------------------- */
/* Modify table "setor"                                                   */
/* ---------------------------------------------------------------------- */
ALTER TABLE public.setor ADD
    codigo_referencia CHARACTER VARYING(50);

/* ---------------------------------------------------------------------- */
/* Add table "modelo_mensagem"                                            */
/* ---------------------------------------------------------------------- */
CREATE TABLE public.modelo_mensagem (
    id_modelo_mensagem public.id  NOT NULL,
    nome CHARACTER VARYING(100),
    status public.status  NOT NULL,
    id_empresa NUMERIC(8),
    CONSTRAINT pk_modelo_mensagem PRIMARY KEY (id_modelo_mensagem)
);

/* ---------------------------------------------------------------------- */
/* Add table "solicitacao"                                                */
/* ---------------------------------------------------------------------- */
CREATE TABLE public.solicitacao (
    id_solicitacao public.id  NOT NULL,
    id_empresa public.id  NOT NULL,
    sequencial public.id  NOT NULL,
    descricao CHARACTER VARYING(500),
    objetivo CHARACTER VARYING(150),
    origem CHARACTER VARYING(400),
    destino CHARACTER VARYING(400),
    data_ida DATE,
    data_volta DATE,
    status public.status_solicitacao_viagem  NOT NULL,
    id_projeto public.id,
    id_cliente public.id,
    id_centro_custo public.id,
    observacao TEXT,
    flag_aereo public.sim_nao  NOT NULL,
    flag_terrestre public.sim_nao  NOT NULL,
    flag_hospedagem public.sim_nao  NOT NULL,
    flag_veiculo public.sim_nao  NOT NULL,
    flag_adiantamento public.sim_nao  NOT NULL,
    id_funcionario_solicitante public.id  NOT NULL,
    id_filial public.id,
    CONSTRAINT pk_solicitacao PRIMARY KEY (id_solicitacao)
);

/* ---------------------------------------------------------------------- */
/* Add table "solicitacao_historico"                                      */
/* ---------------------------------------------------------------------- */
CREATE TABLE public.solicitacao_historico (
    id_solicitacao_historico public.id  NOT NULL,
    id_empresa public.id  NOT NULL,
    id_solicitacao public.id  NOT NULL,
    descricao CHARACTER VARYING(500),
    status public.status_solicitacao_viagem  NOT NULL,
    data_hora TIMESTAMP  NOT NULL,
    id_funcionario public.id,
    CONSTRAINT pk_solicitacao_historico PRIMARY KEY (id_solicitacao_historico)
);

/* ---------------------------------------------------------------------- */
/* Add table "solicitacao_aereo"                                          */
/* ---------------------------------------------------------------------- */
CREATE TABLE public.solicitacao_aereo (
    id_solicitacao_aereo public.id  NOT NULL,
    id_empresa public.id  NOT NULL,
    id_solicitacao public.id  NOT NULL,
    origem CHARACTER VARYING(400),
    destino CHARACTER VARYING(400),
    data_ida DATE,
    data_volta DATE,
    observacao TEXT,
    CONSTRAINT pk_solicitacao_aereo PRIMARY KEY (id_solicitacao_aereo)
);

/* ---------------------------------------------------------------------- */
/* Add table "solicitacao_aereo_funcionario"                              */
/* ---------------------------------------------------------------------- */
CREATE TABLE public.solicitacao_aereo_funcionario (
    id_solicitacao_aereo public.id  NOT NULL,
    id_funcionario public.id  NOT NULL,
    CONSTRAINT pk_solicitacao_aereo_funcionario PRIMARY KEY (id_solicitacao_aereo, id_funcionario)
);

/* ---------------------------------------------------------------------- */
/* Add table "solicitacao_terrestre"                                      */
/* ---------------------------------------------------------------------- */
CREATE TABLE public.solicitacao_terrestre (
    id_solicitacao_terrestre public.id  NOT NULL,
    id_solicitacao public.id  NOT NULL,
    id_empresa public.id  NOT NULL,
    origem CHARACTER VARYING(400),
    destino CHARACTER VARYING(400),
    data_ida DATE,
    data_volta DATE,
    observacao TEXT,
    CONSTRAINT pk_solicitacao_terrestre PRIMARY KEY (id_solicitacao_terrestre)
);

/* ---------------------------------------------------------------------- */
/* Add table "solicitacao_terrestre_funcionario"                          */
/* ---------------------------------------------------------------------- */
CREATE TABLE public.solicitacao_terrestre_funcionario (
    id_solicitacao_terrestre public.id  NOT NULL,
    id_funcionario public.id  NOT NULL,
    CONSTRAINT pk_solicitacao_terrestre_funcionario PRIMARY KEY (id_solicitacao_terrestre, id_funcionario)
);

/* ---------------------------------------------------------------------- */
/* Add table "solicitacao_funcionario"                                    */
/* ---------------------------------------------------------------------- */
CREATE TABLE public.solicitacao_funcionario (
    id_solicitacao public.id  NOT NULL,
    id_funcionario public.id  NOT NULL,
    CONSTRAINT pk_solicitacao_funcionario PRIMARY KEY (id_solicitacao, id_funcionario)
);

/* ---------------------------------------------------------------------- */
/* Add table "solicitacao_hospedagem"                                     */
/* ---------------------------------------------------------------------- */
CREATE TABLE public.solicitacao_hospedagem (
    id_solicitacao_hospedagem public.id  NOT NULL,
    id_solicitacao public.id  NOT NULL,
    id_empresa public.id  NOT NULL,
    data_checkin DATE,
    data_checkout DATE,
    quantidade NUMERIC(4)  NOT NULL,
    cidade CHARACTER VARYING(200),
    local_preferencia CHARACTER VARYING(200),
    observacao TEXT,
    CONSTRAINT pk_solicitacao_hospedagem PRIMARY KEY (id_solicitacao_hospedagem)
);

/* ---------------------------------------------------------------------- */
/* Add table "solicitacao_aluguel_veiculo"                                */
/* ---------------------------------------------------------------------- */
CREATE TABLE public.solicitacao_aluguel_veiculo (
    id_solicitacao_veiculo public.id  NOT NULL,
    id_solicitacao public.id  NOT NULL,
    id_empresa public.id  NOT NULL,
    flag_entrega_igual_retirada public.sim_nao  NOT NULL,
    data_hora_retirada DATE,
    data_hora_entrega DATE,
    quantidade_ocupantes NUMERIC(4)  NOT NULL,
    observacao TEXT,
    id_funcionario_motorista public.id,
    CONSTRAINT pk_solicitacao_aluguel_veiculo PRIMARY KEY (id_solicitacao_veiculo)
);

/* ---------------------------------------------------------------------- */
/* Add table "solicitacao_adiantamento"                                   */
/* ---------------------------------------------------------------------- */
CREATE TABLE public.solicitacao_adiantamento (
    id_solicitacao_adiantamento public.id  NOT NULL,
    id_solicitacao public.id  NOT NULL,
    id_empresa public.id  NOT NULL,
    valor_diaria public.monetario,
    quantidade_diarias NUMERIC(4),
    valor_adiantamento public.monetario  NOT NULL,
    observacao TEXT,
    CONSTRAINT pk_solicitacao_adiantamento PRIMARY KEY (id_solicitacao_adiantamento)
);

/* ---------------------------------------------------------------------- */
/* Add table "solicitacao_aluguel_veiculo_funcionario"                    */
/* ---------------------------------------------------------------------- */
CREATE TABLE public.solicitacao_aluguel_veiculo_funcionario (
    id_solicitacao_veiculo public.id  NOT NULL,
    id_funcionario public.id  NOT NULL,
    CONSTRAINT pk_solicitacao_aluguel_veiculo_funcionario PRIMARY KEY (id_solicitacao_veiculo, id_funcionario)
);

/* ---------------------------------------------------------------------- */
/* Add table "solicitacao_hospedagem_funcionario"                         */
/* ---------------------------------------------------------------------- */
CREATE TABLE public.solicitacao_hospedagem_funcionario (
    id_solicitacao_hospedagem public.id  NOT NULL,
    id_funcionario public.id  NOT NULL,
    CONSTRAINT pk_solicitacao_hospedagem_funcionario PRIMARY KEY (id_solicitacao_hospedagem, id_funcionario)
);

/* ---------------------------------------------------------------------- */
/* Add table "solicitacao_adiantamento_funcionario"                       */
/* ---------------------------------------------------------------------- */
CREATE TABLE public.solicitacao_adiantamento_funcionario (
    id_solicitacao_adiantamento public.id  NOT NULL,
    id_funcionario public.id  NOT NULL,
    CONSTRAINT pk_solicitacao_adiantamento_funcionario PRIMARY KEY (id_solicitacao_adiantamento, id_funcionario)
);

/* ---------------------------------------------------------------------- */
/* Add table "anexo"                                                      */
/* ---------------------------------------------------------------------- */
CREATE TABLE public.anexo (
    id_anexo public.id  NOT NULL,
    id_empresa public.id  NOT NULL,
    id_tipo_anexo public.id  NOT NULL,
    hash CHARACTER VARYING(50)  NOT NULL,
    arquivo BYTEA,
    extensao CHARACTER VARYING(10)  NOT NULL,
    CONSTRAINT pk_anexo PRIMARY KEY (id_anexo)
);

/* ---------------------------------------------------------------------- */
/* Add table "tipo_anexo"                                                 */
/* ---------------------------------------------------------------------- */
CREATE TABLE public.tipo_anexo (
    id_tipo_anexo public.id  NOT NULL,
    nome CHARACTER VARYING(100),
    status public.status  NOT NULL,
    CONSTRAINT pk_tipo_anexo PRIMARY KEY (id_tipo_anexo)
);
COMMENT ON COLUMN public.tipo_anexo.id_tipo_anexo IS 'Tabela estática de tipos de anexos';

/* ---------------------------------------------------------------------- */
/* Add table "solicitacao_aereo_reserva"                                  */
/* ---------------------------------------------------------------------- */
CREATE TABLE public.solicitacao_aereo_reserva (
    id_solicitacao_aereo_reserva public.id  NOT NULL,
    id_solicitacao_aereo public.id  NOT NULL,
    id_empresa public.id NOT NULL,
    codigo_reserva CHARACTER VARYING(50),
    numero_voo CHARACTER VARYING(40),
    data_hora_partida TIMESTAMP,
    data_hora_chegada TIMESTAMP,
    cidade_partida CHARACTER VARYING(200),
    cidade_chegada CHARACTER VARYING(200),
    aeroporto_partida CHARACTER VARYING(200),
    aeroporto_chegada CHARACTER VARYING(200),
    id_funcionario public.id  NOT NULL,
    tipo public.tipo_passagem NOT NULL,
    CONSTRAINT pk_solicitacao_aereo_reserva PRIMARY KEY (id_solicitacao_aereo_reserva)
);

/* ---------------------------------------------------------------------- */
/* Add table "solicitacao_hospedagem_reserva"                             */
/* ---------------------------------------------------------------------- */
CREATE TABLE public.solicitacao_hospedagem_reserva (
    id_solicitacao_hospedagem_reserva public.id  NOT NULL,
    id_solicitacao_hospedagem public.id  NOT NULL,
    id_empresa public.id  NOT NULL,
    data_checkin DATE,
    data_checkout DATE,
    nome_hotel CHARACTER VARYING(200),
    endereco_hotel CHARACTER VARYING(500),
    latitude_hotel CHARACTER VARYING(100),
    longitude_hotel CHARACTER VARYING(100),
    CONSTRAINT pk_solicitacao_hospedagem_reserva PRIMARY KEY (id_solicitacao_hospedagem_reserva)
);

/* ---------------------------------------------------------------------- */
/* Add table "funcionario_solicitacao_hospedagem_reserva"                 */
/* ---------------------------------------------------------------------- */
CREATE TABLE public.funcionario_solicitacao_hospedagem_reserva (
    id_funcionario public.id  NOT NULL,
    id_solicitacao_hospedagem_reserva public.id  NOT NULL,
    CONSTRAINT pk_funcionario_solicitacao_hospedagem_reserva PRIMARY KEY (id_funcionario, id_solicitacao_hospedagem_reserva)
);

/* ---------------------------------------------------------------------- */
/* Add table "anexo_solicitacao_aereo_reserva"                            */
/* ---------------------------------------------------------------------- */
CREATE TABLE public.anexo_solicitacao_aereo_reserva (
    id_anexo public.id  NOT NULL,
    id_solicitacao_aereo_reserva public.id  NOT NULL,
    CONSTRAINT pk_anexo_solicitacao_aereo_reserva PRIMARY KEY (id_anexo, id_solicitacao_aereo_reserva)
);

/* ---------------------------------------------------------------------- */
/* Add table "solicitacao_adiantamento_comprovante"                       */
/* ---------------------------------------------------------------------- */
CREATE TABLE public.solicitacao_adiantamento_comprovante (
    id_solicitacao_adiantamento_comprovante public.id  NOT NULL,
    id_solicitacao_adiantamento public.id  NOT NULL,
    id_categoria public.id  NOT NULL,
    data_deposito_transferencia DATE,
    valor public.monetario  NOT NULL,
    forma public.forma_adiantamento  NOT NULL,
    id_funcionario public.id  NOT NULL,
    id_funcionario_lancamento public.id  NOT NULL,
    id_empresa public.id  NOT NULL,
    CONSTRAINT pk_solicitacao_adiantamento_comprovante PRIMARY KEY (id_solicitacao_adiantamento_comprovante)
);

/* ---------------------------------------------------------------------- */
/* Add table "anexo_solicitacao_adiantamento_comprovante"                 */
/* ---------------------------------------------------------------------- */
CREATE TABLE public.anexo_solicitacao_adiantamento_comprovante (
    id_anexo public.id  NOT NULL,
    id_solicitacao_adiantamento_comprovante public.id  NOT NULL,
    CONSTRAINT pk_anexo_solicitacao_adiantamento_comprovante PRIMARY KEY (id_anexo, id_solicitacao_adiantamento_comprovante)
);

/* ---------------------------------------------------------------------- */
/* Add table "solicitacao_terrestre_reserva"                              */
/* ---------------------------------------------------------------------- */
CREATE TABLE public.solicitacao_terrestre_reserva (
    id_solicitacao_terrestre_reserva public.id  NOT NULL,
    id_solicitacao_terrestre public.id  NOT NULL,
    id_funcionario public.id  NOT NULL,
    id_funcionario_lancamento public.id  NOT NULL,
    id_empresa public.id  NOT NULL,
    codigo_reserva CHARACTER VARYING(50),
    numero_onibus CHARACTER VARYING(50),
    data_hora_partida TIMESTAMP,
    data_hora_chegada TIMESTAMP,
    cidade_partida CHARACTER VARYING(200),
    cidade_chegada CHARACTER VARYING(200),
    tipo public.tipo_passagem NOT NULL,
    CONSTRAINT pk_solicitacao_terrestre_reserva PRIMARY KEY (id_solicitacao_terrestre_reserva)
);

/* ---------------------------------------------------------------------- */
/* Add table "solicitacao_aluguel_veiculo_reserva"                        */
/* ---------------------------------------------------------------------- */
CREATE TABLE public.solicitacao_aluguel_veiculo_reserva (
    id_solicitacao_aluguel_veiculo_reserva public.id  NOT NULL,
    id_solicitacao_veiculo public.id  NOT NULL,
    id_empresa public.id  NOT NULL,
    codigo_reserva CHARACTER VARYING(100),
    modelo_carro CHARACTER VARYING(100),
    data_hora_retirar TIMESTAMP,
    data_hora_devolver TIMESTAMP,
    local_retirar CHARACTER VARYING(100),
    local_devolver CHARACTER VARYING(100),
    id_funcionario_responsavel public.id,
    id_funcionario_lancamento public.id,
    CONSTRAINT pk_solicitacao_aluguel_veiculo_reserva PRIMARY KEY (id_solicitacao_aluguel_veiculo_reserva)
);

/* ---------------------------------------------------------------------- */
/* Add foreign key constraints                                            */
/* ---------------------------------------------------------------------- */
ALTER TABLE public.projeto ADD CONSTRAINT empresa_projeto 
    FOREIGN KEY (id_empresa) REFERENCES public.empresa (id_empresa);
ALTER TABLE public.projeto ADD CONSTRAINT centro_custo_projeto 
    FOREIGN KEY (id_centro_custo) REFERENCES public.centro_custo (id_centro_custo);
ALTER TABLE public.viagem ADD CONSTRAINT cliente_viagem 
    FOREIGN KEY (id_cliente) REFERENCES public.cliente (id_cliente);
ALTER TABLE public.viagem ADD CONSTRAINT projeto_viagem 
    FOREIGN KEY (id_projeto) REFERENCES public.projeto (id_projeto);
ALTER TABLE public.viagem ADD CONSTRAINT empresa_viagem 
    FOREIGN KEY (id_empresa) REFERENCES public.empresa (id_empresa);
ALTER TABLE public.viagem ADD CONSTRAINT funcionario_viagem 
    FOREIGN KEY (id_funcionario) REFERENCES public.funcionario (id_funcionario);
ALTER TABLE public.viagem ADD CONSTRAINT solicitacao_viagem 
    FOREIGN KEY (id_solicitacao) REFERENCES public.solicitacao (id_solicitacao);
ALTER TABLE public.lancamento ADD CONSTRAINT viagem_lancamento 
    FOREIGN KEY (id_viagem) REFERENCES public.viagem (id_viagem);
ALTER TABLE public.lancamento ADD CONSTRAINT funcionario_lancamento_2 
    FOREIGN KEY (id_funcionario) REFERENCES public.funcionario (id_funcionario);
ALTER TABLE public.lancamento ADD CONSTRAINT empresa_lancamento 
    FOREIGN KEY (id_empresa) REFERENCES public.empresa (id_empresa);
ALTER TABLE public.lancamento ADD CONSTRAINT recibo_lancamento 
    FOREIGN KEY (id_recibo) REFERENCES public.recibo (id_recibo);
ALTER TABLE public.lancamento ADD CONSTRAINT categoria_lancamento 
    FOREIGN KEY (id_categoria) REFERENCES public.categoria (id_categoria);
ALTER TABLE public.lancamento ADD CONSTRAINT cliente_lancamento 
    FOREIGN KEY (id_cliente) REFERENCES public.cliente (id_cliente);
ALTER TABLE public.lancamento ADD CONSTRAINT lancamento_lancamento 
    FOREIGN KEY (id_lancamento_referenciado) REFERENCES public.lancamento (id_lancamento);
ALTER TABLE public.lancamento ADD CONSTRAINT fornecedor_lancamento 
    FOREIGN KEY (id_fornecedor) REFERENCES public.fornecedor (id_fornecedor);
ALTER TABLE public.lancamento ADD CONSTRAINT funcionario_lancamento_9 
    FOREIGN KEY (id_funcionario_lancamento) REFERENCES public.funcionario (id_funcionario);
ALTER TABLE public.lancamento ADD CONSTRAINT funcionario_lancamento_10 
    FOREIGN KEY (id_funcionario_glosa) REFERENCES public.funcionario (id_funcionario);
ALTER TABLE public.categoria ADD CONSTRAINT empresa_categoria 
    FOREIGN KEY (id_empresa) REFERENCES public.empresa (id_empresa);
ALTER TABLE public.setor ADD CONSTRAINT setor_setor 
    FOREIGN KEY (id_setor_pai) REFERENCES public.setor (id_setor);
ALTER TABLE public.setor ADD CONSTRAINT empresa_setor 
    FOREIGN KEY (id_empresa) REFERENCES public.empresa (id_empresa);
ALTER TABLE public.setor ADD CONSTRAINT empresa_setor_filial 
    FOREIGN KEY (id_empresa_filial) REFERENCES public.empresa (id_empresa);
ALTER TABLE public.setor ADD CONSTRAINT centro_custo_setor 
    FOREIGN KEY (id_centro_custo) REFERENCES public.centro_custo (id_centro_custo);
ALTER TABLE public.modelo_mensagem ADD CONSTRAINT empresa_modelo_mensagem 
    FOREIGN KEY (id_empresa) REFERENCES public.empresa (id_empresa);
ALTER TABLE public.solicitacao ADD CONSTRAINT empresa_solicitacao_1 
    FOREIGN KEY (id_empresa) REFERENCES public.empresa (id_empresa);
ALTER TABLE public.solicitacao ADD CONSTRAINT projeto_solicitacao 
    FOREIGN KEY (id_projeto) REFERENCES public.projeto (id_projeto);
ALTER TABLE public.solicitacao ADD CONSTRAINT cliente_solicitacao 
    FOREIGN KEY (id_cliente) REFERENCES public.cliente (id_cliente);
ALTER TABLE public.solicitacao ADD CONSTRAINT funcionario_solicitacao 
    FOREIGN KEY (id_funcionario_solicitante) REFERENCES public.funcionario (id_funcionario);
ALTER TABLE public.solicitacao ADD CONSTRAINT empresa_solicitacao_5 
    FOREIGN KEY (id_filial) REFERENCES public.empresa (id_empresa);
ALTER TABLE public.solicitacao ADD CONSTRAINT centro_custo_solicitacao 
    FOREIGN KEY (id_centro_custo) REFERENCES public.centro_custo (id_centro_custo);
ALTER TABLE public.solicitacao_historico ADD CONSTRAINT funcionario_solicitacao_historico 
    FOREIGN KEY (id_funcionario) REFERENCES public.funcionario (id_funcionario);
ALTER TABLE public.solicitacao_historico ADD CONSTRAINT solicitacao_solicitacao_historico 
    FOREIGN KEY (id_solicitacao) REFERENCES public.solicitacao (id_solicitacao);
ALTER TABLE public.solicitacao_historico ADD CONSTRAINT empresa_solicitacao_historico 
    FOREIGN KEY (id_empresa) REFERENCES public.empresa (id_empresa);
ALTER TABLE public.solicitacao_aereo ADD CONSTRAINT solicitacao_solicitacao_aereo 
    FOREIGN KEY (id_solicitacao) REFERENCES public.solicitacao (id_solicitacao);
ALTER TABLE public.solicitacao_aereo ADD CONSTRAINT empresa_solicitacao_aereo 
    FOREIGN KEY (id_empresa) REFERENCES public.empresa (id_empresa);
ALTER TABLE public.solicitacao_aereo_reserva ADD CONSTRAINT empresa_solicitacao_aereo_reserva
    FOREIGN KEY (id_empresa) REFERENCES public.empresa (id_empresa);
ALTER TABLE public.solicitacao_aereo_funcionario ADD CONSTRAINT solicitacao_aereo_solicitacao_aereo_funcionario 
    FOREIGN KEY (id_solicitacao_aereo) REFERENCES public.solicitacao_aereo (id_solicitacao_aereo);
ALTER TABLE public.solicitacao_aereo_funcionario ADD CONSTRAINT funcionario_solicitacao_aereo_funcionario 
    FOREIGN KEY (id_funcionario) REFERENCES public.funcionario (id_funcionario);
ALTER TABLE public.solicitacao_terrestre ADD CONSTRAINT solicitacao_solicitacao_terrestre 
    FOREIGN KEY (id_solicitacao) REFERENCES public.solicitacao (id_solicitacao);
ALTER TABLE public.solicitacao_terrestre ADD CONSTRAINT empresa_solicitacao_terrestre 
    FOREIGN KEY (id_empresa) REFERENCES public.empresa (id_empresa);
ALTER TABLE public.solicitacao_terrestre_funcionario ADD CONSTRAINT solicitacao_terrestre_solicitacao_terrestre_funcionario 
    FOREIGN KEY (id_solicitacao_terrestre) REFERENCES public.solicitacao_terrestre (id_solicitacao_terrestre);
ALTER TABLE public.solicitacao_terrestre_funcionario ADD CONSTRAINT funcionario_solicitacao_terrestre_funcionario 
    FOREIGN KEY (id_funcionario) REFERENCES public.funcionario (id_funcionario);
ALTER TABLE public.solicitacao_funcionario ADD CONSTRAINT solicitacao_solicitacao_funcionario 
    FOREIGN KEY (id_solicitacao) REFERENCES public.solicitacao (id_solicitacao);
ALTER TABLE public.solicitacao_funcionario ADD CONSTRAINT funcionario_solicitacao_funcionario 
    FOREIGN KEY (id_funcionario) REFERENCES public.funcionario (id_funcionario);
ALTER TABLE public.solicitacao_hospedagem ADD CONSTRAINT solicitacao_solicitacao_hospedagem 
    FOREIGN KEY (id_solicitacao) REFERENCES public.solicitacao (id_solicitacao);
ALTER TABLE public.solicitacao_hospedagem ADD CONSTRAINT empresa_solicitacao_hospedagem 
    FOREIGN KEY (id_empresa) REFERENCES public.empresa (id_empresa);
ALTER TABLE public.solicitacao_aluguel_veiculo ADD CONSTRAINT solicitacao_solicitacao_aluguel_veiculo 
    FOREIGN KEY (id_solicitacao) REFERENCES public.solicitacao (id_solicitacao);
ALTER TABLE public.solicitacao_aluguel_veiculo ADD CONSTRAINT funcionario_solicitacao_aluguel_veiculo 
    FOREIGN KEY (id_funcionario_motorista) REFERENCES public.funcionario (id_funcionario);
ALTER TABLE public.solicitacao_aluguel_veiculo ADD CONSTRAINT empresa_solicitacao_aluguel_veiculo 
    FOREIGN KEY (id_empresa) REFERENCES public.empresa (id_empresa);
ALTER TABLE public.solicitacao_adiantamento ADD CONSTRAINT solicitacao_solicitacao_adiantamento 
    FOREIGN KEY (id_solicitacao) REFERENCES public.solicitacao (id_solicitacao);
ALTER TABLE public.solicitacao_adiantamento ADD CONSTRAINT empresa_solicitacao_adiantamento 
    FOREIGN KEY (id_empresa) REFERENCES public.empresa (id_empresa);
ALTER TABLE public.solicitacao_aluguel_veiculo_funcionario ADD CONSTRAINT solicitacao_aluguel_veiculo_1 
    FOREIGN KEY (id_solicitacao_veiculo) REFERENCES public.solicitacao_aluguel_veiculo (id_solicitacao_veiculo);
ALTER TABLE public.solicitacao_aluguel_veiculo_funcionario ADD CONSTRAINT funcionario_solicitacao_aluguel_veiculo_funcionario 
    FOREIGN KEY (id_funcionario) REFERENCES public.funcionario (id_funcionario);
ALTER TABLE public.solicitacao_hospedagem_funcionario ADD CONSTRAINT solicitacao_hospedagem_solicitacao_hospedagem_funcionario 
    FOREIGN KEY (id_solicitacao_hospedagem) REFERENCES public.solicitacao_hospedagem (id_solicitacao_hospedagem);
ALTER TABLE public.solicitacao_hospedagem_funcionario ADD CONSTRAINT funcionario_solicitacao_hospedagem_funcionario 
    FOREIGN KEY (id_funcionario) REFERENCES public.funcionario (id_funcionario);
ALTER TABLE public.solicitacao_adiantamento_funcionario ADD CONSTRAINT solicitacao_adiantamento_solicitacao_adiantamento_funcionario 
    FOREIGN KEY (id_solicitacao_adiantamento) REFERENCES public.solicitacao_adiantamento (id_solicitacao_adiantamento);
ALTER TABLE public.solicitacao_adiantamento_funcionario ADD CONSTRAINT funcionario_solicitacao_adiantamento_funcionario 
    FOREIGN KEY (id_funcionario) REFERENCES public.funcionario (id_funcionario);
ALTER TABLE public.anexo ADD CONSTRAINT empresa_anexo 
    FOREIGN KEY (id_empresa) REFERENCES public.empresa (id_empresa);
ALTER TABLE public.anexo ADD CONSTRAINT tipo_anexo_anexo 
    FOREIGN KEY (id_tipo_anexo) REFERENCES public.tipo_anexo (id_tipo_anexo);
ALTER TABLE public.solicitacao_aereo_reserva ADD CONSTRAINT solicitacao_aereo_solicitacao_aereo_reserva 
    FOREIGN KEY (id_solicitacao_aereo) REFERENCES public.solicitacao_aereo (id_solicitacao_aereo);
ALTER TABLE public.solicitacao_aereo_reserva ADD CONSTRAINT funcionario_solicitacao_aereo_reserva 
    FOREIGN KEY (id_funcionario) REFERENCES public.funcionario (id_funcionario);
ALTER TABLE public.solicitacao_hospedagem_reserva ADD CONSTRAINT solicitacao_hospedagem_solicitacao_hospedagem_reserva 
    FOREIGN KEY (id_solicitacao_hospedagem) REFERENCES public.solicitacao_hospedagem (id_solicitacao_hospedagem);
ALTER TABLE public.solicitacao_hospedagem_reserva ADD CONSTRAINT empresa_solicitacao_hospedagem_reserva 
    FOREIGN KEY (id_empresa) REFERENCES public.empresa (id_empresa);
ALTER TABLE public.funcionario_solicitacao_hospedagem_reserva ADD CONSTRAINT funcionario_funcionario_solicitacao_hospedagem_reserva 
    FOREIGN KEY (id_funcionario) REFERENCES public.funcionario (id_funcionario);
ALTER TABLE public.funcionario_solicitacao_hospedagem_reserva ADD CONSTRAINT solicitacao_hospedagem_reserva_2 
    FOREIGN KEY (id_solicitacao_hospedagem_reserva) REFERENCES public.solicitacao_hospedagem_reserva (id_solicitacao_hospedagem_reserva);
ALTER TABLE public.anexo_solicitacao_aereo_reserva ADD CONSTRAINT anexo_anexo_solicitacao_aereo_reserva 
    FOREIGN KEY (id_anexo) REFERENCES public.anexo (id_anexo);
ALTER TABLE public.anexo_solicitacao_aereo_reserva ADD CONSTRAINT solicitacao_aereo_reserva_anexo_solicitacao_aereo_reserva 
    FOREIGN KEY (id_solicitacao_aereo_reserva) REFERENCES public.solicitacao_aereo_reserva (id_solicitacao_aereo_reserva);
ALTER TABLE public.solicitacao_adiantamento_comprovante ADD CONSTRAINT solicitacao_adiantamento_solicitacao_adiantamento_comprovante 
    FOREIGN KEY (id_solicitacao_adiantamento) REFERENCES public.solicitacao_adiantamento (id_solicitacao_adiantamento);
ALTER TABLE public.solicitacao_adiantamento_comprovante ADD CONSTRAINT funcionario_solicitacao_adiantamento_comprovante 
    FOREIGN KEY (id_funcionario) REFERENCES public.funcionario (id_funcionario);
ALTER TABLE public.solicitacao_adiantamento_comprovante ADD CONSTRAINT funcionario_solicitacao_adiantamento_comprovante_lancamento 
    FOREIGN KEY (id_funcionario_lancamento) REFERENCES public.funcionario (id_funcionario);
ALTER TABLE public.solicitacao_adiantamento_comprovante ADD CONSTRAINT categoria_solicitacao_adiantamento_comprovante 
    FOREIGN KEY (id_categoria) REFERENCES public.categoria (id_categoria);
ALTER TABLE public.solicitacao_adiantamento_comprovante ADD CONSTRAINT empresa_solicitacao_adiantamento_comprovante 
    FOREIGN KEY (id_empresa) REFERENCES public.empresa (id_empresa);
ALTER TABLE public.anexo_solicitacao_adiantamento_comprovante ADD CONSTRAINT anexo_anexo_solicitacao_adiantamento_comprovante 
    FOREIGN KEY (id_anexo) REFERENCES public.anexo (id_anexo);
ALTER TABLE public.anexo_solicitacao_adiantamento_comprovante ADD CONSTRAINT solicitacao_adiantamento_comprovante_2 
    FOREIGN KEY (id_solicitacao_adiantamento_comprovante) REFERENCES public.solicitacao_adiantamento_comprovante (id_solicitacao_adiantamento_comprovante);
ALTER TABLE public.solicitacao_terrestre_reserva ADD CONSTRAINT solicitacao_terrestre_solicitacao_terrestre_reserva 
    FOREIGN KEY (id_solicitacao_terrestre) REFERENCES public.solicitacao_terrestre (id_solicitacao_terrestre);
ALTER TABLE public.solicitacao_terrestre_reserva ADD CONSTRAINT funcionario_solicitacao_terrestre_reserva 
    FOREIGN KEY (id_funcionario) REFERENCES public.funcionario (id_funcionario);
ALTER TABLE public.solicitacao_terrestre_reserva ADD CONSTRAINT funcionario_terrestre_3 
    FOREIGN KEY (id_funcionario_lancamento) REFERENCES public.funcionario (id_funcionario);
ALTER TABLE public.solicitacao_terrestre_reserva ADD CONSTRAINT empresa_solicitacao_terrestre_reserva 
    FOREIGN KEY (id_empresa) REFERENCES public.empresa (id_empresa);
ALTER TABLE public.solicitacao_aluguel_veiculo_reserva ADD CONSTRAINT solicitacao_aluguel_veiculo_solicitacao_aluguel_veiculo_reserva 
    FOREIGN KEY (id_solicitacao_veiculo) REFERENCES public.solicitacao_aluguel_veiculo (id_solicitacao_veiculo);
ALTER TABLE public.solicitacao_aluguel_veiculo_reserva ADD CONSTRAINT funcionario_solicitacao_aluguel_veiculo_reserva 
    FOREIGN KEY (id_funcionario_responsavel) REFERENCES public.funcionario (id_funcionario);
ALTER TABLE public.solicitacao_aluguel_veiculo_reserva ADD CONSTRAINT funcionario_3 
    FOREIGN KEY (id_funcionario_lancamento) REFERENCES public.funcionario (id_funcionario);
ALTER TABLE public.solicitacao_aluguel_veiculo_reserva ADD CONSTRAINT empresa_solicitacao_aluguel_veiculo_reserva 
    FOREIGN KEY (id_empresa) REFERENCES public.empresa (id_empresa);
