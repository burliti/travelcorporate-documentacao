/* ---------------------------------------------------------------------- */
/* Script generated with: DeZign for Databases v6.1.0                     */
/* Target DBMS:           PostgreSQL 8.3                                  */
/* Project file:          modelagem_travelcorporate.dez                   */
/* Project name:                                                          */
/* Author:                                                                */
/* Script type:           Alter database script                           */
/* Created on:            2018-01-06 13:23                                */
/* ---------------------------------------------------------------------- */


/* ---------------------------------------------------------------------- */
/* Drop foreign key constraints                                           */
/* ---------------------------------------------------------------------- */
ALTER TABLE public.devolucao_viagem DROP CONSTRAINT funcionario_devolucao_viagem_1;
ALTER TABLE public.devolucao_viagem DROP CONSTRAINT funcionario_devolucao_viagem;
ALTER TABLE public.devolucao_viagem DROP CONSTRAINT viagem_devolucao_viagem;
ALTER TABLE public.devolucao_viagem DROP CONSTRAINT lancamento_devolucao_viagem;
ALTER TABLE public.devolucao_viagem DROP CONSTRAINT empresa_devolucao_viagem;
ALTER TABLE public.reembolso_viagem DROP CONSTRAINT empresa_reembolso_viagem;
ALTER TABLE public.reembolso_viagem DROP CONSTRAINT lancamento_reembolso_viagem;
ALTER TABLE public.reembolso_viagem DROP CONSTRAINT viagem_reembolso_viagem;
ALTER TABLE public.reembolso_viagem DROP CONSTRAINT funcionario_reembolso_viagem_4;
ALTER TABLE public.reembolso_viagem DROP CONSTRAINT funcionario_reembolso_viagem_5;
ALTER TABLE public.reembolso_viagem DROP CONSTRAINT funcionario_reembolso_viagem_6;
ALTER TABLE public.reembolso_viagem DROP CONSTRAINT funcionario_reembolso_viagem_7;

/* ---------------------------------------------------------------------- */
/* Modify table "devolucao_viagem"                                        */
/* ---------------------------------------------------------------------- */
ALTER TABLE public.devolucao_viagem DROP CONSTRAINT cc_devolucao_viagem_id_devolucao_viagem;
ALTER TABLE public.devolucao_viagem DROP CONSTRAINT cc_devolucao_viagem_id_empresa;
ALTER TABLE public.devolucao_viagem DROP CONSTRAINT cc_devolucao_viagem_id_funcionario_cancelamento;
ALTER TABLE public.devolucao_viagem DROP CONSTRAINT cc_devolucao_viagem_id_funcionario_devolucao;
ALTER TABLE public.devolucao_viagem DROP CONSTRAINT cc_devolucao_viagem_id_funcionario_lancamento;
ALTER TABLE public.devolucao_viagem DROP CONSTRAINT cc_devolucao_viagem_id_funcionario_registro_devolucao;
ALTER TABLE public.devolucao_viagem DROP CONSTRAINT cc_devolucao_viagem_id_lancamento_debito;
ALTER TABLE public.devolucao_viagem DROP CONSTRAINT cc_devolucao_viagem_id_viagem;
ALTER TABLE public.devolucao_viagem DROP CONSTRAINT cc_devolucao_viagem_valor_devolucao;
ALTER TABLE public.devolucao_viagem ADD
    sequencial NUMERIC(8)  NOT NULL;
ALTER TABLE public.devolucao_viagem ADD CONSTRAINT cc_devolucao_viagem_id_devolucao_viagem 
    CHECK (id_devolucao_viagem IN (1,2, 3,4,5,6));
ALTER TABLE public.devolucao_viagem ADD CONSTRAINT cc_devolucao_viagem_id_empresa 
    CHECK (id_empresa IN (1,2, 3,4,5,6));
ALTER TABLE public.devolucao_viagem ADD CONSTRAINT cc_devolucao_viagem_id_funcionario_cancelamento 
    CHECK (id_funcionario_cancelamento IN (1,2, 3,4,5,6));
ALTER TABLE public.devolucao_viagem ADD CONSTRAINT cc_devolucao_viagem_id_funcionario_devolucao 
    CHECK (id_funcionario_devolucao IN (1,2, 3,4,5,6));
ALTER TABLE public.devolucao_viagem ADD CONSTRAINT cc_devolucao_viagem_id_funcionario_lancamento 
    CHECK (id_funcionario_lancamento IN (1,2, 3,4,5,6));
ALTER TABLE public.devolucao_viagem ADD CONSTRAINT cc_devolucao_viagem_id_funcionario_registro_devolucao 
    CHECK (id_funcionario_registro_devolucao IN (1,2, 3,4,5,6));
ALTER TABLE public.devolucao_viagem ADD CONSTRAINT cc_devolucao_viagem_id_lancamento_debito 
    CHECK (id_lancamento_debito IN (1,2, 3,4,5,6));
ALTER TABLE public.devolucao_viagem ADD CONSTRAINT cc_devolucao_viagem_id_viagem 
    CHECK (id_viagem IN (1,2, 3,4,5,6));
ALTER TABLE public.devolucao_viagem ADD CONSTRAINT cc_devolucao_viagem_valor_devolucao 
    CHECK (valor_devolucao IN (1,2, 3,4,5,6));

/* ---------------------------------------------------------------------- */
/* Modify table "reembolso_viagem"                                        */
/* ---------------------------------------------------------------------- */
ALTER TABLE public.reembolso_viagem DROP CONSTRAINT cc_reembolso_viagem_id_empresa;
ALTER TABLE public.reembolso_viagem DROP CONSTRAINT cc_reembolso_viagem_id_funcionario_cancelamento;
ALTER TABLE public.reembolso_viagem DROP CONSTRAINT cc_reembolso_viagem_id_funcionario_lancamento;
ALTER TABLE public.reembolso_viagem DROP CONSTRAINT cc_reembolso_viagem_id_funcionario_reembolso;
ALTER TABLE public.reembolso_viagem DROP CONSTRAINT cc_reembolso_viagem_id_funcionario_registro_reembolso;
ALTER TABLE public.reembolso_viagem DROP CONSTRAINT cc_reembolso_viagem_id_lancamento_credito;
ALTER TABLE public.reembolso_viagem DROP CONSTRAINT cc_reembolso_viagem_id_reembolso_viagem;
ALTER TABLE public.reembolso_viagem DROP CONSTRAINT cc_reembolso_viagem_id_viagem;
ALTER TABLE public.reembolso_viagem DROP CONSTRAINT cc_reembolso_viagem_valor_reembolso;
ALTER TABLE public.reembolso_viagem ADD
    sequencial NUMERIC(8)  NOT NULL;
ALTER TABLE public.reembolso_viagem ADD CONSTRAINT cc_reembolso_viagem_id_empresa 
    CHECK (id_empresa IN (1,2, 3,4,5,6));
ALTER TABLE public.reembolso_viagem ADD CONSTRAINT cc_reembolso_viagem_id_funcionario_cancelamento 
    CHECK (id_funcionario_cancelamento IN (1,2, 3,4,5,6));
ALTER TABLE public.reembolso_viagem ADD CONSTRAINT cc_reembolso_viagem_id_funcionario_lancamento 
    CHECK (id_funcionario_lancamento IN (1,2, 3,4,5,6));
ALTER TABLE public.reembolso_viagem ADD CONSTRAINT cc_reembolso_viagem_id_funcionario_reembolso 
    CHECK (id_funcionario_reembolso IN (1,2, 3,4,5,6));
ALTER TABLE public.reembolso_viagem ADD CONSTRAINT cc_reembolso_viagem_id_funcionario_registro_reembolso 
    CHECK (id_funcionario_registro_reembolso IN (1,2, 3,4,5,6));
ALTER TABLE public.reembolso_viagem ADD CONSTRAINT cc_reembolso_viagem_id_lancamento_credito 
    CHECK (id_lancamento_credito IN (1,2, 3,4,5,6));
ALTER TABLE public.reembolso_viagem ADD CONSTRAINT cc_reembolso_viagem_id_reembolso_viagem 
    CHECK (id_reembolso_viagem IN (1,2, 3,4,5,6));
ALTER TABLE public.reembolso_viagem ADD CONSTRAINT cc_reembolso_viagem_id_viagem 
    CHECK (id_viagem IN (1,2, 3,4,5,6));
ALTER TABLE public.reembolso_viagem ADD CONSTRAINT cc_reembolso_viagem_valor_reembolso 
    CHECK (valor_reembolso IN (1,2, 3,4,5,6));

/* ---------------------------------------------------------------------- */
/* Add foreign key constraints                                            */
/* ---------------------------------------------------------------------- */
ALTER TABLE public.devolucao_viagem ADD CONSTRAINT funcionario_devolucao_viagem_1 
    FOREIGN KEY (id_funcionario_lancamento) REFERENCES public.funcionario (id_funcionario);
ALTER TABLE public.devolucao_viagem ADD CONSTRAINT funcionario_devolucao_viagem 
    FOREIGN KEY (id_funcionario_devolucao) REFERENCES public.funcionario (id_funcionario);
ALTER TABLE public.devolucao_viagem ADD CONSTRAINT viagem_devolucao_viagem 
    FOREIGN KEY (id_viagem) REFERENCES public.viagem (id_viagem);
ALTER TABLE public.devolucao_viagem ADD CONSTRAINT lancamento_devolucao_viagem 
    FOREIGN KEY (id_lancamento_debito) REFERENCES public.lancamento (id_lancamento);
ALTER TABLE public.devolucao_viagem ADD CONSTRAINT empresa_devolucao_viagem 
    FOREIGN KEY (id_empresa) REFERENCES public.empresa (id_empresa);
ALTER TABLE public.reembolso_viagem ADD CONSTRAINT empresa_reembolso_viagem 
    FOREIGN KEY (id_empresa) REFERENCES public.empresa (id_empresa);
ALTER TABLE public.reembolso_viagem ADD CONSTRAINT lancamento_reembolso_viagem 
    FOREIGN KEY (id_lancamento_credito) REFERENCES public.lancamento (id_lancamento);
ALTER TABLE public.reembolso_viagem ADD CONSTRAINT viagem_reembolso_viagem 
    FOREIGN KEY (id_viagem) REFERENCES public.viagem (id_viagem);
ALTER TABLE public.reembolso_viagem ADD CONSTRAINT funcionario_reembolso_viagem_4 
    FOREIGN KEY (id_funcionario_lancamento) REFERENCES public.funcionario (id_funcionario);
ALTER TABLE public.reembolso_viagem ADD CONSTRAINT funcionario_reembolso_viagem_5 
    FOREIGN KEY (id_funcionario_reembolso) REFERENCES public.funcionario (id_funcionario);
ALTER TABLE public.reembolso_viagem ADD CONSTRAINT funcionario_reembolso_viagem_6 
    FOREIGN KEY (id_funcionario_registro_reembolso) REFERENCES public.funcionario (id_funcionario);
ALTER TABLE public.reembolso_viagem ADD CONSTRAINT funcionario_reembolso_viagem_7 
    FOREIGN KEY (id_funcionario_cancelamento) REFERENCES public.funcionario (id_funcionario);
