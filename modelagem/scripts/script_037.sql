/* ---------------------------------------------------------------------- */
/* Script generated with: DeZign for Databases v6.1.0                     */
/* Target DBMS:           PostgreSQL 8.3                                  */
/* Project file:          modelagem_travelcorporate.dez                   */
/* Project name:                                                          */
/* Author:                                                                */
/* Script type:           Alter database script                           */
/* Created on:            2018-05-05 15:36                                */
/* ---------------------------------------------------------------------- */


/* ---------------------------------------------------------------------- */
/* Drop foreign key constraints                                           */
/* ---------------------------------------------------------------------- */
ALTER TABLE public.configuracao_aprovacao_viagem DROP CONSTRAINT empresa_config_aprov_viagem;
ALTER TABLE public.configuracao_aprovacao_viagem DROP CONSTRAINT fillial_config_aprov_viagem;

/* ---------------------------------------------------------------------- */
/* Add sequences                                                          */
/* ---------------------------------------------------------------------- */
CREATE SEQUENCE public.seq_atividade INCREMENT 1 START 1;
CREATE SEQUENCE public.seq_etapa_atividade INCREMENT 1 START 1;
CREATE SEQUENCE public.seq_configuracao_atividade INCREMENT 1 START 1;

/* ---------------------------------------------------------------------- */
/* Add domains                                                            */
/* ---------------------------------------------------------------------- */
CREATE DOMAIN public.tipo_prazo AS NUMERIC(1) CHECK (VALUE IN (1, 2, 3));
CREATE DOMAIN public.acao_expiracao_prazo AS NUMERIC(1) CHECK (VALUE IN (1, 2, 3));

/* ---------------------------------------------------------------------- */
/* Modify table "configuracao_aprovacao_viagem"                           */
/* ---------------------------------------------------------------------- */
ALTER TABLE public.configuracao_aprovacao_viagem ADD
    id_setor public.id;

/* ---------------------------------------------------------------------- */
/* Add table "configuracao_atividade"                                     */
/* ---------------------------------------------------------------------- */
CREATE TABLE public.configuracao_atividade (
    id_configuracao_atividade public.id  NOT NULL,
    id_configuracao_aprovacao_viagem public.id  NOT NULL,
    id_empresa public.id  NOT NULL,
    nivel NUMERIC(4)  NOT NULL,
    prazo NUMERIC(8)  NOT NULL,
    tipo_prazo public.tipo_prazo  NOT NULL,
    id_etapa_atividade public.id  NOT NULL,
    acao_expiracao_prazo public.acao_expiracao_prazo,
    id_funcionario public.id,
    CONSTRAINT pk_configuracao_atividade PRIMARY KEY (id_configuracao_atividade)
);

/* ---------------------------------------------------------------------- */
/* Add table "atividade"                                                  */
/* ---------------------------------------------------------------------- */
CREATE TABLE public.atividade (
    id_atividade public.id  NOT NULL,
    nome CHARACTER VARYING(100),
    descricao CHARACTER VARYING(200),
    status public.status  NOT NULL,
    ordem NUMERIC(4)  NOT NULL,
    CONSTRAINT pk_atividade PRIMARY KEY (id_atividade)
);

/* ---------------------------------------------------------------------- */
/* Add table "etapa_atividade"                                            */
/* ---------------------------------------------------------------------- */
CREATE TABLE public.etapa_atividade (
    id_etapa_atividade public.id  NOT NULL,
    id_atividade public.id  NOT NULL,
    nome CHARACTER VARYING(100)  NOT NULL,
    descricao CHARACTER VARYING(200),
    ordem NUMERIC(4)  NOT NULL,
    flag_configuracao_nivel public.sim_nao,
    CONSTRAINT pk_etapa_atividade PRIMARY KEY (id_etapa_atividade)
);

/* ---------------------------------------------------------------------- */
/* Add table "configuracao_aprovacao_viagem_atividade"                    */
/* ---------------------------------------------------------------------- */
CREATE TABLE public.configuracao_aprovacao_viagem_atividade (
    id_configuracao_aprovacao_viagem public.id  NOT NULL,
    id_atividade public.id  NOT NULL,
    CONSTRAINT pk_configuracao_aprovacao_viagem_atividade PRIMARY KEY (id_configuracao_aprovacao_viagem, id_atividade)
);


/* ---------------------------------------------------------------------- */
/* Add foreign key constraints                                            */
/* ---------------------------------------------------------------------- */
ALTER TABLE public.configuracao_aprovacao_viagem ADD CONSTRAINT empresa_config_aprov_viagem 
    FOREIGN KEY (id_empresa) REFERENCES public.empresa (id_empresa);
ALTER TABLE public.configuracao_aprovacao_viagem ADD CONSTRAINT fillial_config_aprov_viagem 
    FOREIGN KEY (id_empresa) REFERENCES public.empresa (id_empresa);
ALTER TABLE public.configuracao_aprovacao_viagem ADD CONSTRAINT setor_configuracao_aprovacao_viagem 
    FOREIGN KEY (id_setor) REFERENCES public.setor (id_setor);
ALTER TABLE public.configuracao_atividade ADD CONSTRAINT configuracao_aprovacao_viagem_configuracao_atividade 
    FOREIGN KEY (id_configuracao_aprovacao_viagem) REFERENCES public.configuracao_aprovacao_viagem (id_configuracao_aprovacao_viagem);
ALTER TABLE public.configuracao_atividade ADD CONSTRAINT etapa_atividade_configuracao_atividade 
    FOREIGN KEY (id_etapa_atividade) REFERENCES public.etapa_atividade (id_etapa_atividade);
ALTER TABLE public.configuracao_atividade ADD CONSTRAINT empresa_configuracao_atividade 
    FOREIGN KEY (id_empresa) REFERENCES public.empresa (id_empresa);
ALTER TABLE public.configuracao_atividade ADD CONSTRAINT funcionario_configuracao_atividade 
    FOREIGN KEY (id_funcionario) REFERENCES public.funcionario (id_funcionario);
ALTER TABLE public.etapa_atividade ADD CONSTRAINT atividade_etapa_atividade 
    FOREIGN KEY (id_atividade) REFERENCES public.atividade (id_atividade);
ALTER TABLE public.configuracao_aprovacao_viagem_atividade ADD CONSTRAINT configuracao_aprovacao_viagem_configuracao_aprovacao_viagem_atividade 
    FOREIGN KEY (id_configuracao_aprovacao_viagem) REFERENCES public.configuracao_aprovacao_viagem (id_configuracao_aprovacao_viagem);
ALTER TABLE public.configuracao_aprovacao_viagem_atividade ADD CONSTRAINT atividade_configuracao_aprovacao_viagem_atividade 
    FOREIGN KEY (id_atividade) REFERENCES public.atividade (id_atividade);
