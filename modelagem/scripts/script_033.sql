/* ---------------------------------------------------------------------- */
/* Script generated with: DeZign for Databases v6.1.0                     */
/* Target DBMS:           PostgreSQL 8.3                                  */
/* Project file:          modelagem_travelcorporate.dez                   */
/* Project name:                                                          */
/* Author:                                                                */
/* Script type:           Alter database script                           */
/* Created on:            2018-02-26 21:51                                */
/* ---------------------------------------------------------------------- */


/* ---------------------------------------------------------------------- */
/* Add sequences                                                          */
/* ---------------------------------------------------------------------- */
CREATE SEQUENCE public.seq_configuracao_aprovacao_viagem INCREMENT 1 START 1;
CREATE SEQUENCE public.seq_configuracao_funcionarios_aprovacao INCREMENT 1 START 1;

/* ---------------------------------------------------------------------- */
/* Add table "configuracao_aprovacao_viagem"                              */
/* ---------------------------------------------------------------------- */
CREATE TABLE public.configuracao_aprovacao_viagem (
    id_configuracao_aprovacao_viagem public.id  NOT NULL,
    id_empresa public.id  NOT NULL,
    id_filial public.id,
    flag_aprovacao_nivel public.sim_nao  NOT NULL,
    CONSTRAINT pk_configuracao_aprovacao_viagem PRIMARY KEY (id_configuracao_aprovacao_viagem)
);

/* ---------------------------------------------------------------------- */
/* Add table "configuracao_funcionarios_aprovacao"                        */
/* ---------------------------------------------------------------------- */
CREATE TABLE public.configuracao_funcionarios_aprovacao (
    id_funcionarios_aprovacao public.id  NOT NULL,
    id_funcionario public.id  NOT NULL,
    id_configuracao_aprovacao_viagem public.id,
    prazo_aprovacao_horas NUMERIC(8),
    ordem NUMERIC(3)  NOT NULL,
    CONSTRAINT pk_configuracao_funcionarios_aprovacao PRIMARY KEY (id_funcionarios_aprovacao)
);

/* ---------------------------------------------------------------------- */
/* Add foreign key constraints                                            */
/* ---------------------------------------------------------------------- */
ALTER TABLE public.configuracao_aprovacao_viagem ADD CONSTRAINT empresa_config_aprov_viagem 
    FOREIGN KEY (id_empresa) REFERENCES public.empresa (id_empresa);
ALTER TABLE public.configuracao_aprovacao_viagem ADD CONSTRAINT fillial_config_aprov_viagem 
    FOREIGN KEY (id_empresa) REFERENCES public.empresa (id_empresa);
ALTER TABLE public.configuracao_funcionarios_aprovacao ADD CONSTRAINT funcionario_configuracao_funcionarios_aprovacao 
    FOREIGN KEY (id_funcionario) REFERENCES public.funcionario (id_funcionario);
ALTER TABLE public.configuracao_funcionarios_aprovacao ADD CONSTRAINT configuracao_aprovacao_viagem_2 
    FOREIGN KEY (id_configuracao_aprovacao_viagem) REFERENCES public.configuracao_aprovacao_viagem (id_configuracao_aprovacao_viagem);
