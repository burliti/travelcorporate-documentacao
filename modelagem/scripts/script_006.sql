/* ---------------------------------------------------------------------- */
/* Script generated with: DeZign for Databases v6.1.0                     */
/* Target DBMS:           PostgreSQL 8.3                                  */
/* Project file:          modelagem_travelcorporate.dez                   */
/* Project name:                                                          */
/* Author:                                                                */
/* Script type:           Alter database script                           */
/* Created on:            2017-10-09 12:24                                */
/* ---------------------------------------------------------------------- */


/* ---------------------------------------------------------------------- */
/* Modify table "recurso"                                                 */
/* ---------------------------------------------------------------------- */
ALTER TABLE public.recurso ADD
    id_recurso_pai public.id;

ALTER TABLE public.recurso ADD ordem INTEGER;

/* ---------------------------------------------------------------------- */
/* Add foreign key constraints                                            */
/* ---------------------------------------------------------------------- */
ALTER TABLE public.recurso ADD CONSTRAINT recurso_recurso 
    FOREIGN KEY (id_recurso_pai) REFERENCES public.recurso (id_recurso);
