﻿/* ---------------------------------------------------------------------- */
/* Script generated with: DeZign for Databases v6.1.0                     */
/* Target DBMS:           PostgreSQL 8.3                                  */
/* Project file:          modelagem_travelcorporate.dez                   */
/* Project name:                                                          */
/* Author:                                                                */
/* Script type:           Alter database script                           */
/* Created on:            2018-01-06 05:19                                */
/* ---------------------------------------------------------------------- */


/* ---------------------------------------------------------------------- */
/* Drop foreign key constraints                                           */
/* ---------------------------------------------------------------------- */
ALTER TABLE public.sessao_usuario DROP CONSTRAINT empresa_sessao_usuario;
ALTER TABLE public.sessao_usuario DROP CONSTRAINT funcionario_sessao_usuario;
ALTER TABLE public.projeto DROP CONSTRAINT empresa_projeto;
ALTER TABLE public.projeto DROP CONSTRAINT centro_custo_projeto;
ALTER TABLE public.lancamento DROP CONSTRAINT viagem_lancamento;
ALTER TABLE public.lancamento DROP CONSTRAINT funcionario_lancamento_2;
ALTER TABLE public.lancamento DROP CONSTRAINT empresa_lancamento;
ALTER TABLE public.lancamento DROP CONSTRAINT recibo_lancamento;
ALTER TABLE public.lancamento DROP CONSTRAINT categoria_lancamento;
ALTER TABLE public.lancamento DROP CONSTRAINT cliente_lancamento;
ALTER TABLE public.lancamento DROP CONSTRAINT lancamento_lancamento;
ALTER TABLE public.lancamento DROP CONSTRAINT fornecedor_lancamento;
ALTER TABLE public.lancamento DROP CONSTRAINT funcionario_lancamento_9;
ALTER TABLE public.fechamento DROP CONSTRAINT empresa_fechamento;
ALTER TABLE public.fechamento DROP CONSTRAINT funcionario_fechamento;
ALTER TABLE public.fechamento DROP CONSTRAINT funcionario_fechamento_1;
ALTER TABLE public.fechamento DROP CONSTRAINT funcionario_fechamento_2;
ALTER TABLE public.fechamento DROP CONSTRAINT funcionario_fechamento_3;
ALTER TABLE public.fechamento DROP CONSTRAINT viagem_fechamento;
ALTER TABLE public.setor DROP CONSTRAINT setor_setor;
ALTER TABLE public.setor DROP CONSTRAINT empresa_setor;
ALTER TABLE public.setor DROP CONSTRAINT empresa_setor_filial;
ALTER TABLE public.setor DROP CONSTRAINT centro_custo_setor;

/* ---------------------------------------------------------------------- */
/* Add sequences                                                          */
/* ---------------------------------------------------------------------- */
CREATE SEQUENCE public.seq_devolucao_viagem INCREMENT 1 START 1;
CREATE SEQUENCE public.seq_reembolso_viagem INCREMENT 1 START 1;

/* ---------------------------------------------------------------------- */
/* Add domains                                                            */
/* ---------------------------------------------------------------------- */
CREATE DOMAIN public.destino_saldo AS NUMERIC(2) CHECK (VALUE IN (1,2, 3,4,5,6));
CREATE DOMAIN public.status_devolucao_viagem AS NUMERIC(1) CHECK (VALUE IN (1, 2, 3, 9));
CREATE DOMAIN public.status_reembolso_viagem AS NUMERIC(1) CHECK (VALUE IN (1, 2, 3, 9));

/* ---------------------------------------------------------------------- */
/* Modify table "projeto"                                                 */
/* ---------------------------------------------------------------------- */
ALTER TABLE public.projeto ALTER COLUMN id_centro_custo TYPE NUMERIC(8);

/* ---------------------------------------------------------------------- */
/* Modify table "lancamento"                                              */
/* ---------------------------------------------------------------------- */
ALTER TABLE public.lancamento ADD
    flag_glosado public.sim_nao DEFAULT 'N'  NOT NULL;
ALTER TABLE public.lancamento ADD
    valor_original public.monetario;
ALTER TABLE public.lancamento ADD
    valor_glosa public.monetario;
ALTER TABLE public.lancamento ADD
    id_funcionario_glosa public.id;
ALTER TABLE public.lancamento ADD
    observacoes_glosa CHARACTER VARYING(500);
COMMENT ON COLUMN public.categoria.flag_interna IS 'Indica se a categoria é interna e não pode ser alterada.';

/* ---------------------------------------------------------------------- */
/* Modify table "fechamento"                                              */
/* ---------------------------------------------------------------------- */
ALTER TABLE public.fechamento ADD
    fechamento_destino_saldo public.destino_saldo;

/* ---------------------------------------------------------------------- */
/* Modify table "setor"                                                   */
/* ---------------------------------------------------------------------- */
ALTER TABLE public.setor ALTER COLUMN id_centro_custo TYPE public.id;

/* ---------------------------------------------------------------------- */
/* Add table "devolucao_viagem"                                           */
/* ---------------------------------------------------------------------- */
CREATE TABLE public.devolucao_viagem (
    id_devolucao_viagem public.id  NOT NULL,
    id_empresa public.id  NOT NULL,
    data_lancamento DATE  NOT NULL,
    id_funcionario_lancamento public.id  NOT NULL,
    id_funcionario_devolucao public.id  NOT NULL,
    id_viagem public.id  NOT NULL,
    id_lancamento_debito public.id  NOT NULL,
    valor_devolucao public.monetario  NOT NULL,
    status public.status_devolucao_viagem  NOT NULL,
    data_devolucao DATE,
    id_funcionario_registro_devolucao public.id,
    data_cancelado DATE,
    id_funcionario_cancelamento public.id,
    observacoes CHARACTER VARYING(500),
    motivo_cancelamento CHARACTER VARYING(500),
    CONSTRAINT pk_devolucao_viagem PRIMARY KEY (id_devolucao_viagem)
);
ALTER TABLE public.devolucao_viagem ADD CONSTRAINT cc_devolucao_viagem_id_devolucao_viagem 
    CHECK (id_devolucao_viagem IN (1,2, 3,4,5,6));
ALTER TABLE public.devolucao_viagem ADD CONSTRAINT cc_devolucao_viagem_id_empresa 
    CHECK (id_empresa IN (1,2, 3,4,5,6));
ALTER TABLE public.devolucao_viagem ADD CONSTRAINT cc_devolucao_viagem_id_funcionario_cancelamento 
    CHECK (id_funcionario_cancelamento IN (1,2, 3,4,5,6));
ALTER TABLE public.devolucao_viagem ADD CONSTRAINT cc_devolucao_viagem_id_funcionario_devolucao 
    CHECK (id_funcionario_devolucao IN (1,2, 3,4,5,6));
ALTER TABLE public.devolucao_viagem ADD CONSTRAINT cc_devolucao_viagem_id_funcionario_lancamento 
    CHECK (id_funcionario_lancamento IN (1,2, 3,4,5,6));
ALTER TABLE public.devolucao_viagem ADD CONSTRAINT cc_devolucao_viagem_id_funcionario_registro_devolucao 
    CHECK (id_funcionario_registro_devolucao IN (1,2, 3,4,5,6));
ALTER TABLE public.devolucao_viagem ADD CONSTRAINT cc_devolucao_viagem_id_lancamento_debito 
    CHECK (id_lancamento_debito IN (1,2, 3,4,5,6));
ALTER TABLE public.devolucao_viagem ADD CONSTRAINT cc_devolucao_viagem_id_viagem 
    CHECK (id_viagem IN (1,2, 3,4,5,6));
ALTER TABLE public.devolucao_viagem ADD CONSTRAINT cc_devolucao_viagem_valor_devolucao 
    CHECK (valor_devolucao IN (1,2, 3,4,5,6));

/* ---------------------------------------------------------------------- */
/* Add table "reembolso_viagem"                                           */
/* ---------------------------------------------------------------------- */
CREATE TABLE public.reembolso_viagem (
    id_reembolso_viagem public.id  NOT NULL,
    id_empresa public.id  NOT NULL,
    data_lancamento DATE  NOT NULL,
    id_funcionario_lancamento public.id  NOT NULL,
    id_funcionario_reembolso public.id  NOT NULL,
    id_viagem public.id  NOT NULL,
    id_lancamento_credito public.id  NOT NULL,
    valor_reembolso public.monetario  NOT NULL,
    status CHARACTER(40)  NOT NULL,
    data_reembolso DATE,
    id_funcionario_registro_reembolso public.id,
    data_cancelado DATE,
    id_funcionario_cancelamento public.id,
    observacoes CHARACTER VARYING(500),
    motivo_cancelamento CHARACTER VARYING(500),
    CONSTRAINT pk_reembolso_viagem PRIMARY KEY (id_reembolso_viagem)
);
ALTER TABLE public.reembolso_viagem ADD CONSTRAINT cc_reembolso_viagem_id_empresa 
    CHECK (id_empresa IN (1,2, 3,4,5,6));
ALTER TABLE public.reembolso_viagem ADD CONSTRAINT cc_reembolso_viagem_id_funcionario_cancelamento 
    CHECK (id_funcionario_cancelamento IN (1,2, 3,4,5,6));
ALTER TABLE public.reembolso_viagem ADD CONSTRAINT cc_reembolso_viagem_id_funcionario_lancamento 
    CHECK (id_funcionario_lancamento IN (1,2, 3,4,5,6));
ALTER TABLE public.reembolso_viagem ADD CONSTRAINT cc_reembolso_viagem_id_funcionario_reembolso 
    CHECK (id_funcionario_reembolso IN (1,2, 3,4,5,6));
ALTER TABLE public.reembolso_viagem ADD CONSTRAINT cc_reembolso_viagem_id_funcionario_registro_reembolso 
    CHECK (id_funcionario_registro_reembolso IN (1,2, 3,4,5,6));
ALTER TABLE public.reembolso_viagem ADD CONSTRAINT cc_reembolso_viagem_id_lancamento_credito 
    CHECK (id_lancamento_credito IN (1,2, 3,4,5,6));
ALTER TABLE public.reembolso_viagem ADD CONSTRAINT cc_reembolso_viagem_id_reembolso_viagem 
    CHECK (id_reembolso_viagem IN (1,2, 3,4,5,6));
ALTER TABLE public.reembolso_viagem ADD CONSTRAINT cc_reembolso_viagem_id_viagem 
    CHECK (id_viagem IN (1,2, 3,4,5,6));
ALTER TABLE public.reembolso_viagem ADD CONSTRAINT cc_reembolso_viagem_valor_reembolso 
    CHECK (valor_reembolso IN (1,2, 3,4,5,6));

/* ---------------------------------------------------------------------- */
/* Add foreign key constraints                                            */
/* ---------------------------------------------------------------------- */
ALTER TABLE public.sessao_usuario ADD CONSTRAINT empresa_sessao_usuario 
    FOREIGN KEY (id_empresa) REFERENCES public.empresa (id_empresa);
ALTER TABLE public.sessao_usuario ADD CONSTRAINT funcionario_sessao_usuario 
    FOREIGN KEY (id_funcionario) REFERENCES public.funcionario (id_funcionario);
ALTER TABLE public.projeto ADD CONSTRAINT empresa_projeto 
    FOREIGN KEY (id_empresa) REFERENCES public.empresa (id_empresa);
ALTER TABLE public.projeto ADD CONSTRAINT centro_custo_projeto 
    FOREIGN KEY (id_centro_custo) REFERENCES public.centro_custo (id_centro_custo);
ALTER TABLE public.lancamento ADD CONSTRAINT viagem_lancamento 
    FOREIGN KEY (id_viagem) REFERENCES public.viagem (id_viagem);
ALTER TABLE public.lancamento ADD CONSTRAINT funcionario_lancamento_2 
    FOREIGN KEY (id_funcionario) REFERENCES public.funcionario (id_funcionario);
ALTER TABLE public.lancamento ADD CONSTRAINT empresa_lancamento 
    FOREIGN KEY (id_empresa) REFERENCES public.empresa (id_empresa);
ALTER TABLE public.lancamento ADD CONSTRAINT recibo_lancamento 
    FOREIGN KEY (id_recibo) REFERENCES public.recibo (id_recibo);
ALTER TABLE public.lancamento ADD CONSTRAINT categoria_lancamento 
    FOREIGN KEY (id_categoria) REFERENCES public.categoria (id_categoria);
ALTER TABLE public.lancamento ADD CONSTRAINT cliente_lancamento 
    FOREIGN KEY (id_cliente) REFERENCES public.cliente (id_cliente);
ALTER TABLE public.lancamento ADD CONSTRAINT lancamento_lancamento 
    FOREIGN KEY (id_lancamento_referenciado) REFERENCES public.lancamento (id_lancamento);
ALTER TABLE public.lancamento ADD CONSTRAINT fornecedor_lancamento 
    FOREIGN KEY (id_fornecedor) REFERENCES public.fornecedor (id_fornecedor);
ALTER TABLE public.lancamento ADD CONSTRAINT funcionario_lancamento_9 
    FOREIGN KEY (id_funcionario_lancamento) REFERENCES public.funcionario (id_funcionario);
ALTER TABLE public.lancamento ADD CONSTRAINT funcionario_lancamento_10 
    FOREIGN KEY (id_funcionario_glosa) REFERENCES public.funcionario (id_funcionario);
ALTER TABLE public.fechamento ADD CONSTRAINT funcionario_fechamento_1 
    FOREIGN KEY (id_funcionario_fechamento) REFERENCES public.funcionario (id_funcionario);
ALTER TABLE public.fechamento ADD CONSTRAINT funcionario_fechamento_2 
    FOREIGN KEY (id_funcionario_recebimento) REFERENCES public.funcionario (id_funcionario);
ALTER TABLE public.fechamento ADD CONSTRAINT funcionario_fechamento_3 
    FOREIGN KEY (id_funcionario_aprovacao) REFERENCES public.funcionario (id_funcionario);
ALTER TABLE public.fechamento ADD CONSTRAINT empresa_fechamento 
    FOREIGN KEY (id_empresa) REFERENCES public.empresa (id_empresa);
ALTER TABLE public.fechamento ADD CONSTRAINT funcionario_fechamento 
    FOREIGN KEY (id_funcionario_rejeicao) REFERENCES public.funcionario (id_funcionario);
ALTER TABLE public.fechamento ADD CONSTRAINT viagem_fechamento 
    FOREIGN KEY (id_viagem) REFERENCES public.viagem (id_viagem);
ALTER TABLE public.setor ADD CONSTRAINT setor_setor 
    FOREIGN KEY (id_setor_pai) REFERENCES public.setor (id_setor);
ALTER TABLE public.setor ADD CONSTRAINT empresa_setor 
    FOREIGN KEY (id_empresa) REFERENCES public.empresa (id_empresa);
ALTER TABLE public.setor ADD CONSTRAINT empresa_setor_filial 
    FOREIGN KEY (id_empresa_filial) REFERENCES public.empresa (id_empresa);
ALTER TABLE public.setor ADD CONSTRAINT centro_custo_setor 
    FOREIGN KEY (id_centro_custo) REFERENCES public.centro_custo (id_centro_custo);
ALTER TABLE public.devolucao_viagem ADD CONSTRAINT funcionario_devolucao_viagem_1 
    FOREIGN KEY (id_funcionario_lancamento) REFERENCES public.funcionario (id_funcionario);
ALTER TABLE public.devolucao_viagem ADD CONSTRAINT funcionario_devolucao_viagem 
    FOREIGN KEY (id_funcionario_devolucao) REFERENCES public.funcionario (id_funcionario);
ALTER TABLE public.devolucao_viagem ADD CONSTRAINT viagem_devolucao_viagem 
    FOREIGN KEY (id_viagem) REFERENCES public.viagem (id_viagem);
ALTER TABLE public.devolucao_viagem ADD CONSTRAINT lancamento_devolucao_viagem 
    FOREIGN KEY (id_lancamento_debito) REFERENCES public.lancamento (id_lancamento);
ALTER TABLE public.devolucao_viagem ADD CONSTRAINT empresa_devolucao_viagem 
    FOREIGN KEY (id_empresa) REFERENCES public.empresa (id_empresa);
ALTER TABLE public.reembolso_viagem ADD CONSTRAINT empresa_reembolso_viagem 
    FOREIGN KEY (id_empresa) REFERENCES public.empresa (id_empresa);
ALTER TABLE public.reembolso_viagem ADD CONSTRAINT lancamento_reembolso_viagem 
    FOREIGN KEY (id_lancamento_credito) REFERENCES public.lancamento (id_lancamento);
ALTER TABLE public.reembolso_viagem ADD CONSTRAINT viagem_reembolso_viagem 
    FOREIGN KEY (id_viagem) REFERENCES public.viagem (id_viagem);
ALTER TABLE public.reembolso_viagem ADD CONSTRAINT funcionario_reembolso_viagem_4 
    FOREIGN KEY (id_funcionario_lancamento) REFERENCES public.funcionario (id_funcionario);
ALTER TABLE public.reembolso_viagem ADD CONSTRAINT funcionario_reembolso_viagem_5 
    FOREIGN KEY (id_funcionario_reembolso) REFERENCES public.funcionario (id_funcionario);
ALTER TABLE public.reembolso_viagem ADD CONSTRAINT funcionario_reembolso_viagem_6 
    FOREIGN KEY (id_funcionario_registro_reembolso) REFERENCES public.funcionario (id_funcionario);
ALTER TABLE public.reembolso_viagem ADD CONSTRAINT funcionario_reembolso_viagem_7 
    FOREIGN KEY (id_funcionario_cancelamento) REFERENCES public.funcionario (id_funcionario);
