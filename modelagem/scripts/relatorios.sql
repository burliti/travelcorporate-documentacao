﻿-- 	Total de despesas por dia
SELECT
     l.data_inicial,
     SUM(l.valor_lancamento * CASE WHEN l.tipo_lancamento = 'D' THEN -1 ELSE 1 END)
  FROM viagem v
  JOIN lancamento l on l.id_viagem = v.id_viagem
 WHERE 1 = 1  
 GROUP BY l.data_inicial
 ORDER BY l.data_inicial;

--	Total de despesas por dia / categoria
SELECT
     l.data_inicial,
     c.nome,
     SUM(l.valor_lancamento * CASE WHEN l.tipo_lancamento = 'D' THEN -1 ELSE 1 END)
  FROM viagem v
  JOIN lancamento l ON l.id_viagem = v.id_viagem
  JOIN categoria c ON c.id_categoria = l.id_categoria
 WHERE 1 = 1
 GROUP BY c.nome, l.data_inicial
 ORDER BY l.data_inicial, c.nome;

--	Total de despesas por semana
SELECT
     date_part('year', l.data_inicial::date) AS ano,
     date_part('week', l.data_inicial::date) AS semana,
     to_char(date_trunc('week', l.data_inicial)::date, 'DD/MM/YYYY') || ' à ' || to_char((date_trunc('week', l.data_inicial)+ '6 days'::interval)::date, 'DD/MM/YYYY') as intervalo_Semana,
     c.nome,
     SUM(l.valor_lancamento * CASE WHEN l.tipo_lancamento = 'D' THEN -1 ELSE 1 END)
  FROM viagem v
  JOIN lancamento l ON l.id_viagem = v.id_viagem
  JOIN categoria c ON c.id_categoria = l.id_categoria
 WHERE 1 = 1
 GROUP BY c.nome, ano, semana, date_trunc('week', l.data_inicial)
 ORDER BY ano, semana, c.nome;

--	Despesas (detalhadas) por dia



