﻿/* ---------------------------------------------------------------------- */
/* Script generated with: DeZign for Databases v6.1.0                     */
/* Target DBMS:           PostgreSQL 8.3                                  */
/* Project file:          modelagem_travelcorporate.dez                   */
/* Project name:                                                          */
/* Author:                                                                */
/* Script type:           Alter database script                           */
/* Created on:            2018-01-28 16:38                                */
/* ---------------------------------------------------------------------- */


/* ---------------------------------------------------------------------- */
/* Drop foreign key constraints                                           */
/* ---------------------------------------------------------------------- */
ALTER TABLE public.mensagem_forma_notificacao DROP CONSTRAINT mensagem_mensagem_forma_notificacao;
ALTER TABLE public.mensagem DROP CONSTRAINT empresa_mensagem;
ALTER TABLE public.mensagem DROP CONSTRAINT funcionario_mensagem;
ALTER TABLE public.mensagem DROP CONSTRAINT assunto_mensagem;
ALTER TABLE public.mensagem_destinatarios DROP CONSTRAINT mensagem_mensagem_destinatarios;
ALTER TABLE public.mensagem_destinatarios DROP CONSTRAINT funcionario_mensagem_destinatarios;
ALTER TABLE public.mensagem_destinatarios DROP CONSTRAINT empresa_mensagem_destinatarios;
ALTER TABLE public.mensagem_confirmacao_leitura DROP CONSTRAINT forma_notificacao_mensagem_confirmacao_leitura;
ALTER TABLE public.mensagem_confirmacao_leitura DROP CONSTRAINT mensagem_destinatarios_mensagem_confirmacao_leitura;
ALTER TABLE public.mensagem_confirmacao_leitura DROP CONSTRAINT empresa_mensagem_confirmacao_leitura;

/* ---------------------------------------------------------------------- */
/* Drop and recreate table "mensagem"                                     */
/* ---------------------------------------------------------------------- */

/* Table must be recreated because some of the changes can't be done with the regular commands available. */
ALTER TABLE public.mensagem DROP CONSTRAINT pk_mensagem;
CREATE TABLE public.mensagem_TMP (
    id_mensagem NUMERIC(12)  NOT NULL,
    id_empresa public.id  NOT NULL,
    id_assunto public.id,
    assunto_descricao CHARACTER VARYING(100)  NOT NULL,
    id_funcionario_autor public.id,
    flg_mensagem_sistema public.sim_nao  NOT NULL,
    status public.status_mensagem  NOT NULL,
    flg_permite_responder public.sim_nao  NOT NULL,
    flg_permite_encaminhar public.sim_nao  NOT NULL,
    flg_exige_confirmacao public.sim_nao  NOT NULL,
    flg_bloqueia_sem_responder public.sim_nao  NOT NULL,
    texto_botao_confirmacao CHARACTER VARYING(50),
    id_mensagem_origem_resposta NUMERIC(12),
    id_mensagem_origem_encaminhada NUMERIC(12),
    mensagem TEXT,
    data_hora_criacao DATE  NOT NULL,
    data_hora_envio DATE  NOT NULL,
    data_hora_exclusao DATE  NOT NULL);
INSERT INTO public.mensagem_TMP
    (id_mensagem,id_empresa,id_assunto,assunto_descricao,id_funcionario_autor,flg_mensagem_sistema,status,flg_permite_responder,flg_exige_confirmacao,flg_bloqueia_sem_responder,texto_botao_confirmacao)
SELECT
    id_mensagem,id_empresa,id_assunto,assunto_descricao,id_funcionario_autor,flg_mensagem_sistema,status,flg_permite_responder,flg_exige_confirmacao,flg_bloqueia_sem_responder,texto_botao_confirmacao
FROM public.mensagem;

DROP TABLE public.mensagem;
ALTER TABLE public.mensagem_TMP RENAME TO mensagem;
ALTER TABLE public.mensagem ADD CONSTRAINT pk_mensagem 
    PRIMARY KEY (id_mensagem);

/* ---------------------------------------------------------------------- */
/* Modify table "mensagem_destinatarios"                                  */
/* ---------------------------------------------------------------------- */
ALTER TABLE public.mensagem_destinatarios ADD
    status public.status_mensagem  NOT NULL;

/* ---------------------------------------------------------------------- */
/* Drop and recreate table "mensagem_confirmacao_leitura"                 */
/* ---------------------------------------------------------------------- */

/* Table must be recreated because some of the changes can't be done with the regular commands available. */
ALTER TABLE public.mensagem_confirmacao_leitura DROP CONSTRAINT pk_mensagem_confirmacao_leitura;
CREATE TABLE public.mensagem_confirmacao_leitura_TMP (
    id_mensagem_confirmacao_leitura NUMERIC(12)  NOT NULL,
    id_forma_notificacao public.id  NOT NULL,
    id_empresa public.id  NOT NULL,
    id_mensagem_destinatario NUMERIC(12)  NOT NULL,
    data_hora TIMESTAMP  NOT NULL);
INSERT INTO public.mensagem_confirmacao_leitura_TMP
    (id_mensagem_confirmacao_leitura,id_forma_notificacao,id_empresa,id_mensagem_destinatario,data_hora)
SELECT
    id_mensagem_confirmacao_leitura,id_forma_notificacao,id_empresa,id_mensagem_destinatario,data_hora
FROM public.mensagem_confirmacao_leitura;

DROP TABLE public.mensagem_confirmacao_leitura;
ALTER TABLE public.mensagem_confirmacao_leitura_TMP RENAME TO mensagem_confirmacao_leitura;
ALTER TABLE public.mensagem_confirmacao_leitura ADD CONSTRAINT pk_mensagem_confirmacao_leitura 
    PRIMARY KEY (id_mensagem_confirmacao_leitura);

/* ---------------------------------------------------------------------- */
/* Add foreign key constraints                                            */
/* ---------------------------------------------------------------------- */
ALTER TABLE public.mensagem ADD CONSTRAINT empresa_mensagem 
    FOREIGN KEY (id_empresa) REFERENCES public.empresa (id_empresa);
ALTER TABLE public.mensagem ADD CONSTRAINT funcionario_mensagem 
    FOREIGN KEY (id_funcionario_autor) REFERENCES public.funcionario (id_funcionario);
ALTER TABLE public.mensagem ADD CONSTRAINT assunto_mensagem 
    FOREIGN KEY (id_assunto) REFERENCES public.assunto (id_assunto);

ALTER TABLE public.mensagem ADD CONSTRAINT mensagem_mensagem_1
    FOREIGN KEY (id_mensagem_origem_resposta) REFERENCES public.mensagem (id_mensagem);

ALTER TABLE public.mensagem ADD CONSTRAINT mensagem_mensagem_2
    FOREIGN KEY (id_mensagem_origem_encaminhada) REFERENCES public.mensagem (id_mensagem);
    
ALTER TABLE public.mensagem_destinatarios ADD CONSTRAINT mensagem_mensagem_destinatarios 
    FOREIGN KEY (id_mensagem) REFERENCES public.mensagem (id_mensagem);
ALTER TABLE public.mensagem_destinatarios ADD CONSTRAINT funcionario_mensagem_destinatarios 
    FOREIGN KEY (id_funcionario) REFERENCES public.funcionario (id_funcionario);
ALTER TABLE public.mensagem_destinatarios ADD CONSTRAINT empresa_mensagem_destinatarios 
    FOREIGN KEY (id_empresa) REFERENCES public.empresa (id_empresa);
ALTER TABLE public.mensagem_confirmacao_leitura ADD CONSTRAINT forma_notificacao_mensagem_confirmacao_leitura 
    FOREIGN KEY (id_forma_notificacao) REFERENCES public.forma_notificacao (id_forma_notificacao);
ALTER TABLE public.mensagem_confirmacao_leitura ADD CONSTRAINT mensagem_destinatarios_mensagem_confirmacao_leitura 
    FOREIGN KEY (id_mensagem_destinatario) REFERENCES public.mensagem_destinatarios (id_mensagem_destinatario);
ALTER TABLE public.mensagem_confirmacao_leitura ADD CONSTRAINT empresa_mensagem_confirmacao_leitura 
    FOREIGN KEY (id_empresa) REFERENCES public.empresa (id_empresa);
ALTER TABLE public.mensagem_forma_notificacao ADD CONSTRAINT mensagem_mensagem_forma_notificacao 
    FOREIGN KEY (id_mensagem) REFERENCES public.mensagem (id_mensagem);
