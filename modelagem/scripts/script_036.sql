/* ---------------------------------------------------------------------- */
/* Script generated with: DeZign for Databases v6.1.0                     */
/* Target DBMS:           PostgreSQL 8.3                                  */
/* Project file:          modelagem_travelcorporate.dez                   */
/* Project name:                                                          */
/* Author:                                                                */
/* Script type:           Alter database script                           */
/* Created on:            2018-04-30 14:49                                */
/* ---------------------------------------------------------------------- */


/* ---------------------------------------------------------------------- */
/* Drop foreign key constraints                                           */
/* ---------------------------------------------------------------------- */
ALTER TABLE public.solicitacao_aluguel_veiculo DROP CONSTRAINT solicitacao_solicitacao_aluguel_veiculo;
ALTER TABLE public.solicitacao_aluguel_veiculo DROP CONSTRAINT funcionario_solicitacao_aluguel_veiculo;
ALTER TABLE public.solicitacao_aluguel_veiculo DROP CONSTRAINT empresa_solicitacao_aluguel_veiculo;

/* ---------------------------------------------------------------------- */
/* Modify table "solicitacao_aluguel_veiculo"                             */
/* ---------------------------------------------------------------------- */
ALTER TABLE public.solicitacao_aluguel_veiculo ADD
    local_retirar CHARACTER VARYING(100);
ALTER TABLE public.solicitacao_aluguel_veiculo ADD
    local_devolver CHARACTER VARYING(100);
ALTER TABLE public.solicitacao_aluguel_veiculo ALTER COLUMN data_hora_retirada TYPE TIMESTAMP;
ALTER TABLE public.solicitacao_aluguel_veiculo ALTER COLUMN data_hora_entrega TYPE TIMESTAMP;

ALTER TABLE public.funcionario_solicitacao_hospedagem_reserva ADD id_empresa public.id;


/* ---------------------------------------------------------------------- */
/* Add foreign key constraints                                            */
/* ---------------------------------------------------------------------- */
ALTER TABLE public.solicitacao_aluguel_veiculo ADD CONSTRAINT solicitacao_solicitacao_aluguel_veiculo 
    FOREIGN KEY (id_solicitacao) REFERENCES public.solicitacao (id_solicitacao);
ALTER TABLE public.solicitacao_aluguel_veiculo ADD CONSTRAINT funcionario_solicitacao_aluguel_veiculo 
    FOREIGN KEY (id_funcionario_motorista) REFERENCES public.funcionario (id_funcionario);
ALTER TABLE public.solicitacao_aluguel_veiculo ADD CONSTRAINT empresa_solicitacao_aluguel_veiculo 
    FOREIGN KEY (id_empresa) REFERENCES public.empresa (id_empresa);
ALTER TABLE public.funcionario_solicitacao_hospedagem_reserva ADD CONSTRAINT empresa_funcionario_solicitacao_hospedagem_reserva
	FOREIGN KEY (id_empresa) REFERENCES public.empresa (id_empresa);