/* ---------------------------------------------------------------------- */
/* Script generated with: DeZign for Databases v6.1.0                     */
/* Target DBMS:           PostgreSQL 8.3                                  */
/* Project file:          modelagem_travelcorporate.dez                   */
/* Project name:                                                          */
/* Author:                                                                */
/* Script type:           Alter database script                           */
/* Created on:            2017-10-18 09:48                                */
/* ---------------------------------------------------------------------- */


/* ---------------------------------------------------------------------- */
/* Drop foreign key constraints                                           */
/* ---------------------------------------------------------------------- */
ALTER TABLE public.funcionario DROP CONSTRAINT empresa_funcionario;
ALTER TABLE public.funcionario DROP CONSTRAINT funcao_funcionario;
ALTER TABLE public.funcionario DROP CONSTRAINT perfil_funcionario;
ALTER TABLE public.sessao_usuario DROP CONSTRAINT empresa_sessao_usuario;
ALTER TABLE public.sessao_usuario DROP CONSTRAINT funcionario_sessao_usuario;
ALTER TABLE public.cliente DROP CONSTRAINT empresa_cliente;
ALTER TABLE public.cliente DROP CONSTRAINT cidade_cliente;
ALTER TABLE public.viagem DROP CONSTRAINT empresa_viagem;
ALTER TABLE public.viagem DROP CONSTRAINT cliente_viagem;
ALTER TABLE public.viagem DROP CONSTRAINT projeto_viagem;
ALTER TABLE public.lancamento DROP CONSTRAINT viagem_lancamento;
ALTER TABLE public.lancamento DROP CONSTRAINT funcionario_lancamento;
ALTER TABLE public.lancamento DROP CONSTRAINT empresa_lancamento;
ALTER TABLE public.lancamento DROP CONSTRAINT recibo_lancamento;
ALTER TABLE public.lancamento DROP CONSTRAINT categoria_lancamento;
ALTER TABLE public.lancamento DROP CONSTRAINT cliente_lancamento;
ALTER TABLE public.lancamento DROP CONSTRAINT lancamento_lancamento;
ALTER TABLE public.lancamento DROP CONSTRAINT fornecedor_lancamento;
ALTER TABLE public.recibo DROP CONSTRAINT funcionario_recibo;
ALTER TABLE public.recibo DROP CONSTRAINT empresa_recibo;
ALTER TABLE public.recibo DROP CONSTRAINT fornecedor_recibo;
ALTER TABLE public.fechamento DROP CONSTRAINT funcionario_fechamento_1;
ALTER TABLE public.fechamento DROP CONSTRAINT funcionario_fechamento_2;
ALTER TABLE public.fechamento DROP CONSTRAINT funcionario_fechamento_3;
ALTER TABLE public.fechamento DROP CONSTRAINT empresa_fechamento;
ALTER TABLE public.fechamento DROP CONSTRAINT funcionario_fechamento;
ALTER TABLE public.fornecedor DROP CONSTRAINT empresa_fornecedor;
ALTER TABLE public.perfil DROP CONSTRAINT empresa_perfil;
ALTER TABLE public.funcionario_viagem DROP CONSTRAINT funcionario_funcionario_viagem;
ALTER TABLE public.funcionario_viagem DROP CONSTRAINT viagem_funcionario_viagem;
ALTER TABLE public.lancamento_fechamento DROP CONSTRAINT lancamento_lancamento_fechamento;
ALTER TABLE public.lancamento_fechamento DROP CONSTRAINT fechamento_lancamento_fechamento;


-- Clear data
DELETE FROM public.fechamento;
DELETE FROM public.viagem;
DELETE FROM public.lancamento;

/* ---------------------------------------------------------------------- */
/* Add domains                                                            */
/* ---------------------------------------------------------------------- */
CREATE DOMAIN public.tipo_periodo AS NUMERIC(1) CHECK (VALUE IN (1, 2));

/* ---------------------------------------------------------------------- */
/* Drop table "funcionario_viagem"                                        */
/* ---------------------------------------------------------------------- */

/* Drop constraints */
ALTER TABLE public.funcionario_viagem DROP CONSTRAINT pk_funcionario_viagem;

/* Drop table */
DROP TABLE public.funcionario_viagem;

/* ---------------------------------------------------------------------- */
/* Drop table "lancamento_fechamento"                                     */
/* ---------------------------------------------------------------------- */

/* Drop constraints */
ALTER TABLE public.lancamento_fechamento DROP CONSTRAINT pk_lancamento_fechamento;

/* Drop table */
DROP TABLE public.lancamento_fechamento;

/* ---------------------------------------------------------------------- */
/* Modify table "funcionario"                                             */
/* ---------------------------------------------------------------------- */
ALTER TABLE public.funcionario ALTER COLUMN id_perfil TYPE public.id;

/* ---------------------------------------------------------------------- */
/* Modify table "cliente"                                                 */
/* ---------------------------------------------------------------------- */
ALTER TABLE public.cliente ALTER COLUMN id_cidade TYPE public.id;

/* ---------------------------------------------------------------------- */
/* Modify table "viagem"                                                  */
/* ---------------------------------------------------------------------- */
ALTER TABLE public.viagem ADD
    id_funcionario public.id;
ALTER TABLE public.viagem ALTER COLUMN id_empresa TYPE public.id;
ALTER TABLE public.viagem ALTER COLUMN id_cliente TYPE public.id;
ALTER TABLE public.viagem ALTER COLUMN id_projeto TYPE public.id;

/* ---------------------------------------------------------------------- */
/* Drop and recreate table "lancamento"                                   */
/* ---------------------------------------------------------------------- */

/* Table must be recreated because some of the changes can't be done with the regular commands available. */
ALTER TABLE public.lancamento DROP CONSTRAINT pk_lancamento;
CREATE TABLE public.lancamento_TMP (
    id_lancamento public.id  NOT NULL,
    sequencial NUMERIC(10)  NOT NULL,
    id_empresa public.id  NOT NULL,
    id_funcionario public.id  NOT NULL,
    id_funcionario_lancamento public.id  NOT NULL,
    id_viagem public.id,
    id_recibo public.id,
    tipo_lancamento public.tipo_lancamento  NOT NULL,
    valor_lancamento public.monetario,
    data_hora_lancamento TIMESTAMP  NOT NULL,
    data_referencia DATE,
    observacoes CHARACTER VARYING(500),
    id_categoria public.id,
    id_cliente public.id,
    id_lancamento_referenciado public.id,
    id_fornecedor public.id,
    tipo_periodo public.tipo_periodo,
    data_inicial DATE,
    data_final DATE);
INSERT INTO public.lancamento_TMP
    (id_lancamento,sequencial,id_empresa,id_funcionario,id_viagem,id_recibo,tipo_lancamento,valor_lancamento,data_hora_lancamento,data_referencia,observacoes,id_categoria,id_cliente,id_lancamento_referenciado,id_fornecedor)
SELECT
    id_lancamento,sequencial,id_empresa,id_funcionario,id_viagem,id_recibo,tipo_lancamento,valor_lancamento,data_hora_lancamento,data_referencia,observacoes,id_categoria,id_cliente,id_lancamento_referenciado,id_fornecedor
FROM public.lancamento;

DROP TABLE public.lancamento;
ALTER TABLE public.lancamento_TMP RENAME TO lancamento;
ALTER TABLE public.lancamento ADD CONSTRAINT pk_lancamento 
    PRIMARY KEY (id_lancamento);

/* ---------------------------------------------------------------------- */
/* Modify table "recibo"                                                  */
/* ---------------------------------------------------------------------- */
ALTER TABLE public.recibo ALTER COLUMN id_empresa TYPE public.id;
ALTER TABLE public.recibo ALTER COLUMN id_funcionario TYPE public.id;
COMMENT ON COLUMN public.categoria.flag_interna IS 'Indica se a categoria é interna e não pode ser alterada.';

/* ---------------------------------------------------------------------- */
/* Modify table "fechamento"                                              */
/* ---------------------------------------------------------------------- */
ALTER TABLE public.fechamento ADD
    id_viagem public.id  NOT NULL;

/* ---------------------------------------------------------------------- */
/* Modify table "fornecedor"                                              */
/* ---------------------------------------------------------------------- */
ALTER TABLE public.fornecedor ALTER COLUMN id_empresa TYPE public.id;

/* ---------------------------------------------------------------------- */
/* Modify table "perfil"                                                  */
/* ---------------------------------------------------------------------- */
ALTER TABLE public.perfil ALTER COLUMN id_empresa TYPE public.id;

/* ---------------------------------------------------------------------- */
/* Add foreign key constraints                                            */
/* ---------------------------------------------------------------------- */
ALTER TABLE public.funcionario ADD CONSTRAINT empresa_funcionario 
    FOREIGN KEY (id_empresa) REFERENCES public.empresa (id_empresa);
ALTER TABLE public.funcionario ADD CONSTRAINT funcao_funcionario 
    FOREIGN KEY (id_funcao) REFERENCES public.funcao (id_funcao);
ALTER TABLE public.funcionario ADD CONSTRAINT perfil_funcionario 
    FOREIGN KEY (id_perfil) REFERENCES public.perfil (id_perfil);
ALTER TABLE public.sessao_usuario ADD CONSTRAINT empresa_sessao_usuario 
    FOREIGN KEY (id_empresa) REFERENCES public.empresa (id_empresa);
ALTER TABLE public.sessao_usuario ADD CONSTRAINT funcionario_sessao_usuario 
    FOREIGN KEY (id_funcionario) REFERENCES public.funcionario (id_funcionario);
ALTER TABLE public.cliente ADD CONSTRAINT empresa_cliente 
    FOREIGN KEY (id_empresa) REFERENCES public.empresa (id_empresa);
ALTER TABLE public.cliente ADD CONSTRAINT cidade_cliente 
    FOREIGN KEY (id_cidade) REFERENCES public.cidade (id_cidade);
ALTER TABLE public.viagem ADD CONSTRAINT cliente_viagem 
    FOREIGN KEY (id_cliente) REFERENCES public.cliente (id_cliente);
ALTER TABLE public.viagem ADD CONSTRAINT projeto_viagem 
    FOREIGN KEY (id_projeto) REFERENCES public.projeto (id_projeto);
ALTER TABLE public.viagem ADD CONSTRAINT empresa_viagem 
    FOREIGN KEY (id_empresa) REFERENCES public.empresa (id_empresa);
ALTER TABLE public.viagem ADD CONSTRAINT funcionario_viagem 
    FOREIGN KEY (id_funcionario) REFERENCES public.funcionario (id_funcionario);
ALTER TABLE public.lancamento ADD CONSTRAINT viagem_lancamento 
    FOREIGN KEY (id_viagem) REFERENCES public.viagem (id_viagem);
ALTER TABLE public.lancamento ADD CONSTRAINT funcionario_lancamento_2 
    FOREIGN KEY (id_funcionario) REFERENCES public.funcionario (id_funcionario);
ALTER TABLE public.lancamento ADD CONSTRAINT empresa_lancamento 
    FOREIGN KEY (id_empresa) REFERENCES public.empresa (id_empresa);
ALTER TABLE public.lancamento ADD CONSTRAINT recibo_lancamento 
    FOREIGN KEY (id_recibo) REFERENCES public.recibo (id_recibo);
ALTER TABLE public.lancamento ADD CONSTRAINT categoria_lancamento 
    FOREIGN KEY (id_categoria) REFERENCES public.categoria (id_categoria);
ALTER TABLE public.lancamento ADD CONSTRAINT cliente_lancamento 
    FOREIGN KEY (id_cliente) REFERENCES public.cliente (id_cliente);
ALTER TABLE public.lancamento ADD CONSTRAINT lancamento_lancamento 
    FOREIGN KEY (id_lancamento_referenciado) REFERENCES public.lancamento (id_lancamento);
ALTER TABLE public.lancamento ADD CONSTRAINT fornecedor_lancamento 
    FOREIGN KEY (id_fornecedor) REFERENCES public.fornecedor (id_fornecedor);
ALTER TABLE public.lancamento ADD CONSTRAINT funcionario_lancamento_9 
    FOREIGN KEY (id_funcionario_lancamento) REFERENCES public.funcionario (id_funcionario);
ALTER TABLE public.recibo ADD CONSTRAINT funcionario_recibo 
    FOREIGN KEY (id_funcionario) REFERENCES public.funcionario (id_funcionario);
ALTER TABLE public.recibo ADD CONSTRAINT empresa_recibo 
    FOREIGN KEY (id_empresa) REFERENCES public.empresa (id_empresa);
ALTER TABLE public.recibo ADD CONSTRAINT fornecedor_recibo 
    FOREIGN KEY (id_fornecedor) REFERENCES public.fornecedor (id_fornecedor);
ALTER TABLE public.fechamento ADD CONSTRAINT funcionario_fechamento_1 
    FOREIGN KEY (id_funcionario_fechamento) REFERENCES public.funcionario (id_funcionario);
ALTER TABLE public.fechamento ADD CONSTRAINT funcionario_fechamento_2 
    FOREIGN KEY (id_funcionario_recebimento) REFERENCES public.funcionario (id_funcionario);
ALTER TABLE public.fechamento ADD CONSTRAINT funcionario_fechamento_3 
    FOREIGN KEY (id_funcionario_aprovacao) REFERENCES public.funcionario (id_funcionario);
ALTER TABLE public.fechamento ADD CONSTRAINT empresa_fechamento 
    FOREIGN KEY (id_empresa) REFERENCES public.empresa (id_empresa);
ALTER TABLE public.fechamento ADD CONSTRAINT funcionario_fechamento 
    FOREIGN KEY (id_funcionario_rejeicao) REFERENCES public.funcionario (id_funcionario);
ALTER TABLE public.fechamento ADD CONSTRAINT viagem_fechamento 
    FOREIGN KEY (id_viagem) REFERENCES public.viagem (id_viagem);
ALTER TABLE public.fornecedor ADD CONSTRAINT empresa_fornecedor 
    FOREIGN KEY (id_empresa) REFERENCES public.empresa (id_empresa);
ALTER TABLE public.perfil ADD CONSTRAINT empresa_perfil 
    FOREIGN KEY (id_empresa) REFERENCES public.empresa (id_empresa);
