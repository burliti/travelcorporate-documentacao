﻿SELECT
     l.data_inicial,
     c.nome as categoria,
     SUM(l.valor_lancamento * CASE WHEN l.tipo_lancamento = 'D' THEN 1 ELSE 0 END) as valor
  FROM viagem v
  JOIN lancamento l ON l.id_viagem = v.id_viagem
  JOIN categoria c ON c.id_categoria = l.id_categoria
  JOIN empresa e ON e.id_empresa = v.id_empresa
 WHERE 1 = 1
   AND v.id_empresa = 1
   AND l.tipo_lancamento = 'D'
   AND c.flag_interna = 'N'
 GROUP BY c.nome, l.data_inicial, e.id_empresa, e.nome, e.logo
 ORDER BY l.data_inicial, c.nome
 