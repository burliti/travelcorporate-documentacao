/* ---------------------------------------------------------------------- */
/* Script generated with: DeZign for Databases v6.1.0                     */
/* Target DBMS:           PostgreSQL 8.3                                  */
/* Project file:          modelagem_travelcorporate.dez                   */
/* Project name:                                                          */
/* Author:                                                                */
/* Script type:           Alter database script                           */
/* Created on:            2017-10-23 21:42                                */
/* ---------------------------------------------------------------------- */


/* ---------------------------------------------------------------------- */
/* Drop foreign key constraints                                           */
/* ---------------------------------------------------------------------- */
ALTER TABLE public.recibo DROP CONSTRAINT funcionario_recibo;
ALTER TABLE public.recibo DROP CONSTRAINT empresa_recibo;
ALTER TABLE public.recibo DROP CONSTRAINT fornecedor_recibo;

/* ---------------------------------------------------------------------- */
/* Add domains                                                            */
/* ---------------------------------------------------------------------- */
CREATE DOMAIN tipo_pessoa AS NUMERIC(1) CHECK (VALUE IN (1, 2));

/* ---------------------------------------------------------------------- */
/* Modify table "recibo"                                                  */
/* ---------------------------------------------------------------------- */
ALTER TABLE public.recibo ADD
    tipo_fornecedor tipo_pessoa;

/* ---------------------------------------------------------------------- */
/* Add foreign key constraints                                            */
/* ---------------------------------------------------------------------- */
ALTER TABLE public.recibo ADD CONSTRAINT funcionario_recibo 
    FOREIGN KEY (id_funcionario) REFERENCES public.funcionario (id_funcionario);
ALTER TABLE public.recibo ADD CONSTRAINT empresa_recibo 
    FOREIGN KEY (id_empresa) REFERENCES public.empresa (id_empresa);
ALTER TABLE public.recibo ADD CONSTRAINT fornecedor_recibo 
    FOREIGN KEY (id_fornecedor) REFERENCES public.fornecedor (id_fornecedor);
